import 'package:anmol_mart/controllers/login_view_controller.dart';
import 'package:anmol_mart/views/auth/register_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginView extends StatelessWidget {
  final LoginViewController controller = Get.put(LoginViewController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Form(
          key: controller.formKey,
          
          child: Container(
            padding: EdgeInsets.all(30.0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  //      Align(
                  //   alignment: Alignment.center,
                  //   child: Image.asset(
                  //     "assets\\images\\hands-logo.png",
                  //     width: MediaQuery.of(context).size.width / 1.5,
                  //   ),
                  // ),
                  Container(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.02),
                    child: TextFormField(
                      validator: controller.validateUsername,
                      onSaved: controller.saveUsername,
                      decoration: InputDecoration(
                        labelText: "Username",
                        contentPadding: EdgeInsets.only(left: 10.0, right: 2.0),
                        fillColor: Colors.white,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                      ),
                      keyboardType: TextInputType.text,
                      style: new TextStyle(
                        fontFamily: "Poppins",
                      ),
                    ),
                  ),

                  Obx(
                    () => (Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.03),
                      child: TextFormField(
                        onSaved: controller.savePassword,
                        validator: controller.validatePassword,
                        decoration: InputDecoration(
                          suffixIcon: GestureDetector(
                            onTap: controller.changeObsecureText,
                            child: Icon(
                              controller.obsecureText.value == true
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                            ),
                          ),
                          labelText: "Password",
                          contentPadding:
                              EdgeInsets.only(left: 10.0, right: 2.0),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        obscureText: controller.obsecureText.value,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    )),
                  ),
                   Padding(
                      padding: EdgeInsets.only(top: 50.0),
                    ),
                    RaisedButton(
                      elevation: 3.0,
                      onPressed: controller.validateInputs,
                      shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(
                            width: 1.5, color: Colors.lightBlue.shade700),
                      ),
                      textColor: Colors.lightBlue[700],
                      padding: EdgeInsets.only(
                          right: 100.0, left: 100.0, top: 10.0, bottom: 10.0),
                      color: Colors.white,
                      child: Text(
                        "Login",
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            textBaseline: TextBaseline.alphabetic),
                      ),
                    ),
                    TextButton(onPressed: (){
                      Get.to(RegisterView());
                    }, child: Text("Register")),
                    Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.25),
                      child: Text(
                        "© Designed by SMiT Solutions",
                        style: TextStyle(fontSize: 10),
                      ),
                    )
                ],
              ),
            ),
          )),
    );
  }
}
