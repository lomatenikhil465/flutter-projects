import 'package:anmol_mart/controllers/register_view_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterView extends StatelessWidget {
  final RegisterViewController controller = Get.put(RegisterViewController());

  Widget registerFields(String labelText, var variable) {
    return Container(
      child: TextFormField(
        onSaved: (value) {
          variable.value = value;
        },
        validator: (value) {
          if (value == "") {
            return "Please Enter a " + labelText;
          } else {
            return null;
          }
        },
        decoration: InputDecoration(
            contentPadding: EdgeInsets.only(left: 10.0, right: 2.0),
            fillColor: Colors.white,
            labelText: labelText,
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(10))),
      ),
    );
  }

  Widget validateField(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Container(
            width: MediaQuery.of(context).size.width * 0.90,
            color: Colors.green,
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    textStyle: TextStyle(color: Colors.white),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20))),
                onPressed: controller.validateInputs,
                child: Text("Save")),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Register"),
        backgroundColor: Color.fromRGBO(0, 150, 136, 1),
      ),
      body: Form(
          key: controller.formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                Align(
                  child: Image.asset(
                    "assets\\images\\Profile-Avatar-PNG-Free-Download.png",
                    height: 100,
                    width: 100,
                    color: Color.fromRGBO(0, 150, 136, 1),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01,
                      left: MediaQuery.of(context).size.width * 0.02),
                  child: Row(
                    children: [
                      Text("Register"),
                    ],
                  ),
                ),
                registerFields("Name", controller.name),
                registerFields("Contact No.", controller.contact),
                registerFields("Email", controller.email),
                registerFields("Address", controller.address),
                registerFields("Password", controller.password),
                validateField(context)
              ],
            ),
          )),
    );
  }
}
