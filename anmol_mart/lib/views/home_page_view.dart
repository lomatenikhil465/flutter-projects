import 'package:anmol_mart/controllers/home_page_view_controller.dart';
import 'package:anmol_mart/utils/const.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePageView extends StatelessWidget {
  final HomePageViewController controller = Get.put(HomePageViewController());

  Widget appFlash(BuildContext context) {
    return Container(
      height: 200,
      width: MediaQuery.of(context).size.width,
      child: CarouselSlider(
        options: CarouselOptions(
          aspectRatio: 16 / 9,
          viewportFraction: 0.8,
          enableInfiniteScroll: true,
          reverse: false,
          autoPlay: true,
          autoPlayInterval: Duration(seconds: 3),
          autoPlayAnimationDuration: Duration(milliseconds: 800),
          autoPlayCurve: Curves.fastOutSlowIn,
          enlargeCenterPage: true,
          scrollDirection: Axis.horizontal,
        ),
        items: controller.appFlash.map((i) {
          print(i);
          return Builder(
            builder: (BuildContext context) {
              return Container(
                  width: MediaQuery.of(context).size.width,
                  child: Image.network(Constants.url + i["img_path"]));
            },
          );
        }).toList(),
      ),
    );
  }

  Widget iconButtonWithCounter(BuildContext context){
    return  InkWell(
      borderRadius: BorderRadius.circular(100),
      onTap: (){},
      child: Stack(
        
        children: [
          Container(
            padding: EdgeInsets.all(10),
            
            
            child: Icon(Icons.shopping_cart_rounded, size: 30,),
          ),
          if (controller.noOfItems.value != 0)
            Positioned(
              top: -3,
              right: 35,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.04,
                width: MediaQuery.of(context).size.width * 0.04,
                decoration: BoxDecoration(
                  color: Color(0xFFFF4848),
                  shape: BoxShape.circle,
                  border: Border.all(width: 1.5, color: Colors.white),
                ),
                child: Center(
                  child: Text(
                    "1",
                    style: TextStyle(
                      fontSize: 10,
                      height: 1,
                      fontWeight: FontWeight.w600,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ajuna"),
        backgroundColor: Color.fromRGBO(0, 150, 136, 1),
        actions: [iconButtonWithCounter(context)]
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            Container(
              height: 100,
              child: DrawerHeader(
                  decoration: BoxDecoration(color: Color.fromRGBO(0, 150, 136, 1)),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            child: Text(
                              "Hello, ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.normal),
                            ),
                          ),
                        ],
                      ),
                    ],
                  )),
            ),
            Container(
               padding: EdgeInsets.only(top: 0),
              child: ListTile(
                leading: Icon(Icons.home, color: Color.fromRGBO(0, 150, 136, 1),),
                title: Text("Home", style: TextStyle(fontSize: 16),),
              ),
            ),
            Divider(thickness: 1,height: 0,),
             Container(
               padding: EdgeInsets.only(top: 0),
              child: ListTile(
                leading: Icon(Icons.home, color: Color.fromRGBO(0, 150, 136, 1),),
                title: Text("Home", style: TextStyle(fontSize: 16),),
              ),
            ),
            Divider(thickness: 1,height: 0,),
          ],
        ),
      ),
      body: Container(
          child: SingleChildScrollView(
        child: Column(
          children: [
            appFlash(context),
            Container(
              padding: EdgeInsets.only(
                left: MediaQuery.of(context).size.width * 0.02,
                right: MediaQuery.of(context).size.width * 0.02,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Categories",
                      style:
                          TextStyle(color: Colors.lightBlue[700], fontSize: 18),
                    ),
                  ),
                  Container(
                    child:
                        TextButton(onPressed: () {}, child: Text("View All")),
                  )
                ],
              ),
            )
          ],
        ),
      )),
    );
  }
}
