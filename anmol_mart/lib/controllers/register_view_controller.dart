import 'package:anmol_mart/services/remote_services.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterViewController extends GetxController {
  var name = "".obs;
  var contact = "".obs;
  var email = "".obs;
  var address = "".obs;
  var password = "".obs;
  var autovalidate = false.obs;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  waiting() {
    return Get.defaultDialog(
        title: "",
        content: WillPopScope(
            onWillPop: () async => false,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  child: CircularProgressIndicator(),
                ),
                Container(
                  child: Text("Loading..."),
                )
              ],
            )));
  }

  registerUser() async {
    try {
      var products = await RemoteServices.registerUser(
          name, contact, email, address, password);
      return products;
    } finally {
      print("Value got");
    }
  }

  void validateInputs() {
    //checkConnection();
    if (formKey.currentState!.validate()) {
      waiting();
//    If all data are correct then save data to out variables
      formKey.currentState!.save();
      registerUser().then((value) {
        Get.back();
        if (value == "Success") {
          Get.defaultDialog(
              title: "Success",
              titleStyle: TextStyle(color: Colors.green),
              content: new Text("Registered Successfully!!!"),
              textCancel: "Close");
        } else {
          Get.defaultDialog(
              title: "Error!",
              titleStyle: TextStyle(color: Colors.red[600]),
              content: new Text("Please check your fields"),
              textCancel: "Close");
        }

        // if (productList.length != 0) {
        //   // print(productList[0].cid);
        //   // prefs.write('user', username);
        //   // prefs.write('pass', password);
        //   // prefs.write('custId', productList[0].cid);
        //   // Navigator.of(context)
        //   //     .pushReplacement(MaterialPageRoute(builder: (context) {
        //   //   return Menu(username: username,);
        //   // }));
        // } else {
        //   Get.defaultDialog(
        //       title: "Error!",
        //       titleStyle: TextStyle(color: Colors.red[600]),
        //       content: new Text("Please check your Username and Password"),
        //       textCancel: "Close");

        // }
      });
      formKey.currentState!.reset();
    } else {
//    If all data are not valid then start auto validation.

      autovalidate.value = true;
    }
  }
}
