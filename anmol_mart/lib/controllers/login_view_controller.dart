import 'package:anmol_mart/services/remote_services.dart';
import 'package:anmol_mart/views/home_page_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LoginViewController extends GetxController {
  var username = "".obs;
  var password = "".obs;
  var autovalidate = false.obs;
  var obsecureText = true.obs;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final prefs = GetStorage("user");
  var userList = [].obs;

  String? validateUsername(value) {
    if (value.length < 3)
      return 'Username must be more than 2 charater';
    else
      return null;
  }

  String? saveUsername(user) {
    username.value = user;
  }

  String? savePassword(pass) {
    password.value = pass;
  }

  String? validatePassword(value) {
    if (value == "")
      return 'Please Enter a Password';
    else
      return null;
  }

  changeObsecureText() {
    if (obsecureText.value == true) {
      obsecureText.value = false;
    } else {
      obsecureText.value = true;
    }
  }

  waiting() {
    return Get.defaultDialog(
        title: "",
        content: WillPopScope(
            onWillPop: () async => false,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  child: CircularProgressIndicator(),
                ),
                Container(
                  child: Text("Loading..."),
                )
              ],
            )));
  }

  fetchUsers() async {
    try {
      var products = await RemoteServices.fetchUsers();
      if (products != null) {
        userList.value = products;
      }
    } finally {
      print("Value got");
    }
  }

  void validateInputs() {
    //checkConnection();
    if (formKey.currentState!.validate()) {
      waiting();
//    If all data are correct then save data to out variables
      formKey.currentState!.save();
      fetchUsers().then((value) {
        Get.back();
        if (userList.length != 0) {
          print(userList[0].cid);
          prefs.write('user', username);
          prefs.write('pass', password);
          prefs.write('custId', userList[0].cid);
          // Navigator.of(context)
          //     .pushReplacement(MaterialPageRoute(builder: (context) {
          //   return Menu(username: username,);
          // }));

          Get.off(HomePageView());
        } else {
          Get.defaultDialog(
              title: "Error!",
              titleStyle: TextStyle(color: Colors.red[600]),
              content: new Text("Please check your Username and Password"),
              textCancel: "Close");

          
        }
      });
      formKey.currentState!.reset();
    } else {
//    If all data are not valid then start auto validation.

      autovalidate.value = true;
    }
  }
}
