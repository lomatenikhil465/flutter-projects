import 'package:anmol_mart/services/remote_services.dart';
import 'package:anmol_mart/utils/const.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class HomePageViewController extends GetxController {
  var appFlash = [].obs;
  var offerProductList = [].obs;
  var noOfItems = 0.obs;
  var deviceId = "".obs;
  final prefs = GetStorage("user");

  @override
  void onInit() {
    super.onInit();

    // Constants.getId().then((value) {
    //   deviceId.value = value;
    // });
    fetchAppFlash();
    fetchOfferProducts();
  }

  fetchAppFlash() async {
    try {
      var appFlashList = await RemoteServices.fetchAppFlash();
      if (appFlashList != null) {
        appFlash.value = appFlashList;
      }
    } finally {
      print("Value got");
    }
  }

  fetchOfferProducts() async {
    try {
      var offerProducts = await RemoteServices.fetchAppFlash();
      if (offerProducts != null) {
        offerProductList.value = offerProducts;
      }
    } finally {
      print("Value got");
    }
  }

  fetchNoOfItems() async {
     var custId = prefs.read('custId');
    try {
      var fetchedNoOfItems = await RemoteServices.fetchNoOfItems(deviceId, custId);
      if (fetchedNoOfItems != null) {
        noOfItems.value = fetchedNoOfItems[0]["count"];
        print(noOfItems.value);
      }
    } finally {
      print("Value got");
    }
  }
}
