import 'dart:async';
import 'package:anmol_mart/views/auth/login_view.dart';
import 'package:anmol_mart/views/home_page_view.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class SplashViewController extends GetxController {
  final isLogged = false.obs;
  final prefs = GetStorage("user");

  void _navigateToHome() {
    var user = prefs.read('user');
    var pass = prefs.read('pass');
    print(user);
    if (user == null && pass == null) {
      Get.off(() => LoginView());
    } else {
      Get.off(() => HomePageView());
    }
  }

   _mockCheckForSession() {
    Timer(Duration(seconds: 5), _navigateToHome);
    
  }

  changeLogged() {
    if (isLogged.value == false)
      isLogged.value = true;
    else
      isLogged.value = false;
  }

  @override
  void onInit() async {
    super.onInit();
    _mockCheckForSession();
  }
}
