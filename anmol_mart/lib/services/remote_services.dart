import 'dart:convert';

import 'package:anmol_mart/models/user_model.dart';
import 'package:anmol_mart/utils/const.dart';
import 'package:http/http.dart' as http;

class RemoteServices {
  static var client = http.Client();

  static Future<List<User>?> fetchUsers() async {
    var response = await client.get(Uri.parse(
        'https://cp.ajuna.in/app/ecommerce/getquerycode.php?apicall=login&userid=7385111191&pass=0101'));
    if (response.statusCode == 200) {
      var jsonString = response.body;
      print(jsonString);
      return userFromJson(jsonString);
    } else {
      //show error message
      return null;
    }
  }

  static Future<List<User>?> fetchOfferProducts() async {
    var response = await client.get(Uri.parse(
        'https://cp.ajuna.in/app/ecommerce/getquerycode.php?apicall=getOfferItems'));
    if (response.statusCode == 200) {
      var jsonString = response.body;
      print(jsonString);
      return json.decode(jsonString)['data'];
    } else {
      //show error message
      return null;
    }
  }

  static Future<List?> fetchAppFlash() async {
    var response = await client.get(Uri.parse(
        'https://cp.ajuna.in/app/ecommerce/getquerycode.php?apicall=getAppFlash&main_admin_id=1'));
    if (response.statusCode == 200) {
      var jsonString = response.body;
      print(jsonString);
      return json.decode(jsonString)['data'];
    } else {
      //show error message
      return null;
    }
  }

  static Future? registerUser(
      var name, var contact, var email, var address, var password) async {
    var response = await client.post(
        Uri.parse(
            'https://cp.ajuna.in/app/ecommerce/getquerycode.php?apicall=signup'),
        body: {
          "name": name,
          "mobile": contact,
          "email": email,
          "address": address,
          "pass": password
        });
    if (response.statusCode == 200) {
      //var jsonString = response.body;

      return "Success";
    } else {
      //show error message
      return null;
    }
  }

  static Future<List?> fetchNoOfItems(var deviceId, var custId) async {
    var response = await client.get(Uri.parse(
        'https://cp.ajuna.in/app/ecommerce/getquerycode.php?apicall=getDealerCartItems&custId=' +
            custId +
            "&deviceId=" +
            deviceId));
    if (response.statusCode == 200) {
      var jsonString = response.body;
      print(jsonString);
      return json.decode(jsonString)['data'];
    } else {
      //show error message
      return null;
    }
  }
}
