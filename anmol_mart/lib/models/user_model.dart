import 'dart:convert';

List<User> userFromJson(String str) =>
    List<User>.from(json.decode(str)['data'].map((x) => User.fromJson(x)));

String userToJson(List<User> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class User {
  User({
    required this.cid,
    required this.username,
    required this.password,
    required this.mob,
    required this.role,
    required this.custName,
    required this.mainAdminId,
    required this.adminFirmId,
    required this.disFirmId
  });

  String cid , username, password, mob, role, custName, mainAdminId, adminFirmId, disFirmId;

  factory User.fromJson(Map<String, dynamic> json) => User(
        cid: json["cid"] == null ? "" : json["cid"],
        username: json["UserName"] == null ? "" : json["UserName"],
        password: json["upassword"] == null ? "": json["upassword"],
        mob: json["UserMob"] == null ? "" : json["UserMob"],
        role: json["role"] == null ? "": json["role"],
        custName: json["cust_name"]== null ? "": json["cust_name"],
        mainAdminId: json["main_admin_id"]== null ? "": json["main_admin_id"],
        adminFirmId: json["admin_firm_id"]== null ? "": json["admin_firm_id"],
        disFirmId: json["dis_firm_id"]== null ? "": json["dis_firm_id"]
       
      );

  Map<String, dynamic> toJson() => {
        "cid": cid,
        "username": username,
        "password": password,
        "mob": mob,
        "role": role,
        "custName": custName,
        "mainAdminId": mainAdminId,
        "adminFirmId": adminFirmId,
        "disFirmId": disFirmId
       
       
      };
}