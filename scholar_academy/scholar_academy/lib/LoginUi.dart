import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'pageController.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Login();
  }
}

List userData;

class _Login extends State<Login> {
  TextEditingController _controller = new TextEditingController();

  String user, passwd;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _obscureText = true;
  bool _autoValidate = false, isLoading;
  SharedPreferences prefs;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  Future<String> userResponse;

  Future<String> getJsonData() async {
    print(user);
    print(passwd);
    prefs = await SharedPreferences.getInstance();
    String url =
        "http://cp.scholarsacademysolapur.com/Application/getquerycode.php?apicall=login&userid=" +
            user +
            "&pass=" +
            passwd;
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      userData = convertDataToJson['data'];
    });

    print(userData);
    return "success";
  }

  @override
  void initState() {
    super.initState();
    isLoading = true;
  }
  waiting(BuildContext context) {
    return showDialog(
      
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
                  child: AlertDialog(
            key: _keyLoader,
              content: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                child: CircularProgressIndicator(),
              ),
              Container(
                child: Text("Loading..."),
              )
            ],
          )),
        );
      },
    );
  }

  void _validateInputs() {
    if (_formKey.currentState.validate()) {
//    If all data are correct then save data to out variables
      _formKey.currentState.save();

      getJsonData().then((value) {
        Navigator.of(_keyLoader.currentContext).pop();
        if (userData != null) {
          
          prefs.setString('user', user);
          prefs.setString('pass', passwd);
          Navigator.of(context)
              .pushReplacement(MaterialPageRoute(builder: (context) {
            return HomePageController();
          }));
        } else {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: new Text(
                  "Error!",
                  style: TextStyle(color: Colors.red[600]),
                ),
                content: new Text("Please check your Username and Password"),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  new FlatButton(
                    child: new Text("Close"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        }
      });
      _formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    isLoading = false;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Form(
          key: _formKey,
          child: Container(
              child: Column(
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.10)),
              Align(
                alignment: Alignment.center,
                child: Image.asset(
                  "assets/images/collaboration.png",
                  width: MediaQuery.of(context).size.width / 1.5,
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(left: 15, bottom: 10),
                    child: Text(
                      "Mobile No",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.orangeAccent,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    width: 330,
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width / 30,
                        right: MediaQuery.of(context).size.width / 20),
                    child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          setState(() {
                            user = value;
                          });
                          return 'Please Enter Mobile No';
                        }
                        return null;
                      },
                      onSaved: (String user) {
                        this.user = user;
                      },
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.person,
                          color: Colors.orangeAccent,
                        ),
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                      ),
                      keyboardType: TextInputType.number,
                      inputFormatters: <TextInputFormatter>[
                        WhitelistingTextInputFormatter.digitsOnly
                      ],
                      style: new TextStyle(
                        fontFamily: "Poppins",
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 15, bottom: 10, top: 20),
                    child: Text(
                      "Password",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.orangeAccent,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    width: 330,
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width / 30,
                        right: MediaQuery.of(context).size.width / 20),
                    child: TextFormField(
                      onSaved: (String pass) {
                        passwd = pass;
                      },
                      decoration: InputDecoration(
                        prefixIcon: Icon(
                          Icons.lock,
                          color: Colors.orangeAccent,
                        ),
                        suffixIcon: GestureDetector(
                          onTap: () {
                            setState(() {
                              _obscureText = !_obscureText;
                            });
                          },
                          child: Icon(
                            _obscureText
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Colors.orangeAccent,
                          ),
                        ),
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                      ),
                      obscureText: _obscureText,
                      style: new TextStyle(
                        fontFamily: "Poppins",
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.width / 18,
                        left: MediaQuery.of(context).size.width / 4.5),
                    child: RaisedButton(
                      elevation: 3.0,
                      onPressed: () async {
                        waiting(context);
                        _validateInputs();
                      },
                      shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                          side: BorderSide(color: Colors.orangeAccent)),
                      textColor: Colors.orangeAccent,
                      padding: EdgeInsets.only(
                          right: 50.0, left: 50.0, top: 10.0, bottom: 10.0),
                      color: Colors.white,
                      child: Text(
                        "Login",
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            textBaseline: TextBaseline.alphabetic),
                      ),
                    ),
                  )
                ],
              ),

              // Stack(
              //   alignment: Alignment.bottomLeft,
              //   children: <Widget>[
              //     WavyFooter(),
              //     CircleYellow()
              //   ],
              // )
            ],
          ))),
    );
  }
}

const List<Color> orangeGradients = [
  Color(0xFFFF9844),
  Color(0xFFFE8853),
  Color(0xFFFD7267),
];

const List<Color> aquaGradients = [
  Color(0xFF5AEAF1),
  Color(0xFF8EF7DA),
];

class WavyFooter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ClipPath(
      clipper: FooterWaveClipper(),
      child: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: aquaGradients,
              begin: Alignment.center,
              end: Alignment.bottomRight),
        ),
        height: MediaQuery.of(context).size.height / 3,
      ),
    );
  }
}

class CircleYellow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Transform.translate(
      offset: Offset(-70.0, 90.0),
      child: Material(
        color: Colors.yellow,
        child: Padding(padding: EdgeInsets.all(120)),
        shape: CircleBorder(side: BorderSide(color: Colors.white, width: 15.0)),
      ),
    );
  }
}

class FooterWaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.moveTo(size.width, 0.0);
    path.lineTo(size.width, size.height);
    path.lineTo(0.0, size.height);
    path.lineTo(0.0, size.height - 60);
    var secondControlPoint = Offset(size.width - (size.width / 6), size.height);
    var secondEndPoint = Offset(size.width, 0.0);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
