import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';

class PDFScreen extends StatelessWidget {

  
  String pathPDF = "";

  PDFScreen(this.pathPDF);
  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return PDFViewerScaffold(
        appBar: AppBar(
            backgroundColor: Color.fromRGBO(65, 202, 198, 1.0),
            leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back_ios),
            ),
            centerTitle: true,
            title: Text(
              "PDF",
              style: TextStyle(fontSize: 20, color: Colors.white),
            )),
        path: pathPDF);
  }
}
