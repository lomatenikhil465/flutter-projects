import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'pageOnlineExam.dart';

typedef ChangeValueCallback = void Function(int);
typedef CheckTimeCallback = void Function();

class QuestionsGrid extends StatefulWidget {
  final ChangeValueCallback changeValueCallback;
  final CheckTimeCallback checkTimeCallback;
  final BoxConstraints constraints;
  final int index;
  final int counter;
  //var solution;

  QuestionsGrid(
      {this.index,
      this.changeValueCallback,
      this.constraints,
      this.counter,
      this.checkTimeCallback});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _QuestionsGrid();
  }
}

class _QuestionsGrid extends State<QuestionsGrid> {
  int selectedIndex = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.all(MediaQuery.of(context).size.height * 0.009),
      child: RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        color: reviewData[widget.index] == 1
            ? Colors.purple[300]
            : solutionData[widget.index] == -1
                ? Colors.red[600]
                : solutionData[widget.index] == null
                    ? Color.fromRGBO(65, 202, 198, 1.0)
                    : Colors.green[700],
        textColor: Colors.white,
        onPressed: () {
          widget.checkTimeCallback();
          widget.changeValueCallback(widget.index);
          setState(() {
            selectedIndex = widget.index;
            if (solutionData[widget.index] == null)
              solutionData[widget.index] = -1;
          });
          //print(widget.solution);
        },
        child: Text(
          (widget.index + 1).toString(),
          style: TextStyle(fontSize: 10),
        ),
      ),
    );
    ;
  }
}
