import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:scholar_academy/LoginUi.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ScholarsPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ScholarsPage();
  }

}

List profileData;
Future<String> profileResponse;
class _ScholarsPage extends State<ScholarsPage>{

  num _stackToView = 1;

  void _handleLoad(String value) {
    setState(() {
      _stackToView = 0;
    });
  }
  
  

  Future<String> getJsonData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
      var user = prefs.getString('user');
    String url =
      "http://cp.scholarsacademysolapur.com/Application/getquerycode.php?apicall=profile&username=" + user;
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      profileData = convertDataToJson['data'];
    });

    return "success";
  }

  @override
  void initState() {
    super.initState();
    profileResponse = this.getJsonData();
  }
  var _url = "https://www.scholarsacademysolapur.com";
  final _key = UniqueKey();
  @override
  Widget build(BuildContext context) {
    
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations(
      [
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown
      ]
    );
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          leading: new Container(),
          elevation: 1,
        backgroundColor: Colors.teal[800],
        centerTitle: true,
          title: Text(
            "Scholars Academy",
            style: TextStyle(
              fontSize: 20,
              color: Colors.white
            ),
            ),
        ),
        body: IndexedStack(
          index: _stackToView,
          children: <Widget>[
            Container(
          child: WebView(
            key: _key,
            javascriptMode: JavascriptMode.unrestricted,
            initialUrl: _url,
            onPageFinished: _handleLoad,
          ),
        ),
          Center(child: CircularProgressIndicator(),)
                    
          ],
        )
      );
  
  }

}