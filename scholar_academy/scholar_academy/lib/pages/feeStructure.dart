import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'custom_container.dart';

class FeeStructure extends State{
  @override
  Widget build(BuildContext context) {

    // TODO: implement build
    
        return null;
  }

  Future buildShowDialog(BuildContext context) {
    return showDialog(
          context: context,
          builder: (BuildContext context) {
            return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)), //this right here
              child: Container(
                height: 300,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        height: 35,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(18.0),
                                topRight: Radius.circular(18.0)),
                            color: Color(0xFFBC3358)),
                        child: Row(
                          children: <Widget>[
                            CustomContainer(25, 16, "Fee : ", FontWeight.w500,
                                Colors.white, 0.02, 0, 0, 0.01),
                            CustomContainer(25, 16, "500", FontWeight.w500,
                                Colors.white, 0.01, 0, 0, 0.01),
                            CustomContainer(25, 16, "Remaining : ",
                                FontWeight.w500, Colors.white, 0.10, 0, 0, 0.01),
                            CustomContainer(25, 16, "400", FontWeight.w500,
                                Colors.white, 0.01, 0, 0, 0.01),
                          ],
                        )),
                    DataTable(columnSpacing: 10.0, dataRowHeight: 30, columns: [
                      DataColumn(
                          label: Flexible(
                              child: Text(
                        "Date",
                        softWrap: true,
                      ))),
                      DataColumn(
                          label: Flexible(
                              child: Text(
                        "Receipt No.",
                        softWrap: true,
                      ))),
                      DataColumn(
                          label: Flexible(
                              child: Text(
                        "Paid Amt",
                        softWrap: true,
                      ))),
                      DataColumn(
                          label: Flexible(
                              child: Text(
                        "Receiver",
                        softWrap: true,
                      )))
                    ], rows: [
                      DataRow(cells: [
                        DataCell(Text("27/2/2020")),
                        DataCell(Text("1")),
                        DataCell(Text("200")),
                        DataCell(Text("Nikhil"))
                      ])
                    ])
                  ],
                ),
              ),
            );
          });
  }

}