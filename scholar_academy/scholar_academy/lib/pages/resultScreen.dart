import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'pageScholars.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'pageOnlineExam.dart';

class ResultScreen extends StatefulWidget {
  final int index;
  ResultScreen({this.index});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ResultScreen();
  }
}

class _ResultScreen extends State<ResultScreen> {
  List resultData;
  Future<String> resultResponse;
  int solved = 0, notSolved = 0, wrong = 0, right = 0;
  List testScore;
  int obtainedMarks;

  Future<String> getJsonData() async {
    String url =
        "http://cp.scholarsacademysolapur.com/Application/getquerycode.php?apicall=fetchResult&testId=" +
            onlineExamData[widget.index]["ID"].toString() +
            "&userId=" +
            profileData[0]["id"] + "&test_scoreId=" + testScore[0]["id"];
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      resultData = convertDataToJson['data'];
    });

    for (int i = 0; i < resultData.length; i++) {
      setState(() {
        if (resultData[i]["user_ans"].toString() == '0') {
          notSolved = notSolved + 1;
        } else {
          obtainedMarks = int.parse(resultData[i]["obtained_marks"]);
          solved = solved + 1;
        }
        if (resultData[i]["mark"].toString() == '1') {
          right = right + 1;
        } else if(resultData[i]["user_ans"] != '0'){
          wrong = wrong + 1;
        }
      });
    }

    return "success";
  }

   Future<String> fetchTestScoreId() async {
    String url =
        "http://cp.scholarsacademysolapur.com/Application/getquerycode.php?apicall=fetchTestScoreId&userId=" +
            profileData[0]["id"] +
            "&testId=" +
            onlineExamData[widget.index]["ID"].toString();
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print("online exam : " + response.body);
    if (testScore == null) {
      if (mounted) {
        setState(() {
          var convertDataToJson = json.decode(response.body);
          testScore = convertDataToJson['data'];
        });
      }
    }
    return "success";
  }


  @override
  void initState() {
    this.fetchTestScoreId().then((value){
      resultResponse = this.getJsonData();
    });
    
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.teal[800],
          leading: IconButton(
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(Icons.arrow_back_ios),
          ),
          centerTitle: true,
          title: Text(
            "Result",
            style: TextStyle(fontSize: 20, color: Colors.white),
          )),
      body: FutureBuilder<String>(
        future: resultResponse,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
              child: Column(
                children: <Widget>[
                  Container(
                    margin: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.teal[800],
                        width: 2.0
                      ),
                      borderRadius: BorderRadius.circular(10.0)
                    ),
                    child: Column(
                      children: <Widget>[
                        Container(
                    //padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                    child: Row(
                      
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                          child: Text(
                            "Test Name : ",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Flexible(
                          child: Text(
                            resultData[0]["test"],
                            softWrap: true,
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.teal[800]),
                          ),
                        ),
                        
                      ],
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                          child: Text(
                            "Test Date : ",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          child: Text(
                            resultData[0]["test_dt"],
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.teal[800]),
                          ),
                        )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                        child: Text(
                          "Total Marks        : " + resultData[0]["total_marks"],
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.10),
                        child: Text(
                          "Obtained Marks : " + obtainedMarks.toString(),
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                        child: Text(
                          "Solved                 : " + solved.toString(),
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.10),
                        child: Text(
                          "Not Solved : " + notSolved.toString(),
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  ),
                  Container(
                    child: Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                          child: Text(
                            "Right Answer     : " ,
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          //padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                          child: Text(
                           right.toString(),
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.green[600]),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.18),
                          child: Text(
                            "Wrong Answer : ",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          //padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                          child: Text(
                           wrong.toString(),
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold, color: Colors.red[600]),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    //padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                    child: Row(
                      //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                          child: Text(
                            "System Answer : ",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          
                          height: 10,
                          width: 10,
                          color: Colors.green[600],
                        ),
                        Container(
                          padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.14),
                          child: Text(
                            "Wrong Answer : ",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          height: 10,
                          width: 10,
                          color: Colors.red[600],
                        ),
                       
                      ],
                    ),
                  ),
                  Container(
                    //padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          child: Text(
                            "Correct Answer (System Ans + Student Ans) :  ",
                            style: TextStyle(
                                fontSize: 16, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(right: 10),
                          height: 10,
                          width: 10,
                          color: Colors.orange[600],
                        ),
                        
                       
                      ],
                    ),
                  )
                      ],
                    ),
                  ),
                  Divider(
                    thickness: 2.0,
                  ),
                  Expanded(
                    child: ListView.separated(
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            child: Column(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.all(10),
                                  child: Image.network(
                                      "http://cp.scholarsacademysolapur.com/" +
                                          resultData[index]["question_img"],
                                      fit: BoxFit.contain,
                                      loadingBuilder: (context, child, progress) {
                                    return progress == null
                                        ? child
                                        : Center(
                                            child: LinearProgressIndicator());
                                  }),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Container(
                                      child: Text(
                                        "A",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                            color: (resultData[index]["user_ans"].toString() == resultData[index]["correct_ans"].toString() && resultData[index]["user_ans"].toString() == '1')
                                            ? Colors.orange[300]
                                            :(resultData[index]["user_ans"].toString() != resultData[index]["correct_ans"].toString() && resultData[index]["correct_ans"].toString() == '1') 
                                              ? Colors.green[600]
                                              : ((resultData[index]["user_ans"].toString() == 'null' || resultData[index]["user_ans"].toString() == '-1') && resultData[index]["user_ans"].toString() == '1')
                                                ? Colors.black
                                                :(resultData[index]["user_ans"].toString() != resultData[index]["correct_ans"].toString() && resultData[index]["user_ans"].toString() == '1')
                                                  ?Colors.red[600]
                                                  :(resultData[index]["correct_ans"].toString() == '1')
                                                  ?Colors.green[600]
                                                  :null
                                                  
                                              
                                            
                                            ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "B",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                            color: (resultData[index]["user_ans"].toString() == resultData[index]["correct_ans"].toString() && resultData[index]["user_ans"].toString() == '2')
                                            ? Colors.orange[300]
                                            :(resultData[index]["user_ans"].toString() != resultData[index]["correct_ans"].toString() && resultData[index]["correct_ans"].toString() == '2') 
                                              ? Colors.green[600]
                                              : ((resultData[index]["user_ans"].toString() == 'null' || resultData[index]["user_ans"].toString() == '-1') && resultData[index]["user_ans"].toString() == '2')
                                                ? Colors.black
                                                :(resultData[index]["user_ans"].toString() != resultData[index]["correct_ans"].toString() && resultData[index]["user_ans"].toString() == '2')
                                                  ?Colors.red[600]
                                                  :(resultData[index]["correct_ans"].toString() == '2')
                                                  ?Colors.green[600]
                                                  :null
                                            ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "C",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                            color: (resultData[index]["user_ans"].toString() == resultData[index]["correct_ans"].toString() && resultData[index]["user_ans"].toString() == '3')
                                            ? Colors.orange[300]
                                            :(resultData[index]["user_ans"].toString() != resultData[index]["correct_ans"].toString() && resultData[index]["correct_ans"].toString() == '3') 
                                              ? Colors.green[600]
                                              : ((resultData[index]["user_ans"].toString() == 'null' || resultData[index]["user_ans"].toString() == '-1') && resultData[index]["user_ans"].toString() == '3')
                                                ? Colors.black
                                                :(resultData[index]["user_ans"].toString() != resultData[index]["correct_ans"].toString() && resultData[index]["user_ans"].toString() == '3')
                                                  ?Colors.red[600]
                                                  :(resultData[index]["correct_ans"].toString() == '3')
                                                  ?Colors.green[600]
                                                  :null
                                            ),
                                      ),
                                    ),
                                    Container(
                                      child: Text(
                                        "D",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold,
                                            color: (resultData[index]["user_ans"].toString() == resultData[index]["correct_ans"].toString() && resultData[index]["user_ans"].toString() == '4')
                                            ? Colors.orange[300]
                                            :(resultData[index]["user_ans"].toString() != resultData[index]["correct_ans"].toString() && resultData[index]["correct_ans"].toString() == '4') 
                                              ? Colors.green[600]
                                              : ((resultData[index]["user_ans"].toString() == 'null' || resultData[index]["user_ans"].toString() == '-1') && resultData[index]["user_ans"].toString() == '4')
                                                ? Colors.black
                                                :(resultData[index]["user_ans"].toString() != resultData[index]["correct_ans"].toString() && resultData[index]["user_ans"].toString() == '4')
                                                  ?Colors.red[600]
                                                  :(resultData[index]["correct_ans"].toString() == '4')
                                                  ?Colors.green[600]
                                                  :null
                                            ),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          );
                        },
                        separatorBuilder: (BuildContext context, int index) =>
                            Divider(
                              thickness: 2.0,
                            ),
                        itemCount: resultData == null ? 0 : resultData.length),
                  )
                ],
              ),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
