import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scholar_academy/LoginUi.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'pageController.dart';
import './pages/splashScreen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  var email = prefs.getString('user');
  var pass = prefs.getString('pass');
  print(email);
  runApp(MaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Scholars Academy',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashScreen(),
    );
  }
}
