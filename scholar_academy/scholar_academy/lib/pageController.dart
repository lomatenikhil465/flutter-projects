import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'pages/pageAssignments.dart';
import 'pages/pageOnlineExam.dart';
import 'pages/pageProfile.dart';
import 'pages/pageScholars.dart';
import 'pages/pageVideoLecture.dart';

class HomePageController extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _HomePageController();
  }
}

class _HomePageController extends State<HomePageController> {
  int _pageIndex = 2;
  GlobalKey _bottomNavigationKey = GlobalKey();

  final VideoLecturesPage _videoLecturesPage = new VideoLecturesPage();
  final AssignmentPage _assignmentPage = new AssignmentPage();
  final ScholarsPage _scholarsPage = new ScholarsPage();
  final OnlineExamPage _onlineExamPage = new OnlineExamPage();
  final ProfilePage _profilePage = new ProfilePage();

  Widget _showPage = new ScholarsPage();

  Widget _pageChooser(int page) {
    switch (page) {
      case 0:
        return _videoLecturesPage;
        break;

      case 1:
        return _assignmentPage;
        break;
      case 2:
        return _scholarsPage;
        break;

      case 3:
        return _onlineExamPage;
        break;

      case 4:
        return _profilePage;
        break;

      default:
        return Container(
          child: Center(
            child: Text(
              "No page found!!!",
              style: TextStyle(fontSize: 30.0),
            ),
          ),
        );
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return 
    Scaffold(
      bottomNavigationBar: CurvedNavigationBar(
        key: _bottomNavigationKey,
        index: 2,
        height: 60.0,
        items: <Widget>[
          Icon(
            Icons.ondemand_video,
            size: 30,
            color: _pageIndex == 0 ? Colors.teal[800] : Colors.grey[50],
          ),
          Icon(
            Icons.assignment_ind,
            size: 30,
            color: _pageIndex == 1 ? Colors.teal[800] : Colors.grey[50],
          ),
          Icon(
            Icons.home,
            size: 30,
            color: _pageIndex == 2 ? Colors.teal[800] : Colors.grey[50],
          ),
          Icon(
            Icons.computer,
            size: 30,
            color: _pageIndex == 3 ? Colors.teal[800] : Colors.grey[50],
          ),
          Icon(
            Icons.person,
            size: 30,
            color: _pageIndex == 4 ? Colors.teal[800] : Colors.grey[50],
          )
        ],
        color: Colors.teal[800],
        buttonBackgroundColor: Colors.green[50],
        backgroundColor: Colors.white,
        animationCurve: Curves.easeInOut,
        animationDuration: Duration(milliseconds: 300),
        onTap: (int tappedIndex) {
          setState(() {
            _pageIndex = tappedIndex;
            _showPage = _pageChooser(tappedIndex);
          });
        },
      ),
      body: Container(
        child: _showPage,
      ),
    );
  }
}
