import 'package:flutter/cupertino.dart';

class CustomContainer extends StatelessWidget {
  final double containerHeight, sizeFont, left, right, bottom, top;
  final String testTiming;
  final FontWeight fontWeight;
  final Color color;

  CustomContainer(
      this.containerHeight,
      this.sizeFont,
      this.testTiming,
      this.fontWeight,
      this.color,
      this.left,
      this.right,
      this.bottom,
      this.top) {}

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      height: this.containerHeight,
      child: Container(
        padding: EdgeInsets.only(
            top: MediaQuery.of(context).size.height * top,
            left: MediaQuery.of(context).size.height * left,
            right: MediaQuery.of(context).size.height * right,
            bottom: MediaQuery.of(context).size.height * bottom),
        child: Center(
          child: Container(
            child: Text(
            this.testTiming,
            softWrap: true,
            style: TextStyle(
                fontWeight: this.fontWeight,
                fontSize: this.sizeFont,
                color: this.color),
          ),
          )
        ),
      ),
    );
  }
}
