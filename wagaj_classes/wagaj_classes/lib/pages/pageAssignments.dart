import 'dart:io';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'custom_container.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'pageWagaj.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_full_pdf_viewer/flutter_full_pdf_viewer.dart';

class AssignmentPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AssignmentPage();
  }
}

class _AssignmentPage extends State<AssignmentPage> {
  bool _isButtonDisabled, isLoading;
  List assignmentData;
  Future<String> assignmentResponse;
  double containerHeight = 150;

  Future<File> createFileOfPdfUrl({String urlP = ""}) async {
    final url = urlP;
    String filename = '${DateTime.now().microsecondsSinceEpoch}';
    var request = await HttpClient()?.getUrl(Uri.parse(url));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    String dir = (await getApplicationDocumentsDirectory()).path;

    File file = new File('$dir/$filename');
    await file.writeAsBytes(bytes);
    return file;
  }

  Future<String> getJsonData() async {
    String url =
        "http://cp.wagajclasses.com/Application/getquerycode.php?apicall=assignment_display&sub_nm=" +
            profileData[0]["subject_nm"].toString();
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      assignmentData = convertDataToJson['data'];
    });

    return "success";
  }

  @override
  void initState() {
    isLoading = false;
    _isButtonDisabled = true;
    assignmentResponse = this.getJsonData();
  }

  final appBar = AppBar(
    title: Text("Assignments"),
    centerTitle: true,
    leading: new Container(),
    elevation: 1,
    backgroundColor: Colors.cyan[800],
  );

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: appBar,
        body: Container(
          color: Color.fromRGBO(242, 242, 242, 1.0),
          child: Center(
            child: LayoutBuilder(
              builder: (context, constraints) {
                return ListView.builder(
                    padding: EdgeInsets.all(1),
                    itemCount:
                        assignmentData == null ? 0 : assignmentData.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        padding: EdgeInsets.only(
                            top: (MediaQuery.of(context).size.height) * 0.01,
                            left: MediaQuery.of(context).size.height * 0.01,
                            right: MediaQuery.of(context).size.height * 0.01),
                        height: (MediaQuery.of(context).size.height -
                                appBar.preferredSize.height -
                                MediaQuery.of(context).padding.top) *
                            0.25,
                        child: Card(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(
                                  color: Colors.cyan[600],
                                  width: 3.0)),
                          elevation: 5.0,
                          child: Column(
                            children: <Widget>[
                              Container(
                                  height: 30,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(18.0),
                                          topRight: Radius.circular(18.0)),
                                      color: Colors.cyan[600]),
                                  child: Row(
                                    children: <Widget>[
                                      CustomContainer(
                                          25,
                                          16,
                                          "Date : ",
                                          FontWeight.w500,
                                          Colors.white,
                                          0.02,
                                          0,
                                          0,
                                          0),
                                      CustomContainer(
                                          25,
                                          16,
                                          assignmentData[index]["ch_dt"],
                                          FontWeight.normal,
                                          Colors.white,
                                          0,
                                          0,
                                          0,
                                          0.0),
                                    ],
                                  )),
                              Row(
                                
                                children: <Widget>[
                                  CustomContainer(
                                      25,
                                      16,
                                      "Subject : ",
                                      FontWeight.normal,
                                      Colors.black,
                                      0.02,
                                      0,
                                      0,
                                      0.01),
                                  CustomContainer(
                                      25,
                                      16,
                                      assignmentData[index]["sub_name"]
                                          .toString(),
                                      FontWeight.bold,
                                      Colors.black,
                                      0,
                                      0,
                                      0,
                                      0.01),
                                      Spacer(),
                                  Container(
                                    alignment: Alignment.centerRight,
                                    margin: EdgeInsets.only(
                                        right:
                                            MediaQuery.of(context).size.width *
                                                0.03,
                                                top: MediaQuery.of(context).size.width *
                                                0.03),
                                    child: Container(
                                      height: 40,
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 2.0,
                            color: Colors.cyan,
                          )
                        ),
                                      child: RaisedButton(
                                        onPressed: _isButtonDisabled == false
                                            ? null
                                            : () {
                                                setState(() {
                                                  isLoading = true;
                                                });
                                                if (isLoading == true) {
                                                  showDialog(
                                                    barrierDismissible: false,
                                                    context: context,
                                                    builder:
                                                        (BuildContext context) {
                                                      return Container(
                                                        height:
                                                            MediaQuery.of(context)
                                                                    .size
                                                                    .height *
                                                                0.30,
                                                        child: AlertDialog(
                                                          content: Column(
                                                            mainAxisSize:
                                                                MainAxisSize.min,
                                                            children: <Widget>[
                                                              Center(
                                                                  child: Text(
                                                                      "Loading...")),
                                                              Container(
                                                                  padding: EdgeInsets
                                                                      .only(
                                                                          top:
                                                                              10),
                                                                  child: Center(
                                                                    child:
                                                                        CircularProgressIndicator(),
                                                                  ))
                                                            ],
                                                          ),
                                                        ),
                                                      );
                                                    },
                                                  );
                                                }
                                                createFileOfPdfUrl(
                                                        urlP: "https://cp.wagajclasses.com/" +
                                                            assignmentData[index][
                                                                "assign_pdf_doc"])
                                                    .then((res) {
                                                  setState(() {
                                                    isLoading = false;
                                                  });
                                                  if (isLoading == false) {
                                                    Navigator.of(context).pop();
                                                  }
                                                  Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                          builder: (context) =>
                                                              PDFScreen(
                                                                  res.path)));
                                                });
                                              },
                                        color: Colors.white,
                                        textColor: Colors.cyan,
                                        elevation: 5.0,
                                        child: Center(
                                          child: Text("DOWNLOAD",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold
                                          ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                              Row(
                                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  CustomContainer(
                                      25,
                                      16,
                                      "Description : ",
                                      FontWeight.normal,
                                      Colors.black,
                                      0.02,
                                      0,
                                      0,
                                      0.01),
                                  Flexible(
                                      child: Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      assignmentData[index]["description"],
                                      softWrap: true,
                                      style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  )),
                                  // Container(

                                  //   alignment: Alignment.centerRight,
                                  //   margin: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.03),
                                  //   child: RaisedButton(
                                  //     onPressed: _isButtonDisabled == false
                                  //         ? null
                                  //         : () {
                                  //             createFileOfPdfUrl(
                                  //                     urlP:
                                  //                         "https://cp.wagajclasses.com/" +
                                  //                             assignmentData[index][
                                  //                                 "assign_pdf_doc"])
                                  //                 .then((res) {
                                  //               Navigator.push(
                                  //                   context,
                                  //                   MaterialPageRoute(
                                  //                       builder: (context) =>
                                  //                           PDFScreen(res.path)));
                                  //             });

                                  //           },
                                  //     color: Color.fromRGBO(255, 102, 102, 1.0),
                                  //     textColor: Colors.white,
                                  //     disabledColor: Colors.grey,
                                  //     disabledTextColor: Colors.white,
                                  //     splashColor: Colors.greenAccent,
                                  //     child: Center(
                                  //       child: Text("Download"),
                                  //     ),
                                  //   ),
                                  // )
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    });
              },
            ),
          ),
        ));
  }
}

class PDFScreen extends StatelessWidget {
  String pathPDF = "";

  PDFScreen(this.pathPDF);
  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return PDFViewerScaffold(
        appBar: AppBar(
            backgroundColor: Color.fromRGBO(65, 202, 198, 1.0),
            leading: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: Icon(Icons.arrow_back_ios),
            ),
            centerTitle: true,
            title: Text(
              "PDF",
              style: TextStyle(fontSize: 20, color: Colors.white),
            )),
        path: pathPDF);
  }
}
