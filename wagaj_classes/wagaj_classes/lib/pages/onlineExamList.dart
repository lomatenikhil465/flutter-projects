import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'custom_container.dart';
import 'pageOnlineExam.dart';
import 'resultScreen.dart';
import 'pdfScreen.dart';
import 'pageWagaj.dart';

//typedef NewChangeStatusCallback = void Function();

class OnlineExamList extends StatefulWidget {
  final BoxConstraints constraints;
  final int index;
  // final NewChangeStatusCallback tempStatus;
  OnlineExamList({this.constraints, this.index});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _OnlineExamList();
  }
}

final appBar = AppBar(
  title: Text("Online Test"),
  centerTitle: true,
  leading: new Container(),
  elevation: 1,
  backgroundColor: Colors.cyan[800],
);

class _OnlineExamList extends State<OnlineExamList> {
  bool isLoading;
  int isTiming;
  var tempStatusCopy;
  List countOfQuestion;

  Future<String> getQuestionsCount(String testId) async {
    String url =
        "http://cp.wagajclasses.com/Application/getquerycode.php?apicall=fetchQuestionsCount&testId=" +
            testId.toString();
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print("StudentStatus : " + response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      countOfQuestion = convertDataToJson['data'];
    });

    return "success";
  }

  Future<String> getStudTestStatus(String testId) async {
    String url =
        "http://cp.wagajclasses.com/Application/getquerycode.php?apicall=fetchStudentStatus&testId=" +
            testId.toString() +
            "&userId=" +
            profileData[0]["id"];
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print("StudentStatus : " + response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      tempStatusCopy = convertDataToJson['data'];
    });

    return "success";
  }

  Future<File> createFileOfPdfUrl({String urlP = ""}) async {
    final url = urlP;
    String filename = '${DateTime.now().microsecondsSinceEpoch}';
    var request = await HttpClient()?.getUrl(Uri.parse(url));
    var response = await request.close();
    var bytes = await consolidateHttpClientResponseBytes(response);
    String dir = (await getApplicationDocumentsDirectory()).path;

    File file = new File('$dir/$filename');
    await file.writeAsBytes(bytes);
    return file;
  }

  double _timeOfDayToDouble(TimeOfDay tod) => tod.hour + tod.minute / 60.0;

  Future enrollTest(int index) async {
    TimeOfDay currentTimeMinus = TimeOfDay.fromDateTime(DateTime.now());
    TimeOfDay currentTimeAdd = TimeOfDay.fromDateTime(DateFormat.Hm()
        .parse(onlineExamData[index]["test_timing"])
        .add(Duration(
            minutes: int.parse(onlineExamData[widget.index]["timing1"]))));
    TimeOfDay testTimeMinus = TimeOfDay.fromDateTime(DateFormat.Hm()
        .parse(onlineExamData[index]["test_timing"])
        .subtract(Duration(minutes: 2)));
    String formattedDate = DateFormat("dd-MM-yyyy").format(DateTime.now());

    double startTime = _timeOfDayToDouble(currentTimeMinus);
    double endTime = _timeOfDayToDouble(currentTimeAdd);
    double testStartTime = _timeOfDayToDouble(testTimeMinus);
    if (startTime >= testStartTime && startTime <= endTime) {
      print(startTime);
      print(endTime);
    } else {
      print("Wrong");
    }

    if (onlineExamData[index]["ch_dt"].toString() == formattedDate.toString()) {
      if (startTime >= testStartTime && startTime < endTime) {
        final result = await Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) {
          return QuestionsPage(
            testId: onlineExamData[index]["ID"],
            index: index,

            ///tempStatus: widget.tempStatus,
          );
        })).then((value) {
          checkTiming(index);
        });
        Scaffold.of(context)
          ..removeCurrentSnackBar()
          ..showSnackBar(SnackBar(content: Text("Submitted Successfully...")));
      }
    } else {
      return null;
    }
  }

  bool checkTiming(int index) {
    TimeOfDay currentTimeMinus = TimeOfDay.fromDateTime(DateTime.now());
    TimeOfDay currentTimeAdd = TimeOfDay.fromDateTime(DateFormat.Hm()
        .parse(onlineExamData[index]["test_timing"])
        .add(Duration(
            minutes: int.parse(onlineExamData[widget.index]["timing1"]))));
    TimeOfDay testTimeMinus = TimeOfDay.fromDateTime(DateFormat.Hm()
        .parse(onlineExamData[index]["test_timing"])
        .subtract(Duration(minutes: 2)));
    DateTime now = DateTime.now();
    String formattedDate = DateFormat("dd-MM-yyyy").format(now);
    DateTime newDate =
        DateFormat("dd-MM-yyyy").parse(onlineExamData[widget.index]["ch_dt"]);

    double startTime = _timeOfDayToDouble(currentTimeMinus);
    double endTime = _timeOfDayToDouble(currentTimeAdd);
    double testStartTime = _timeOfDayToDouble(testTimeMinus);
    // print(currentTimeMinus);
    // print(testTimeMinus);
    // print(temp);
    //print(now);
    if (onlineExamData[index]["ch_dt"].toString() == formattedDate.toString()) {
      if (startTime < testStartTime) {
        setState(() {
          isTiming = 0; // join test disable
        });
      } else if (startTime >= testStartTime && startTime < endTime) {
        swatch.start();
        getStudTestStatus(onlineExamData[index]["ID"]).then((value) {
          if (tempStatusCopy == null) {
            setState(() {
              isTiming = 1; //join test enable
            });
          } else {
            setState(() {
              isTiming = 2; //result posted soon
            });
          }
        });
      } else {
        getStudTestStatus(onlineExamData[index]["ID"]).then((value) {
          if (tempStatusCopy == null) {
            setState(() {
              isTiming = 0; // join test disable
            });
          } else {
            if (onlineExamData[index]["ans_sts"] == '1') {
              setState(() {
                isTiming = 3;
              });
            } else {
              setState(() {
                isTiming = 2; //result posted soon
              });
            }
          }
        });
      }
    } else if (now.isBefore(newDate)) {
      setState(() {
        isTiming = 0; // join test disable
      });
    } else {
      getStudTestStatus(onlineExamData[index]["ID"]).then((value) {
        if (tempStatusCopy == null) {
          setState(() {
            isTiming = 0; // join test disable
          });
        } else {
          if (onlineExamData[index]["ans_sts"] == '1') {
            setState(() {
              isTiming = 3;
            });
          } else {
            setState(() {
              isTiming = 2; //result posted soon
            });
          }
        }
      });
    }
  }

  @override
  void initState() {
    if (profileData[0]["reg_type"] != '2') {
      checkTiming(widget.index);
      isLoading = true;
      getQuestionsCount(onlineExamData[widget.index]["ID"]);
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      padding: EdgeInsets.only(
          top: MediaQuery.of(context).size.height * 0.01,
          left: MediaQuery.of(context).size.width * 0.01,
          right: MediaQuery.of(context).size.width * 0.01),
      height: (MediaQuery.of(context).size.height -
              appBar.preferredSize.height -
              MediaQuery.of(context).padding.top) *
          0.33,
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
            side: BorderSide(color: Colors.cyan[600], width: 3.0)),
        elevation: 5.0,
        child: Column(
          children: <Widget>[
            Container(
                height: MediaQuery.of(context).size.height * 0.05,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(18.0),
                        topRight: Radius.circular(18.0)),
                    color: Colors.cyan[600]),
                child: Row(
                  children: <Widget>[
                    CustomContainer(
                        MediaQuery.of(context).size.height * 1,
                        16,
                        "Date : ",
                        FontWeight.normal,
                        Colors.white,
                        MediaQuery.of(context).size.width * 0.00009,
                        0,
                        0,
                        0),
                    CustomContainer(
                        widget.constraints.maxHeight,
                        16,
                        onlineExamData[widget.index]["ch_dt"],
                        FontWeight.bold,
                        Colors.white,
                        0,
                        0,
                        0,
                        0.0),
                    CustomContainer(
                        widget.constraints.maxHeight,
                        16,
                        "Time : ",
                        FontWeight.normal,
                        Colors.white,
                        MediaQuery.of(context).size.width * 0.0003,
                        0,
                        0,
                        0.0),
                    CustomContainer(
                        widget.constraints.maxHeight,
                        16,
                        onlineExamData[widget.index]["test_timing"],
                        FontWeight.bold,
                        Colors.white,
                        0,
                        0,
                        0,
                        0.0)
                  ],
                )),
            Row(
              children: <Widget>[
                CustomContainer(
                    MediaQuery.of(context).size.height * 0.05,
                    16,
                    "Subject : ",
                    FontWeight.normal,
                    Colors.black,
                    MediaQuery.of(context).size.width * 0.00009,
                    0,
                    0,
                    MediaQuery.of(context).size.height * 0.00001),
                CustomContainer(
                    MediaQuery.of(context).size.height * 0.05,
                    16,
                    onlineExamData[widget.index]["subject"],
                    FontWeight.bold,
                    Colors.black,
                    0,
                    0,
                    0,
                    MediaQuery.of(context).size.height * 0.00001),
                CustomContainer(
                    MediaQuery.of(context).size.height * 0.05,
                    16,
                    "Total Ques : ",
                    FontWeight.normal,
                    Colors.black,
                    MediaQuery.of(context).size.width * 0.00009,
                    0,
                    0,
                    MediaQuery.of(context).size.height * 0.00001),
                CustomContainer(
                    MediaQuery.of(context).size.height * 0.05,
                    16,
                    countOfQuestion != null
                        ? countOfQuestion[0]["tot_que"].toString()
                        : '0',
                    FontWeight.bold,
                    Colors.black,
                    0,
                    0,
                    0,
                    0.01),
              ],
            ),
            Row(
              children: <Widget>[
                CustomContainer(
                    MediaQuery.of(context).size.height * 0.05,
                    16,
                    "Topic : ",
                    FontWeight.normal,
                    Colors.black,
                    MediaQuery.of(context).size.width * 0.00009,
                    0,
                    0,
                    0),
                // CustomContainer(
                //     constraints.maxHeight * 0.04,
                //     16,
                //     onlineExamData[index]["test_nm"],
                //     FontWeight.normal,
                //     Colors.black,
                //     0,
                //     0,
                //     0,
                //     constraints.maxWidth * 0.00003),
                Flexible(
                  child: Text(
                    onlineExamData[widget.index]["test_nm"]
                        .toString()
                        .toUpperCase(),
                    style: TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                )
              ],
            ),
            Row(
              children: <Widget>[
                CustomContainer(
                    MediaQuery.of(context).size.height * 0.05,
                    16,
                    "Test Duration : ",
                    FontWeight.normal,
                    Colors.black,
                    MediaQuery.of(context).size.width * 0.00009,
                    0,
                    0,
                    MediaQuery.of(context).size.height * 0.00001),
                CustomContainer(
                    MediaQuery.of(context).size.height * 0.05,
                    16,
                    onlineExamData[widget.index]["timing1"],
                    FontWeight.bold,
                    Colors.black,
                    0,
                    0,
                    0,
                    MediaQuery.of(context).size.height * 0.00001),
                CustomContainer(
                    MediaQuery.of(context).size.height * 0.05,
                    16,
                    "Total Marks : ",
                    FontWeight.normal,
                    Colors.black,
                    MediaQuery.of(context).size.width * 0.00009,
                    0,
                    0,
                    MediaQuery.of(context).size.height * 0.00001),
                CustomContainer(
                    MediaQuery.of(context).size.height * 0.05,
                    16,
                    onlineExamData[widget.index]["marks"],
                    FontWeight.bold,
                    Colors.black,
                    0,
                    0,
                    0,
                    MediaQuery.of(context).size.height * 0.00001)
              ],
            ),
            isTiming == null
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      Center(
                        child: Container(
                          padding: EdgeInsets.only(
                              left:
                                  MediaQuery.of(context).size.height * 0.00001,
                              top:
                                  MediaQuery.of(context).size.height * 0.00001),
                          child: RaisedButton(
                            onPressed: () {},
                            color: Color.fromRGBO(76, 175, 80, 1.0),
                            textColor: Colors.white,
                            disabledColor: Colors.grey,
                            disabledTextColor: Colors.white,
                            splashColor: Colors.greenAccent,
                            child: Center(
                              child: Text("Checking Status"),
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                : isTiming == 3
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.height *
                                    0.00001,
                                top: MediaQuery.of(context).size.height *
                                    0.00001),
                            child: Container(
                              height: 35,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                width: 2.0,
                                color: Colors.red[600],
                              )),
                              child: RaisedButton(
                                onPressed: () {
                                  if (onlineExamData[widget.index]["sol_img"] !=
                                      "") {
                                    setState(() {
                                      isLoading = true;
                                    });
                                    if (isLoading == true) {
                                      showDialog(
                                        barrierDismissible: false,
                                        context: context,
                                        builder: (BuildContext context) {
                                          return Container(
                                            height: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.30,
                                            child: AlertDialog(
                                              content: Column(
                                                mainAxisSize: MainAxisSize.min,
                                                children: <Widget>[
                                                  Center(
                                                      child:
                                                          Text("Loading...")),
                                                  Container(
                                                      padding: EdgeInsets.only(
                                                          top: 10),
                                                      child: Center(
                                                        child:
                                                            CircularProgressIndicator(),
                                                      ))
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    }

                                    createFileOfPdfUrl(
                                            urlP:
                                                "https://cp.wagajclasses.com/" +
                                                    onlineExamData[widget.index]
                                                        ["sol_img"])
                                        .then((res) {
                                      setState(() {
                                        isLoading = false;
                                      });
                                      if (isLoading == false) {
                                        Navigator.of(context).pop();
                                      }
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  PDFScreen(res.path)));
                                    });
                                  } else {
                                    showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (BuildContext context) {
                                        return Container(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.30,
                                          child: AlertDialog(
                                            content:
                                                Text("No Data Available..."),
                                            actions: <Widget>[
                                              FlatButton(
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: Text("OK"))
                                            ],
                                          ),
                                        );
                                      },
                                    );
                                  }
                                },
                                color: Colors.white,
                                textColor: Colors.red[600],
                                elevation: 5.0,
                                child: Center(
                                  child: Text(
                                    "Solution",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.height *
                                    0.00001,
                                top: MediaQuery.of(context).size.height *
                                    0.00001),
                            child: Container(
                              height: 35,
                              decoration: BoxDecoration(
                                  border: Border.all(
                                width: 2.0,
                                color: Colors.green[600],
                              )),
                              child: RaisedButton(
                                onPressed: () {
                                  Navigator.of(context).push(
                                      MaterialPageRoute(builder: (context) {
                                    return ResultScreen(
                                      index: widget.index,
                                    );
                                  }));
                                },
                                elevation: 5.0,
                                color: Colors.white,
                                textColor: Colors.green[600],
                                child: Center(
                                  child: Text(
                                    "Result",
                                    style:
                                        TextStyle(fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      )
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          // getStudTestStatus(onlineExamData[widget.index]["ID"]) ==
                          //         Future<bool>.value(false)
                          //     ?
                          isTiming != 2
                              ? Container(
                                  padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.height *
                                          0.00001,
                                      top: MediaQuery.of(context).size.height *
                                          0.00001),
                                  child: RaisedButton(
                                    onPressed: isTiming == 1
                                        ? () => enrollTest(widget.index)
                                        : null,
                                    color: Color.fromRGBO(76, 175, 80, 1.0),
                                    textColor: Colors.white,
                                    disabledColor: Colors.grey,
                                    disabledTextColor: Colors.white,
                                    splashColor: Colors.greenAccent,
                                    child: Center(
                                      child: Text(
                                        "JOIN TEST",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                )
                              : Container(
                                  padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.height *
                                          0.00001,
                                      top: MediaQuery.of(context).size.height *
                                          0.00001),
                                  child: RaisedButton(
                                    onPressed: () {},
                                    color: Color.fromRGBO(76, 175, 80, 1.0),
                                    textColor: Colors.white,
                                    disabledColor: Colors.grey,
                                    disabledTextColor: Colors.white,
                                    splashColor: Colors.greenAccent,
                                    child: Center(
                                      child: Text("Result Posted Soon..."),
                                    ),
                                  ),
                                ),
                        ],
                      ),
          ],
        ),
      ),
    );
  }
}
