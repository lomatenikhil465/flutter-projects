import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../pageController.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../LoginUi.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SplashScreenState();
  }
}

class _SplashScreenState extends State<SplashScreen> {
  SharedPreferences prefs;

  @override
  void initState() {
    super.initState();

    _mockCheckForSession();
  }

  Future<bool> _mockCheckForSession() async {
    Timer(Duration(seconds: 5), _navigateToHome);
    prefs = await SharedPreferences.getInstance();
    return true;
  }

  void _navigateToHome() {
    var email = prefs.getString('user');
    var pass = prefs.getString('pass');
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (BuildContext context) {
      if (email == null && pass == null) {
        return Login();
      } else {
        return HomePageController();
      }
    }));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        body: Container(
          color: Colors.yellow[50],
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Opacity(
            opacity: 1,
            child:Image.asset(
              'assets/images/wagajLOGO.png',
              height: 200,
              width: 200,
              )
          ),
          Center(
            child: Text("Wagaj Classes",
                style: TextStyle(
                  fontSize: 30,
                  color: Colors.red[900],
                  fontWeight: FontWeight.bold
                )),
          )
        ],
      ),
    ));
  }
}
