import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'pageWagaj.dart';
import 'package:flutter/services.dart';

class VideoLecturesPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _VideoLecturesPage();
  }
}

List videoLecturesData;

class _VideoLecturesPage extends State<VideoLecturesPage> {
  Future<String> videoLecturesResponse;

  Future<String> getJsonData() async {
    String url =
        "http://cp.wagajclasses.com/Application/getquerycode.php?apicall=lecture_video&sub_nm=" +
            profileData[0]["subject_nm"].toString();
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      videoLecturesData = convertDataToJson['data'];
    });

    return "success";
  }

  @override
  void initState() {
    super.initState();
    if(profileData[0]['reg_type'] != '1'){
      videoLecturesResponse = this.getJsonData();
    }
  }

  @override
  Widget build(BuildContext context) {
    
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations(
      [
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown
      ]
    );
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        leading: Container(),
        backgroundColor: Colors.cyan[800],
        centerTitle: true,
        title: Text(
          "Video Lectures",
          style: TextStyle(
              fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
        ),
      ),
      // floatingActionButton: FloatingActionButton(
      //   backgroundColor: Color.fromRGBO(65, 202, 198, 1.0),
      //   onPressed: (){},
      //   child: Icon(Icons.search),
      // ),
      body: videoLecturesData == null? new Container(
        child: Center(
          child: Text("No Videos are available..."),
        ),
      ): Container(
        child: ListView.separated(
          separatorBuilder: (BuildContext context, int index) => Divider(
            thickness: 1.0,
          ),
          itemCount: videoLecturesData == null ? 0 : videoLecturesData.length,
          itemBuilder: (context, index) {
            return Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.height * 0.01,
                          top: MediaQuery.of(context).size.height * 0.01),
                      child: Text(
                        "Date : ",
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            fontSize: 16),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.01),
                      child: Text(
                         videoLecturesData[index]["ch_dt"],
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 16),
                      ),
                    )
                  ],
                ),
                Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.height * 0.01),
                      child: Text(
                        "Subject : ",
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            fontSize: 16),
                      ),
                    ),
                    Container(
                      child: Text(
                        videoLecturesData[index]["subject"],
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 16),
                      ),
                    ),
                    Spacer(),
                    Container(
                      padding: EdgeInsets.only(
                          right: MediaQuery.of(context).size.height * 0.01),
                      child: Container(height: 40,
                        decoration: BoxDecoration(
                          border: Border.all(
                            width: 2.0,
                            color: Colors.cyan,
                          )
                        ),
                        child: RaisedButton(
                          onPressed: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) =>
                                        WatchVideos(videoIndex: index)));
                          },
                          color: Colors.white,
                          textColor: Colors.cyan,
                          elevation: 5.0,
                          child: Center(
                            child: Text(
                              "WATCH",

                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                              ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    
                         Container(
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.height * 0.01),
                      child: Text(
                        "Description : ",
                        softWrap: true,
                        style: TextStyle(
                            fontWeight: FontWeight.normal,
                            color: Colors.black,
                            fontSize: 16),
                      ),
                    ),

                    Flexible(
                        child: Container(
                      padding: EdgeInsets.only(right: MediaQuery.of(context).size.width *0.30),
                      child: Text(

                            videoLecturesData[index]["description"],
                        softWrap: true,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                            fontSize: 16),
                      ),
                    ))
                  ],
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}

class WatchVideos extends StatefulWidget {
  final int videoIndex;
  WatchVideos({this.videoIndex});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _WatchVideos();
  }
}

class _WatchVideos extends State<WatchVideos> {
  final _key = UniqueKey();
  num _stackToView = 1;

  void _handleLoad(String value) {
    setState(() {
      _stackToView = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    var _url = videoLecturesData[widget.videoIndex]["video_url"];
    //var _url = "https://www.wagajclasses.com/view_video.php?id=206";
    Orientation currentOrientation = MediaQuery.of(context).orientation;
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations(
      [
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
        DeviceOrientation.landscapeLeft,
        DeviceOrientation.landscapeRight
      ]
    );
    // TODO: implement build
    return Scaffold(
      appBar: currentOrientation != Orientation.landscape
          ? AppBar(
              leading: IconButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                icon: Icon(Icons.arrow_back_ios),
              ),
              elevation: 1,
              backgroundColor: Colors.cyan[600],
              centerTitle: true,
              title: Text(
                "Wagaj Classes",
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
            )
          : null,
      body: IndexedStack(
        index: _stackToView,
        children: <Widget>[
          Container(
            child: WebView(
              key: _key,
              javascriptMode: JavascriptMode.unrestricted,
              initialUrl: _url,
              onPageFinished: _handleLoad,
              navigationDelegate: (NavigationRequest request) {
                if (request.url.startsWith("https://www.youtube.com/")) {
                  print("Block $request");
                  return NavigationDecision.prevent;
                }
              },
            ),
          ),
          Center(
            child: CircularProgressIndicator(),
          )
        ],
      ),
      floatingActionButton: Container(
        height: 200,
        width: 200.0,
        padding: EdgeInsets.all(0.0),
        child: FittedBox(
          child: FloatingActionButton(
            onPressed: null,
            child: Text(""),
            backgroundColor: Colors.transparent,
            elevation: 0.0,
            shape: RoundedRectangleBorder(),
          ),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.endTop,
    );
  }
}
