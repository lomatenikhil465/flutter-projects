import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:timer_builder/timer_builder.dart';
import 'dart:async';
import 'dart:convert';
import 'questions_grid.dart';
import 'pageWagaj.dart';
import 'onlineExamList.dart';

class OnlineExamPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _OnlineExamPage();
  }
}

List onlineExamData;

class _OnlineExamPage extends State<OnlineExamPage> {
  bool isLoading;
  Future<String> onlineExamResponse;
  int returnedIndex;
  // var tempStatusCopy;
  static int temp = 0, result;

  Future<String> getJsonData() async {
    String url =
        "http://cp.wagajclasses.com/Application/getquerycode.php?apicall=online_exam&class=" +
            profileData[0]["class"].toString();
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      onlineExamData = convertDataToJson['data'];
    });
    return "success";
  }

  @override
  void initState() {
    if (profileData[0]["reg_type"] != '2') {
      onlineExamResponse = this.getJsonData();
      //tempStatus();
      //getStatus();
      isLoading = true;
      result = null;
    }
  }

  final appBar = AppBar(
    title: Text("Online Test"),
    centerTitle: true,
    leading: new Container(),
    elevation: 1,
    backgroundColor: Colors.cyan[800],
  );

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: appBar,
        body: profileData[0]["reg_type"] == '2'
            ? Container(
                child: Center(
                  child: Text("No Exams are available..."),
                ),
              )
            : Center(
                child: FutureBuilder<String>(
                future: onlineExamResponse,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return LayoutBuilder(
                      builder: (context, constraints) {
                        return ListView.builder(
                            itemCount: onlineExamData == null
                                ? 0
                                : onlineExamData.length,
                            itemBuilder: (BuildContext context, int index) {
                              return OnlineExamList(
                                constraints: constraints,
                                index: index,
                              );
                            });
                      },
                    );
                  } else {
                    return CircularProgressIndicator();
                  }
                },
              )));
  }
}

//typedef ChangeStatusCallback = void Function();
class QuestionsPage extends StatefulWidget {
  final String testId;
  final int index;
  //final ChangeStatusCallback tempStatus;
  QuestionsPage({this.testId, this.index});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _QuestionsPage();
  }
}

var solutionData;
var reviewData;
String time = "00:00:00";
var swatch = Stopwatch();

class _QuestionsPage extends State<QuestionsPage> {
  List questionsData;
  List testScore;
  Future<String> questionsResponse;
  int result = 0;
  int _counter = 0;
  DateTime joinTime;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  Future<String> getJsonData() async {
    String url =
        "http://cp.wagajclasses.com/Application/getquerycode.php?apicall=getQuestions&id=" +
            widget.testId;
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print("online exam : " + response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      questionsData = convertDataToJson['data'];
    });
    solutionData = new List<int>(questionsData.length);
    reviewData = new List<int>(questionsData.length);
    solutionData[0] = -1;
    for (var i = 0; i < questionsData.length; i++) {
      reviewData[i] = 0;
    }
    return "success";
  }

  Future<String> updateAnswers() async {
    //DateTime now = DateTime.now();
    // String formattedDate = DateFormat('dd/MM/yyyy').format(now);
    // String formattedTime = DateFormat('HH:mm').format(now);
    String url =
        "http://cp.wagajclasses.com/Application/getquerycode.php?apicall=insertAnswers";

    for (int i = 0; i < questionsData.length; i++) {
      if (solutionData[i].toString() == 'null' ||
          solutionData[i].toString() == '-1') {
        continue;
      }
      int marks = solutionData[i].toString() == questionsData[i]["ans"]
          ? int.parse(onlineExamData[widget.index]["rightMark"])
          : int.parse(onlineExamData[widget.index]["wrongMark"]);
      var response = await http.post(url,
          // headers: {"Content-Type": "application/json"},
          body: {
            'testId': testScore[0]["id"],
            'queRefId': widget.testId,
            'sub': onlineExamData[widget.index]["subject"],
            'userId': profileData[0]["id"],
            'test': onlineExamData[widget.index]["test_nm"],
            'queId': questionsData[i]["ID"],
            'userAns': (solutionData[i].toString() == 'null' ||
                    solutionData[i].toString() == '-1')
                ? '0'
                : solutionData[i].toString(),
            'correctAns': questionsData[i]["ans"],
            'mark': marks.toString() ==
                    onlineExamData[widget.index]["rightMark"].toString()
                ? '1'
                : '0',
            'testDate': onlineExamData[widget.index]["dt"]
          });
    }

    return "success";
  }

  Future<String> insertScore() async {
    String url =
        "http://cp.wagajclasses.com/Application/getquerycode.php?apicall=insertScore";

    for (int i = 0; i < questionsData.length; i++) {
      // int marks = solutionData[i].toString() == questionsData[i]["ans"]
      //     ? int.parse(onlineExamData[widget.index]["rightMark"])
      //     : int.parse(onlineExamData[widget.index]["wrongMark"]);
      if (solutionData[i] != null &&
          solutionData[i] != -1 &&
          solutionData[i].toString() == questionsData[i]["ans"]) {
        result = result + int.parse(onlineExamData[widget.index]["rightMark"]);
      } else if (solutionData[i] != null && solutionData[i] != -1) {
        result = result - int.parse(onlineExamData[widget.index]["wrongMark"]);
      }
    }
    var response = await http.post(url,
        // headers: {"Content-Type": "application/json"},
        body: {
          'userId': profileData[0]["id"],
          'testId': widget.testId,
          'studId': profileData[0]["std_id"],
          'studName': profileData[0]["std_name"],
          'batch': profileData[0]["batch_group_name"],
          'subject': onlineExamData[widget.index]["subject"],
          'class': profileData[0]["class"],
          'testTotalMarks': onlineExamData[widget.index]["marks"],
          'mark': result.toString(),
          'testDate': onlineExamData[widget.index]["dt"],
          'mobNo': profileData[0]["studentmobile"]
        });

    return "success";
  }

  Future<String> fetchTestScoreId() async {
    String url =
        "http://cp.wagajclasses.com/Application/getquerycode.php?apicall=fetchTestScoreId&userId=" +
            profileData[0]["id"] +
            "&testId=" +
            widget.testId;
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print("online exam : " + response.body);
    if (testScore == null) {
      if (mounted) {
        setState(() {
          var convertDataToJson = json.decode(response.body);
          testScore = convertDataToJson['data'];
        });
      }
    }
    return "success";
  }

  Future<String> insertEndTest() async {
    String url =
        "http://cp.wagajclasses.com/Application/getquerycode.php?apicall=insertEndTest&";
    var response = await http.post(url,
        // headers: {"Content-Type": "application/json"},
        body: {
          'userId': profileData[0]["id"],
          'testId': widget.testId,
          'testScoreId': testScore[0]["id"],
          'subject': onlineExamData[widget.index]["subject"],
          'testName': onlineExamData[widget.index]["test_nm"]
        });

    // print("insert End Test : " + profileData[0]["id"]);
    // print("insert End Test : " + widget.testId);
    // print("insert End Test : " + testScore[0]["id"]);
    // print("insert End Test : " + onlineExamData[widget.index]["subject"]);
    // print("insert End Test : " + onlineExamData[widget.index]["test_nm"]);

    return "success";
  }

  void startTimer() {
    print("StartTimer");
    Timer(Duration(seconds: 1), keepRunning);
  }

  void keepRunning() {
    print("keepRunning");
    if (swatch.isRunning) {
      startTimer();
    }
    setState(() {
      time = swatch.elapsed.inHours.toString().padLeft(2, "0") +
          ":" +
          (swatch.elapsed.inMinutes % 60).toString().padLeft(2, "0") +
          ":" +
          (swatch.elapsed.inSeconds % 60).toString().padLeft(2, "0");

      if (swatch.elapsed.inMinutes ==
          int.parse(onlineExamData[widget.index]["timing1"])) {
        swatch.stop();
      }
    });
    print(time);
  }

  @override
  void initState() {
    startTimer();
    questionsResponse = this.getJsonData();
    joinTime = DateTime.now().add(
        Duration(minutes: int.parse(onlineExamData[widget.index]["timing1"])));
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.landscapeLeft, DeviceOrientation.landscapeRight]);
  }

  @override
  void dispose() {
    super.dispose();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
  }

  void changeValue(int index) {
    setState(() {
      _counter = index;
    });
  }

  double _timeOfDayToDouble(TimeOfDay tod) => tod.hour + tod.minute / 60.0;

  void checkTime() {
    TimeOfDay currentTimeMinus = TimeOfDay.fromDateTime(DateTime.now());
    TimeOfDay currentTimeAdd = TimeOfDay.fromDateTime(DateFormat.Hm()
        .parse(onlineExamData[widget.index]["test_timing"])
        .add(Duration(
            minutes: int.parse(onlineExamData[widget.index]["timing1"]))));
    TimeOfDay testTimeMinus = TimeOfDay.fromDateTime(DateFormat.Hm()
        .parse(onlineExamData[widget.index]["test_timing"])
        .subtract(Duration(minutes: 2)));

    double startTime = _timeOfDayToDouble(currentTimeMinus);
    double endTime = _timeOfDayToDouble(currentTimeAdd);
    double testStartTime = _timeOfDayToDouble(testTimeMinus);

    print(currentTimeAdd);
    if (startTime >= testStartTime && startTime < endTime) {
    } else {
      endTest();
    }
  }

  void endTest() {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: MediaQuery.of(context).size.height * 0.30,
          child: WillPopScope(
            onWillPop: () => Future.value(false),
            child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Center(child: Text("Submitting Answers...")),
                  Container(
                      padding: EdgeInsets.only(top: 10),
                      child: Center(
                        child: CircularProgressIndicator(),
                      ))
                ],
              ),
            ),
          ),
        );
      },
    );

    insertScore().then((value) {
      fetchTestScoreId().then((value) {
        updateAnswers().then((newvalue) {
          insertEndTest().then((value) {
            //widget.tempStatus();
            Navigator.of(_keyLoader.currentContext).pop();
            Navigator.of(context).pop(widget.index);
          });
        });
      });
    });
  }

  // String getSystemTime() {
  //   var now = new DateTime.now();
  //   return new DateFormat("H:m:s").format(now.subtract(Duration(seconds: 1)));
  // }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
      onWillPop: () => Future.value(false),
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.cyan[800],
          leading: Container(),
          elevation: 1,
          centerTitle: true,
          title: Text(
            "Wagaj Classes",
            style: TextStyle(fontSize: 20, color: Colors.white),
          ),
          actions: <Widget>[
            Center(
                // child: TimerBuilder.periodic(Duration(seconds: 1),
                //     builder: (context) {
                child: Text(
              time,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            )
                //}),
                ),
          ],
        ),
        body: FutureBuilder<String>(
          future: questionsResponse,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return LayoutBuilder(
                builder: (context, constraints) {
                  return SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              width: MediaQuery.of(context).size.width * 0.32,
                              height: MediaQuery.of(context).size.height * 0.6,
                              child: GridView.builder(
                                itemCount: questionsData == null
                                    ? 0
                                    : questionsData.length,
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                        crossAxisCount: 4),
                                itemBuilder: (BuildContext context, int index) {
                                  return QuestionsGrid(
                                    index: index,
                                    changeValueCallback: changeValue,
                                    constraints: constraints,
                                    counter: _counter,
                                    checkTimeCallback: checkTime,
                                    //solution: solutionData[_counter],
                                  );
                                },
                              ),
                            ),
                            Container(
                                width: MediaQuery.of(context).size.width * 0.68,
                                height:
                                    MediaQuery.of(context).size.height * 0.6,
                                //padding: EdgeInsets.all(10.0),
                                //padding: EdgeInsets.only(left: constraints.maxHeight *),
                                child: Card(
                                  elevation: 0.0,
                                  child: (questionsData[_counter]
                                              ["question_img"] !=
                                          "")
                                      ? Image.network(
                                          "http://cp.wagajclasses.com/" +
                                              questionsData[_counter]
                                                  ["question_img"],
                                          fit: BoxFit.contain,
                                          loadingBuilder:
                                              (context, child, progress) {
                                            return progress == null
                                                ? child
                                                : Center(
                                                    child:
                                                        LinearProgressIndicator());
                                          },
                                        )
                                      : Text(
                                          questionsData[_counter]["question"],
                                          style: TextStyle(
                                              fontSize: 20,
                                              fontWeight: FontWeight.w500),
                                        ),
                                )),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.all(8.0),
                          child: Row(
                            children: <Widget>[
                              FloatingActionButton(
                                onPressed: () {
                                  checkTime();
                                  setState(() {
                                    if (_counter != 0) {
                                      _counter--;
                                      print(solutionData[_counter]);
                                      if (solutionData[_counter] == null) {
                                        solutionData[_counter] = -1;
                                      }
                                      print(solutionData[_counter]);
                                    }
                                  });
                                },
                                child: Icon(Icons.navigate_before),
                                backgroundColor:
                                    Color.fromRGBO(65, 202, 198, 1.0),
                              ),
                              Row(
                                //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.15,
                                    padding: EdgeInsets.only(
                                        left:
                                            MediaQuery.of(context).size.width *
                                                0.06),
                                    child: FlatButton(
                                        color: solutionData[_counter] == 1
                                            ? Colors.orange
                                            : Color.fromRGBO(65, 202, 198, 1.0),
                                        onPressed: () {
                                          checkTime();
                                          setState(() {
                                            solutionData[_counter] = 1;
                                          });
                                          print(solutionData[_counter]);
                                        },
                                        child: Text(
                                          "A",
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: solutionData[_counter] == 1
                                                  ? Colors.white
                                                  : Colors.black),
                                        ),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(8.0))),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.12,
                                    padding: EdgeInsets.only(
                                        left:
                                            MediaQuery.of(context).size.width *
                                                0.03),
                                    child: FlatButton(
                                        color: solutionData[_counter] == 2
                                            ? Colors.orange
                                            : Color.fromRGBO(65, 202, 198, 1.0),
                                        onPressed: () {
                                          checkTime();
                                          setState(() {
                                            solutionData[_counter] = 2;
                                          });
                                        },
                                        child: Text(
                                          "B",
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: solutionData[_counter] == 2
                                                  ? Colors.white
                                                  : Colors.black),
                                        ),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(8.0))),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.12,
                                    padding: EdgeInsets.only(
                                        left:
                                            MediaQuery.of(context).size.width *
                                                0.03),
                                    child: FlatButton(
                                      color: solutionData[_counter] == 3
                                          ? Colors.orange
                                          : Color.fromRGBO(65, 202, 198, 1.0),
                                      onPressed: () {
                                        checkTime();
                                        setState(() {
                                          solutionData[_counter] = 3;
                                        });
                                      },
                                      child: Text(
                                        "C",
                                        style: TextStyle(
                                            fontSize: 20,
                                            color: solutionData[_counter] == 3
                                                ? Colors.white
                                                : Colors.black),
                                      ),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(8.0)),
                                    ),
                                  ),
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.12,
                                    padding: EdgeInsets.only(
                                        left:
                                            MediaQuery.of(context).size.width *
                                                0.03),
                                    child: FlatButton(
                                        color: solutionData[_counter] == 4
                                            ? Colors.orange
                                            : Color.fromRGBO(65, 202, 198, 1.0),
                                        onPressed: () {
                                          checkTime();
                                          setState(() {
                                            solutionData[_counter] = 4;
                                          });
                                        },
                                        child: Text(
                                          "D",
                                          style: TextStyle(
                                            fontSize: 20,
                                            color: solutionData[_counter] == 4
                                                ? Colors.white
                                                : Colors.black,
                                          ),
                                        ),
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(8.0))),
                                  )
                                ],
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.03),
                                child: FlatButton(
                                    color: Color.fromRGBO(65, 202, 198, 1.0),
                                    onPressed: () {
                                      checkTime();
                                      setState(() {
                                        if (reviewData[_counter] == 0 &&
                                            (solutionData[_counter] != null &&
                                                solutionData[_counter] != -1)) {
                                          reviewData[_counter] = 1;
                                          setState(() {
                                            if (_counter !=
                                                questionsData.length - 1) {
                                              _counter++;
                                              if (solutionData[_counter] ==
                                                      null ||
                                                  solutionData[_counter] ==
                                                      -1) {
                                                solutionData[_counter] = -1;
                                              }
                                            }
                                          });
                                        } else if ((solutionData[_counter] !=
                                                null &&
                                            solutionData[_counter] != -1)) {
                                          reviewData[_counter] = 0;
                                        }
                                      });
                                    },
                                    child: Text(
                                      "Mark for review",
                                      style: TextStyle(
                                        fontSize: 13,
                                        color: Colors.white,
                                      ),
                                    ),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(8.0))),
                              ),
                              Spacer(),
                              _counter == questionsData.length - 1
                                  ? FloatingActionButton.extended(
                                      onPressed: endTest,
                                      label: Text("End Test"),
                                      backgroundColor: Colors.red[600],
                                    )
                                  : FloatingActionButton(
                                      onPressed: () {
                                        setState(() {
                                          if (_counter !=
                                              questionsData.length - 1) {
                                            _counter++;

                                            if (solutionData[_counter] ==
                                                null) {
                                              solutionData[_counter] = -1;
                                            }
                                          }
                                        });
                                      },
                                      child: Icon(Icons.navigate_next),
                                      backgroundColor:
                                          Color.fromRGBO(65, 202, 198, 1.0),
                                    )
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
      ),
    );
  }
}
