import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wagaj_classes/LoginUi.dart';
import 'feeStructure.dart';
import 'dart:async';
import 'pageWagaj.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProfilePage();
  }
}

class _ProfilePage extends State<ProfilePage> {
  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
      print('Image Path $_image');
    });
  }

  Future uploadPic(BuildContext context) async {
    setState(() {
      print("Profile Picture uploaded");
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text('Profile Picture Uploaded')));
    });
  }

  Gradient appGradient = LinearGradient(
      begin: Alignment.centerLeft,
      end: Alignment.centerRight,
      colors: [
        Colors.cyan[200],
        Colors.cyan[800],
      ],
      stops: [
        0,
        0.7
      ]);
  String imageUrl =
      "https://www.google.com/url?sa=i&source=imgres&cd=&cad=rja&uact=8&ved=2ahUKEwj4usnl1e3pAhWr9XMBHfSjBjwQjRx6BAgBEAQ&url=https%3A%2F%2Fwww.kindpng.com%2Fimgv%2FiTmTb_person-icon-png-customer-image-black-and-white%2F&psig=AOvVaw0eqwtvNY0PmTyYE7F3S-dt&ust=1591549431675549";

  final appBar = AppBar(
    leading: new Container(),
    centerTitle: true,
    elevation: 1,
    backgroundColor: Colors.cyan[800],
    title: Text("Profile"),
    actions: <Widget>[
      Builder(
          builder: (context) => IconButton(
                icon: Icon(Icons.settings),
                onPressed: () => Scaffold.of(context).openEndDrawer(),
                tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
              ))
    ],
  );

  Widget buildCustomAppBar() {
    return Container(
      height: (MediaQuery.of(context).size.height -
              appBar.preferredSize.height -
              MediaQuery.of(context).padding.top) *
          0.2,
      child: LayoutBuilder(
        builder: (context, constraints) {
          return Stack(
            alignment: Alignment.topCenter,
            children: <Widget>[
              Container(
                height: constraints.maxHeight,
                width: constraints.maxWidth * 0.8,
                child: Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0)),
                    elevation: 5.0,
                    child: Container(
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(18.0),
                          gradient: appGradient),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.all(
                                MediaQuery.of(context).size.height * 0.01),
                            child: Text(
                              profileData[0]["std_name"] == null
                                  ? '-'
                                  : profileData[0]["std_name"],
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(
                                MediaQuery.of(context).size.height * 0.01),
                            child: Text(
                              "Roll no.: " +
                                  (profileData[0]["std_id"] == null
                                      ? '-'
                                      : profileData[0]["std_id"]),
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                              ),
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.all(
                                MediaQuery.of(context).size.height * 0.01),
                            child: Text(
                              "Mobile no.: " +
                                  (profileData[0]["studentmobile"] == null
                                      ? '-'
                                      : profileData[0]["studentmobile"]),
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                              ),
                            ),
                          )
                        ],
                      ),
                    )),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.03),
                  child: CircleAvatar(
                    radius: 40,
                    backgroundColor: Colors.white,
                    child: ClipOval(
                      child: new SizedBox(
                        width: MediaQuery.of(context).size.width * 0.30,
                        height: MediaQuery.of(context).size.height * 0.30,
                        child: (_image != null)
                            ? Image.file(
                                _image,
                                fit: BoxFit.fill,
                              )
                            : Image.asset(
                                "assets/images/stud_photo.png",
                                fit: BoxFit.fill,
                              ),
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: Padding(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.05,
                      left: MediaQuery.of(context).size.width * 0.18),
                  child: IconButton(
                    icon: Icon(
                      Icons.photo_camera,
                      size: 30.0,
                      color: Colors.black54,
                    ),
                    onPressed: () {
                      getImage();
                    },
                  ),
                ),
              )
            ],
          );
        },
      ),
    );
  }

  void getFeeStructure() {
    FeeStructure feeStructure = new FeeStructure();
    feeStructure.buildShowDialog(context);
  }

  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized();
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    // TODO: implement build
    return Scaffold(
        endDrawer: Drawer(
          child: ListView(
            children: <Widget>[
              Container(
                height: 70,
                child: DrawerHeader(
                    //padding: ,
                    child: Container(
                      child: Center(
                        child: Text(
                          "Settings",
                          style: TextStyle(fontSize: 20, color: Colors.white),
                        ),
                      ),
                    ),
                    decoration: BoxDecoration(color: Colors.cyan[600])
                    //padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.02),
                    ),
              ),
              ListTile(
                leading: Icon(Icons.group),
                title: Text("Fee Structure"),
                onTap: () {
                  getFeeStructure();
                },
              ),
              // ListTile(
              //   leading: Icon(Icons.lock_outline),
              //   title: Text("Change Password"),
              //   onTap: () {},
              // ),
              ListTile(
                leading: Icon(Icons.power_settings_new),
                title: Text("Logout"),
                onTap: () async {
                  SharedPreferences prefs =
                      await SharedPreferences.getInstance();
                  prefs.remove('user');
                  prefs.remove('pass');
                  Navigator.of(context)
                      .pushReplacement(MaterialPageRoute(builder: (context) {
                    return Login();
                  }));
                },
              ),
            ],
          ),
        ),
        appBar: appBar,
        body: FutureBuilder<String>(
          future: profileResponse,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return LayoutBuilder(
                builder: (context, constraints) {
                  return Container(
                    height: (MediaQuery.of(context).size.height -
                            appBar.preferredSize.height -
                            MediaQuery.of(context).padding.top) *
                        1,
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          buildCustomAppBar(),
                          Column(
                            //mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                    top: constraints.maxHeight * 0.01,
                                    left: constraints.maxHeight * 0.01,
                                    right: constraints.maxHeight * 0.01),
                                // width: 300,
                                height:
                                    MediaQuery.of(context).size.height * 0.30,
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(
                                          color: Colors.cyan[600], width: 3.0)),
                                  elevation: 5.0,
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        height: constraints.maxHeight * 0.06,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(18.0),
                                                topRight:
                                                    Radius.circular(18.0)),
                                            color: Colors.cyan[600]),
                                        child: Container(
                                          child: Center(
                                            child: Text(
                                              "About",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20,
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(
                                                left: constraints.maxHeight *
                                                    0.012,
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              "Parent Mobile No. : ",
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              profileData[0]["parentmobile"] ==
                                                      null
                                                  ? '-'
                                                  : profileData[0]
                                                      ["parentmobile"],
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.normal,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(
                                                left: constraints.maxHeight *
                                                    0.012,
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              "Email Id : ",
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              profileData[0]["email"] == null
                                                  ? '-'
                                                  : profileData[0]["email"],
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.normal,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(
                                                left: constraints.maxHeight *
                                                    0.012,
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              "Date of Birth : ",
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              profileData[0]["dob_dt"] == null
                                                  ? "-"
                                                  : profileData[0]["dob_dt"],
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.normal,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(
                                                left: constraints.maxHeight *
                                                    0.012,
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              "Class : ",
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              profileData[0]["class"] == null
                                                  ? '-'
                                                  : profileData[0]["class"],
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.normal,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(
                                                left: constraints.maxHeight *
                                                    0.012,
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              "Address : ",
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              profileData[0]["address"] == null
                                                  ? '-'
                                                  : profileData[0]["address"],
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.normal,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                    top: constraints.maxHeight * 0.01,
                                    left: constraints.maxHeight * 0.01,
                                    right: constraints.maxHeight * 0.01),
                                // width: 300,
                                height:
                                    MediaQuery.of(context).size.height * 0.28,
                                child: Card(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(18.0),
                                      side: BorderSide(
                                          color: Colors.cyan[600], width: 3.0)),
                                  elevation: 5.0,
                                  child: Column(
                                    children: <Widget>[
                                      Container(
                                        height: constraints.maxHeight * 0.06,
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.only(
                                                topLeft: Radius.circular(18.0),
                                                topRight:
                                                    Radius.circular(18.0)),
                                            color: Colors.cyan[600]),
                                        child: Container(
                                          child: Center(
                                            child: Text(
                                              "Course",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20,
                                                  color: Colors.white),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(
                                                left: constraints.maxHeight *
                                                    0.012,
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              "Batch Name : ",
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              profileData[0][
                                                          "batch_group_name"] ==
                                                      null
                                                  ? '-'
                                                  : profileData[0]
                                                      ["batch_group_name"],
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight:
                                                      FontWeight.normal),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(
                                                left: constraints.maxHeight *
                                                    0.012,
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              "Subject : ",
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              profileData[0]["subject_nm"] ==
                                                      null
                                                  ? '-'
                                                  : profileData[0]
                                                      ["subject_nm"],
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.normal,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(
                                                left: constraints.maxHeight *
                                                    0.012,
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              "Timing : ",
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              profileData[0]["batch_timing"] ==
                                                      null
                                                  ? '-'
                                                  : profileData[0]
                                                      ["batch_timing"],
                                              style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.normal,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(
                                                left: constraints.maxHeight *
                                                    0.012,
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              "Joining Date : ",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                top: constraints.maxHeight *
                                                    0.012),
                                            child: Text(
                                              profileData[0]["join_dt"] == null
                                                  ? '-'
                                                  : profileData[0]["join_dt"],
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight:
                                                      FontWeight.normal),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                          Padding(
                              padding: EdgeInsets.only(
                                  bottom: constraints.maxHeight * 0.02)),
                        ],
                      ),
                    ),
                  );
                },
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ));
  }
}
