import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvkk/login_page.dart';
import 'package:mvkk/sabhasad_registration.dart';
import 'package:mvkk/website_page.dart';

class BeforeNavigationController extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BeforeNavigationController();
  }
}

class _BeforeNavigationController extends State<BeforeNavigationController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("महाराष्ट्र वीज कंत्राटी कामगार संघ"),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
      body: Container(
        child: Column(
          children: [
            
                  Container(
                    child: Expanded(
                      child: GridView.count(
                        crossAxisCount: 2,
                        childAspectRatio: (15 / 14),
                        crossAxisSpacing: 5,
                        mainAxisSpacing: 0,
                        children: [
                          GestureDetector(
                            onTap: (){
                              Navigator.of(context).push(MaterialPageRoute(builder: (context){
                                return SendOTP();
                              }));
                            },
                            child: Image.asset("assets\\images\\reg.png"),
                          ),
                           GestureDetector(
                            onTap: (){
                              Navigator.of(context).push(MaterialPageRoute(builder: (context){
                                return Login();
                              }));
                            },
                            child: Image.asset("assets\\images\\logins.png"),
                          ),
                           GestureDetector(
                            onTap: (){
                               Navigator.of(context).push(MaterialPageRoute(builder: (context){
                                return WebsitePage();
                              }));
                            },
                            child: Image.asset("assets\\images\\webs.png"),
                          ),
                           GestureDetector(
                            onTap: (){},
                            child: Image.asset("assets\\images\\notifyicon.png"),
                          ),
                           GestureDetector(
                            onTap: (){},
                            child: Image.asset("assets\\images\\info.png"),
                          ),
                           GestureDetector(
                            onTap: (){},
                            child: Image.asset("assets\\images\\reg.png"),
                          ),
                        ],
                      ),
                    ),
                  ),
                
            
            Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.015),
              height: 50,
              color: Colors.orange,
              child: Text("Contact: 9970004474, 9822418395, 9890169667, 9552874821", style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w500,
                 fontSize: 16
              ),),
            )
          ],
        ),
      ),
    );
  }
}
