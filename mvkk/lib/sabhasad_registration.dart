import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SendOTP extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SendOTP();
  }
}

class _SendOTP extends State<SendOTP> {
  final formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("OTP"),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
      body: Form(
        key: formKey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              alignment: Alignment.center,
              child: Text(
                "सभासद नोंदणी साठी मोबाईल नं. टाका",
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01),
              width: MediaQuery.of(context).size.width * 0.96,
              child: TextFormField(
                decoration: InputDecoration(
                    labelText: "मोबाईल नं.", border: OutlineInputBorder()),
                keyboardType: TextInputType.numberWithOptions(
                    decimal: true, signed: false),
                inputFormatters: [
                  LengthLimitingTextInputFormatter(10),
                  new FilteringTextInputFormatter.allow(RegExp("[0-9]"))
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01),
              child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      primary: Colors.green,
                      textStyle: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      )),
                  onPressed: () {},
                  child: Text("Send OTP")),
            )
          ],
        ),
      ),
    );
  }
}
