import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mvkk/navigation_controller.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'before_navigation_controller.dart';


class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SplashScreenState();
  }
}

class _SplashScreenState extends State<SplashScreen> {
  SharedPreferences? prefs;

  @override
  void initState() {
    super.initState();

    _mockCheckForSession();
  }

  Future<bool> _mockCheckForSession() async {
    Timer(Duration(seconds: 5), _navigateToHome);
    prefs = await SharedPreferences.getInstance();
    return true;
  }

  void _navigateToHome() {
    var user = prefs?.getString('user');
    var pass = prefs?.getString('pass');
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (BuildContext context) {
      if (user == null && pass == null) {
        return BeforeNavigationController(); //Login();
      } else {
        return NavigationController(); //Menu(username: user.toString(),);
      }
    }));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        body: Container(
      color: Colors.orange[100],
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Opacity(
              opacity: 1,
              child: Image.asset(
                'assets\\images\\splash.png',
              )),
        ],
      ),
    ));
  }
}
