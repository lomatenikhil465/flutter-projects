import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'home_page.dart';

class NavigationController extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _NavigationController();
  }
  
}

class _NavigationController extends State<NavigationController> {
   final pageOptions = [Container(),HomePage(), Container()];
  int page = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(""),
        backgroundColor: Colors.orange,
        centerTitle: true,
      ),
      bottomNavigationBar:  CurvedNavigationBar(
    backgroundColor: Colors.orange,
    buttonBackgroundColor: Colors.orange[100],
    
    items: <Widget>[
      Icon(Icons.public, size: 30, color: page == 0 ? Colors.orange : Colors.white,),
      Icon(Icons.home, size: 30, color: page == 1 ? Colors.orange : Colors.white),
      Icon(Icons.assignment, size: 30,color: page == 2 ? Colors.orange : Colors.white),
    ],
    onTap: (index) {
     setState(() {
       page = index;
     });
    },
  ),
      body: pageOptions[page],
    );
  }
  
}

