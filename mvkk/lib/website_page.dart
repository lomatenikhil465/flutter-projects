import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebsitePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _WebsitePage();
  }
  
}

class _WebsitePage extends State<WebsitePage> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
     if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("वेबसाईट"),
        backgroundColor: Colors.orange,
        centerTitle: true,

      ),
      body: WebView(
       initialUrl: 'https://www.mvkksangh.com/',
     ),
    );
  }
  
}