import 'package:smit_sp/pages/menuPage.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';


class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Login();
  }

}

List? userData;

class _Login extends State<Login> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  bool _obscureText = true;
   SharedPreferences? prefs;
  String? username, passwd, _currentItemSelected;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final GlobalKey<State> _internetCheck = new GlobalKey<State>();
  // Map _source = {ConnectivityResult.none: false};
  // InternetConnectivity _connectivity = InternetConnectivity.instance;
  Connectivity? _connectivity;
StreamSubscription<ConnectivityResult>? _subscription;

_Login(){
  _connectivity = new Connectivity();
    _subscription = _connectivity!.onConnectivityChanged.listen(onConnectivityChange);
}
void onConnectivityChange(ConnectivityResult result) {
    // TODO: Show snackbar, etc if no connectivity
    if(result == ConnectivityResult.none){
      _showDialogBox(context);
    }
}

 
  Future<String> getJsonData() async {
    prefs = await SharedPreferences.getInstance();
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=login&userid=" +
            username.toString() +
            "&pass=" +
            passwd.toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      userData = convertDataToJson['data'];
    });

    print(userData);
    return "success";
  }


  waiting(BuildContext context) {
    return showDialog(
      
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
                  child: AlertDialog(
            key: _keyLoader,
              content: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                child: CircularProgressIndicator(),
              ),
              Container(
                child: Text("Loading..."),
              )
            ],
          )),
        );
        
      },
      
    );
  }

  void _validateInputs() {
    //checkConnection();
    if (_formKey.currentState!.validate() ) {
      waiting(context);
//    If all data are correct then save data to out variables
      _formKey.currentState!.save();
      getJsonData().then((value) {
        Navigator.of(context).pop();
        if (userData != null) {
          print(userData![0]["id"]);
           prefs!.setString('user', username);
          prefs!.setString('pass', passwd);
          prefs!.setString('empId', userData![0]["id"]);
          Navigator.of(context)
              .pushReplacement(MaterialPageRoute(builder: (context) {
            return Menu(username: username.toString(),);
          }));
        } else {
          
          showDialog(
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: new Text(
                  "Error!",
                  style: TextStyle(color: Colors.red[600]),
                ),
                content: new Text("Please check your Username and Password"),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  new FlatButton(
                    child: new Text("Close"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        }
      });
      _formKey.currentState!.reset();
    } else {
//    If all data are not valid then start auto validation.

      setState(() {
        _autoValidate = true;
      });
    }
  }

   String? validateUsername(String? value) {
    if (value!.length < 3)
      return 'Username must be more than 2 charater';
    else
      return null;
  }

  Future _showDialogBox(BuildContext context) {
    return showDialog(
      
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
                  child: AlertDialog(
                    backgroundColor: Colors.transparent,
            key: _internetCheck,
              content: Container(
                height: 500,
                decoration: BoxDecoration(
                  border: Border.all(width: 5, color: Colors.white),
                  borderRadius: BorderRadius.circular(10),
                  
                ),
                child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
                Container(
                  alignment: Alignment.topCenter,
                  child: Image.asset("assets\\images\\no-internet.png"),
                ),
                
                Container(
                  child: Text("Please Check Your \nInternet Connection", style: TextStyle(color: Colors.white, fontSize: 18,fontWeight: FontWeight.w500),),
                ),
                Container(
                  child: RaisedButton(onPressed: (){
                    Navigator.of(context).pop();
                  },
                  color: Color.fromRGBO(255, 104, 90, 1),
                  child: Text("Try Again", style: TextStyle(color:Colors.white ),),
                  ),
                )
            ],
          ),
              )),
        );
      },
    );
  }

//   void checkConnection(){
// switch (_source.keys.toList()[0]) {
//       case ConnectivityResult.none:
     
//         _showDialogBox(context,"Warning","Check your Internet Connection");
//         break;
// }
//   }


  @override
  void initState() {
    super.initState();
// _connectivity.initialise();
//     _connectivity.myStream.listen((source) {
//       setState(() => _source = source);
//     });
    
  }

  @override
  void dispose() {
    // TODO: implement dispose
    // _connectivity.disposeStream();
    _subscription!.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Form(
            key: _formKey,
            autovalidate: _autoValidate,
            child: Container(
              padding: EdgeInsets.all(30.0),
              child: Center(
                  child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Align(
                alignment: Alignment.center,
                child: Image.asset(
                  "assets\\images\\hands-logo.png",
                  width: MediaQuery.of(context).size.width / 1.5,
                ),
              ),
                    Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.02),
                      child: TextFormField(
                        validator: validateUsername,
                        onSaved: (String? user) {
                          username = user.toString();
                        },
                        decoration: InputDecoration(
                          labelText: "Username",
                          contentPadding:
                              EdgeInsets.only(left: 10.0, right: 2.0),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.03),
                      child: TextFormField(
                        onSaved: (String? pass) {
                          passwd = pass.toString();
                        },
                        validator: (value){
                          if(value == ""){
                            return "please enter password";
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                          suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                _obscureText = !_obscureText;
                              });
                            },
                            child: Icon(
                              _obscureText
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                            ),
                          ),
                          labelText: "Password",
                          contentPadding:
                              EdgeInsets.only(left: 10.0, right: 2.0),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        obscureText: _obscureText,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 50.0),
                    ),
                    RaisedButton(
                      elevation: 3.0,
                      onPressed: (){
                        
                         //_showDialogBox(context, "", "");
                        _validateInputs();
                      },
                      shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(
                            width: 1.5, color: Colors.lightBlue.shade700),
                      ),
                      textColor: Colors.lightBlue[700],
                      padding: EdgeInsets.only(
                          right: 100.0, left: 100.0, top: 10.0, bottom: 10.0),
                      color: Colors.white,
                      child: Text(
                        "Login",
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            textBaseline: TextBaseline.alphabetic),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.25),
                      child: Text(
                        "© Designed by SMiT Solutions",
                        style: TextStyle(fontSize: 10),
                      ),
                    )
                  ],
                ),
              )),
            ))

    );
  }

}