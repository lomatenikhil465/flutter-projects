import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class ExpensesList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ExpensesList();
  }
}

class _ExpensesList extends State<ExpensesList> {
  TextEditingController _fromDateController, _toDateController;
  DateTime _dateTime;
  List expensesList;
  int customerVendor = 0;
  SharedPreferences prefs;
  Future<String> expensesResponse;

  Future<String> getJsonData() async {
    var empId = prefs.getString('empId');
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=expensesList&fdt=" +
            _fromDateController.text +
            "&tdt=" +
            _toDateController.text +
            "&empId=" +
            empId;
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      expensesList = convertDataToJson['data'];
    });
    return "success";
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    DateTime now = DateTime.now();
    String today = DateFormat("dd/MM/yyyy").format(now);
    _fromDateController = TextEditingController(text: today);
    _toDateController = TextEditingController(text: today);
    getEmpId().then((value){
      expensesResponse = this.getJsonData();
    });
    
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Expenses"),
        backgroundColor: Colors.lightBlue[700],
      ),
      body: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                child: Text(
                  " From : ",
                  style: TextStyle(fontSize: 16),
                ),
              ),
              Container(
                  height: 50,
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01,
                      left: MediaQuery.of(context).size.width * 0.01),
                  width: MediaQuery.of(context).size.width * 0.35,
                  child: DateTimeField(
                      format: DateFormat('dd/MM/yyyy'),
                      style: TextStyle(fontSize: 15),
                      onChanged: (value) {
                        this.getJsonData();
                      },
                      initialValue: DateTime.now(),
                      controller: _fromDateController,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.02,
                          ),
                          border: OutlineInputBorder(borderSide: BorderSide()),
                          prefixIcon: Icon(Icons.calendar_today)),
                      resetIcon: null,
                      onShowPicker: (context, currentValue) {
                        return showDatePicker(
                            context: context,
                            initialDate:
                                _dateTime == null ? DateTime.now() : _dateTime,
                            firstDate: DateTime(2001),
                            lastDate: DateTime(2025));
                      },
                      validator: (val) {
                        if (val != null) {
                          return null;
                        } else {
                          return 'Date Field is Empty';
                        }
                      })),
              Spacer(),
              Container(
                child: Text(
                  "To : ",
                  style: TextStyle(fontSize: 16),
                ),
              ),
              Container(
                  height: 50,
                  padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01,
                    left: MediaQuery.of(context).size.width * 0.01,
                  ),
                  width: MediaQuery.of(context).size.width * 0.35,
                  child: DateTimeField(
                      format: DateFormat('dd/MM/yyyy'),
                      onChanged: (value) {
                        this.getJsonData();
                      },
                      style: TextStyle(fontSize: 15),
                      initialValue: DateTime.now(),
                      controller: _toDateController,
                      decoration: InputDecoration(
                          contentPadding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.02,
                          ),
                          border: OutlineInputBorder(borderSide: BorderSide()),
                          prefixIcon: Icon(Icons.calendar_today)),
                      resetIcon: null,
                      onShowPicker: (context, currentValue) {
                        return showDatePicker(
                            context: context,
                            initialDate:
                                _dateTime == null ? DateTime.now() : _dateTime,
                            firstDate: DateTime(2001),
                            lastDate: DateTime(2025));
                      },
                      validator: (val) {
                        if (val != null) {
                          return null;
                        } else {
                          return 'Date Field is Empty';
                        }
                      }))
            ],
          ),
          Container(
            child: Expanded(
                child: expensesList == null
                    ? Center(
                        child: Text("No Data Available..."),
                      )
                    : FutureBuilder<String>(
                        future: expensesResponse,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return ListView.separated(
                              separatorBuilder:
                                  (BuildContext context, int index) => Divider(
                                thickness: 0.0,
                                color: Colors.lightBlue[700],
                              ),
                              itemCount: expensesList == null
                                  ? 0
                                  : expensesList.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  child: Card(
                                    elevation: 3.0,
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.01,
                                                  top: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.01),
                                              child: Text(
                                                expensesList[index]["to_whom"],
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            )
                                          ],
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.01,
                                                  top: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.01),
                                              child: Text(
                                                expensesList[index]["reason"] +
                                                    " by " +
                                                    expensesList[index]
                                                        ["payment_mode"],
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            )
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.01,
                                                  top: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.01),
                                              child: Text(
                                                expensesList[index]["dt"],
                                                style: TextStyle(
                                                    color: Colors.grey[700]),
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                  right: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.01,
                                                  top: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.01),
                                              child: Text(
                                                "₹ " +
                                                    expensesList[index]
                                                        ["amount"],
                                                style: TextStyle(
                                                    color: Colors.deepOrange),
                                              ),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              },
                            );
                          } else {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                        },
                      )),
          )
        ],
      ),
    );
  }
}
