import 'package:smit_sp/pages/PurchasePage.dart';
import 'package:smit_sp/pages/SalePage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'CustomerVendorReceipt.dart';

class SheetButton extends StatefulWidget {
  final String text, custId;
  final Color buttonColor;
  final int payReceive, salePurchase, getData;

  SheetButton(
      {required this.text,
      required this.buttonColor,
     required this.payReceive,
     required this.custId,
     required this.salePurchase,
     required this.getData});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SheetButton();
  }
}

class _SheetButton extends State<SheetButton> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: MediaQuery.of(context).size.width * 0.85,
      child: RaisedButton(
        onPressed: () async {
          Navigator.of(context).pop();
          if(widget.payReceive != null){
          if (widget.getData == 0) {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return ReceiptTab(
                payReceive: widget.payReceive,
              );
            }));
          } else {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return MainReceiptPage(
                payReceive: widget.payReceive,
                index: null,
                salePurchase: widget.salePurchase,
                custId: widget.custId,
                getData: widget.getData,
              );
            }));
          }
          } else {
            if(widget.salePurchase == 0){
              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context){
                return SaleTabPage(
                  billNo: null,
                  customerId: widget.custId,
                  editFlag: 2,
                );
              }));
              } else {
                Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context){
                return PurchaseTabPage(
                  billNo: null,
                  customerId: widget.custId,
                  editFlag: 2,
                );
              }));
              }
          }
        },
        color: widget.buttonColor,
        child: Text(
          widget.text,
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }
}

class ReceiptTab extends StatefulWidget {
  final int payReceive;
  ReceiptTab({required this.payReceive});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ReceiptTab();
  }
}

class _ReceiptTab extends State<ReceiptTab> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
      length: 1,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Bahi Khata Receipt"),
          backgroundColor: Colors.lightBlue[700],
          bottom: TabBar(
              labelColor: Colors.lightBlue[700],
              unselectedLabelColor: Colors.white,
              indicatorSize: TabBarIndicatorSize.label,
              indicator: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10)),
                  color: Colors.white),
              tabs: [
                Tab(
                  child: Align(
                    alignment: Alignment.center,
                    child: Text("Customer"),
                  ),
                ),
                
              ]),
        ),
        body: TabBarView(children: [
          CustomerVendorReceiptPage(
            salePurchase: 0,
            payReceive: widget.payReceive,
          ),
          
        ]),
      ),
    );
  }
}
