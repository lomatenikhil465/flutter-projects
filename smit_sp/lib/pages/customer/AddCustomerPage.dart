import 'package:bhandari/pages/customer/CreateNewCustomerPage.dart';
import 'package:bhandari/pages/customer/ExistingCustomerPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AddCustomerPage extends StatefulWidget {

  final int salePurchase;
  AddCustomerPage({ this.salePurchase});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AddCustomerPage();
  }

}

class _AddCustomerPage extends State<AddCustomerPage>{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
      length: 2,
          child: Scaffold(
        appBar: AppBar(
          title: Text("Add Customer"),
          backgroundColor: Colors.lightBlue[700],
          bottom: TabBar(
                  labelColor: Colors.lightBlue[700],
                  unselectedLabelColor: Colors.white,
                  indicatorSize: TabBarIndicatorSize.label,
                  indicator: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10)),
                      color: Colors.white),
                  tabs: [
                    Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text("Existing Cusomer"),
                      ),
                    ),
                    
                    Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text("Create New Customer"),
                      ),
                    ),
                  ]
              ),
            
      
      
        ),
        body: TabBarView(
          children: [
            ExistingCustomerPage(salePurchase: widget.salePurchase,),
              
             CreateNewCustomerPage(salePurchase: widget.salePurchase,)
          ]
          ),
      ),
    );
  }

}