import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import '../Customer.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class CreateNewCustomerPage extends StatefulWidget {
  final int salePurchase;
  CreateNewCustomerPage({this.salePurchase});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CreateNewCustomerPage();
  }
}

class _CreateNewCustomerPage extends State<CreateNewCustomerPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  static List<Customer> customer = new List<Customer>();
  
  int radio = 0, crde = 0;
  String? compName,
      custName,
      contactNo,
      email,
      gst,
      address,
      changedRadio,
      openingValue;
  bool _autoValidate, _switch;
  var temp;
  TextEditingController _customerController;
 GlobalKey<AutoCompleteTextFieldState<Customer>> key = new GlobalKey();

  Future<String> insertCustomer() async {
    String url=
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertCustomer&";
    
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'custName': custName,
          'firm_name': compName,
          'address': address,
          'contact': contactNo,
          'email': email,
          'cst': '',
          'gst': gst,
          'state': radio.toString()
        }).then((value) {
      print(value.statusCode);
    });

    //print(response.reasonPhrase);

    return "success";
  }

//   Future<String> getJsonData() async {
//     String url=
//           "https://qot.constructionsmall.com/app/getquerycode.php?apicall=customer";
   
//     print(url);
//     var response = await http.get(
//         //Encode the url
//         Uri.encodeFull(url),
//         //only accept Json response
//         headers: {"Accept": "application/json"});

    
//     setState(() {
//       var convertDataToJson = json.decode(response.body);
//       customer = convertDataToJson['data'];
//     });
// print(customer.toString());
//     return "success";
//   }

//   void getUsers() async {
//     try {
//         final response =
//             await http.get("https://qot.constructionsmall.com/app/getquerycode.php?apicall=customer");
//             print(response.statusCode == 200);
//         if (response.statusCode == 200) {
//         customer = loadUsers(response.body);
//         print('Users: ${customer.length}');
        
//         } else {
//         print("Error getting users.1");
//         }
//     } catch (e) {
//         print("Error getting users.2" + e.toString());
//     }
// }
 
// static List<Customer> loadUsers(String jsonString) {    
//     final parsed = json.decode(jsonString);
//     return parsed['data'].map<Customer>((json) => Customer.fromJson(json)).toList();
// }

  Future<String> insertOpeningStock() async {
    String url;

    print(widget.salePurchase);
    if (widget.salePurchase == 0) {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertOpeningStock";
    } else {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertOpeningStockPurchase";
    }
    print(url);
    print(openingValue);
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'date': DateFormat("dd/MM/yyyy").format(DateTime.now()),
          'amount': openingValue,
          'firm_id': '1',
          'emp_id': '1',
          'crde': crde == 0 ? 'cr' : 'dr'
        }).then((value) {
      print(value.statusCode);
    });

    //print(response.reasonPhrase);

    return "success";
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  String validateMobile(String value) {
// Indian Mobile number are of 10 digit only
    if (value.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }

  void radioButtonChanges(int val) {
    setState(() {
      if (radio == 0) {
        changedRadio = "Maharashtra";
        radio = 1;
      } else {
        changedRadio = "Out of Maharashtra";
        radio = 0;
      }
    });
  }

  void radioButtonCreDeb(int val) {
    setState(() {
      if (crde == 0) {
        crde = 1;
      } else {
        crde = 0;
      }
    });
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  void saveCustomer() {
    if (_formKey.currentState.validate()) {
      print("enter");
      _formKey.currentState.save();

      insertCustomer().then((value) {
        if (_switch) {
          insertOpeningStock().then((value) {
            _formKey.currentState.reset();
            _showDialogBox(context, "Saved to Customer List Successfully", "");
          });
        } else {
          _formKey.currentState.reset();
          _showDialogBox(context, "Saved to Customer List Successfully", "");
        }
      });
    } else {
      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    customer = new List();
    _autoValidate = false;
    _switch = false;
    crde = 0;
    _customerController = TextEditingController(text: "");
    CustomerViewModel.loadCategories();
    // getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            textAlignVertical: TextAlignVertical.bottom,
                            autofocus: false,
                            onSaved: (String custName) {
                              setState(() {
                                this.custName = custName;
                              });
                            },
                            validator: (value) {
                              if (value == "") {
                                return "Please Enter name";
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                                labelText: "Customer Name",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                                suffixIcon: Container(
                                  padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.03,
                                      left: MediaQuery.of(context).size.width *
                                          0.07),
                                  child: Icon(Icons.perm_contact_calendar),
                                )),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            autofocus: false,
                            textAlignVertical: TextAlignVertical.bottom,
                            onSaved: (String compName) {
                              setState(() {
                                this.compName = compName;
                              });
                            },
                            validator: (value) {
                              if (value == "") {
                                return "Please Enter Company Name";
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              labelText: "Company Name",
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            autofocus: false,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(10),
                              new WhitelistingTextInputFormatter(
                                  RegExp("[0-9]"))
                            ],
                            onSaved: (String contact) {
                              setState(() {
                                this.contactNo = contact;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: "Contact No",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                                suffixIcon: Container(
                                  padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.03,
                                      left: MediaQuery.of(context).size.width *
                                          0.07),
                                  child: Icon(Icons.perm_contact_calendar),
                                )),
                            validator: validateMobile,
                            keyboardType: TextInputType.phone,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: AutoCompleteTextField(
                      itemSubmitted: (item){
                       
                          print("Submit" + item.cust_name.toString());
                          _customerController.text = item.cust_name.toString();
                    
                      }, 
                      clearOnSubmit: false,
                      key: key, 
                      suggestions: CustomerViewModel.customer, 
                      controller: _customerController,
                      itemBuilder: (context, item){
                       print( item.cust_name.toString());
                        return Container(
                          child: Row(
                            children: [
                              Container(
                                child: Text(
                                  item.cust_name.toString(),

                                ),
                              )
                            ],
                          ),
                        );
                      }, 
                      itemSorter: (a, b) =>
          a.cust_name.compareTo(b.cust_name), 
                      itemFilter: (suggestion, input) {
                        
         return  suggestion.cust_name.toLowerCase().startsWith(input.toLowerCase());
                      }
          ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            autofocus: false,
                            //inputFormatters: [new BlacklistingTextInputFormatter(RegExp(r"/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/")),],
                            onSaved: (value) {
                              setState(() {
                                this.email = value;
                              });
                            },

                            decoration: InputDecoration(
                              labelText: "Email",
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            validator: validateEmail,
                            keyboardType: TextInputType.emailAddress,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: <Widget>[
                        Radio(
                          value: 0,
                          groupValue: radio,
                          onChanged: radioButtonChanges,
                        ),
                        Container(
                          child: Text(
                            "Maharashtra",
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                        Radio(
                          value: 1,
                          groupValue: radio,
                          onChanged: radioButtonChanges,
                        ),
                        Container(
                          child: Text(
                            "Out of Maharashtra",
                            style: TextStyle(fontSize: 16),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            autofocus: false,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(15),
                              new BlacklistingTextInputFormatter(
                                  RegExp(r"[^\s\w]")),
                            ],
                            textCapitalization: TextCapitalization.characters,
                            onSaved: (String gst) {
                              setState(() {
                                this.gst = gst;
                              });
                            },
                            validator: (value) {
                              if (value == "") {
                                return "Please Enter GST no";
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              labelText: "GST",
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            autofocus: false,
                            validator: (value) {
                              if (value == "") {
                                return "Please Enter address";
                              } else {
                                return null;
                              }
                            },
                            maxLines: 2,
                            onSaved: (String address) {
                              setState(() {
                                this.address = address;
                              });
                            },
                            decoration: InputDecoration(
                              labelText: "Address",
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Container(
                        child: Text(
                          "Do you Want to fill opening stock?",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      Container(
                        child: Switch(
                            value: _switch,
                            onChanged: (value) {
                              setState(() {
                                _switch = value;
                              });
                            }),
                      )
                    ],
                  ),
                  _switch
                      ? Container(
                          padding: EdgeInsets.only(
                              //top: MediaQuery.of(context).size.height * 0.02,
                              left: MediaQuery.of(context).size.width * 0.04,
                              right: MediaQuery.of(context).size.width * 0.04),
                          child: Row(
                            children: <Widget>[
                              Container(
                                width: MediaQuery.of(context).size.width * 0.92,
                                child: TextFormField(
                                  autofocus: false,
                                  validator: (value) {
                                    if (value == "") {
                                      return "Please Enter Opening Stock";
                                    } else {
                                      return null;
                                    }
                                  },
                                  onSaved: (String value) {
                                    setState(() {
                                      this.openingValue = value;
                                    });
                                  },
                                  decoration: InputDecoration(
                                    labelText: "Opening Stock",
                                    contentPadding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.015),
                                  ),
                                  keyboardType: TextInputType.numberWithOptions(
                                      decimal: true, signed: false),
                                  style: new TextStyle(
                                    fontFamily: "Poppins",
                                  ),
                                ),
                              )
                            ],
                          ),
                        )
                      : Container(),
                  _switch
                      ? Container(
                          child: Row(
                            children: <Widget>[
                              Radio(
                                value: 0,
                                groupValue: crde,
                                onChanged: radioButtonCreDeb,
                              ),
                              Container(
                                child: Text(
                                  "Credit",
                                  style: TextStyle(fontSize: 16),
                                ),
                              ),
                              Radio(
                                value: 1,
                                groupValue: crde,
                                onChanged: radioButtonCreDeb,
                              ),
                              Container(
                                child: Text(
                                  "Debit",
                                  style: TextStyle(fontSize: 16),
                                ),
                              )
                            ],
                          ),
                        )
                      : Container()
                ],
              ),
            ),
          ),
          Container(
            height: 50,
            width: MediaQuery.of(context).size.width * 1,
            color: Colors.lightBlue[700],
            child: FlatButton(
                onPressed: () {
                  saveCustomer();
                },
                child: Text(
                  "Save",
                  style: TextStyle(color: Colors.white),
                )),
          )
        ],
      ),
    );
  }
}
