import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class ExistingCustomerPage extends StatefulWidget {
  final int salePurchase;
  ExistingCustomerPage({this.salePurchase});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ExistingCustomerPage();
  }
}

List nameOfCustomer = List();
List _searchResult;

class _ExistingCustomerPage extends State<ExistingCustomerPage> {
  Future<String> customerResponse;
  TextEditingController _controller;

  Future<String> getJsonData() async {
    String url=
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=customer";
   
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      nameOfCustomer = convertDataToJson['data'];
    });

    return "success";
  }

  void onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      //return ;
    }

    nameOfCustomer.forEach((userDetail) {
      print("search result : " +userDetail["cust_name"].toString() );
      if (userDetail["cust_name"].toLowerCase().contains(text.toLowerCase()) ){

        _searchResult.add(userDetail);
      }
    });

    setState(() {});
  
}

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _searchResult = new List();
    _controller = TextEditingController(text: ""); 
    nameOfCustomer.clear();
    customerResponse = getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: <Widget>[
        Container(
            height: 55,
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                onChanged: (value) {
                  onSearchTextChanged(value);
                },
                controller: _controller,
                decoration: InputDecoration(
                    labelText: "Search",
                    hintText: "Search",
                    prefixIcon: Icon(Icons.search),
                    contentPadding: EdgeInsets.only(top: 10),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5.0)))),
              ),
            ),
        Expanded(
                  child: FutureBuilder<String>(
            future: customerResponse,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return nameOfCustomer == null
                    ? Center(
                        child: Text("No Data Available"),
                      )
                    :(_searchResult ==null ? false :_searchResult.length != 0 )|| _controller.text.isNotEmpty ?ListView.separated(
                        separatorBuilder: (BuildContext context, int index) =>
                            Divider(
                          thickness: 0.0,
                          color: Colors.lightBlue[900],
                        ),
                        itemCount: _searchResult == null ? 0 : _searchResult.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.pop(context, index.toString());
                            },
                            child: Container(
                              height: 60,
                              child: Card(
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(
                                              left:
                                                  MediaQuery.of(context).size.width *
                                                      0.01,
                                              top:
                                                  MediaQuery.of(context).size.height *
                                                      0.01),
                                          child: Text(_searchResult[index]
                                                  ["cust_name"]
                                              .toString()),
                                        )
                                      ],
                                    ),
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Container(
                                          child: Icon(
                                            Icons.location_on,
                                            color: Colors.grey,
                                            size: 15,
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              left:
                                                  MediaQuery.of(context).size.width *
                                                      0.01,
                                              top:
                                                  MediaQuery.of(context).size.height *
                                                      0.01),
                                          child: Text(
                                            _searchResult[index]["address"],
                                            style: TextStyle(color: Colors.grey),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      ) :ListView.separated(
                        separatorBuilder: (BuildContext context, int index) =>
                            Divider(
                          thickness: 0.0,
                          color: Colors.lightBlue[900],
                        ),
                        itemCount: nameOfCustomer == null ? 0 : nameOfCustomer.length,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () {
                              Navigator.pop(context, index.toString());
                            },
                            child: Container(
                              height: 60,
                              child: Card(
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(
                                              left:
                                                  MediaQuery.of(context).size.width *
                                                      0.01,
                                              top:
                                                  MediaQuery.of(context).size.height *
                                                      0.01),
                                          child: Text(nameOfCustomer[index]
                                                  ["cust_name"]
                                              .toString()),
                                        )
                                      ],
                                    ),
                                    Row(
                                      crossAxisAlignment: CrossAxisAlignment.end,
                                      children: <Widget>[
                                        Container(
                                          child: Icon(
                                            Icons.location_on,
                                            color: Colors.grey,
                                            size: 15,
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              left:
                                                  MediaQuery.of(context).size.width *
                                                      0.01,
                                              top:
                                                  MediaQuery.of(context).size.height *
                                                      0.01),
                                          child: Text(
                                            nameOfCustomer[index]["address"],
                                            style: TextStyle(color: Colors.grey),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                      );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ),
      ],
    );
  }
}
