import 'package:smit_sp/pages/InvoiceDetails.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class QuotationListPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _QuotationListPage();
  }
}

class _QuotationListPage extends State<QuotationListPage> {
  Future<String>? quotationListResponse;
  String? currDate;
  List? quotationList;
  Future<String> getJsonData() async {
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=quotationList";
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      quotationList = convertDataToJson['data'];
    });

    return "success";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    DateTime now = DateTime.now();
    currDate = DateFormat('dd/MM/yyyy').format(now);
    quotationListResponse = this.getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Quotation List"),
          backgroundColor: Colors.lightBlue[700],
        ),
        body: FutureBuilder<String>(
          future: quotationListResponse,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Container(
                  child: ListView.separated(
                separatorBuilder: (BuildContext context, int index) => Divider(
                  thickness: 0.0,
                  color: Colors.lightBlue[900],
                ),
                itemCount: quotationList == null ? 0 : quotationList!.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return InvoiceDetails(
                          billNo: quotationList![index]["bill_no"],
                          custId: quotationList![index]["cust_id"],
                          custName: quotationList![index]["cust_name"],
                          salePurchase: 2,
                        );
                      })).then((value) {
                        this.getJsonData();
                      });
                    },
                    child: Container(
                      child: Card(
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width *
                                          0.01,
                                      top: MediaQuery.of(context).size.height *
                                          0.01),
                                  child: Text(
                                    quotationList![index]["cust_name"]
                                        .toString(),
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.lightBlue[700]),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width *
                                          0.01,
                                      top: MediaQuery.of(context).size.height *
                                          0.01),
                                  child: Text(
                                    quotationList![index]["grand_total"]
                                            .toString() +
                                        "₹",
                                  ),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width *
                                          0.01,
                                      top: MediaQuery.of(context).size.height *
                                          0.01),
                                  child: Text(
                                    quotationList![index]["bill_no"].toString(),
                                  ),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.01,
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.01),
                                      child: Text(
                                        quotationList![index]["date"].toString(),
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  ],
                                ),
                                Spacer(),
                                Container(
                                  height: MediaQuery.of(context).size.height *
                                      0.022,
                                  width:
                                      MediaQuery.of(context).size.width * 0.20,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10.0),
                                      color: quotationList![index]
                                                  ["approval_status"] ==
                                              "0"
                                          ? Colors.deepOrange
                                          : Colors.green[600]),
                                  child: Center(
                                    child: Text(
                                      quotationList![index]["approval_status"] ==
                                              "0"
                                          ? "Pending"
                                          : "Approved",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ));
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ));
  }
}
