
import 'dart:io';

import 'package:smit_sp/Login.dart';
import 'package:smit_sp/pages/CollectionCash.dart';
import 'package:smit_sp/pages/CollectionLedger.dart';
import 'package:smit_sp/pages/CollectionLedgerReceipt.dart';
import 'package:smit_sp/pages/CustomerList.dart';
import 'package:smit_sp/pages/CustomerVendorReceipt.dart';
import 'package:smit_sp/pages/DayClose.dart';
import 'package:smit_sp/pages/ExpensesCustomerList.dart';
import 'package:smit_sp/pages/ExpensesList.dart';
import 'package:smit_sp/pages/ExpensesPage.dart';
import 'package:smit_sp/pages/PdfViewer.dart';
import 'package:smit_sp/pages/ProductList.dart';
import 'package:smit_sp/pages/ProfilePage.dart';
import 'package:smit_sp/pages/PurchasePage.dart';
import 'package:smit_sp/pages/QuotationListPage.dart';
import 'package:smit_sp/pages/QuotationPage.dart';
import 'package:smit_sp/pages/ReceiptPage.dart';
import 'package:smit_sp/pages/changePasswordPage.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'BottomSheetWidget.dart';
import 'package:smit_sp/pages/SaleListPage.dart';
import 'package:smit_sp/pages/SalePage.dart';
import 'package:smit_sp/pages/StockPage.dart';
import 'package:smit_sp/pages/VendorList.dart';

import 'package:smit_sp/pages/ledger/LedgerPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'PurchaseList.dart';
import 'package:pdf/pdf.dart';
import 'package:printing/printing.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class Menu extends StatefulWidget {
  final String username;
  Menu({required this.username});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Menu();
  }
}

class _Menu extends State<Menu> {
  List? todayCollection,todayCollection1;
  SharedPreferences? prefs;
  double saleCollection = 0.0,
      purchaseCollection = 0.0,
      receiptCollection = 0.0,
      cashCollection = 0.0,
      outstandingReceivable = 0.0,
      outstandingPayable = 0.0;
      String _currentItemSelected = "Today";
      String today = DateFormat("yyyy-MM-dd").format(DateTime.now());
      String destination = DateFormat("yyyy-MM-dd").format(DateTime.now());
      var _dueInterval = ['Today', 'Yesterday', 'Last Week', 'Last Month', 'Last 3 Months', 'Last 6 Months', 'Last Year'];
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  Future<String> getJsonData() async {
    var empId = prefs!.getString('empId');
    print(empId);
    

    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=saledayclose&fdt=" +
            today +
            "&tdt=" +
            destination +
            "&uid=" +
            empId;
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      todayCollection = convertDataToJson['data'];
      if (todayCollection != null) {
      for (var i = 0; i < todayCollection!.length; i++) {
        if (todayCollection![i]["eType"] == "SALE") {
          saleCollection =
              saleCollection + double.parse(todayCollection![i]["grand_total"]);
        }

        if (todayCollection![i]["eType"] == "RCPT") {
          receiptCollection =
              receiptCollection + double.parse(todayCollection![i]["cr"]);
        }

        if (todayCollection![i]["eType"] == "ON ACCOUNT" && todayCollection![i]["cr"] != "0") {
          receiptCollection =
              receiptCollection + double.parse(todayCollection![i]["cr"]);
        } else if(todayCollection![i]["eType"] == "ON ACCOUNT" && todayCollection![i]["dr"] != "0"){
           receiptCollection =
              receiptCollection + double.parse(todayCollection![i]["dr"]);
        }

        if (todayCollection![i]["eType"] == "PURCHASE") {
          purchaseCollection = purchaseCollection +
              double.parse(todayCollection![i]["grand_total"]);
          
        }

        if (todayCollection![i]["eType"] == "VCH") {
          cashCollection =
              cashCollection + double.parse(todayCollection![i]["dr"]);
        }

       

      }

      outstandingReceivable = saleCollection - receiptCollection;
      outstandingPayable = purchaseCollection - cashCollection;

    }
    });

    
    return "success";
  }

  Future<String> getAllTime() async {
    var saleCollection1 = 0.0,receiptCollection1= 0.0,purchaseCollection1= 0.0,cashCollection1= 0.0;
    var empId = prefs!.getString('empId');
    print("get all time" + empId);
    

    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=saledayclose2&uid=" +
            empId;
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      todayCollection1 = convertDataToJson['data'];
    });

    if (todayCollection1 != null) {
      for (var i = 0; i < todayCollection1!.length; i++) {
        if (todayCollection1![i]["eType"] == "SALE") {
          saleCollection1 =
              saleCollection1 + double.parse(todayCollection1![i]["grand_total"]);
        }

        if (todayCollection1![i]["eType"] == "RCPT") {
          receiptCollection1 =
              receiptCollection1 + double.parse(todayCollection1![i]["cr"]);
        }

        if (todayCollection1![i]["eType"] == "ON ACCOUNT" && todayCollection1![i]["cr"] != "0") {
          receiptCollection1 =
              receiptCollection1 + double.parse(todayCollection1![i]["cr"]);
        } else if(todayCollection1![i]["eType"] == "ON ACCOUNT" && todayCollection1![i]["dr"] != "0"){
           receiptCollection1 =
              receiptCollection1 + double.parse(todayCollection1![i]["dr"]);
        }

        if (todayCollection1![i]["eType"] == "PURCHASE") {
          purchaseCollection1 = purchaseCollection1 +
              double.parse(todayCollection1![i]["grand_total"]);
          
        }

        if (todayCollection1![i]["eType"] == "VCH") {
          cashCollection1 =
              cashCollection1 + double.parse(todayCollection1![i]["dr"]);
        }

       

      }

      outstandingReceivable = saleCollection1 - receiptCollection1;
      outstandingPayable = purchaseCollection1 - cashCollection1;

    }
    return "success";
  }

  void _onDropDownItemSelected(String newValueSelected) {
    setState(() {
      this._currentItemSelected = newValueSelected;
      if(_currentItemSelected == 'Today'){
        today = DateFormat("yyyy-MM-dd").format(DateTime.now());
        destination = DateFormat("yyyy-MM-dd").format(DateTime.now());
        saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
        this.getAllTime();
      } else if(_currentItemSelected == 'Yesterday'){
        today = DateFormat("yyyy-MM-dd").format(DateTime.now().subtract(Duration(days: 1)));
        destination = DateFormat("yyyy-MM-dd").format(DateTime.now().subtract(Duration(days: 1)));
        saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
        print(today);
      } else if(_currentItemSelected == 'Last Week'){
        today = DateFormat("yyyy-MM-dd").format(DateTime.now().subtract(Duration(days: 7)));
        destination = DateFormat("yyyy-MM-dd").format(DateTime.now());
        saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
        print(today);
      } else if(_currentItemSelected == 'Last Month'){
        DateTime now = DateTime.now();
        today = DateFormat("yyyy-MM-dd").format(DateTime(now.year,now.month - 1, now.day));
        
        print(today);
        destination = DateFormat("yyyy-MM-dd").format(DateTime.now());
        saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
      } else if(_currentItemSelected == 'Last 3 Months'){
        DateTime now = DateTime.now();
        today = DateFormat("yyyy-MM-dd").format(DateTime(now.year,now.month - 3, now.day));
        destination = DateFormat("yyyy-MM-dd").format(DateTime.now());
        saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
        print(today);
      }else if(_currentItemSelected == 'Last 6 Months'){
        DateTime now = DateTime.now();
        today = DateFormat("yyyy-MM-dd").format(DateTime(now.year,now.month - 6, now.day));
        destination = DateFormat("yyyy-MM-dd").format(DateTime.now());
        saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
        print(today);
      }else if(_currentItemSelected == 'Last Year'){
        DateTime now = DateTime.now();
        today = DateFormat("yyyy-MM-dd").format(DateTime(now.year-1,now.month, now.day));
        destination = DateFormat("yyyy-MM-dd").format(DateTime.now());
        saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
        print(today);
      }
    });
    this.getJsonData().then((value) {
      onChange();
    });
  }  
  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  void onChange(){
    setState(() {
      
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEmpId().then((value) {
      this.getJsonData().then((value) {
        outstandingPayable = 0.0;
        outstandingReceivable = 0.0;
        this.getAllTime();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: scaffoldKey,
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
                decoration: BoxDecoration(color: Colors.lightBlue[700]),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        GestureDetector(
                          onTap: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context){
                          return ProfilePage();
                        }));
                      },
                                                  child: Text(
                            "Hello, " + widget.username,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.normal),
                          ),
                        ),
                      ],
                    ),
                    GestureDetector(
                      onTap: (){
                        Navigator.of(context).push(MaterialPageRoute(builder: (context){
                          return ProfilePage();
                        }));
                      },
                                          child: Center(
                        child: Image.asset(
                          "assets\\images\\profile.png",
                          height: 100,
                        ),
                      ),
                    ),
                  ],
                )),
            Container(
              padding: EdgeInsets.only(top: 0),
              child: ListTile(
                leading: Icon(Icons.lock),
                title: Text(
                  "Change Password",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal),
                ),
                onTap: () {
                  //_showDialogBox(context, "Message", "Coming Soon");
                  Navigator.of(context).push(MaterialPageRoute(builder: (context){
                    return ChangePassPage();
                  })).then((value) {
                  saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
                  this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                });
                },
              ),
            ),
            Divider(
              thickness: 1,
              height: 0,
            ),
            ListTile(
              leading: Icon(Icons.account_balance_wallet),
              title: Text("Sale List",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return SaleListPage();
                })).then((value) {
                  saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
                  this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                });
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.add_shopping_cart),
              title: Text("Purchase List",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return PurchaseListPage();
                })).then((value) {
                  saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
                  this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                });
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.shopping_basket),
              title: Text("Customer List",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return CustomerList();
                })).then((value) {
                  saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
                  this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                });
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.business),
              title: Text("Product List",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return ProductListPage();
                })).then((value) {
                  saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
                  this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                });
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.collections),
              title: Text("Stock",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () {
                // Navigator.of(context)
                //     .push(MaterialPageRoute(builder: (BuildContext context) {
                //   return ExpensesPage();
                // })).then((value) {
                //   saleCollection = 0.0;
                //   purchaseCollection = 0.0;
                //   receiptCollection = 0.0;
                //   cashCollection = 0.0;
                //   outstandingReceivable = 0.0;
                //   outstandingPayable = 0.0;
                //   this.getJsonData();
                // });
                Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return StockPage();
                      })).then((value) {
                        saleCollection = 0.0;
                        purchaseCollection = 0.0;
                        receiptCollection = 0.0;
                        cashCollection = 0.0;
                        outstandingReceivable = 0.0;
                        outstandingPayable = 0.0;
                        this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                      });
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.attach_money),
              title: Text("Expenses List",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return ExpensesList();
                })).then((value) {
                  saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
                  this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                });
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.assignment_turned_in),
              title: Text("Quotation List",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return QuotationListPage();
                })).then((value) {
                  saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
                  this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                });
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.power_settings_new),
              title: Text("Logout",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.remove('user');
                prefs.remove('pass');
                Navigator.of(context)
                    .pushReplacement(MaterialPageRoute(builder: (context) {
                  return Login();
                }));
              },
            )
          ],
        ),
      ),
      appBar: AppBar(
        elevation: 0,
        title: Text("Sale And Purchase"),
        backgroundColor: Colors.lightBlue[700],
        actions: <Widget>[
         Container(
            child: Center(
                child: Text(
              DateFormat("dd/MM/yyyy").format(DateTime.now()),
              style: TextStyle(fontWeight: FontWeight.w500),
            )),
          ),
          
        ],
      ),
      body: Column(
        children: <Widget>[
          Container(
            //height: MediaQuery.of(context).size.height * 0.25,
            width: MediaQuery.of(context).size.width,
            color: Colors.lightBlue[700],
            child: Column(
              children: <Widget>[
                Container(
                  
                  margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05,
                  right:MediaQuery.of(context).size.width * 0.05 ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    color: Colors.white,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        color: Colors.white,
                        child: Text(
                          "Report",
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.lightBlue[700]
                          ),
                        ),
                      ),
                      Container(
                            height: MediaQuery.of(context).size.height * 0.03,
                            width: MediaQuery.of(context).size.width * 0.4,
                            
                            
                            child: DropdownButtonHideUnderline(
                              child: ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton<String>(
                                  itemHeight: 80,
                                  elevation: 1,
                                  iconEnabledColor: Colors.lightBlue[700],
                                  autofocus: true,
                                  focusColor: Colors.lightBlue[700],
                                  //style: Theme.of(context).textTheme.title,
                                  isExpanded: true,
                                  hint: Text("Choose a Due", style: TextStyle(
                                    color: Colors.lightBlue[700]
                                  ),),
                                  items: _dueInterval
                                      .map((String dropDownStringItem) {
                                    return DropdownMenuItem<String>(

                                        value: dropDownStringItem,
                                        child: Text(dropDownStringItem,
                                            style: TextStyle(
                                                fontSize: 17,
                                                fontWeight: FontWeight.normal,
                                                color: Colors.lightBlue[700])));
                                  }).toList(),
                                  onChanged: (String? newValueSelected) =>
                                      _onDropDownItemSelected(newValueSelected.toString()),
                                  value: _currentItemSelected,
                                ),
                              ),
                            ),
                          )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return CollectionLedger(
                              index: 0,
                            );
                          })).then((value) {
                            saleCollection = 0.0;
                            purchaseCollection = 0.0;
                            receiptCollection = 0.0;
                            cashCollection = 0.0;
                            outstandingReceivable = 0.0;
                            outstandingPayable = 0.0;
                            this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                          });
                        },
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.04,
                          width: MediaQuery.of(context).size.width * 0.4,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.white, width: 1),
                              borderRadius: BorderRadius.circular(5)),
                          child: Container(
                            padding: EdgeInsets.all(
                                MediaQuery.of(context).size.height * 0.008),
                            child: Center(
                              child: Text(
                                "₹ " + saleCollection.toString(),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16),
                              ),
                            ),
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return CollectionLedger(
                              index: 1,
                            );
                          })).then((value) {
                            saleCollection = 0.0;
                            purchaseCollection = 0.0;
                            receiptCollection = 0.0;
                            cashCollection = 0.0;
                            outstandingReceivable = 0.0;
                            outstandingPayable = 0.0;
                            this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                          });
                        },
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.04,
                          width: MediaQuery.of(context).size.width * 0.4,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.white, width: 1),
                              borderRadius: BorderRadius.circular(5)),
                          child: Container(
                            padding: EdgeInsets.all(
                                MediaQuery.of(context).size.height * 0.008),
                            child: Center(
                              child: Text(
                                "₹ " + purchaseCollection.toString(),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return CollectionLedger(
                        index: 0,
                      );
                    })).then((value) {
                      saleCollection = 0.0;
                      purchaseCollection = 0.0;
                      receiptCollection = 0.0;
                      cashCollection = 0.0;
                      outstandingReceivable = 0.0;
                      outstandingPayable = 0.0;
                      this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                    });
                  },
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.04,
                          width: MediaQuery.of(context).size.width * 0.4,
                          child: Container(
                            padding: EdgeInsets.all(
                                MediaQuery.of(context).size.height * 0.008),
                            child: Center(
                              child: Text(
                                "Sale",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 14),
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context)
                                .push(MaterialPageRoute(builder: (context) {
                              return CollectionLedger(
                                index: 1,
                              );
                            })).then((value) {
                              saleCollection = 0.0;
                              purchaseCollection = 0.0;
                              receiptCollection = 0.0;
                              cashCollection = 0.0;
                              outstandingReceivable = 0.0;
                              outstandingPayable = 0.0;
                              this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                            });
                          },
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.04,
                            width: MediaQuery.of(context).size.width * 0.4,
                            child: Container(
                              padding: EdgeInsets.all(
                                  MediaQuery.of(context).size.height * 0.008),
                              child: Center(
                                child: Text(
                                  "Purchase",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 14),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return CollectionLedgerReceipt();
                    })).then((value) {
                      saleCollection = 0.0;
                      purchaseCollection = 0.0;
                      receiptCollection = 0.0;
                      cashCollection = 0.0;
                      outstandingReceivable = 0.0;
                      outstandingPayable = 0.0;
                      this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                    });
                  },
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.04,
                          width: MediaQuery.of(context).size.width * 0.4,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.white, width: 1),
                              borderRadius: BorderRadius.circular(5)),
                          child: Container(
                            padding: EdgeInsets.all(
                                MediaQuery.of(context).size.height * 0.008),
                            child: Center(
                              child: Text(
                                "₹ " + receiptCollection.toString(),
                                style: TextStyle(
                                    color: Colors.white, fontSize: 16),
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.of(context)
                                .push(MaterialPageRoute(builder: (context) {
                              return CollectionLedgerCash();
                            })).then((value) {
                              saleCollection = 0.0;
                              purchaseCollection = 0.0;
                              receiptCollection = 0.0;
                              cashCollection = 0.0;
                              outstandingReceivable = 0.0;
                              outstandingPayable = 0.0;
                              this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                            });
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            height: MediaQuery.of(context).size.height * 0.04,
                            decoration: BoxDecoration(
                                border:
                                    Border.all(color: Colors.white, width: 1),
                                borderRadius: BorderRadius.circular(5)),
                            child: Container(
                              padding: EdgeInsets.all(
                                  MediaQuery.of(context).size.height * 0.009),
                              child: Center(
                                child: Text(
                                  "₹ " + cashCollection.toString(),
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.04,
                          width: MediaQuery.of(context).size.width * 0.4,
                          child: Container(
                            padding: EdgeInsets.all(
                                MediaQuery.of(context).size.height * 0.008),
                            child: Center(
                              child: Text(
                                "Receipt ",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 14),
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.04,
                            width: MediaQuery.of(context).size.width * 0.4,
                            child: Container(
                              padding: EdgeInsets.all(
                                  MediaQuery.of(context).size.height * 0.009),
                              child: Center(
                                child: Text(
                                  "Payment",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 14),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.04,
                          width: MediaQuery.of(context).size.width * 0.4,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.white, width: 1),
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.white),
                          child: Container(
                            padding: EdgeInsets.all(
                                MediaQuery.of(context).size.height * 0.008),
                            child: Center(
                              child: Text(
                                "₹ " + outstandingReceivable.toStringAsFixed(2),
                                style: TextStyle(
                                    color: Colors.green[700], fontSize: 16, fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.4,
                            height: MediaQuery.of(context).size.height * 0.04,
                            decoration: BoxDecoration(
                                border:
                                    Border.all(color: Colors.white, width: 1),
                                borderRadius: BorderRadius.circular(5),
                                color: Colors.white),
                            child: Container(
                              padding: EdgeInsets.all(
                                  MediaQuery.of(context).size.height * 0.009),
                              child: Center(
                                child: Text(
                                  "₹ " + outstandingPayable.toStringAsFixed(2),
                                  style: TextStyle(
                                      color: Colors.deepOrange, fontSize: 16,fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {},
                  child: Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.04,
                          child: Container(
                            padding: EdgeInsets.all(
                                MediaQuery.of(context).size.height * 0.008),
                            child: Center(
                              child: Text(
                                "OutStanding Receivable",
                                style: TextStyle(
                                    color: Colors.white, fontSize: 14),
                              ),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            height: MediaQuery.of(context).size.height * 0.04,
                            child: Container(
                              padding: EdgeInsets.all(
                                  MediaQuery.of(context).size.height * 0.009),
                              child: Center(
                                child: Text(
                                  "OutStanding Payable",
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 14),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: GridView.count(
              crossAxisCount: 2,
              primary: false,
              childAspectRatio: 16 / 9,
              padding: EdgeInsets.all(20.0),
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              children: <Widget>[
                GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return SaleTabPage(
                          billNo: null,
                          customerId: null,
                          editFlag: 0,
                        );
                      })).then((value) {
                        saleCollection = 0.0;
                        purchaseCollection = 0.0;
                        receiptCollection = 0.0;
                        cashCollection = 0.0;
                        outstandingReceivable = 0.0;
                        outstandingPayable = 0.0;
                        this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                      });
                    },
                    child: Card(
                      elevation: 5.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Icon(
                              Icons.account_balance_wallet,
                              size: 30,
                              color: Colors.lightBlue[700],
                            )),
                          ),
                          new Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Text(
                              "Sale",
                              style: new TextStyle(
                                  color: Colors.black, fontSize: 20.0),
                            )),
                          ),
                        ],
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return PurchaseTabPage(
                           billNo: null,
                          customerId: null,
                          editFlag: 0,
                        );
                      })).then((value) {
                        saleCollection = 0.0;
                        purchaseCollection = 0.0;
                        receiptCollection = 0.0;
                        cashCollection = 0.0;
                        outstandingReceivable = 0.0;
                        outstandingPayable = 0.0;
                        this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                      });
                    },
                    child: Card(
                      elevation: 5.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Icon(
                              Icons.add_shopping_cart,
                              size: 30,
                              color: Colors.lightBlue[700],
                            )),
                          ),
                          new Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Text(
                              "Purchase",
                              style: new TextStyle(
                                  color: Colors.black, fontSize: 20.0),
                            )),
                          ),
                        ],
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return MainReceiptPage(
                payReceive: 1,
                getData: 0,
                salePurchase: 0,
                index: null,
              );
            })).then((value) {
                        saleCollection = 0.0;
                        purchaseCollection = 0.0;
                        receiptCollection = 0.0;
                        cashCollection = 0.0;
                        outstandingReceivable = 0.0;
                        outstandingPayable = 0.0;
                        this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                      });
                    },
                    child: Card(
                      elevation: 5.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Icon(
                              Icons.assignment,
                              size: 30,
                              color: Colors.lightBlue[700],
                            )),
                          ),
                          new Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Text(
                              "Receipt",
                              style: new TextStyle(
                                  color: Colors.black, fontSize: 20.0),
                            )),
                          ),
                        ],
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      // Navigator.of(context)
                      //     .push(MaterialPageRoute(builder: (context) {
                      //   return LedgerPage();
                      // })).then((value) {
                      //   saleCollection = 0.0;
                      //   purchaseCollection = 0.0;
                      //   receiptCollection = 0.0;
                      //   cashCollection = 0.0;
                      //   outstandingReceivable = 0.0;
                      //   outstandingPayable = 0.0;
                      //   this.getJsonData();
                      // });
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return MainReceiptPage(
                payReceive: 0,
                getData: 0,
                salePurchase: 1,
                index: null,
              );
            })).then((value) {
                        saleCollection = 0.0;
                        purchaseCollection = 0.0;
                        receiptCollection = 0.0;
                        cashCollection = 0.0;
                        outstandingReceivable = 0.0;
                        outstandingPayable = 0.0;
                        this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                      });
                    },
                    child: Card(
                      elevation: 5.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Icon(
                              //Icons.compare_arrows
                              Icons.payment,
                              size: 30,
                              color: Colors.lightBlue[700],
                            )),
                          ),
                          new Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Text(
                              "Payment",
                              style: new TextStyle(
                                  color: Colors.black, fontSize: 20.0),
                            )),
                          ),
                        ],
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      // Future<void> sheetController = showModalBottomSheet<void>(
                      //     context: context,
                      //     builder: (context) => BottomSheetWidget(),
                      //     backgroundColor: Colors.transparent);

                      // sheetController.then((value) {
                      //   print("sheet closed");
                      // });
                      
                       Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return DayClose();
                      })).then((value) {
                        saleCollection = 0.0;
                        purchaseCollection = 0.0;
                        receiptCollection = 0.0;
                        cashCollection = 0.0;
                        outstandingReceivable = 0.0;
                        outstandingPayable = 0.0;
                        this.getJsonData().then((value) {
                          this.getAllTime();
                        });
                      });
                    },
                    child: Card(
                      elevation: 5.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Icon(
                              
                              Icons.business_center,
                              size: 30,
                              color: Colors.lightBlue[700],
                            )),
                          ),
                          new Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Text(
                              "Day Close",
                              style: new TextStyle(
                                  color: Colors.black, fontSize: 20.0),
                            )),
                          ),
                        ],
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return QuotationTabPage(
                          billNo: null,
                          customerId: null,
                          editFlag: 0,
                        );
                      })).then((value) {
                        saleCollection = 0.0;
                        purchaseCollection = 0.0;
                        receiptCollection = 0.0;
                        cashCollection = 0.0;
                        outstandingReceivable = 0.0;
                        outstandingPayable = 0.0;
                        this.getJsonData().then((value) {
                          this.getAllTime();
                        });
                      });
                      // _showDialogBox(context, "Message", "Coming Soon");
                    },
                    child: Card(
                      elevation: 5.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Icon(
                              Icons.assignment_turned_in,
                              size: 30,
                              color: Colors.lightBlue[700],
                            )),
                          ),
                          new Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Text(
                              "Quotation",
                              style: new TextStyle(
                                  color: Colors.black, fontSize: 20.0),
                            )),
                          ),
                        ],
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      // Navigator.of(context)
                      //     .push(MaterialPageRoute(builder: (context) {
                      //   return DayClose();
                      // })).then((value) {
                      //   saleCollection = 0.0;
                      //   purchaseCollection = 0.0;
                      //   receiptCollection = 0.0;
                      //   cashCollection = 0.0;
                      //   outstandingReceivable = 0.0;
                      //   outstandingPayable = 0.0;
                      //   this.getJsonData();
                      // });
                      Navigator.of(context)
                    .push(MaterialPageRoute(builder: (BuildContext context) {
                  return ExpensesPage(
                    
                  );
                })).then((value) {
                  saleCollection = 0.0;
                  purchaseCollection = 0.0;
                  receiptCollection = 0.0;
                  cashCollection = 0.0;
                  outstandingReceivable = 0.0;
                  outstandingPayable = 0.0;
                  this.getJsonData().then((value) {
                    this.getAllTime();
                  });
                });

                      
                    },
                    child: Card(
                      elevation: 5.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Icon(
                              Icons.attach_money,
                              size: 30,
                              color: Colors.lightBlue[700],
                            )),
                          ),
                          new Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Text(
                              "Expenses",
                              style: new TextStyle(
                                  color: Colors.black, fontSize: 20.0),
                            )),
                          ),
                        ],
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return LedgerPage();
                      })).then((value) {
                        saleCollection = 0.0;
                        purchaseCollection = 0.0;
                        receiptCollection = 0.0;
                        cashCollection = 0.0;
                        outstandingReceivable = 0.0;
                        outstandingPayable = 0.0;
                        this.getJsonData().then((value) {
                          this.getAllTime();
                        });
                      });
                      // Navigator.of(context)
                      //     .push(MaterialPageRoute(builder: (context) {
                      //   return ExpensesPage();
                      // })).then((value) {
                      //   saleCollection = 0.0;
                      //   purchaseCollection = 0.0;
                      //   receiptCollection = 0.0;
                      //   cashCollection = 0.0;
                      //   outstandingReceivable = 0.0;
                      //   outstandingPayable = 0.0;
                      //   this.getJsonData();
                      // });
                    },
                    child: Card(
                      elevation: 5.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0)),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Icon(
                               Icons.compare_arrows,
                              size: 30,
                              color: Colors.lightBlue[700],
                            )),
                          ),
                          new Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.01),
                            child: new Center(
                                child: Text(
                              "Ledger",
                              style: new TextStyle(
                                  color: Colors.black, fontSize: 20.0),
                            )),
                          ),
                        ],
                      ),
                    ))
              ],
            ),
          )
        ],
      ),
    );
  }
}
