import 'dart:js';

import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:smit_sp/pages/Customer.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class ExpensesPage extends StatefulWidget {
  final String? custId;
  ExpensesPage({this.custId});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ExpensesPage();
  }
  
}

class _ExpensesPage extends State<ExpensesPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  GlobalKey<AutoCompleteTextFieldState<Customer>> key = new GlobalKey();
  TextEditingController? _dateController, _custController,_contactController;
  DateTime? _dateTime;
  List? expensesCustomer, temp;
  SharedPreferences? prefs ;
   var _modeOfPayment = ['Cash', 'Cheque', 'Online'];
   String? _currentItemSelected, amount, custName, reason, description;

   Future<String> insertExpenses() async {
     var empId = prefs!.getString('empId');
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertExpenses";
    print(url);
    
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        
        body: {
          'empId':empId,
          'custId': temp!.isEmpty?expensesCustomer![0]["cid"].toString(): temp![0]["cid"].toString(),
          'custName': _custController!.text,
          'amount': amount.toString(),
          'reason': reason,
          'description': description,
          'payment_mode': _currentItemSelected,
          'date': _dateController!.text,
        });

    return "success";
  }

   Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

    waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

   

  Future getEmpId() async{
    prefs = await SharedPreferences.getInstance();
  }

  void onChange(){
    setState(() {
      
    });
  }

  Future<String> insertCustomer() async {
    String url=
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertCustomer&";
    
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'custName': _custController!.text,
          'firm_name': '-',
          'address': "-",
          'contact': _contactController!.text,
          'email': '-',
          'cst': '-',
          'gst': '-',
          'state':'0'
        }).then((value) {
      print(value.statusCode);
    });
    

    //print(response.reasonPhrase);

    return "success";
  }

  Future<String> getCust() async {
    String url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=findCustomer&contact=" + _contactController!.text + "&custName="+ _custController!.text;
    
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      expensesCustomer = convertDataToJson['data'];
    });

    return "success";
  }

   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEmpId();
    DateTime now = DateTime.now();
    String today = DateFormat("dd/MM/yyyy").format(now);
    
    _custController = TextEditingController(text: "");
    _contactController = TextEditingController(text: "");
    CustomerViewModel.loadCategories();
    _dateController = TextEditingController(text: today);
    
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Expenses"),
        backgroundColor: Colors.lightBlue[700],
      ),

      body: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.width * 0.01,
                            right: MediaQuery.of(context).size.width * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01),
                        width: MediaQuery.of(context).size.width * 0.92,
                        child: DateTimeField(
                            format: DateFormat('dd/MM/yyy'),
                            initialValue: DateTime.now(),
                            controller: _dateController,
                            decoration: InputDecoration(
                              labelText: "Date",
                              prefixIcon: Container(
                                  margin: EdgeInsets.only(right: 20, top: 20),
                                  child: Icon(
                                    Icons.calendar_today,
                                  )),
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            onShowPicker: (context, currentValue) {
                              return showDatePicker(
                                  context: context,
                                  initialDate: _dateTime == null
                                      ? DateTime.now()
                                      : _dateTime!.toUtc(),
                                  firstDate: DateTime(2001),
                                  lastDate: DateTime(2100));
                            },
                            validator: (val) {
                              if (val != null) {
                                return null;
                              } else {
                                return 'Date Field is Empty';
                              }
                            })),
                    Container(
                              width: MediaQuery.of(context).size.width * 0.92,
                              child: AutoCompleteTextField(
                                  itemSubmitted: (item) {
                                    temp!.clear();
                                    temp!.insert(0, {"cid":item.cid});

                                    print("Submit" + temp.toString());
                                    onChange();
                                    _custController!.text =
                                        item.cust_name.toString();
                                        _contactController!.text = item.contact == null
                                            ? ""
                                            : item.contact.toString();
                                  
                                  },
                                  clearOnSubmit: false,
                                  key: key,
                                  decoration: InputDecoration(
                                      labelText: "Customer Name",
                                      contentPadding: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.015),
                                      suffixIcon: Container(
                                        padding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.03,
                                            left: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.07),
                                        child:
                                            Icon(Icons.perm_contact_calendar),
                                      )),
                                  suggestions: CustomerViewModel.customer,
                                  controller: _custController,
                                  itemBuilder: (context, item) {
                                    //print(item.cust_name.toString());
                                    return Container(
                                      padding: EdgeInsets.all(10),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            child: Text(
                                              item.cust_name.toString(),
                                              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          Container(
                                            child: Text(
                                            item.firm_name == null ? "" :  item.firm_name.toString(),
                                              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Colors.grey),
                                            ),
                                          )
                                        ],
                                      ),
                                    );
                                  },
                                  itemSorter: (a, b) =>
                                      a.cust_name.compareTo(b.cust_name),
                                  itemFilter: (suggestion, input) {
                                    return suggestion.cust_name
                                        .toLowerCase()
                                        .startsWith(input.toLowerCase());
                                  }),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  //top: MediaQuery.of(context).size.height * 0.02,
                                  left:
                                      MediaQuery.of(context).size.width * 0.04,
                                  right:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.92,
                                    child: TextFormField(
                                      autofocus: false,
                                  
                                      controller: _contactController,
                                      // initialValue: widget.salePurchase == 0
                                      //     ? widget.getData == 0
                                      //         ? customerInfo[widget.index]["contact"].toString()
                                      //         : indCustomer == null ? "" : indCustomer[0]["contact"].toString()
                                      //     : widget.getData == 0
                                      //         ? customerInfo[widget.index]
                                      //             ["contactno"].toString()
                                      //         : indCustomer == null ? "": indCustomer[0]["contactno"].toString(),
                                      decoration: InputDecoration(
                                          labelText: "Contact No",
                                          contentPadding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.015),
                                          suffixIcon: Container(
                                            padding: EdgeInsets.only(
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.03,
                                                left: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.07),
                                            child: Icon(
                                                Icons.perm_contact_calendar),
                                          )),
                                          inputFormatters: [
                              LengthLimitingTextInputFormatter(10),
                              new WhitelistingTextInputFormatter(
                                  RegExp("[0-9]"))
                            ],
                                      keyboardType: TextInputType.numberWithOptions(decimal: true,signed: false),
                                      style: new TextStyle(
                                        fontFamily: "Poppins",
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                    Container(
                      padding: EdgeInsets.only(
                          //top: MediaQuery.of(context).size.height * 0.02,
                          left: MediaQuery.of(context).size.width * 0.04,
                          right: MediaQuery.of(context).size.width * 0.04),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.92,
                            child: TextFormField(
                              autofocus: false,
                              textAlignVertical: TextAlignVertical.bottom,
                              onSaved: (value){
                                setState(() {
                                  reason = value;
                                });
                              },
                     validator: (value){
                                if (value == "") {
                                  return "Please enter reason";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                labelText: "Reason",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                              ),
                              keyboardType: TextInputType.text,
                              style: new TextStyle(
                                fontFamily: "Poppins",
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                   Container(
                      padding: EdgeInsets.only(
                          //top: MediaQuery.of(context).size.height * 0.02,
                          left: MediaQuery.of(context).size.width * 0.04,
                          right: MediaQuery.of(context).size.width * 0.04),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.92,
                            child: TextFormField(
                              autofocus: false,
                              textAlignVertical: TextAlignVertical.bottom,
                              onSaved: (value){
                                setState(() {
                                  description = value;
                                });
                              },
                     validator: (value){
                                if (value == "") {
                                  return "Please enter description";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                labelText: "Description",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                              ),
                              keyboardType: TextInputType.text,
                              style: new TextStyle(
                                fontFamily: "Poppins",
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          //top: MediaQuery.of(context).size.height * 0.02,
                          left: MediaQuery.of(context).size.width * 0.04,
                          right: MediaQuery.of(context).size.width * 0.04),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.92,
                            child: TextFormField(
                              autofocus: false,
                              inputFormatters: [WhitelistingTextInputFormatter( RegExp(r"^\d*\.?\d*"))],
                              validator: (value) {
                                if (value == "") {
                                  return "Please Enter amount";
                                } else {
                                  return null;
                                }
                              },
                              onSaved: (value) {
                                setState(() {
                                  amount = value;
                                });
                              },
                              decoration: InputDecoration(
                                labelText: "₹ Amount",
                                prefixText: "",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                              ),
                              keyboardType: TextInputType.numberWithOptions(decimal: true,signed: false),
                              style: new TextStyle(
                                fontFamily: "Poppins",
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          //top: MediaQuery.of(context).size.height * 0.02,
                          left: MediaQuery.of(context).size.width * 0.04,
                          right: MediaQuery.of(context).size.width * 0.04),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.01,
                              right: MediaQuery.of(context).size.width * 0.01,
                            ),
                            child: Container(
                              height: 35,
                              width: MediaQuery.of(context).size.width * 0.90,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                border: Border.all(
                                    color: Colors.grey.shade700,
                                    style: BorderStyle.solid,
                                    width: 1.0),
                              ),
                              child: DropdownButtonHideUnderline(
                                child: ButtonTheme(
                                  alignedDropdown: true,
                                  child: DropdownButton<String>(
                                    isDense: true,
                                    autofocus: true,
                                    focusColor: Colors.lightBlue[900],
                                    //style: Theme.of(context).textTheme.title,
                                    isExpanded: true,
                                    hint: Text("Mode of Payment"),
                                    items: _modeOfPayment
                                        .map((String dropDownStringItem) {
                                      return DropdownMenuItem<String>(
                                          value: dropDownStringItem,
                                          child: Text(dropDownStringItem,
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w500,
                                                  color: Colors.black)));
                                    }).toList(),
                                    onChanged: (String? newValueSelected) {
                                      setState(() {
                                        _currentItemSelected = newValueSelected;
                                      });
                                    },
                                    value: _currentItemSelected,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
            Container(
              height: 50,
              width: MediaQuery.of(context).size.width * 1,
              color: Colors.lightBlue[700],
              child: FlatButton(
                  onPressed: () {
                    
                    if (_formKey.currentState!.validate() &&
                        _currentItemSelected != null) {
                          waiting(context);
                      _formKey.currentState!.save();

                      if(temp!.isEmpty){
                        insertCustomer().then((value){
                          insertExpenses().then((value){
                        Navigator.of(context).pop();
                        _formKey.currentState!.reset();
                        _currentItemSelected = null;
                        Navigator.of(context).pop();
                        _showDialogBox(context, "Successfully Saved in Expenses","");
                      });
                        
                      });
                      } else {
                      insertExpenses().then((value){
                        Navigator.of(context).pop();
                        _formKey.currentState!.reset();
                        _currentItemSelected = null;
                        Navigator.of(context).pop();
                        _showDialogBox(context, "Successfully Saved in Expenses","");
                      });
                      }
                      

                    } else {

                      
                      if(_currentItemSelected == null){
                        _showDialogBox(context, "Warning","Please Choose Option");
                      }
                      
                    }
                  },
                  child: Text(
                    "Save",
                    style: TextStyle(color: Colors.white),
                  )),
            )
          ],
        ),
      ),
    );
  }
  
}