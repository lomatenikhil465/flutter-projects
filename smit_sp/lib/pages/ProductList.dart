import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class ProductListPage extends StatefulWidget {
 
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProductListPage();
  }
}

List? produtsData;

class _ProductListPage extends State<ProductListPage> {
  Future<String>? productResponse;

  Future<String> getJsonData() async {
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=products";
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      produtsData = convertDataToJson['data'];
    });

    return "success";
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    productResponse = this.getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Existing Product"),
          backgroundColor: Colors.lightBlue[700],
        ),
       
        body: ListView.builder(
          
          itemCount: produtsData == null ? 0 : produtsData!.length,
          itemBuilder: (BuildContext context, int index) {
            return GestureDetector(
              onTap: () {
               
              },
              child: Container(
                child: Card(
                  child: Row(children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                                          child: Container(
                        
                        height: 75,
                        width: 60,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10.0),
                            border: Border.all(color: Colors.grey, width: 1)),
                        child: Image.network(
                          "https://qot.constructionsmall.com/" +
                              produtsData![index]["img"],
                          loadingBuilder: (context, child, progress) {
                            return progress == null
                                ? child
                                : LinearProgressIndicator();
                          },
                          fit: BoxFit.scaleDown,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.01,
                                    top: MediaQuery.of(context).size.height *
                                        0.01),
                                child: Text(
                                    produtsData![index]["newitem"].toString()),
                              )
                            ],
                          ),
                          Row(
                            
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.01,
                                    top: MediaQuery.of(context).size.height *
                                        0.01),
                                child: Text(
                                 "GST : " +
                                              produtsData![index]["gst_per"] 
                                                  .toString() + " %"
                                         ,
                                  style: TextStyle(color: Colors.grey),
                                ),
                              )
                            ],
                          ),
                          Row(
                            
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.01,
                                    top: MediaQuery.of(context).size.height *
                                        0.01),
                                child: Text(
                                 "Sale Rate : ₹ " +
                                              produtsData![index]["sale_rate"]
                                                  .toString()
                                         ,
                                  style: TextStyle(color: Colors.grey),
                                ),
                              )
                            ],
                          ),
                          Row(
                            
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.01,
                                    top: MediaQuery.of(context).size.height *
                                        0.01),
                                child: Text(
                                 "Purchase rate : ₹ " +
                                              produtsData![index]["purchase_rate"]
                                                  .toString()
                                         ,
                                  style: TextStyle(color: Colors.grey),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ]),
                ),
              ),
            );
          },
        ));
  }
}
