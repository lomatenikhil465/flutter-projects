import 'dart:js';

import 'package:smit_sp/pages/InvoiceDetails.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

class MapInvoiceList extends StatefulWidget {
  final String? custId, date,bkId;
  final int? salePurchase;
  MapInvoiceList({this.custId, this.salePurchase, this.date, this.bkId});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MapInvoiceList();
  }
}

class _MapInvoiceList extends State<MapInvoiceList> {
  List? invoiceList;
  Future<String>? invoiceListResponse;
  SharedPreferences? prefs;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();

  Future<String> getJsonData() async {
    prefs = await SharedPreferences.getInstance();
    String url;
    if (widget.salePurchase == 0) {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=customerInvoice&custId=" +
              widget.custId.toString();
    } else {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=customerInvoicePurchase&custId=" +
              widget.custId.toString();
    }

    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      invoiceList = convertDataToJson['data'];
    });

    return "success";
  }

  Future<String> mapInvoicePurchase(int index) async {
    var empId = prefs!.getString('empId');
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=bahiKhataMapping-Purchase";
    print(url);
    
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'bhaikhataId': widget.bkId,
          'dt': widget.date,
          'custId': widget.custId.toString(),
          'uid': empId,
          'bill_no': invoiceList![index]["bill_no"] 
          
        });

    return "success";
  }

  Future<String> mapInvoiceSale(int index) async {
    var empId = prefs!.getString('empId');
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=bahiKhataMapping-Sale";
    print(url);
    
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'bhaikhataId': widget.bkId,
          'dt': widget.date,
          'custId': widget.custId.toString(),
          'uid': empId,
          'bill_no': invoiceList![index]["bill_no"] 
          
        });

    return "success";
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    invoiceListResponse = getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Invoice List"),
        backgroundColor: Colors.lightBlue[700],
      ),
      body: FutureBuilder<String>(
        future: invoiceListResponse,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
                child: ListView.separated(
              separatorBuilder: (BuildContext context, int index) => Divider(
                thickness: 0.0,
                color: Colors.lightBlue[900],
              ),
              itemCount: invoiceList == null ? 0 : invoiceList!.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return InvoiceDetails(
                        custId: widget.custId.toString(),
                        billNo: invoiceList![index]["bill_no"],
                        custName: invoiceList![index]["cust_name"],
                        salePurchase: widget.salePurchase,
                      );
                    }));
                  },
                  child: Container(
                    child: Card(
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.01,
                                    top: MediaQuery.of(context).size.height *
                                        0.01),
                                child: Text(
                                  invoiceList![index]["cust_name"].toString(),
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.lightBlue[700]),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.01,
                                    top: MediaQuery.of(context).size.height *
                                        0.01),
                                child: Text(
                                  widget.salePurchase == 0
                                  ? "Sale"
                                  :"Purchase",
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.lightBlue[700]),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.01,
                                    top: MediaQuery.of(context).size.height *
                                        0.01),
                                child: Text(
                                  invoiceList![index]["grand_total"].toString() +
                                      "₹",
                                ),
                              )
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.01,
                                    top: MediaQuery.of(context).size.height *
                                        0.01),
                                child: Text(
                                  invoiceList![index]["bill_no"].toString(),
                                ),
                              )
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                        left:
                                            MediaQuery.of(context).size.width *
                                                0.01,
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      widget.salePurchase == 0? invoiceList![index]['dt'].toString()
                                     : invoiceList![index]["date"].toString(),
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ),
                                ],
                              ),
                              Spacer(),
                              invoiceList![index]["current_balance"] != "0" ? Container(
                               
                                child: RaisedButton(onPressed: (){
                                  waiting(context);
                                  if(widget.salePurchase == 0){
                                    mapInvoiceSale(index).then((value){
                                      Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                    });
                                  }else{
                                  mapInvoicePurchase(index).then((value){
                                    Navigator.of(context).pop();
                                    Navigator.of(context).pop();
                                  });
                                  }
                                },
                                                child: Text("MAP"),
                                                textColor: Colors.white,
                                                color: Colors.lightBlue[700],
                                                ),
                              ): Container()
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                );
              },
            ));
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
