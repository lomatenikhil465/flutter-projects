import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfilePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProfilePage();
  }
}

class _ProfilePage extends State<ProfilePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  List? profileData;
   bool _autoValidate = false;
  Future<String>? profileResponse;
  SharedPreferences? prefs;
  TextEditingController? _nameController, _emailController, _mobileController, _addressController;
  File? _image;
  bool? _status ;
  final picker = ImagePicker();

  Future getImage() async {
    var image = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      _image = File(image.path);
      print('Image Path $_image');
    });
  }

  Future uploadPic(BuildContext context) async {
    setState(() {
      print("Profile Picture uploaded");
      Scaffold.of(context)
          .showSnackBar(SnackBar(content: Text('Profile Picture Uploaded')));
    });
  }
  Future<String> uploadImage(filename, url) async {
    var request = http.MultipartRequest('POST', Uri.parse(url));
    request.files.add(await http.MultipartFile.fromPath('picture', filename));
    var res = await request.send();
    print(res.statusCode);
    return res.reasonPhrase;
  }

  Future<String> getJsonData() async {
    var empId = prefs!.getString('empId');
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=profile&id=" + empId;

    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      profileData = convertDataToJson['data'];
      _nameController!.text = profileData![0]["user_name"] == null ? "" :profileData![0]["user_name"];
      _emailController!.text = profileData![0]["email"] == null ? "" :profileData![0]["email"];
      _mobileController!.text = profileData![0]["contact"]== null ? "" :profileData![0]["contact"];
      _addressController!.text = profileData![0]["address"] == null ? "" :profileData![0]["address"];
    });

    return "success";
  }

  waiting(BuildContext context) {
    return showDialog(
      
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
                  child: AlertDialog(
            key: _keyLoader,
              content: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                child: CircularProgressIndicator(),
              ),
              Container(
                child: Text("Loading..."),
              )
            ],
          )),
        );
        
      },
      
    );
  }

  void _validateInputs() {
    //checkConnection();
    if (_formKey.currentState!.validate() ) {
      waiting(context);
//    If all data are correct then save data to out variables
      _formKey.currentState!.save();
      getJsonData().then((value) {
        Navigator.of(context).pop();
        
      });
      _formKey.currentState!.reset();
    } else {
//    If all data are not valid then start auto validation.

      setState(() {
        _autoValidate = true;
      });
    }
  }

  Widget _getActionButtons() {
    return Padding(
      padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 45.0),
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Container(
                  child: new RaisedButton(
                child: new Text("Save"),
                textColor: Colors.white,
                color: Colors.green,
                onPressed: () {
                  _validateInputs();
                  setState(() {
                    
                    _status = true;
                    FocusScope.of(context).requestFocus(new FocusNode());
                  });
                },
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0)),
              )),
            ),
            flex: 2,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Container(
                  child: new RaisedButton(
                child: new Text("Cancel"),
                textColor: Colors.white,
                color: Colors.red,
                onPressed: () {
                  setState(() {
                    _status = true;
                    FocusScope.of(context).requestFocus(new FocusNode());
                  });
                },
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0)),
              )),
            ),
            flex: 2,
          ),
        ],
      ),
    );
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _status = true;
    _nameController = TextEditingController(text: "");
    _emailController = TextEditingController(text: "");
    _mobileController = TextEditingController(text: "");
    _addressController = TextEditingController(text: "");
    getEmpId().then((value) {
      profileResponse = this.getJsonData();
    });
    
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
        backgroundColor: Colors.lightBlue[700],
      ),
      body: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: FutureBuilder<String>(
            future: profileResponse,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return Column(
                  children: [
                    Expanded(
                        child: SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.03),
                            child: Stack(
                              fit: StackFit.loose,
                              children: [
                                Align(
                                  alignment: Alignment.center,
                                  child: Container(
                                    // padding: EdgeInsets.only(
                                    //     left: MediaQuery.of(context).size.width * 0.03),
                                    child: CircleAvatar(
                                      radius: 70,
                                      backgroundColor: Colors.white,
                                      child: ClipOval(
                                        child: new SizedBox(
                                          width: MediaQuery.of(context).size.width * 0.60,
                                          height: MediaQuery.of(context).size.height * 0.60,
                                          child: (_image != null)
                                              ? Image.file(
                                                  _image!.absolute,
                                                  fit: BoxFit.cover
                                                )
                                              : Image.asset(
                                                  "assets\\images\\as.png",
                                                  fit: BoxFit.fill,
                                                ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                // Align(
                                //   alignment: Alignment.center,
                                //   child: Padding(
                                //     padding: EdgeInsets.only(
                                //         top:
                                //             MediaQuery.of(context).size.height *
                                //                 0.12,
                                //         left:
                                //             MediaQuery.of(context).size.width *
                                //                 0.20),
                                //     child: IconButton(
                                //       icon: Icon(
                                //         Icons.photo_camera,
                                //         size: 30.0,
                                //         color: Colors.black54,
                                //       ),
                                //       onPressed: ()  {
                                //         getImage().then((value) {
                                //           var res = uploadImage(_image.path, "https://qot.constructionsmall.com/app/getquerycode.php?apicall=changeProPic");
                                //           print(res);
                                //         });
                                //       },
                                //     ),
                                //   ),
                                // )
                              ],
                            ),
                          ),
                          Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.05,
                                left: MediaQuery.of(context).size.width * 0.05),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  child: Text(
                                    "Personal Information",
                                    style: TextStyle(
                                        fontSize: 18.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                               _status == true ? Container(
                                  padding:EdgeInsets.only(
                                
                                right: MediaQuery.of(context).size.width * 0.05),
                                  child: GestureDetector(
                                    child: new CircleAvatar(
                                      backgroundColor: Colors.red,
                                      radius: 16.0,
                                      child: new Icon(
                                        Icons.edit,
                                        color: Colors.white,
                                        size: 18.0,
                                      ),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        _status = false;
                                      });
                                    },
                                  ),
                                ):Container()
                              ],
                            ),
                          ),
                           Padding(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.03),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Text(
                                    'Name',
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          )),
                          Padding(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                          
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Flexible(
                                child: new TextFormField(
                                  controller: _nameController,
                                  keyboardType: TextInputType.text,
                                  decoration: const InputDecoration(
                                    hintText: "Name",
                                  ),
                                  enabled: _status == true ? false : true,
                                  autofocus: false,

                                ),
                              ),
                            ],
                          )),
                           Padding(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.03),
                          
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Text(
                                    'Email ID',
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          )),
                          Padding(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                          
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Flexible(
                                child: new TextFormField(
                                  controller: _emailController,
                                  keyboardType: TextInputType.emailAddress,
                                  decoration: const InputDecoration(
                                      hintText: "Email ID"),
                                  enabled: _status == true ? false : true,
                                ),
                              ),
                            ],
                          )),
                           Padding(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.03),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Text(
                                    'Mobile',
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          )),
                           Padding(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Flexible(
                                child: new TextFormField(
                                  controller: _mobileController,
                                  keyboardType: TextInputType.numberWithOptions(decimal: true, signed: false),
                                  decoration: const InputDecoration(
                                      hintText: "Mobile Number"),
                                  enabled:  _status == true ? false : true,
                                ),
                              ),
                            ],
                          )),
                          Padding(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height * 0.03),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  new Text(
                                    'Address',
                                    style: TextStyle(
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            ],
                          )),
                           Padding(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.05, right: MediaQuery.of(context).size.width * 0.05),
                          child: new Row(
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              new Flexible(
                                child: new TextFormField(
                                  controller: _addressController,
                                  keyboardType: TextInputType.text,
                                  decoration: const InputDecoration(
                                      hintText: "Address"),
                                  enabled: _status == true ? false : true,
                                ),
                              ),
                            ],
                          )),
                           _status == false ? _getActionButtons() : new Container(),
                        ],
                      ),
                    )),
                    
                  ],
                );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          )),
    );
  }
}
