import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class DayClose extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DayClose();
  }
}

class _DayClose extends State<DayClose> {
  TextEditingController _fromDateController, _toDateController;
  DateTime _dateTime;
  List dayCloseData;
  SharedPreferences prefs;
  int customerVendor = 0;
  Future<String> dayCloseResponse;
  double saleCollection = 0.0,
      purchaseCollection = 0.0,
      receiptCollection = 0.0,
      cashCollection = 0.0,
      expensesCollection = 0.0,
      totalCashInHand = 0.0;

  Future<String> getJsonData() async {
    var empId = prefs.getString('empId');
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=saledayclose&fdt=" +
            _fromDateController.text +
            "&tdt=" +
            _toDateController.text +
            "&uid=" +
            empId;
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      dayCloseData = convertDataToJson['data'];
    });
    if (dayCloseData != null) {
      for (var i = 0; i < dayCloseData.length; i++) {
        if (dayCloseData[i]["eType"] == "SALE") {
          saleCollection =
              saleCollection + double.parse(dayCloseData[i]["grand_total"]);
        }

        if (dayCloseData[i]["eType"] == "RCPT") {
          receiptCollection =
              receiptCollection + double.parse(dayCloseData[i]["cr"]);
        }

        if (dayCloseData[i]["eType"] == "ON ACCOUNT" && dayCloseData[i]["cr"] != "0") {
          receiptCollection =
              receiptCollection + double.parse(dayCloseData[i]["cr"]);
        } else if(dayCloseData[i]["eType"] == "ON ACCOUNT" && dayCloseData[i]["dr"] != "0"){
           receiptCollection =
              receiptCollection + double.parse(dayCloseData[i]["dr"]);
        }

        if (dayCloseData[i]["eType"] == "PURCHASE") {
          purchaseCollection =
              purchaseCollection + double.parse(dayCloseData[i]["grand_total"]);
        }

        if (dayCloseData[i]["eType"] == "VCH") {
          cashCollection = cashCollection + double.parse(dayCloseData[i]["dr"]);
        }

        if (dayCloseData[i]["eType"] == "EXPENSES") {
          expensesCollection =
              expensesCollection + double.parse(dayCloseData[i]["dr"]);
        }
      }

      totalCashInHand = receiptCollection - cashCollection - expensesCollection;
    }
    return "success";
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    DateTime now = DateTime.now();
    String today = DateFormat("yyyy-MM-dd").format(now);
    _fromDateController = TextEditingController(text: today);
    _toDateController = TextEditingController(text: today);
    getEmpId().then((value) {
      dayCloseResponse = this.getJsonData();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Day Close"),
        backgroundColor: Colors.lightBlue[700],
      ),
      body: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                child: Text(
                  " From : ",
                  style: TextStyle(fontSize: 16),
                ),
              ),
              Container(
                  height: 50,
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.01),
                  width: MediaQuery.of(context).size.width * 0.35,
                  child: DateTimeField(
                      format: DateFormat('yyyy-MM-dd'),
                      style: TextStyle(fontSize: 15),
                      onChanged: (value) {
                        saleCollection = 0.0;
                        purchaseCollection = 0.0;
                        receiptCollection = 0.0;
                        cashCollection = 0.0;
                        expensesCollection = 0.0;
                        totalCashInHand = 0.0;
                        this.getJsonData();
                      },
                      initialValue: DateTime.now(),
                      controller: _fromDateController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                          prefixIcon: Icon(Icons.calendar_today)),
                      resetIcon: null,
                      onShowPicker: (context, currentValue) {
                        return showDatePicker(
                            context: context,
                            initialDate:
                                _dateTime == null ? DateTime.now() : _dateTime,
                            firstDate: DateTime(2001),
                            lastDate: DateTime(2025));
                      },
                      validator: (val) {
                        if (val != null) {
                          return null;
                        } else {
                          return 'Date Field is Empty';
                        }
                      })),
              Spacer(),
              Container(
                child: Text(
                  "To : ",
                  style: TextStyle(fontSize: 16),
                ),
              ),
              Container(
                  height: 50,
                  padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.01,
                  ),
                  width: MediaQuery.of(context).size.width * 0.35,
                  child: DateTimeField(
                      format: DateFormat('yyyy-MM-dd'),
                      onChanged: (value) {
                        saleCollection = 0.0;
                        purchaseCollection = 0.0;
                        receiptCollection = 0.0;
                        cashCollection = 0.0;
                        expensesCollection = 0.0;
                        totalCashInHand = 0.0;
                        this.getJsonData();
                      },
                      style: TextStyle(fontSize: 15),
                      initialValue: DateTime.now(),
                      controller: _toDateController,
                      decoration: InputDecoration(
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                          prefixIcon: Icon(Icons.calendar_today)),
                      resetIcon: null,
                      onShowPicker: (context, currentValue) {
                        return showDatePicker(
                            context: context,
                            initialDate:
                                _dateTime == null ? DateTime.now() : _dateTime,
                            firstDate: DateTime(2001),
                            lastDate: DateTime(2025));
                      },
                      validator: (val) {
                        if (val != null) {
                          return null;
                        } else {
                          return 'Date Field is Empty';
                        }
                      }))
            ],
          ),
          Card(
            elevation: 3.0,
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
            child: Container(
              
              padding: EdgeInsets.all(10),
              height: MediaQuery.of(context).size.height * 0.20,
              width: MediaQuery.of(context).size.width  * 1,
              child: DataTable(
                dataRowHeight: MediaQuery.of(context).size.height * 0.03,
                dividerThickness: 0,

                headingRowHeight: MediaQuery.of(context).size.height * 0.0,
                columns: [
                  DataColumn(label: Text("")),
                  DataColumn(label: Text("",))
                ], 
                rows: [
                  DataRow(cells: [
                    DataCell(Text("Total Sale",style: TextStyle(fontSize: 13))),
                    DataCell(Text("₹ " + saleCollection.toStringAsFixed(2),style: TextStyle(fontSize: 13, color: Colors.green[700], fontWeight: FontWeight.bold)))
                  ]),
                  DataRow(cells: [
                    DataCell(Text("Total Purchase ",style: TextStyle(fontSize: 13))),
                    DataCell(Text("₹ " + purchaseCollection.toStringAsFixed(2),style: TextStyle(fontSize: 13, color: Colors.green[700], fontWeight: FontWeight.bold)))
                  ]),
                  DataRow(cells: [
                    DataCell(Text("Total Receipt ",style: TextStyle(fontSize: 13))),
                    DataCell(Text("₹ " + receiptCollection.toStringAsFixed(2),style: TextStyle(fontSize: 13, color: Colors.green[700], fontWeight: FontWeight.bold)))
                  ]),
                  DataRow(cells: [
                    DataCell(Text("Total Payment ",style: TextStyle(fontSize: 13))),
                    DataCell(Text("₹ " + cashCollection.toStringAsFixed(2),style: TextStyle(fontSize: 13, color: Colors.deepOrange, fontWeight: FontWeight.bold)))
                  ]),
                  DataRow(cells: [
                    DataCell(Text("Total Expenses ",style: TextStyle(fontSize: 13))),
                    DataCell(Text("₹ " + expensesCollection.toStringAsFixed(2),style: TextStyle(fontSize: 13, color: Colors.deepOrange, fontWeight: FontWeight.bold)))
                  ]),
                  DataRow(cells: [
                    DataCell(Text("Total Cash in Hand",style: TextStyle(fontSize: 13))),
                    DataCell(Text("₹ " + totalCashInHand.toStringAsFixed(2),style: TextStyle(fontSize: 13, color: Colors.green[700], fontWeight: FontWeight.bold)))
                  ])
                ]
              )
            ),
          ),
          Container(
            child: Expanded(
                child: dayCloseData == null
                    ? Center(
                        child: Text("No Data Available..."),
                      )
                    : FutureBuilder<String>(
                        future: dayCloseResponse,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return ListView.separated(
                              separatorBuilder:
                                  (BuildContext context, int index) => Divider(
                                thickness: 0.0,
                                color: Colors.lightBlue[700],
                              ),
                              itemCount: dayCloseData == null
                                  ? 0
                                  : dayCloseData.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  child: Card(
                                    elevation: 0.0,
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.01,
                                                  top: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.01),
                                              child: Text(
                                                dayCloseData[index]["eType"] ==
                                                        'SALE'
                                                    ? "Sale"
                                                    : (dayCloseData[index]
                                                                ["eType"] ==
                                                            'RCPT'
                                                        ? "Sale Receipt to bill " +
                                                            dayCloseData[index]
                                                                ["bill_no"] +
                                                            " by " +
                                                            dayCloseData[index]
                                                                ["pMode"]
                                                        : dayCloseData[index]
                                                                    ["eType"] ==
                                                                'PURCHASE'
                                                            ? "Purchase"
                                                            : dayCloseData[index]["eType"] ==
                                                                    'VCH'
                                                                ? "Purchase Receipt to bill " +
                                                                    dayCloseData[index]
                                                                        [
                                                                        "bill_no"] +
                                                                    " by " +
                                                                    dayCloseData[index]
                                                                        [
                                                                        "pMode"]
                                                                : dayCloseData[index]
                                                                            ["eType"] ==
                                                                        'EXPENSES'
                                                                    ? "Expenses"
                                                                    : "Bahi Khata"),
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                  top: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.01),
                                              child: Text(
                                                dayCloseData[index]["eType"] ==
                                                        'SALE'
                                                    ? dayCloseData[index]
                                                        ["bill_no"]
                                                    : (dayCloseData[index]
                                                                ["eType"] ==
                                                            'RCPT'
                                                        ? dayCloseData[index]
                                                            ["rptNo"]
                                                        : dayCloseData[index]
                                                                    ["eType"] ==
                                                                'PURCHASE'
                                                            ? dayCloseData[index]
                                                                ["bill_no"]
                                                            : dayCloseData[index]
                                                                        [
                                                                        "eType"] ==
                                                                    'VCH'
                                                                ? dayCloseData[
                                                                        index]
                                                                    ["rptNo"]
                                                                : dayCloseData[index]
                                                                            ["eType"] ==
                                                                        'EXPENSES'
                                                                    ? ""
                                                                    : ""),
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.01,
                                                  top: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.01),
                                              child: Text(
                                                dayCloseData[index]["cust_name"]
                                                    .toString(),
                                                style: TextStyle(
                                                    fontWeight:
                                                        FontWeight.w500),
                                              ),
                                            )
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.01,
                                                  top: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.01),
                                              child: Text(
                                                dayCloseData[index]["dt"],
                                                style: TextStyle(
                                                    color: Colors.grey[700]),
                                              ),
                                            ),
                                            Container(
                                                padding: EdgeInsets.only(
                                                    right:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .width *
                                                            0.01,
                                                    top: MediaQuery.of(context)
                                                            .size
                                                            .height *
                                                        0.01),
                                                child: Text(
                                                  dayCloseData[index]["eType"] == 'SALE'
                                                      ? "Total : ₹ " +
                                                          dayCloseData[index]
                                                              ["grand_total"]
                                                      : (dayCloseData[index]
                                                                  ["eType"] ==
                                                              'RCPT'
                                                          ? (dayCloseData[index]
                                                                      ["dr"] !=
                                                                  "0"
                                                              ? "Paid : ₹ " +
                                                                  dayCloseData[index]
                                                                      ["dr"]
                                                              : "Received : ₹ " +
                                                                  dayCloseData[index]
                                                                      ["cr"])
                                                          : dayCloseData[index]["eType"] ==
                                                                  'PURCHASE'
                                                              ? "Total : ₹ " +
                                                                  dayCloseData[index]
                                                                      [
                                                                      "grand_total"]
                                                              : dayCloseData[index]["eType"] == 'VCH'
                                                                  ? (dayCloseData[index]["dr"] != "0"
                                                                      ? "Paid : ₹ " + dayCloseData[index]["dr"]
                                                                      : "Received : ₹ " + dayCloseData[index]["cr"])
                                                                  : dayCloseData[index]["eType"] == 'EXPENSES' ? "Paid : ₹ " + dayCloseData[index]["dr"] : (dayCloseData[index]["dr"] != "0" ? "Paid : ₹ " + dayCloseData[index]["dr"] : "Received : ₹ " + dayCloseData[index]["cr"])),
                                                  style: TextStyle(
                                                    color: dayCloseData[index]
                                                                ["eType"] ==
                                                            'SALE'
                                                        ? Colors.green[600]
                                                        : (dayCloseData[index]
                                                                    ["eType"] ==
                                                                'RCPT'
                                                            ? (dayCloseData[index]["dr"] != "0"
                                                                ? Colors
                                                                    .deepOrange
                                                                : Colors
                                                                    .green[600])
                                                            : dayCloseData[index]["eType"] ==
                                                                    'PURCHASE'
                                                                ? Colors
                                                                    .green[600]
                                                                : dayCloseData[index]["eType"] == 'VCH'
                                                                    ? (dayCloseData[index]["dr"] != "0"
                                                                        ? Colors
                                                                            .deepOrange
                                                                        : Colors.green[
                                                                            600])
                                                                    : dayCloseData[index]["eType"] == 'EXPENSES'
                                                                        ? Colors.deepOrange
                                                                        : (dayCloseData[index]["dr"] != "0" ? Colors.deepOrange : Colors.green[600])),
                                                  ),
                                                ))
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              },
                            );
                          } else {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                        },
                      )),
          )
        ],
      ),
    );
  }
}
