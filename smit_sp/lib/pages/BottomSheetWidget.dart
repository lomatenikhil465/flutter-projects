import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'ReceiptPage.dart';

class BottomSheetWidget extends StatefulWidget {

  final String custId;
  final int sale , purchase;
  BottomSheetWidget({ this.custId, this.sale,this.purchase});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BottomSheetWidget();
  }
}

class _BottomSheetWidget extends State<BottomSheetWidget> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: const EdgeInsets.only(top: 5, left: 15, right: 15),
      height: 275,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            
            width: MediaQuery.of(context).size.width * 1,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey[300])
                ]),
            child: Column(
              children: <Widget>[
                Container(
                  height: 50,
                  child: Center(
                    
                    child: Text(
                      "Bahi Khata Receipt",
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
                Divider(thickness: 2,),
                SheetButton(
                  buttonColor: Colors.deepOrange,
                  text: "Pay",
                  payReceive:0,
                  custId: widget.custId,
                  salePurchase: widget.sale,
                  getData: widget.sale == null ? 0 : 1,
                ),
                SheetButton(
                  buttonColor: Colors.green[600],
                  text: "Receive",
                  payReceive:1,
                  custId: widget.custId,
                  salePurchase: widget.sale,
                  getData: widget.sale == null ? 0 : 1,
                ),
               widget.sale != null && widget.purchase != null? SheetButton(
                  buttonColor: Colors.lightBlue[700],
                  text:  "Sale Entry" ,
                  payReceive:null,
                  custId: widget.custId,
                  salePurchase: widget.sale,
                  getData: widget.sale == null ? 0 : 1,
                ) : Container(),
                widget.sale != null && widget.purchase != null? SheetButton(
                  buttonColor: Colors.lightBlue[700],
                  text:  "Purchase Entry" ,
                  payReceive:null,
                  custId: widget.custId,
                  salePurchase: widget.purchase,
                  getData: widget.purchase == null ? 0 : 1,
                ) : Container()
              ],
            ),
          )
        ],
      ),
    );
  }
}
