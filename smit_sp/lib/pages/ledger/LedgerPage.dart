import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import '../CustomerDetails.dart';

class LedgerPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LedgerPage();
  }
}

List nameOfCustomer;
List _searchResult;

class _LedgerPage extends State<LedgerPage> {
  TextEditingController _controller;

  Future<String> customerResponse;

  Future<String> getJsonData() async {
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=customer";
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      nameOfCustomer = convertDataToJson['data'];
    });

    return "success";
  }

 void onSearchTextChanged(String text) async {
    if(_searchResult != null){
      _searchResult.clear();
    }
    if (text.isEmpty) {
      setState(() {});
      //return ;
    }

    nameOfCustomer.forEach((userDetail) {
      print("search result : " +userDetail["cust_name"].toString() );
      if (userDetail["cust_name"].toLowerCase().contains(text.toLowerCase()) ){

        _searchResult.add(userDetail);
      }
    });

    setState(() {});
  
}


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _searchResult = new List();
    customerResponse = getJsonData();
    _controller = TextEditingController(text: "");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Ledger"),
          backgroundColor: Colors.lightBlue[700],
        ),
        body: Column(
          children: <Widget>[
            Container(
              height: 55,
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                onChanged: (value) {
                  onSearchTextChanged(value);
                },
                controller: _controller,
                decoration: InputDecoration(
                    labelText: "Search",
                    hintText: "Search",
                    contentPadding: EdgeInsets.only(top: 10),
                    prefixIcon: Icon(Icons.search),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5.0)))),
              ),
            ),
            Expanded(
              child: FutureBuilder<String>(
                future: customerResponse,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return nameOfCustomer == null
                        ? Center(
                            child: Text("No Data Available"),
                          )
                        : ((_searchResult == null ? false : _searchResult.length != 0) || _controller.text.isNotEmpty ? ListView.separated(
                            separatorBuilder:
                                (BuildContext context, int index) => Divider(
                              thickness: 0.0,
                              color: Colors.lightBlue[900],
                            ),
                            itemCount: _searchResult == null
                                ? 0
                                : _searchResult.length,
                            itemBuilder: (BuildContext context, int index) {
                              return LedgerCustomer(
                                index: index,
                              );
                            },
                          ) :ListView.separated(
                            separatorBuilder:
                                (BuildContext context, int index) => Divider(
                              thickness: 0.0,
                              color: Colors.lightBlue[900],
                            ),
                            itemCount: nameOfCustomer == null
                                ? 0
                                : nameOfCustomer.length,
                            itemBuilder: (BuildContext context, int index) {
                              return LedgerCustomer(
                                index: index,
                              );
                            },
                          ));
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
            ),
          ],
        ));
  }
}

class LedgerCustomer extends StatefulWidget {
  final int index;
  LedgerCustomer({this.index});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LedgerCustomer();
  }
}

class _LedgerCustomer extends State<LedgerCustomer> {
  List customerOutstanding;
  Future<String> customerOutstandingResponse;
  double totalPayment = 0.0, temp = 0.0;

  Future<String> getJsonData() async {
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=AllInLedger&custId=" +
            //"https://qot.constructionsmall.com/app/getquerycode.php?apicall=customerLedger&custId=" +
            ((_searchResult == null ? false : _searchResult.length !=0) ? _searchResult[widget.index]["cid"] :nameOfCustomer[widget.index]["cid"]);
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      customerOutstanding = convertDataToJson['data'];
    });

    if (customerOutstanding != null) {
      for (var i = 0; i < customerOutstanding.length; i++) {
        if (customerOutstanding[i]["lType"] == "s") {
          if (customerOutstanding[i]["c_type"] == 'cr') {
            temp = temp+ double.parse(customerOutstanding[i]["amt"]);
          } 
          if(customerOutstanding[i]["c_type"] == 'dr'){
            if(customerOutstanding[i]["chal_type"] == 'Sale'){
              totalPayment = totalPayment+ (double.parse(customerOutstanding[i]["amt"]) - double.parse(customerOutstanding[i]["pay_amt"]));
            } else {
            temp = temp + double.parse(customerOutstanding[i]["amt"]);
            }
          }
        }

        if (customerOutstanding[i]["lType"] == "p") {
          if (customerOutstanding[i]["c_type"] == 'dr') {
            if(customerOutstanding[i]["bill_no"] == "-" && customerOutstanding[i]["recpt_no"] == "-"){
              totalPayment = totalPayment+ double.parse(customerOutstanding[i]["paid_amt"]);
            } else if(customerOutstanding[i]["bill_no"] != "0" || customerOutstanding[i]["recpt_no"] != "0"){
              
              temp = temp + (double.parse(customerOutstanding[i]["grand_total"]) - double.parse(customerOutstanding[i]["paid_amt"]));
            
            } 
          } 
          if(customerOutstanding[i]["c_type"] == 'cr'){
            totalPayment = totalPayment + double.parse(customerOutstanding[i]["paid_amt"]);
          }
        }
        print(i.toString() + " : " + temp.toString());
      }
    }
    return "success";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    customerOutstandingResponse = this.getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return CustomerDetails(
            name: (_searchResult == null ? false : _searchResult.length !=0 )? _searchResult[widget.index]["cust_name"] :nameOfCustomer[widget.index]["cust_name"],
            custId:(_searchResult == null ? false : _searchResult.length !=0 )? _searchResult[widget.index]["cid"] :nameOfCustomer[widget.index]["cid"],
          );
        }));
      },
      child: Container(
        height: 60,
        child: Card(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.01,
                        top: MediaQuery.of(context).size.height * 0.01),
                    child: Text(
                        ((_searchResult == null ? false : _searchResult.length !=0 ) ? _searchResult[widget.index]["cust_name"] :nameOfCustomer[widget.index]["cust_name"]).toString()),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.01),
                    child: Text(
                      "Tot. Outst. : ₹ " +
                          (totalPayment - temp).toStringAsFixed(2),
                      style: TextStyle(color: Colors.green[600]),
                    ),
                  )
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    child: Icon(
                      Icons.location_on,
                      color: Colors.grey,
                      size: 15,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.01,
                        top: MediaQuery.of(context).size.height * 0.01),
                    child: Text(
                      ((_searchResult == null ? false : _searchResult.length !=0 ) ? _searchResult[widget.index]["address"]:nameOfCustomer[widget.index]["address"]).toString(),
                      style: TextStyle(color: Colors.grey),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
