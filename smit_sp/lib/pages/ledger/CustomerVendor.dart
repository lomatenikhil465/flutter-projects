
import 'package:bhandari/pages/VendorDetails.dart';
import 'package:bhandari/pages/customer/ExistingCustomerPage.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import '../CustomerDetails.dart';

class CustomerVendorPage extends StatefulWidget {

  final int salePurchase;
  CustomerVendorPage({ this.salePurchase});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CustomerVendorPage();
  }
}

List nameOfCustomer;

class _CustomerVendorPage extends State<CustomerVendorPage> {

  Future<String> customerResponse;

    Future<String> getJsonData() async {
    String url;
    if(widget.salePurchase == 0){
     url=
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=customer";
    } else {
      url=
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=customerPurchase";
    }
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      nameOfCustomer = convertDataToJson['data'];
    });

    return "success";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    
    customerResponse = getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FutureBuilder<String>(
      future: customerResponse,
      builder: (context, snapshot){
        if(snapshot.hasData){
          return nameOfCustomer == null?
          Center(
            child: Text("No Data Available"),
          )
          : ListView.separated(
      separatorBuilder: (BuildContext context, int index) => Divider(
        thickness: 0.0,
        color: Colors.lightBlue[900],
      ),
      itemCount: nameOfCustomer == null ? 0 : nameOfCustomer.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () {
            if (widget.salePurchase == 0) {
              
              Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return CustomerDetails(
                              name: nameOfCustomer[index]["cust_name"],
                              custId: nameOfCustomer[index]["cid"],
                              
                            );
                          }));
            } else {
              print(nameOfCustomer[index]["cust_name"]);
              print(nameOfCustomer[index]["cpid"]);
               Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return VendorDetails(
                              name: nameOfCustomer[index]["cust_name"],
                              custId: nameOfCustomer[index]["cpid"],
                              
                            );
                          }));
            }
          },
          child: Container(
            height: 60,
            child: Card(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * 0.01,
                            top: MediaQuery.of(context).size.height * 0.01),
                        child: Text(nameOfCustomer[index]["cust_name"].toString()),
                      )
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        child: Icon(
                          Icons.location_on,
                          color: Colors.grey,
                          size: 15,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * 0.01,
                            top: MediaQuery.of(context).size.height * 0.01),
                        child: Text(
                          nameOfCustomer[index]["address"],
                          style: TextStyle(color: Colors.grey),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    ); 
    
    
  }
}
