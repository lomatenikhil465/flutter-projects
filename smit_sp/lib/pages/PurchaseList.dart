import 'package:smit_sp/pages/InvoiceDetails.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class PurchaseListPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PurchaseListPage();
  }
}

class _PurchaseListPage extends State<PurchaseListPage> {
  String? currDate;
  List? purchaseList;
  Future<String>? purchaseResponse;
  Future<String> getJsonData() async {
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=purchaseList";
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      purchaseList = convertDataToJson['data'];
    });

    return "success";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    DateTime now = DateTime.now();
    currDate = DateFormat('dd/MM/yyyy').format(now);
    purchaseResponse = this.getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Purchase List"),
          backgroundColor: Colors.lightBlue[700],
        ),
        body: FutureBuilder<String>(
          future: purchaseResponse,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Container(
                  child: ListView.separated(
                separatorBuilder: (BuildContext context, int index) => Divider(
                  thickness: 0.0,
                  color: Colors.lightBlue[900],
                ),
                itemCount: purchaseList == null ? 0 : purchaseList!.length,
                itemBuilder: (BuildContext context, int index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return InvoiceDetails(
                          billNo: purchaseList![index]["bill_no"],
                          custId: purchaseList![index]["party_id"],
                          custName: purchaseList![index]["cust_name"],
                          salePurchase: 1,
                        );
                      })).then((value){
                        this.getJsonData();
                      });
                    },
                    child: Container(
                      child: Card(
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width *
                                          0.01,
                                      top: MediaQuery.of(context).size.height *
                                          0.01),
                                  child: Text(
                                    purchaseList![index]["cust_name"].toString(),
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: Colors.lightBlue[700]),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width *
                                          0.01,
                                      top: MediaQuery.of(context).size.height *
                                          0.01),
                                  child: Text(
                                    purchaseList![index]["grand_total"]
                                            .toString() +
                                        "₹",
                                  ),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width *
                                          0.01,
                                      top: MediaQuery.of(context).size.height *
                                          0.01),
                                  child: Text(
                                    purchaseList![index]["bill_no"].toString(),
                                  ),
                                )
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.01,
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.01),
                                      child: Text(
                                        purchaseList![index]["date"].toString(),
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    ),
                                  ],
                                ),
                                Spacer(),
                                Container(
                                  height: 20,
                                  width: 60,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10.0),
                                      color: purchaseList![index]
                                                  ["current_balance"] !=
                                              "0"
                                          ? Colors.deepOrange
                                          : Colors.green[600]),
                                  child: Center(
                                    child: Text(
                                      purchaseList![index]["current_balance"] !=
                                              "0"
                                          ? "Unpaid"
                                          : "Paid",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ));
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ));
  }
}
