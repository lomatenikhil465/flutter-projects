import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'CustomerDetails.dart';
import 'VendorDetails.dart';

class VendorList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _VendoList();
  }
}

class _VendoList extends State<VendorList> {
  List? vendorList;
  Future<String>? vendorListResponse;

  Future<String> getJsonData() async {
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=customerPurchase";
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      vendorList = convertDataToJson['data'];
    });

    return "success";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    vendorListResponse = getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Vendor"),
        backgroundColor: Colors.lightBlue[700],
      ),
      body: FutureBuilder<String>(
        future: vendorListResponse,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return vendorList == null
                ? Center(
                    child: Text("No Data Available"),
                  )
                : ListView.separated(
                    separatorBuilder: (BuildContext context, int index) =>
                        Divider(
                      thickness: 0.0,
                      color: Colors.lightBlue[700],
                    ),
                    itemCount: vendorList == null ? 0 : vendorList!.length,
                    itemBuilder: (BuildContext context, int index) {
                      return GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return VendorDetails(
                              name: vendorList![index]["cust_name"],
                              custId: vendorList![index]["cpid"],
                              
                            );
                          }));
                        },
                        child: Container(
                          height: 60,
                          child: Card(
                            elevation: 3.0,
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.01,
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.01),
                                      child: Text(vendorList![index]["cust_name"]
                                          .toString()),
                                    )
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      child: Icon(
                                        Icons.location_on,
                                        color: Colors.grey,
                                        size: 15,
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.01,
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.01),
                                      child: Text(
                                        vendorList![index]["address"],
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ),
                      );
                    },
                  );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
