import 'package:bhandari/pages/PurchasePage.dart';
import 'package:bhandari/pages/products/CreateNewProductPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../SalePage.dart';
import '../PurchasePage.dart';
import '../QuotationPage.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class AddProductPage extends StatefulWidget {
  final int? salePurchase, radio;
  AddProductPage({this.salePurchase, this.radio});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AddProductPage();
  }
}

List produtsData;
List _searchResult;
class _AddProductPage extends State<AddProductPage> {
  Future<String> productResponse;
  TextEditingController _controller;

  Future<String> getJsonData() async {
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=products";
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      produtsData = convertDataToJson['data'];
    });

    return "success";
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  void onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      //return ;
    }

    produtsData.forEach((userDetail) {
      print("search result : " +userDetail["newitem"].toString() );
      if (userDetail["newitem"].toLowerCase().contains(text.toLowerCase()) ){

        _searchResult.add(userDetail);
      }
    });

    setState(() {});
  
}

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _searchResult = new List();
_controller = TextEditingController(text: ""); 
    productResponse = this.getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Existing Product"),
          backgroundColor: Colors.lightBlue[700],
        ),
        // floatingActionButton: FloatingActionButton.extended(
        //   onPressed: (){
        //     if(widget.salePurchase == 0){
        //     Navigator.of(context).push(MaterialPageRoute(builder: (context){
        //       return PurchasePage();
        //     }));
        //     } else {
        //        Navigator.of(context).push(MaterialPageRoute(builder: (context){
        //       return CreateNewProductPage();
        //     }));
        //     }
        //   },
        //   label: Text("Create New"),
        //   icon: Icon(Icons.add),
        //   isExtended: true,
        //   ),
        body: Column(
          children: [
            Container(
            height: 55,
              padding: const EdgeInsets.all(8.0),
              child: TextField(
                onChanged: (value) {
                  onSearchTextChanged(value);
                },
                controller: _controller,
                decoration: InputDecoration(
                    labelText: "Search",
                    hintText: "Search",
                    prefixIcon: Icon(Icons.search),
                    contentPadding: EdgeInsets.only(top: 10),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5.0)))),
              ),
            ),
            Expanded(
                      child: produtsData == null
                    ? Center(
                        child: Text("No Data Available"),
                      )
                    :(_searchResult ==null ? false :_searchResult.length != 0 )|| _controller.text.isNotEmpty ? ListView.builder(
                
                itemCount: _searchResult == null ? 0 : _searchResult.length,
                itemBuilder: (BuildContext context, int index) {
                  return _searchResult[index]["sale_rate"] != ''?  GestureDetector(
                    onTap: () {
                      if (widget.salePurchase == 0) {
                        
                        productsSale!.add(_searchResult[index]);
                        print("Sale : " + productsSale.toString());
                      } else if (widget.salePurchase == 1) {
                        
                        productsPurchase!.add(_searchResult[index]);
                        print("Purchase : " + productsPurchase.toString());
                      } else {
                        productsQuotation!.add(_searchResult[index]);
                      }
                      Navigator.pop(context, index.toString());
                    },
                    child: Container(
                      child: Card(
                        child: Row(children: <Widget>[
                          Container(
                            height: 75,
                            width: 60,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                border: Border.all(color: Colors.grey, width: 1)),
                            child: Image.network(
                              "https://qot.constructionsmall.com/" +
                                  _searchResult[index]["img"],
                              loadingBuilder: (context, child, progress) {
                                return progress == null
                                    ? child
                                    : LinearProgressIndicator();
                              },
                              fit: BoxFit.scaleDown,
                            ),
                          ),
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context).size.width *
                                              0.01,
                                          top: MediaQuery.of(context).size.height *
                                              0.01),
                                      child: Text(
                                          _searchResult[index]["newitem"].toString()),
                                    )
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context).size.width *
                                              0.01,
                                          top: MediaQuery.of(context).size.height *
                                              0.01),
                                      child: Text(
                                        widget.salePurchase == 0 ||
                                                widget.salePurchase == 2
                                            ? (widget.radio != 0
                                                ? "₹ " +
                                                    _searchResult[index]["sale_rate"]
                                                        .toString()
                                                : "₹ " +
                                                    (double.parse(_searchResult[index]
                                                                    ["sale_rate"]) + (double.parse(_searchResult[index]
                                                                    ["sale_rate"]) *
                                                                double.parse(
                                                                    _searchResult[index]
                                                                        [
                                                                        "gst_per"].toString())) /
                                                            100)
                                                        .toString())
                                            : "Qty : " +
                                                _searchResult[index]['bal_qty']
                                                    .toString() +
                                                "                Purchase Price : " +
                                                (widget.radio != 0
                                                    ? _searchResult[index]
                                                        ['purchase_rate']
                                                    : (double.parse(_searchResult[index]
                                                                    ["purchase_rate"]) +(double.parse(_searchResult[index]
                                                                    ["purchase_rate"]) *
                                                                double.parse(
                                                                    produtsData[index]
                                                                        [
                                                                        "gst_per"])) /
                                                            100)
                                                        .toString()),
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ]),
                      ),
                    ),
                  ) : Container();
                },
              ): ListView.builder(
                
                itemCount: produtsData == null ? 0 : produtsData.length,
                itemBuilder: (BuildContext context, int index) {
                  return produtsData[index]["sale_rate"] != ''?  GestureDetector(
                    onTap: () {
                      if (widget.salePurchase == 0) {
                        
                        productsSale.add(produtsData[index]);
                        print("Sale : " + productsSale.toString());
                      } else if (widget.salePurchase == 1) {
                        
                        productsPurchase.add(produtsData[index]);
                        print("Purchase : " + productsPurchase.toString());
                      } else {
                        productsQuotation.add(produtsData[index]);
                      }
                      Navigator.pop(context, index.toString());
                    },
                    child: Container(
                      child: Card(
                        child: Row(children: <Widget>[
                          Container(
                            height: 75,
                            width: 60,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10.0),
                                border: Border.all(color: Colors.grey, width: 1)),
                            child: Image.network(
                              "https://qot.constructionsmall.com/" +
                                  produtsData[index]["img"],
                              loadingBuilder: (context, child, progress) {
                                return progress == null
                                    ? child
                                    : LinearProgressIndicator();
                              },
                              fit: BoxFit.scaleDown,
                            ),
                          ),
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context).size.width *
                                              0.01,
                                          top: MediaQuery.of(context).size.height *
                                              0.01),
                                      child: Text(
                                          produtsData[index]["newitem"].toString()),
                                    )
                                  ],
                                ),
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context).size.width *
                                              0.01,
                                          top: MediaQuery.of(context).size.height *
                                              0.01),
                                      child: Text(
                                        widget.salePurchase == 0 ||
                                                widget.salePurchase == 2
                                            ? (widget.radio != 0
                                                ? "₹ " +
                                                    produtsData[index]["sale_rate"]
                                                        .toString()
                                                : "₹ " +
                                                    (double.parse(produtsData[index]
                                                                    ["sale_rate"]) + (double.parse(produtsData[index]
                                                                    ["sale_rate"]) *
                                                                double.parse(
                                                                    produtsData[index]
                                                                        [
                                                                        "gst_per"].toString())) /
                                                            100)
                                                        .toString())
                                            : "Qty : " +
                                                produtsData[index]['bal_qty']
                                                    .toString() +
                                                "                Purchase Price : " +
                                                (widget.radio != 0
                                                    ? produtsData[index]
                                                        ['purchase_rate']
                                                    : (double.parse(produtsData[index]
                                                                    ["purchase_rate"]) +(double.parse(produtsData[index]
                                                                    ["purchase_rate"]) *
                                                                double.parse(
                                                                    produtsData[index]
                                                                        [
                                                                        "gst_per"])) /
                                                            100)
                                                        .toString()),
                                        style: TextStyle(color: Colors.grey),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        ]),
                      ),
                    ),
                  ) : Container();
                },
              ),
            ),
          ],
        ));
  }
}
