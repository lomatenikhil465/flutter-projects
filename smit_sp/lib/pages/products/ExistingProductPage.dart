import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../SalePage.dart';
import '../PurchasePage.dart';

class ExistingProductPage extends StatefulWidget {

  final int index;
  ExistingProductPage({ this.index});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ExistingProductPage();
  }
  
}



List produtsData = [
		{
			"imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/M63H24W7JF0-L302-ALTGHOST?wid=1500&fmt=jpeg&qlt=80&op_sharpen=0&op_usm=1.0,1.0,5,0&iccEmbed=0",
			"name": "CHECK PRINT SHIRT",
			"price": 110
		},
		{
			"imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/FLGLO4FAL12-BEIBR?wid=700&amp;fmt=jpeg&amp;qlt=80&amp;op_sharpen=0&amp;op_usm=1.0,1.0,5,0&amp;iccEmbed=0",
			"name": "GLORIA HIGH LOGO SNEAKER",
			"price": 91
		},
		{
			"imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/HWVG6216060-TAN?wid=700&amp;fmt=jpeg&amp;qlt=80&amp;op_sharpen=0&amp;op_usm=1.0,1.0,5,0&amp;iccEmbed=0",
			"name": "CATE RIGID BAG",
			"price": 94.5
		},
		{
			"imgUrl": "http://guesseu.scene7.com/is/image/GuessEU/WC0001FMSWC-G5?wid=520&fmt=jpeg&qlt=80&op_sharpen=0&op_usm=1.0,1.0,5,0&iccEmbed=0",
			"name": "GUESS CONNECT WATCH",
			"price": 438.9
		},
		{
			"imgUrl": "https://guesseu.scene7.com/is/image/GuessEU/AW6308VIS03-SAP?wid=700&amp;fmt=jpeg&amp;qlt=80&amp;op_sharpen=0&amp;op_usm=1.0,1.0,5,0&amp;iccEmbed=0",
			"name": "'70s RETRO GLAM KEFIAH",
			"price": 20
		}
	];
class _ExistingProductPage extends State<ExistingProductPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return ListView.builder(
     
      itemCount: produtsData.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () {
            if(widget.index == 0){
              
            productsPurchase.add({
              "name": produtsData[index]["name"].toString(),
              "price": produtsData[index]["price"].toString()
              });
            } else{
              
            productsSale.add({
              "name": produtsData[index]["name"].toString(),
              "price": produtsData[index]["price"].toString()
              });
            }
            Navigator.pop(context, index.toString());
          },
          child: Container(
            height: 60,
            child: Card(
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * 0.01,
                            top: MediaQuery.of(context).size.height * 0.01),
                        child: Text(produtsData[index]["name"].toString()),
                      )
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        child: Icon(
                          Icons.attach_money,
                          color: Colors.grey,
                          size: 15,
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * 0.01,
                            top: MediaQuery.of(context).size.height * 0.01),
                        child: Text(
                          produtsData[index]["price"].toString(),
                          style: TextStyle(color: Colors.grey),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
  
}