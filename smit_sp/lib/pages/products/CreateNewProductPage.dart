import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'ExistingProductPage.dart';

class CreateNewProductPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CreateNewProductPage();
  }
}

class _CreateNewProductPage extends State<CreateNewProductPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  var _unit = ['Litre', 'Kg', 'Piece'];
  String _currentItemSelected;
  bool _autoValidate;
  String prodName,prodDesc,stock,rate,stockValue,salePrice;

  void _onDropDownItemSelected(String newValueSelected) {
    setState(() {
      this._currentItemSelected = newValueSelected;
    });
  }

  
  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  void saveProduct(){
    
     if (_formKey.currentState.validate()) {
      print("enter");
      _formKey.currentState.save();
     
      produtsData.add({
        "id": produtsData.length,
        "name": this.prodName,
        "desc": this.prodDesc,
        "stock": this.stock,
        "price": this.salePrice,
        
      });
      
      
      _formKey.currentState.reset();
      
      _showDialogBox(context, "Saved to Product List Successfully", "");
    } else {
     
      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _autoValidate = false;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Create New Product"),
        backgroundColor: Colors.lightBlue[700],
      ),
      body: Form(
      key: _formKey,
      autovalidate: _autoValidate,
      child: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            textAlignVertical: TextAlignVertical.bottom,
                            
                            onSaved: (String name) {
                              setState(() {
                                this.prodName = name;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: "Product Name",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                                suffixIcon: Container(
                                  padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.03,
                                      left: MediaQuery.of(context).size.width *
                                          0.07),
                                  child: Icon(Icons.perm_contact_calendar),
                                )),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            textAlignVertical: TextAlignVertical.bottom,
                            
                            onSaved: (String desc) {
                              setState(() {
                                this.prodDesc = desc;
                              });
                            },
                            decoration: InputDecoration(
                              labelText: "Product Description",
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          child: TextFormField(
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(10),
                              new WhitelistingTextInputFormatter(
                                  RegExp("[0-9]"))
                            ],
                            onSaved: (String stock) {
                              setState(() {
                                this.stock = stock;
                              });
                            },
                            decoration: InputDecoration(
                              labelText: "Opening Stock Quantity",
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            validator: null,
                            keyboardType: TextInputType.phone,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.01),
                          child: Container(
                            height: 35,
                            width: MediaQuery.of(context).size.width * 0.3,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5.0),
                              border: Border.all(
                                  color: Colors.lightBlue[700],
                                  style: BorderStyle.solid,
                                  width: 1.0),
                            ),
                            child: DropdownButtonHideUnderline(
                              child: ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton<String>(
                                  isDense: true,
                                  autofocus: true,
                                  focusColor: Colors.lightBlue[900],
                                  //style: Theme.of(context).textTheme.title,
                                  isExpanded: true,
                                  hint: Text("Units"),
                                  items: _unit.map((String dropDownStringItem) {
                                    return DropdownMenuItem<String>(
                                        value: dropDownStringItem,
                                        child: Text(dropDownStringItem,
                                            style: TextStyle(
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold,
                                                color: Colors.lightBlue[700])));
                                  }).toList(),
                                  onChanged: (String newValueSelected) =>
                                      _onDropDownItemSelected(newValueSelected),
                                  value: _currentItemSelected,
                                ),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            //inputFormatters: [new BlacklistingTextInputFormatter(RegExp(r"/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/")),],
                            onSaved: (value) {
                              setState(() {
                                this.rate = value;
                              });
                            },

                            decoration: InputDecoration(
                              labelText: "Rate Per Unit",
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            validator: null,
                            keyboardType: TextInputType.number,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(15),
                              new BlacklistingTextInputFormatter(
                                  RegExp(r"[^\s\w]")),
                            ],
                            textCapitalization: TextCapitalization.characters,
                            onSaved: (String value) {
                              setState(() {
                                this.stockValue = value;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: "Opening Stock Value",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015,
                                    left: MediaQuery.of(context).size.width *
                                        0.0),
                                prefixIcon: Container(
                                    padding: EdgeInsets.only(
                                        right:
                                            MediaQuery.of(context).size.width *
                                                0.05,
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.015),
                                    child: Icon(Icons.attach_money))),
                            keyboardType: TextInputType.number,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            
                            onSaved: (String price) {
                              setState(() {
                                this.salePrice = price;
                              });
                            },
                            decoration: InputDecoration(
                              labelText: "Sale Price Per Unit",
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                                      prefixIcon: Container(
                                    padding: EdgeInsets.only(
                                        right:
                                            MediaQuery.of(context).size.width *
                                                0.05,
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.015),
                                    child: Icon(Icons.attach_money))
                            ),
                            keyboardType: TextInputType.number,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 50,
            width: MediaQuery.of(context).size.width * 1,
            color: Colors.lightBlue[700],
            child: FlatButton(
                onPressed: () {
                  saveProduct();
                  
                },
                child: Text(
                  "Save",
                  style: TextStyle(color: Colors.white),
                )),
          )
        ],
      ),
    ),
    );
    
    
  }
}
