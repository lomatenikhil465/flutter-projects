import 'package:bhandari/pages/InvoiceList.dart';
import 'package:bhandari/pages/MapInvoiceList.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'BottomSheetWidget.dart';

class CustomerDetails extends StatefulWidget {
  final String name, custId;
  CustomerDetails({this.name, this.custId});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CustomerDetails();
  }
}

class _CustomerDetails extends State<CustomerDetails> {
  List customerStatement, customerTotalInvoice;
  Future<String> customerStatementResponse;
  int flag = 0;
  double totalPayment = 0.0;
  var temp = 0.0;

  Future<String> getJsonData() async {
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=AllInLedger&custId="+
    //"https://qot.constructionsmall.com/app/getquerycode.php?apicall=customerLedger&custId=" +
     widget.custId;
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      customerStatement = convertDataToJson['data'];
    });

    if (customerStatement != null) {
      for (var i = 0; i < customerStatement.length; i++) {
        if (customerStatement[i]["lType"] == "s") {
          if (customerStatement[i]["c_type"] == 'cr') {
            temp = temp+ double.parse(customerStatement[i]["amt"]);
          } 
          if(customerStatement[i]["c_type"] == 'dr'){
            if(customerStatement[i]["chal_type"] == 'Sale'){
              totalPayment = totalPayment+ (double.parse(customerStatement[i]["amt"]) - double.parse(customerStatement[i]["pay_amt"]));
            } else {
            temp = temp + double.parse(customerStatement[i]["amt"]);
            }
          }
        }

        if (customerStatement[i]["lType"] == "p") {
          if (customerStatement[i]["c_type"] == 'dr') {
            if(customerStatement[i]["bill_no"] == "-" && customerStatement[i]["recpt_no"] == "-"){
              totalPayment = totalPayment+ double.parse(customerStatement[i]["paid_amt"]);
            } else if(customerStatement[i]["bill_no"] != "0" || customerStatement[i]["recpt_no"] != "0"){
              
              temp = temp + (double.parse(customerStatement[i]["grand_total"]) - double.parse(customerStatement[i]["paid_amt"]));
            
            } 
          } 
          if(customerStatement[i]["c_type"] == 'cr'){
            totalPayment = totalPayment + double.parse(customerStatement[i]["paid_amt"]);
          }
        }
        print(i.toString() + " : " + temp.toString());
      }
    }
    return "success";
  }

  Future<String> getTotal() async {
    print(widget.custId);
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=totalPaymentSale&custId=" +
            widget.custId;
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      customerTotalInvoice = convertDataToJson['data'];
    });

    for (var i = 0; i < customerTotalInvoice.length; i++) {
      if (customerTotalInvoice[i]["transaction_type"] == null
          ? false
          : customerTotalInvoice[i]["transaction_type"] == "dr") {
        temp = temp - double.parse(customerTotalInvoice[i]["paid_amt"]);
      }
    }
    totalPayment =
        totalPayment + double.parse(customerTotalInvoice[0]["grand_total"]);

    return "success";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
totalPayment = 0.0;
temp = 0.0;
    customerStatementResponse = getJsonData();
    //getTotal();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  widget.name,
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  "Customer",
                  style: TextStyle(fontSize: 10),
                ),
              ],
            )
          ],
        ),
        backgroundColor: Colors.lightBlue[700],
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: 80,
            width: MediaQuery.of(context).size.width,
            color: Colors.lightBlue[700],
            child: Column(
              children: <Widget>[
                Container(
                    child: Text(
                  "You will receive",
                  style: TextStyle(color: Colors.grey[300], fontSize: 10),
                )),
                Container(
                  child: Text(
                    "₹ " + (totalPayment - temp).toStringAsFixed(2),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Container(
                  child: Text(
                    "Balance Outstanding",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  ),
                )
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.16,
            child: GridView.count(
              crossAxisCount: 3,
              primary: false,
              childAspectRatio: 6 / 5,
              padding: EdgeInsets.only(top: 10.0),
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              children: <Widget>[
                GestureDetector(
                    onTap: () {
                      Future<void> sheetController = showModalBottomSheet<void>(
                          context: context,
                          builder: (context) => BottomSheetWidget(
                                custId: widget.custId,
                                sale: 0,
                                purchase: 1,
                              ),
                          backgroundColor: Colors.transparent);

                      sheetController.then((value) {
                        print("sheet closed");
                      });
                    },
                    child: Card(
                      elevation: 5.0,
                      child: Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Colors.deepOrange, Colors.orange])),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Icon(
                                Icons.add,
                                size: 20,
                                color: Colors.white,
                              )),
                            ),
                            new Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Text(
                                "     Add\n New Entry",
                                style: new TextStyle(
                                    color: Colors.white, fontSize: 15.0),
                              )),
                            ),
                          ],
                        ),
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return InvoiceList(
                          custId: widget.custId,
                          salePurchase: 0,
                        );
                      })).then((value) {
                        totalPayment = 0.0;
                        temp = 0.0;
                        this.getJsonData();
                        //this.getTotal();
                      });
                    },
                    child: Card(
                      elevation: 5.0,
                      child: Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Colors.blue, Colors.lightBlue])),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Icon(
                                Icons.assessment,
                                size: 20,
                                color: Colors.white,
                              )),
                            ),
                            new Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Text(
                                "Invoice\n   List",
                                style: new TextStyle(
                                    color: Colors.white, fontSize: 15.0),
                              )),
                            ),
                          ],
                        ),
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      setState(() {});
                    },
                    child: Card(
                      elevation: 5.0,
                      child: Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Colors.green, Colors.lightGreen])),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Icon(Icons.compare_arrows,
                                      size: 20, color: Colors.white)),
                            ),
                            new Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Text(
                                "     View\nStatement",
                                style: new TextStyle(
                                    color: Colors.white, fontSize: 15.0),
                              )),
                            ),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
          Expanded(
            child: customerStatement == null
                ? Center(
                    child: Text("No Transaction Data..."),
                  )
                : FutureBuilder<String>(
                    future: customerStatementResponse,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return ListView.separated(
                          separatorBuilder: (BuildContext context, int index) =>
                              Divider(
                            thickness: 0.0,
                            color: Colors.lightBlue[700],
                          ),
                          itemCount: customerStatement == null
                              ? 0
                              : customerStatement.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Container(
                              child: Card(
                                elevation: 0,
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Flexible(
                                                                                  child: Container(
                                            padding: EdgeInsets.only(
                                                left: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.01,
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.01),
                                            child: Text(
                                              customerStatement[index]["lType"] == 's'
                                                  ? (customerStatement[index]
                                                              ["chal_type"] ==
                                                          'Sale'
                                                      ? "Sale"
                                                      : customerStatement[index]
                                                                  ["chal_type"] ==
                                                              'Rcpt'
                                                          ? "Sale Receipt to bill " +
                                                              customerStatement[index]
                                                                  ["chall_no"] +
                                                              " by " +
                                                              customerStatement[index]["payment_mode"]
                                                                  .toString()
                                                          : "Bahi Khata ")
                                                  : (customerStatement[index]["lType"] == 'p'
                                                      ? (customerStatement[index]["bill_no"] !=
                                                                  "0" &&
                                                              customerStatement[index]["recpt_no"] !=
                                                                  "0"
                                                          ? (customerStatement[index]["bill_no"] !=
                                                                  "-" &&
                                                              customerStatement[index]["recpt_no"] !=
                                                                  "-" ? "Purchase Receipt to bill " +
                                                              customerStatement[index]
                                                                  ["bill_no"] +
                                                              " by " +
                                                              customerStatement[index]
                                                                      ["payment_mode"]
                                                                  .toString(): "Bahi Khata " )
                                                          : "Purchase")
                                                      : ""),
                                              // customerStatement[index]["chal_type"] == "Rcpt"
                                              //     ? (customerStatement[index]["c_type"] == "cr"
                                              //         ? "Payment received against Receipt " +
                                              //             customerStatement[index]
                                              //                 ["recpt_no"] +
                                              //             " by " +
                                              //             (customerStatement[index]["payment_mode"] == null
                                              //                 ? "Cash"
                                              //                 : customerStatement[index][
                                              //                     "payment_mode"])
                                              //         : "Payment paid to receipt " +
                                              //             customerStatement[index]
                                              //                     ["recpt_no"]
                                              //                 .toString() +
                                              //             " by " +
                                              //             (customerStatement[index]["payment_mode"] == null
                                              //                 ? "Cash"
                                              //                 : customerStatement[index][
                                              //                     "payment_mode"]))
                                              //     : (customerStatement[index]
                                              //                 ["chal_type"] ==
                                              //             "OnAccount"
                                              //         ? (customerStatement[index]["c_type"] == "cr"
                                              //             ? "Payment received On Account " +
                                              //                 customerStatement[index]
                                              //                     ["recpt_no"] +
                                              //                 " by " +
                                              //                 (customerStatement[index]["payment_mode"] == null
                                              //                     ? "Cash"
                                              //                     : customerStatement[index]
                                              //                         ["payment_mode"])
                                              //             : "Payment paid On Account " + customerStatement[index]["recpt_no"].toString() + " by " + (customerStatement[index]["payment_mode"] == null ? "Cash" : customerStatement[index]["payment_mode"]))
                                              //         : (customerStatement[index]["chal_type"] == "OpeningStock" ? (customerStatement[index]["c_type"] == "cr" ? "Payment taken for Opening Stock " : "Payment given for opening stock") : "invoice issued against " + customerStatement[index]["chall_no"])),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                        ),
                                        customerStatement[index]["recpt_no"] ==
                                                    "-" &&
                                                
                                                (customerStatement[index]
                                                        ["chal_type"] ==
                                                    "OnAccount" || customerStatement[index]
                                                        ["bill_no"] == "-")
                                            ? Container(
                                                child: RaisedButton(
                                                  onPressed: () {
                                                    Navigator.of(context).push(
                                                        MaterialPageRoute(
                                                            builder: (context) {
                                                      return MapInvoiceList(
                                                        custId:
                                                            customerStatement[
                                                                    index]
                                                                ["cust_id"],
                                                        salePurchase: customerStatement[index]["lType"] == 'p' ? 1 : 0,
                                                        bkId: customerStatement[
                                                                index]
                                                            ["bahi_khata_id"],
                                                        date: customerStatement[
                                                            index]["s_date"],
                                                      );
                                                    })).then((value) {
                                                      setState(() {
                                                        totalPayment = 0.0;
                                                        temp = 0.0;
                                                      });
                                                      this.getJsonData();
                                                      
                                                    });
                                                  },
                                                  child: Text("MAP"),
                                                  textColor: Colors.white,
                                                  color: Colors.lightBlue[700],
                                                ),
                                              )
                                            : Container(
                                                child: Text(
                                                  customerStatement[index]
                                                              ["lType"] ==
                                                          's'
                                                      ? (customerStatement[index]
                                                                  [
                                                                  "chal_type"] ==
                                                              "Sale"
                                                          ? "Inv no. : " + customerStatement[index]
                                                              ["chall_no"]
                                                          :customerStatement[index]
                                                              ["recpt_no"] == "-" ? "" : "Recpt No. : " +  customerStatement[index]
                                                              ["recpt_no"])
                                                      : (customerStatement[index]
                                                                  ["lType"] ==
                                                              "p"
                                                          ? (customerStatement[index]["bill_no"] !=
                                                                      "0" &&
                                                                  customerStatement[index]["recpt_no"] !=
                                                                      "0"
                                                              ?customerStatement[index]
                                                              ["recpt_no"] == "-" ? "" : "Recpt No. : " + customerStatement[index]
                                                                  ["recpt_no"]
                                                              : "Inv No. : " +  customerStatement[index]
                                                                  ["bill_no"])
                                                          : ""),
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(
                                              right: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.01,
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01),
                                          child: Text(
                                            customerStatement[index]["lType"] == 's'
                                                ? ((customerStatement[index]["chal_type"] == "Sale"
                                                    ? "Total : ₹ " +
                                                        customerStatement[index]
                                                            ["amt"]
                                                    : (customerStatement[index]["c_type"] == "cr"
                                                        ? "Received : ₹ " +
                                                            customerStatement[index]
                                                                ["amt"]
                                                        : "Paid : ₹ " +
                                                            customerStatement[index]
                                                                ["amt"])))
                                                : (customerStatement[index]["lType"] == "p"
                                                    ? (customerStatement[index]["bill_no"] != "0" && customerStatement[index]["recpt_no"] != "0"
                                                        ? (customerStatement[index]["c_type"] == "cr"
                                                            ? "Paid : ₹ " +
                                                                customerStatement[index]
                                                                    ["paid_amt"]
                                                            : "Received : ₹ " +
                                                                customerStatement[index][
                                                                    "paid_amt"])
                                                        : "Total : ₹ " +
                                                            (customerStatement[index]["paid_amt"] == "0"
                                                                ? customerStatement[index]["amt"]
                                                                : customerStatement[index]["paid_amt"]))
                                                    : ""),
                                            // customerStatement[index]["chal_type"] == "Rcpt" ||
                                            //         customerStatement[index]
                                            //                 ["chal_type"] ==
                                            //             "OpeningStock"
                                            //     ? "₹ " +
                                            //         customerStatement[index]
                                            //             ["amt"]
                                            //     : (customerStatement[index]
                                            //                 ["chal_type"] ==
                                            //             "OnAccount"
                                            //         ? (customerStatement[index]["remain_amt"] != "0"
                                            //             ? "₹ " +
                                            //                 customerStatement[index]
                                            //                     ["remain_amt"]
                                            //             : "₹ " +
                                            //                 customerStatement[index]
                                            //                     ["amt"])
                                            //         : (customerStatement[index]
                                            //                     ["pay_amt"] ==
                                            //                 "0"
                                            //             ? "₹ " +
                                            //                 customerStatement[index]
                                            //                     ["amt"]
                                            //             : "₹ " +
                                            //                 customerStatement[index]
                                            //                     ["pay_amt"])),
                                            style: TextStyle(
                                                color: customerStatement[index]
                                                            ["lType"] ==
                                                        's'
                                                    ? (customerStatement[index]
                                                                ["chal_type"] ==
                                                            "Sale"
                                                        ? Colors.green[600]
                                                        : (customerStatement[index]["c_type"] == "cr"
                                                            ? Colors.green[600]
                                                            : Colors
                                                                .deepOrange))
                                                    : (customerStatement[index]
                                                                ["lType"] ==
                                                            "p"
                                                        ? (customerStatement[index]["bill_no"] !=
                                                                    "0" &&
                                                                customerStatement[index]["recpt_no"] !=
                                                                    "0"
                                                            ? (customerStatement[index]["c_type"] == "cr"
                                                                ? Colors.deepOrange
                                                                : Colors.green[600])
                                                            : Colors.green[600])
                                                        : Colors.green[600])),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              left: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.01,
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01),
                                          child: Text(
                                            customerStatement[index]["chdt"],
                                            style: TextStyle(
                                                color: Colors.grey[700]),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      } else {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                  ),
          )
        ],
      ),
    );
  }
}
