import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class StockPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _StockPage();
  }
  
}

class _StockPage extends State<StockPage> {

  List? stockData;
  Future<String>? stockResponse;

  Future<String> getJsonData() async {
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=products";
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      stockData = convertDataToJson['data'];
    });

    return "success";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    stockResponse = this.getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Stock"),
        backgroundColor: Colors.lightBlue[700],
      ),
      body: FutureBuilder<String>(
        future: stockResponse,
        builder: (context,snapshot){
          if (snapshot.hasData) {
            return Container(
              child: ListView.separated(
                separatorBuilder: (BuildContext context, int index) => Divider(
        thickness: 0.0,
        color: Colors.lightBlue[900],
      ), 
                itemCount: stockData == null ? 0 : stockData!.length,
                itemBuilder: (BuildContext context, int index){
                  return Container(
            height: 60,
            child: Card(
              elevation: 3.0,
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * 0.01,
                            top: MediaQuery.of(context).size.height * 0.01),
                        child: Text(stockData![index]["newitem"].toString()),
                      )
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      
                      Container(
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * 0.01,
                            top: MediaQuery.of(context).size.height * 0.01),
                        child: Text(
                          "Stock Qty ( " + stockData![index]["bal_qty"].toString() + " X " + stockData![index]["purchase_rate"].toString() + "₹ )",
                          style: TextStyle(color: Colors.grey[700]),
                        ),
                      ),
                       Spacer(),
                       Container(
                        
                        child: Text(
                          "Total : " ,
                          style: TextStyle(color: Colors.black,
                        ),
                      )
                       ),
                       Container(
                        padding: EdgeInsets.only(
                            right: MediaQuery.of(context).size.width * 0.01,
                            ),
                        child: Text(
                          (double.parse(stockData![index]["bal_qty"]) * double.parse( stockData![index]["purchase_rate"])).toString(),
                          style: TextStyle(color: Colors.green[600],
                          fontWeight: FontWeight.w500
                        ),
                      )
                       )
                    ],
                  )
                ],
              ),
            ),
          );
                }, 
                ),
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
        ),
    );
  }
  
}