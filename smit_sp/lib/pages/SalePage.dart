import 'package:smit_sp/pages/customer/AddCustomerPage.dart';
import 'package:smit_sp/pages/customer/ExistingCustomerPage.dart';
import 'package:smit_sp/pages/products/AddProductPage.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class SaleTabPage extends StatefulWidget {
  final int editFlag;
  final String? customerId, billNo;

  const SaleTabPage({required this.editFlag, required this.customerId, required this.billNo});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SaleTabPage();
  }
}

class _SaleTabPage extends State<SaleTabPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text("Sale"),
            backgroundColor: Colors.lightBlue[700],
            bottom: TabBar(
                labelColor: Colors.lightBlue[700],
                unselectedLabelColor: Colors.white,
                indicatorSize: TabBarIndicatorSize.label,
                indicator: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10)),
                    color: Colors.white),
                tabs: [
                  Tab(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text("With Bill"),
                    ),
                  ),
                  Tab(
                    child: Align(
                      alignment: Alignment.center,
                      child: Text("Without Bill"),
                    ),
                  ),
                ]),
          ),
          body: TabBarView(children: [
            SalePage(
              billNo: widget.billNo,
              customerId: widget.customerId,
              editFlag: widget.editFlag,
              billType: 0,
            ),
            SalePage(
              billNo: widget.billNo,
              customerId: widget.customerId,
              editFlag: widget.editFlag,
              billType: 1,
            )
          ]),
        ));
  }
}

class SalePage extends StatefulWidget {
  final int editFlag, billType;
  final String? customerId, billNo;
  SalePage({required this.editFlag, required this.customerId, required this.billNo, required this.billType});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SalePage();
  }
}

List? productsSale ;
double subtotal = 0, altSubtotal = 0, subtotalTemp = 0, disc = 0;
int? radio;

class _SalePage extends State<SalePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  SharedPreferences? prefs;
  int flag = 0;
  Future<String>? customerResponse;
  TextEditingController? _dateController,
      _nameController,
      _phoneController,
      _emailController,
      _addressController,
      _gstController,
      _referenceController,
      _empController,
      _discController,
      _firmController,
      _billController;
  String? today, billNo, note;
  bool? _autoValidate;
  var _dueInterval = ['15 Days', '30 Days', 'Due on Receipt'];
  var _gstPerc = ['28 %', '18 %', '12 %', '5 %'];
  var _delieveryType = ['Delivered', 'Not Delivered'];
  var sgst = 0.0, cgst = 0.0;
  String? _currentItemSelected,
      _currentBillSelected,
      _currentPercentage,
      _currentDeliverSelected;
  int? index, total = 0;
  DateTime? _dateTime;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _autoValidate = false;
    radio = null;
    subtotal = 0;
    altSubtotal = 0;
    subtotalTemp = 0;
    DateTime now = new DateTime.now();
    today = DateFormat("dd/MM/yyyy").format(now);
    productsSale!.clear();
    _nameController = TextEditingController(text: "");
    _phoneController = TextEditingController(text: "");
    _emailController = TextEditingController(text: "");
    _addressController = TextEditingController(text: "");
    _gstController = TextEditingController(text: "");
    _referenceController = TextEditingController(text: "");
    _empController = TextEditingController(text: "");
    _discController = TextEditingController(text: "0");
    _firmController = TextEditingController(text: "");
    _dateController = TextEditingController(text: today);
    _billController = TextEditingController(text: "");
    if (widget.editFlag == 1) {
      getJsonData().then((value) {
        getItems().then((value) {
          _nameController =
              TextEditingController(text: nameOfCustomer[0]["cust_name"]);
          _phoneController =
              TextEditingController(text: nameOfCustomer[0]["contact"]);
          _emailController =
              TextEditingController(text: nameOfCustomer[0]["emailid"]);
          _addressController = TextEditingController(
              text: nameOfCustomer[0]["address"].toString());
          _gstController = TextEditingController(
              text: nameOfCustomer[0]["GST_no"].toString());
          _firmController = TextEditingController(
              text: (nameOfCustomer[0]["firm_name"] == null
                  ? ''
                  : nameOfCustomer[0]["firm_name"].toString()));
          _referenceController = TextEditingController(text: "-");
          print(productsSale![0]["dt"]);
          _dateController =
              TextEditingController(text: productsSale![0]["chdt"]);
          _empController =
              TextEditingController(text: productsSale![0]["emp_name"]);
          _billController = TextEditingController(text: widget.billNo);
          _discController =
              TextEditingController(text: productsSale![0]["discount_amt"]);
          altSubtotal = double.parse(productsSale![0]["grand_total"]);
          subtotalTemp = double.parse(productsSale![0]["grand_total"]);
          subtotal = double.parse(productsSale![0]["grand_total"]);
          //Navigator.of(_keyLoader.currentContext).pop();
        });
      });
    } else if (widget.editFlag == 2) {
      getBillNo();
      getJsonData().then((value) {
        _nameController =
            TextEditingController(text: nameOfCustomer[0]["cust_name"]);
        _phoneController =
            TextEditingController(text: nameOfCustomer[0]["contact"]);
        _emailController =
            TextEditingController(text: nameOfCustomer[0]["emailid"]);
        _addressController = TextEditingController(
            text: nameOfCustomer[0]["address"].toString());
        _gstController =
            TextEditingController(text: nameOfCustomer[0]["GST_no"].toString());
        _firmController = TextEditingController(
            text: (nameOfCustomer[0]["firm_name"] == null
                ? ''
                : nameOfCustomer[0]["firm_name"].toString()));
        _referenceController = TextEditingController(text: "-");
      });
    } else {
      if (widget.billType == 0) {
        radio = 0;
        getBillNo();
      } else {
        radio = null;
        getBillNoWoGST();
      }

      _nameController = TextEditingController(text: "");
      _phoneController = TextEditingController(text: "");
      _emailController = TextEditingController(text: "");
      _addressController = TextEditingController(text: "");
      _gstController = TextEditingController(text: "");
      _referenceController = TextEditingController(text: "");
      _empController = TextEditingController(text: "");
      _discController = TextEditingController(text: "0");
      _firmController = TextEditingController(text: "");
      _dateController = TextEditingController(text: today);
      _billController = TextEditingController(text: "");
    }
  }

  void radioButtonCreDeb(int? val) {
    setState(() {
      print(val);
      radio = val;
    });
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  Future<String> getJsonData() async {
    prefs = await SharedPreferences.getInstance();
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=individualCustomer&custId=" +
            widget.customerId.toString();

    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      nameOfCustomer = convertDataToJson['data'];
    });

    return "success";
  }

  Future<String> getItems() async {
    var empId = prefs!.getString('empId');
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=editingSaleItems&empId=" +
            empId +
            "&custId=" +
            widget.customerId.toString() +
            "&billNo=" +
            widget.billNo.toString();

    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      productsSale = convertDataToJson['data'];
    });

    return "success";
  }

  Future<String> getBillNo() async {
    prefs = await SharedPreferences.getInstance();
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=generateBill";

    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      billNo = convertDataToJson['data'];
      _billController!.text = billNo.toString();
    });
    print(billNo);

    return "success";
  }

  Future<String> getBillNoWoGST() async {
    prefs = await SharedPreferences.getInstance();
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=generateBillWoGST";

    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      _billController!.text = convertDataToJson['data'].toString();
    });
    print(billNo);

    return "success";
  }

  Future<String> insertSaleEntry(int i) async {
    var empId = prefs!.getString('empId');
    String url;
    if (widget.editFlag != 1) {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertsale";
    } else {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=editingSale";
    }
    print(url);
    print(cgst);
    print(sgst);
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'firm': '1',
          'bill': _billController!.text,
          'txtDate': _dateController!.text,
          'txtMname': widget.editFlag == 1 || widget.editFlag == 2
              ? nameOfCustomer[0]["cid"]
              : nameOfCustomer[index!.toInt()]["cid"],
          'txtaddress': widget.editFlag == 1 || widget.editFlag == 2
              ? nameOfCustomer[0]["address"]
              : nameOfCustomer[index!.toInt()]["address"],
          'txtphone': widget.editFlag == 1 || widget.editFlag == 2
              ? nameOfCustomer[0]["contact"]
              : nameOfCustomer[index!.toInt()]["contact"],
          'txtemail': widget.editFlag == 1 || widget.editFlag == 2
              ? nameOfCustomer[0]["emailid"]
              : nameOfCustomer[index!.toInt()]["emailid"],
          'txtgst': widget.editFlag == 1 || widget.editFlag == 2
              ? nameOfCustomer[0]["GST_no"]
              : nameOfCustomer[index!.toInt()]["GST_no"],
          'empref': '',
          'emp1': empId,
          'stock_id': productsSale![i]["item_id"],
          'item_name': productsSale![i]["item_id"],
          'uom': productsSale![i]["uom"],
          'stock_qty': productsSale![i]["bal_qty"],
          'rate': widget.editFlag == 1
              ? productsSale![i]["rate"].toString()
              : (radio != 0
                      ? double.parse(productsSale![i]["sale_rate"])
                      : (double.parse(productsSale![i]["sale_rate"]) +
                          (double.parse(productsSale![i]["sale_rate"]) *
                                  double.parse(productsSale![i]["gst_per"])) /
                              100))
                  .toStringAsFixed(2),
          'qty': productsSale![i]["qty"],
          'total': productsSale![i]['total'].toStringAsFixed(2),
          'sale_type': '1',
          'gstcat': '1',
          'txttotal': productsSale![i]['total'].toStringAsFixed(2),
          'vcharge': '',
          'sgst1Amt': sgst.toStringAsFixed(2),
          'cgstAmt': cgst.toStringAsFixed(2),
          'igstAmt': '0',
          'gtotal': subtotal.toStringAsFixed(2),
          'cash': '1',
          'getdata': 'Saleentry_wise',
          'onlinepaymenttype': '0',
          'transaction_id': '',
          'bank': '',
          'chk_no': '',
          'cheq_dt': '',
          'advance': '0',
          'ba': subtotal.toStringAsFixed(2),
          'txtdrs': '1',
          'discvl': disc.toStringAsFixed(2),
          'dsAmt': disc.toStringAsFixed(2),
          'dchsn_code': '',
          'dcitemid': ''
        });

    return "success";
  }

  void _onDropDownItemSelected(String newValueSelected) {
    setState(() {
      this._currentItemSelected = newValueSelected;
    });
  }

  void _onDropDownBill(String newValueSelected) {
    setState(() {
      this._currentBillSelected = newValueSelected;
      if (_currentBillSelected == 'With GST') {
        _billController!.text = billNo.toString();
        _currentPercentage = null;
        radio = 0;
      } else {
        radio = null;
        getBillNoWoGST();
        sgst = 0.0;
        cgst = 0.0;
        subtotal = subtotalTemp;
      }
    });
  }

  void _onDropDownDeliverSelected(String newValueSelected) {
    setState(() {
      this._currentDeliverSelected = newValueSelected;
    });
  }

  void _onDropDownPerc(String newValueSelected) {
    setState(() {
      this._currentPercentage = newValueSelected;
      if (_currentPercentage == "12 %") {
        subtotal = subtotalTemp;
        var gst = (subtotal * 6) / 100;
        sgst = gst;
        cgst = gst;
        var percent = (subtotal * 12) / 100;
        subtotal = subtotal + percent;
      } else if (_currentPercentage == "5 %") {
        subtotal = subtotalTemp;
        var gst = (subtotal * 2.5) / 100;
        sgst = gst;
        cgst = gst;
        var percent = (subtotal * 5) / 100;
        subtotal = subtotal + percent;
      } else if (_currentPercentage == "18 %") {
        subtotal = subtotalTemp;
        var gst = (subtotal * 9) / 100;
        sgst = gst;
        cgst = gst;
        var percent = (subtotal * 18) / 100;
        subtotal = subtotal + percent;
      } else if (_currentPercentage == "28 %") {
        subtotal = subtotalTemp;
        var gst = (subtotal * 14) / 100;
        sgst = gst;
        cgst = gst;
        var percent = (subtotal * 28) / 100;
        subtotal = subtotal + percent;
      }
    });
  }

  void change() {
    setState(() {});
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  Future<String> insertSaleItems() async {
    for (var i = 0; i < productsSale!.length; i++) {
      insertSaleEntry(i).then((value) {
        print(value);
      });
    }

    return "Success";
  }

  void submitSale() {
    if (_formKey.currentState!.validate()) {
      flag = 1;
      _formKey.currentState!.save();
      insertSaleItems().then((value) {
        productsSale!.clear();
        _formKey.currentState!.reset();
        _nameController!.text = "";
        _phoneController!.text = "";
        _emailController!.text = "";
        _addressController!.text = "";
        _gstController!.text = "";
        _referenceController!.text = "";
        _empController!.text = "";
        _discController!.text = "";
        _firmController!.text = "";
        _currentItemSelected = null;
        altSubtotal = 0;
        subtotal = 0;
      });
    } else {
      flag = 0;
      // if (_currentItemSelected == null) {
      //   _showDialogBox(context, "Warning", "Please Choose Option");
      // }
      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Form(
     // autovalidate: _autoValidate,
      key: _formKey,
      child: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  widget.billType == 0
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Radio(
                              value: 0,
                              groupValue: radio,
                              onChanged: radioButtonCreDeb,
                            ),
                            GestureDetector(
                              onTap: () {
                                radioButtonCreDeb(0);
                              },
                              child: Container(
                                child: Text(
                                  "Including GST",
                                  style: TextStyle(fontSize: 16),
                                ),
                              ),
                            ),
                            Radio(
                              value: 1,
                              groupValue: radio,
                              onChanged: radioButtonCreDeb,
                            ),
                            GestureDetector(
                              onTap: () {
                                radioButtonCreDeb(1);
                              },
                              child: Container(
                                child: Text(
                                  "Excluding GST",
                                  style: TextStyle(fontSize: 16),
                                ),
                              ),
                            ),
                          ],
                        )
                      : Container(),
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * 0.01),
                        width: MediaQuery.of(context).size.width * 0.5,
                        child: TextFormField(
                          autofocus: false,
                          readOnly:true,
                          validator: (value) {
                            if (value == "") {
                              return "Please Enter Bill no";
                            } else {
                              return null;
                            }
                          },
                          onSaved: (value) {
                            setState(() {
                              this.billNo = value;
                            });
                          },
                          controller: _billController,
                          decoration: InputDecoration(
                            labelText: "Bill No.",
                            contentPadding:
                                EdgeInsets.only(left: 10.0, right: 2.0),
                            fillColor: Colors.white,
                          ),
                          keyboardType: TextInputType.text,
                          style: new TextStyle(
                              fontFamily: "Poppins",
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.01),
                          width: MediaQuery.of(context).size.width * 0.5,
                          child: DateTimeField(
                              format: DateFormat('dd/MM/yyy'),
                              controller: _dateController,
                              initialValue: DateTime.now(),
                              decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.calendar_today)),
                              onShowPicker: (context, currentValue) {
                                return showDatePicker(
                                    context: context,
                                    initialDate: _dateTime == null
                                        ? DateTime.now()
                                        : _dateTime!.toUtc(),
                                    firstDate: DateTime(2001),
                                    lastDate: DateTime(2100));
                              },
                              validator: (val) {
                                if (val != null) {
                                  return null;
                                } else {
                                  return 'Date Field is Empty';
                                }
                              })),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * 0.01),
                        width: MediaQuery.of(context).size.width * 0.5,
                        child: Text(
                          "Add Customer",
                          style: TextStyle(
                              color: Colors.lightBlue[700],
                              fontSize: 20,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                        height: 40,
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.009),
                        child: FloatingActionButton(
                          heroTag: "customer",
                          onPressed: () async {
                            var result = await Navigator.of(context)
                                .push(MaterialPageRoute(builder: (context) {
                              return AddCustomerPage(
                                salePurchase: 0,
                              );
                            }));

                            setState(() {
                              if (result != null) {
                                index = int.parse(result);
                                print(index);
                                _nameController!.text =
                                    nameOfCustomer[index!.toInt()]["cust_name"];
                                _phoneController!.text =
                                    nameOfCustomer[index!.toInt()]["contact"];
                                _emailController!.text =
                                    nameOfCustomer[index!.toInt()]["emailid"];
                                _addressController!.text =
                                    nameOfCustomer[index!.toInt()]["address"].toString();
                                _gstController!.text =
                                    nameOfCustomer[index!.toInt()]["GST_no"].toString();
                                _firmController!.text =
                                    nameOfCustomer[index!.toInt()]["firm_name"] == null
                                        ? ''
                                        : nameOfCustomer[index!.toInt()]["firm_name"]
                                            .toString();
                              }
                            });
                          },
                          child: Icon(
                            Icons.add,
                            color: Colors.white,
                          ),
                          backgroundColor: Colors.lightBlue[700],
                        ),
                      ),
                    ],
                  ),
                  nameOfCustomer != null
                      ? Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width *
                                          0.01),
                                  width:
                                      MediaQuery.of(context).size.width * 0.98,
                                  child: TextFormField(
                                    validator: null,
                                    readOnly: true,
                                    decoration:
                                        InputDecoration(labelText: "Name"),
                                    enabled: false,
                                    controller: _nameController,
                                    keyboardType: TextInputType.text,
                                    style: new TextStyle(
                                        fontFamily: "Poppins", fontSize: 18),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width *
                                          0.01),
                                  width:
                                      MediaQuery.of(context).size.width * 0.98,
                                  child: TextFormField(
                                    validator: null,
                                    readOnly: true,
                                    decoration:
                                        InputDecoration(labelText: "Firm Name"),
                                    enabled: false,
                                    controller: _firmController,
                                    keyboardType: TextInputType.text,
                                    style: new TextStyle(
                                        fontFamily: "Poppins", fontSize: 18),
                                  ),
                                ),
                              ],
                            ),
                            // Row(
                            //   children: <Widget>[
                            //     Container(
                            //       padding: EdgeInsets.only(
                            //           left:
                            //               MediaQuery.of(context).size.width *
                            //                   0.01),
                            //       width:
                            //           MediaQuery.of(context).size.width * 0.5,
                            //       child: TextFormField(
                            //         validator: null,
                            //         readOnly: true,
                            //         decoration: InputDecoration(
                            //             labelText: "Email ID"),
                            //         enabled: false,
                            //         controller: _emailController,
                            //         keyboardType: TextInputType.text,
                            //         style: new TextStyle(
                            //           fontFamily: "Poppins",
                            //         ),
                            //       ),
                            //     ),
                            //     Container(
                            //       padding: EdgeInsets.only(
                            //           left:
                            //               MediaQuery.of(context).size.width *
                            //                   0.01),
                            //       width:
                            //           MediaQuery.of(context).size.width * 0.5,
                            //       child: TextFormField(
                            //         validator: null,
                            //         readOnly: true,
                            //         enabled: false,
                            //         decoration: InputDecoration(
                            //             labelText: "Phone No."),
                            //         controller: _phoneController,
                            //         keyboardType: TextInputType.text,
                            //         style: new TextStyle(
                            //           fontFamily: "Poppins",
                            //         ),
                            //       ),
                            //     )
                            //   ],
                            // ),
                            // Row(
                            //   children: <Widget>[
                            //     Container(
                            //       padding: EdgeInsets.only(
                            //           left:
                            //               MediaQuery.of(context).size.width *
                            //                   0.01),
                            //       width: MediaQuery.of(context).size.width *
                            //           0.98,
                            //       child: TextFormField(
                            //         validator: null,
                            //         readOnly: true,
                            //         enabled: false,
                            //         maxLines: 2,
                            //         decoration: InputDecoration(
                            //           labelText: "Address",
                            //         ),
                            //         controller: _addressController,
                            //         keyboardType: TextInputType.text,
                            //         style: new TextStyle(
                            //           fontFamily: "Poppins",
                            //         ),
                            //       ),
                            //     )
                            //   ],
                            // ),
                            // Row(
                            //   children: <Widget>[
                            //     Container(
                            //       padding: EdgeInsets.only(
                            //           left:
                            //               MediaQuery.of(context).size.width *
                            //                   0.01),
                            //       width: MediaQuery.of(context).size.width *
                            //           0.98,
                            //       child: TextFormField(
                            //         autofocus: false,
                            //         validator: null,
                            //         readOnly: true,
                            //         decoration: InputDecoration(
                            //           labelText: "GST No",
                            //         ),
                            //         enabled: false,
                            //         controller: _gstController,
                            //         keyboardType: TextInputType.text,
                            //         style: new TextStyle(
                            //           fontFamily: "Poppins",
                            //           fontSize: 18,
                            //         ),
                            //       ),
                            //     ),
                            //   ],
                            // ),
                            // Row(
                            //   children: <Widget>[
                            //     Container(
                            //       padding: EdgeInsets.only(
                            //           left:
                            //               MediaQuery.of(context).size.width *
                            //                   0.01),
                            //       width:
                            //           MediaQuery.of(context).size.width * 0.5,
                            //       child: TextFormField(
                            //         autofocus: false,
                            //         validator: (value) {
                            //           if (value == "") {
                            //             return "Please Enter Emp name";
                            //           } else {
                            //             return null;
                            //           }
                            //         },
                            //         decoration: InputDecoration(
                            //             labelText: "Emp Name"),
                            //         controller: _empController,
                            //         keyboardType: TextInputType.text,
                            //         style: new TextStyle(
                            //           fontFamily: "Poppins",
                            //         ),
                            //       ),
                            //     ),
                            //     Container(
                            //       padding: EdgeInsets.only(
                            //           left:
                            //               MediaQuery.of(context).size.width *
                            //                   0.01),
                            //       width:
                            //           MediaQuery.of(context).size.width * 0.5,
                            //       child: TextFormField(
                            //         validator: (value) {
                            //           if (value == "") {
                            //             return "Please Enter reference name";
                            //           } else {
                            //             return null;
                            //           }
                            //         },
                            //         decoration: InputDecoration(
                            //             labelText: "Reference By"),
                            //         controller: _referenceController,
                            //         keyboardType: TextInputType.text,
                            //         style: new TextStyle(
                            //           fontFamily: "Poppins",
                            //         ),
                            //       ),
                            //     )
                            //   ],
                            // )
                          ],
                        )
                      : Container(),
                  Row(
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.96,
                        margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.height * 0.01),
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.lightBlue[900],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Due"),
                              items:
                                  _dueInterval.map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.normal,
                                            color: Colors.lightBlue[700])));
                              }).toList(),
                              onChanged: (String? newValueSelected) =>
                                  _onDropDownItemSelected(newValueSelected.toString()),
                              value: _currentItemSelected,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  widget.editFlag != 1
                      ? Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.01),
                              width: MediaQuery.of(context).size.width * 0.5,
                              child: Text(
                                "Add Product",
                                style: TextStyle(
                                    color: Colors.lightBlue[700],
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                            Container(
                              height: 40,
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.009),
                              child: FloatingActionButton(
                                heroTag: "product",
                                onPressed: () {
                                  Navigator.of(context).push(
                                      MaterialPageRoute(builder: (context) {
                                    return AddProductPage(
                                        salePurchase: 0, radio: radio!.toInt());
                                  })).then((value) {
                                    change();
                                  });
                                },
                                child: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                ),
                                backgroundColor: Colors.lightBlue[700],
                              ),
                            ),
                          ],
                        )
                      : Container(),
                  Column(
                    children: List.generate(
                        productsSale == null ? 0 : productsSale!.length,
                        (int index) {
                      return ProductDetails(
                        index: index,
                        change: change,
                        editFlag: widget.editFlag,
                      );
                    }),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          child: Text(
                            "Total : " +
                                double.parse(altSubtotal.toStringAsFixed(2))
                                    .toString(),
                            style: TextStyle(
                                fontSize: 17, fontWeight: FontWeight.normal),
                          ),
                        )
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.01,
                        ),
                        width: MediaQuery.of(context).size.width * 0.5,
                        child: TextFormField(
                          validator: (value) {
                            if (value == "") {
                              return "Please Enter Discount";
                            } else {
                              return null;
                            }
                          },
                          autofocus: false,
                          decoration: InputDecoration(
                            labelText: "Discount",
                            contentPadding: EdgeInsets.only(
                                top:
                                    MediaQuery.of(context).size.height * 0.015),
                            suffixText: "%",
                          ),
                          controller: _discController,
                          keyboardType: TextInputType.number,
                          style: new TextStyle(
                            fontFamily: "Poppins",
                          ),
                          onChanged: (value) {
                            setState(() {
                              if (value != "") {
                                subtotal = altSubtotal;
                                disc = (subtotal * int.parse(value)) / 100;
                                subtotal = subtotal - disc;
                                subtotalTemp = subtotal;
                              } else {
                                disc = 0;
                                subtotal = altSubtotal;
                              }
                            });
                          },
                        ),
                      ),
                      Container(
                        child: Text(
                          "Discount Price : " + disc.toString(),
                          style: TextStyle(fontSize: 17),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      //     Radio(
                      //       value: 0,
                      //       groupValue: radio,
                      //       onChanged: radioButtonCreDeb,
                      //     ),
                      //      Container(
                      //   child: Text(
                      //     "28 %",
                      //     style: TextStyle(fontSize: 16),
                      //   ),
                      // ),
                      // Radio(
                      //   value: 1,
                      //   groupValue: radio,
                      //   onChanged: radioButtonCreDeb,
                      // ),
                      // Container(
                      //   child: Text(
                      //     "18 %",
                      //     style: TextStyle(fontSize: 16),
                      //   ),
                      // ),
                      widget.billType == 0 && radio == 1
                          ? Container(
                              height: MediaQuery.of(context).size.height * 0.05,
                              width: MediaQuery.of(context).size.width * 0.5,
                              margin: EdgeInsets.only(
                                  left: MediaQuery.of(context).size.height *
                                      0.01),
                              decoration: BoxDecoration(
                                border: Border(bottom: BorderSide(width: 1.0)),
                              ),
                              child: DropdownButtonHideUnderline(
                                child: ButtonTheme(
                                  alignedDropdown: true,
                                  child: DropdownButton<String>(
                                    itemHeight: 50,
                                    elevation: 1,

                                    autofocus: true,
                                    focusColor: Colors.lightBlue[900],
                                    //style: Theme.of(context).textTheme.title,
                                    isExpanded: true,

                                    hint: Text("Choose GST %"),
                                    items: _gstPerc
                                        .map((String dropDownStringItem) {
                                      return DropdownMenuItem<String>(
                                          value: dropDownStringItem,
                                          child: Text(dropDownStringItem,
                                              style: TextStyle(
                                                  fontSize: 17,
                                                  fontWeight: FontWeight.normal,
                                                  color:
                                                      Colors.lightBlue[700])));
                                    }).toList(),
                                    onChanged: (String? newValueSelected) =>
                                        _onDropDownPerc(newValueSelected.toString()),
                                    value: _currentPercentage,
                                  ),
                                ),
                              ),
                            )
                          : Container(),
                      Spacer(),
                      widget.billType == 0
                          ? Container(
                              child: Text(
                                "GST : " +
                                    double.parse(
                                            (cgst + sgst).toStringAsFixed(2))
                                        .toString(),
                                style: TextStyle(
                                    fontSize: 17,
                                    fontWeight: FontWeight.normal),
                              ),
                            )
                          : Container()
                    ],
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      //     Radio(
                      //       value: 2,
                      //       groupValue: radio,
                      //       onChanged: radioButtonCreDeb,
                      //     ),
                      //      Container(
                      //   child: Text(
                      //     "12 %",
                      //     style: TextStyle(fontSize: 16),
                      //   ),
                      // ),
                      // Radio(
                      //   value: 3,
                      //   groupValue: radio,
                      //   onChanged: radioButtonCreDeb,
                      // ),
                      // Container(
                      //   child: Text(
                      //     "5 %",
                      //     style: TextStyle(fontSize: 16),
                      //   ),
                      // ),
                      Spacer(),
                      Container(
                          child: Container(
                        child: Text(
                          "Subtotal : " +
                              double.parse(subtotal.toStringAsFixed(2))
                                  .toString(),
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                      )),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.96,
                        margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.height * 0.01),
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.lightBlue[900],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Delivery Type"),
                              items: _delieveryType
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.lightBlue[700])));
                              }).toList(),
                              onChanged: (String? newValueSelected) =>
                                  _onDropDownDeliverSelected(newValueSelected.toString()),
                              value: _currentDeliverSelected,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width * 0.01),
                        width: MediaQuery.of(context).size.width * 0.98,
                        child: TextFormField(
                          validator: null,
                          maxLines: 2,
                          decoration: InputDecoration(
                              labelText: "Note",
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                              )),
                          onSaved: (value) {
                            setState(() {
                              note = value;
                            });
                          },
                          keyboardType: TextInputType.text,
                          style: new TextStyle(
                              fontFamily: "Poppins", fontSize: 18),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          Container(
            height: 50,
            width: MediaQuery.of(context).size.width * 1,
            color: Colors.lightBlue[700],
            child: FlatButton(
                onPressed: () async {
                  if (widget.editFlag == 1) {
                    submitSale();
                    change();
                    if (flag == 1) {
                      Navigator.of(context).pop();
                      _showDialogBox(
                          context, "Updated to Sale Entry Successfully", "");
                    }
                  } else {
                    submitSale();
                    change();
                    if (flag == 1) {
                      Navigator.of(context).pop();
                      _showDialogBox(
                          context, "Saved to Sale List Successfully", "");
                    }
                  }
                },
                child: Text(
                  widget.editFlag == 1 ? "Update" : "Save",
                  style: TextStyle(color: Colors.white),
                )),
          )
        ],
      ),
    );
  }
}

typedef CallBack = void Function();

class ProductDetails extends StatefulWidget {
  final int index, editFlag;
  final CallBack change;
  ProductDetails({required this.index,required this.change,required this.editFlag});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProductDetails();
  }
}

class _ProductDetails extends State<ProductDetails> {
  double? total, temp;
  TextEditingController? _qtyController, _rateController;
  @override
  void initState() {
    super.initState();
    if (widget.editFlag == 1) {
      total = double.parse(productsSale![widget.index]["total"]);
      _qtyController =
          TextEditingController(text: productsSale![widget.index]["qty"]);
    } else {
      total = 0;
      temp = 0;
      _qtyController = TextEditingController(text: "");
      print("radio " + radio.toString());
      if (radio != 0) {
        _rateController =
            TextEditingController(text: productsSale![widget.index]["rate"]);
      } else {
        _rateController = TextEditingController(
            text: (double.parse(productsSale![widget.index]["sale_rate"]) +
                    (double.parse(productsSale![widget.index]["sale_rate"]) *
                            double.parse(
                                productsSale![widget.index]["gst_per"])) /
                        100)
                .toString());
      }
      print(total);
    }
  }

  callback() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      color: Colors.grey[200],
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.height * 0.05),
                child: IconButton(
                    icon: Icon(Icons.delete),
                    color: Colors.red,
                    iconSize: 30,
                    onPressed: () {
                      productsSale!.remove(productsSale![widget.index]);
                      print(productsSale);
                      callback();
                      widget.change();
                    }),
              ),
              Container(
                height: 100,
                width: 75,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(color: Colors.grey, width: 1)),
                child: Image.network(
                  "https://qot.constructionsmall.com/" +
                      productsSale![widget.index]["img"],
                  loadingBuilder: (context, child, progress) {
                    return progress == null ? child : LinearProgressIndicator();
                  },
                  fit: BoxFit.scaleDown,
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.01),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            child: Text(
                              productsSale![widget.index]["newitem"].toString(),
                              style: TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          // Container(
                          //   child: Text(
                          //     "₹ " +
                          //         (widget.editFlag == 1
                          //                 ? productsSale[widget.index]["rate"]
                          //                 : radio != 0
                          //                     ? productsSale[widget.index]
                          //                         ["sale_rate"]
                          //                     : (double.parse(
                          //                                 productsSale[widget.index]
                          //                                     ["sale_rate"]) +
                          //                             (double.parse(productsSale[
                          //                                             widget
                          //                                                 .index]
                          //                                         [
                          //                                         "sale_rate"]) *
                          //                                     double.parse(
                          //                                         productsSale[widget.index]
                          //                                             ["gst_per"])) /
                          //                                 100)
                          //                         .toString())
                          //             .toString(),
                          //     style: TextStyle(
                          //         fontSize: 16, fontWeight: FontWeight.w500),
                          //   ),
                          // ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.3,
                            child: TextFormField(
                              autofocus: false,
                              controller: _rateController,
                              validator: (value) {
                                if (value == "" ||
                                    value == "0" ||
                                    value == "0.0") {
                                  return "Please Enter Price";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                labelText: "Price",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                              ),
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly
                              ],
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true, signed: false),
                              style: new TextStyle(
                                fontFamily: "Poppins",
                              ),
                              onChanged: (value) {},
                            ),
                          ),
                          Container(
                            child: Text(
                              "Total = " + total.toString(),
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w500),
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.3,
                            child: TextFormField(
                              autofocus: false,
                              controller: _qtyController,
                              validator: (value) {
                                if (value == "") {
                                  return "Please Enter Quantity";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                  labelText: "Qty",
                                  contentPadding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.015),
                                  suffixIcon: Container(
                                    child: IconButton(
                                      icon: Icon(Icons.help),
                                      alignment: Alignment.bottomRight,
                                      padding: EdgeInsets.all(0),
                                      onPressed: () {
                                        showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return AlertDialog(
                                                title: Text(
                                                  "Stock",
                                                  style: TextStyle(
                                                      color: Colors.red),
                                                ),
                                                content: Text(
                                                    "Stock is coming soon"),
                                                actions: <Widget>[
                                                  FlatButton(
                                                      onPressed: () =>
                                                          Navigator.of(context)
                                                              .pop(),
                                                      child: Text("Close"))
                                                ],
                                              );
                                            });
                                      },
                                    ),
                                  )),
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly
                              ],
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true, signed: false),
                              style: new TextStyle(
                                fontFamily: "Poppins",
                              ),
                              onChanged: (value) {
                                print(value);
                                setState(() {
                                  if (widget.editFlag == 1) {
                                    if (value != "") {
                                      productsSale![widget.index]["qty"] = value;
                                      print("products : " +
                                          productsSale![widget.index]
                                              .toString());

                                      subtotal = subtotal - total!.toDouble();
                                      total =
                                          double.parse(_rateController!.text) *
                                              int.parse(value);
                                      productsSale![widget.index]["total"] =
                                          total;

                                      subtotal = (subtotal) + total!.toDouble();
                                      subtotalTemp = subtotal;
                                      altSubtotal = subtotal;
                                      print(subtotal);
                                      print(altSubtotal);
                                    } else {
                                      subtotal = subtotal - total!.toDouble();
                                      altSubtotal = subtotal;
                                      subtotalTemp = altSubtotal;
                                      total = 0;
                                    }
                                  } else {
                                    if (value != "") {
                                      productsSale![widget.index]["qty"] = value;
                                      print("rate : " + _rateController!.text);

                                      subtotal = subtotal - total!.toDouble();
                                      total = (radio != 0
                                              ? double.parse(
                                                  _rateController!.text)
                                              : (double.parse(
                                                      _rateController!.text) +
                                                  (double.parse(_rateController!
                                                              .text) *
                                                          double.parse(
                                                              productsSale![widget
                                                                      .index][
                                                                  "gst_per"])) /
                                                      100)) *
                                          int.parse(value);
                                      productsSale![widget.index]["total"] =
                                          total;
                                      print(total);

                                      subtotal = (subtotal) + total!.toDouble();
                                      subtotalTemp = subtotal;
                                      altSubtotal = subtotal;
                                      print("subtotal temp : " +
                                          subtotalTemp.toString());
                                      print(altSubtotal);
                                    } else {
                                      subtotal = subtotal - total!.toDouble();
                                      altSubtotal = subtotal;
                                      subtotalTemp = altSubtotal;
                                      total = 0;
                                    }
                                  }
                                });
                                widget.change();
                                print(total);
                              },
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Divider(
            thickness: 1.0,
          )
        ],
      ),
    );
  }
}
