import 'package:smit_sp/pages/InvoiceList.dart';
import 'package:smit_sp/pages/MapInvoiceList.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'BottomSheetWidget.dart';

class VendorDetails extends StatefulWidget {
  final String name, custId;
  VendorDetails({required this.name, required this.custId});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _VendorDetails();
  }
}

class _VendorDetails extends State<VendorDetails> {
  List? vendorStatement, vendorTotalInvoice;
  Future<String>? vendorStatementResponse;
  int flag = 0;
  double totalPayment = 0.0, temp = 0.0;

  Future<String> getJsonData() async {
    print(widget.custId);
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=customerLedgerPurchase&custId=" +
            widget.custId;
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      vendorStatement = convertDataToJson['data'];
    });

    for (var i = 0; i < vendorStatement!.length; i++) {
      if (vendorStatement![i]["c_type"] == "dr") {
        temp = temp + double.parse(vendorStatement![i]["pay_amt"]);
      }

      if (vendorStatement![i]["c_type"] == "cr") {
        temp = temp + double.parse(vendorStatement![i]["paid_amt"]);
      }
      print("temp" + temp.toString());
    }

    return "success";
  }

  Future<String> getTotal() async {
    print(widget.custId);
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=totalPaymentPurchase&custId=" +
            widget.custId;
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      vendorTotalInvoice = convertDataToJson['data'];
    });

    for (var i = 0; i < vendorTotalInvoice!.length; i++) {
      if (vendorTotalInvoice![i]["transaction_type"] == null
          ? false
          : vendorTotalInvoice![i]["transaction_type"] == "dr") {
        temp = temp - double.parse(vendorTotalInvoice![i]["paid_amt"]);
      }
    }
    totalPayment =
        totalPayment + double.parse(vendorTotalInvoice![0]["grand_total"]);
    print(totalPayment);
    return "success";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    totalPayment = 0.0;
    vendorStatementResponse = getJsonData();
    getTotal();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  widget.name,
                  style: TextStyle(fontSize: 16),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  "Vendor",
                  style: TextStyle(fontSize: 10),
                ),
              ],
            )
          ],
        ),
        backgroundColor: Colors.lightBlue[700],
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: 80,
            width: MediaQuery.of(context).size.width,
            color: Colors.lightBlue[700],
            child: Column(
              children: <Widget>[
                Container(
                    child: Text(
                  totalPayment > temp
                      ? "You will receive"
                      : "You will pay",
                  style: TextStyle(color: Colors.grey[300], fontSize: 10),
                )),
                Container(
                  child: Text(
                    "₹ " +
                        (totalPayment > temp
                        ? (totalPayment - temp)
                        : (temp - totalPayment))
                            .toString(),
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w500),
                  ),
                ),
                Container(
                  child: Text(
                    "Balance Outstanding",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  ),
                )
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.16,
            child: GridView.count(
              crossAxisCount: 3,
              primary: false,
              childAspectRatio: 6 / 5,
              padding: EdgeInsets.only(top: 10.0),
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              children: <Widget>[
                GestureDetector(
                    onTap: () {
                      Future<void> sheetController = showModalBottomSheet<void>(
                          context: context,
                          builder: (context) => BottomSheetWidget(
                                custId: widget.custId,
                                sale: 0,
                                purchase: 1,
                              ),
                          backgroundColor: Colors.transparent);

                      sheetController.then((value) {
                        print("sheet closed");
                      });
                    },
                    child: Card(
                      elevation: 5.0,
                      child: Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Colors.deepOrange, Colors.orange])),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Icon(
                                Icons.add,
                                size: 20,
                                color: Colors.white,
                              )),
                            ),
                            new Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Text(
                                "     Add\n New Entry",
                                style: new TextStyle(
                                    color: Colors.white, fontSize: 15.0),
                              )),
                            ),
                          ],
                        ),
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return InvoiceList(
                          custId: widget.custId,
                          salePurchase: 1,
                        );
                      })).then((value) {
                        setState(() {
                          totalPayment = 0.0;
                          temp = 0.0;
                        });
                        this.getJsonData();
                        this.getTotal();
                      });
                    },
                    child: Card(
                      elevation: 5.0,
                      child: Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Colors.blue, Colors.lightBlue])),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Icon(
                                Icons.assessment,
                                size: 20,
                                color: Colors.white,
                              )),
                            ),
                            new Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Text(
                                "Invoice\n   List",
                                style: new TextStyle(
                                    color: Colors.white, fontSize: 15.0),
                              )),
                            ),
                          ],
                        ),
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      setState(() {});
                    },
                    child: Card(
                      elevation: 5.0,
                      child: Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Colors.green, Colors.lightGreen])),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Icon(Icons.compare_arrows,
                                      size: 20, color: Colors.white)),
                            ),
                            new Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Text(
                                "     View\nStatement",
                                style: new TextStyle(
                                    color: Colors.white, fontSize: 15.0),
                              )),
                            ),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
          Expanded(
            child: vendorStatement == null
                ? Center(
                    child: Text("No Transaction Data..."),
                  )
                : FutureBuilder<String>(
                    future: vendorStatementResponse,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return ListView.separated(
                          separatorBuilder: (BuildContext context, int index) =>
                              Divider(
                            thickness: 0.0,
                            color: Colors.lightBlue[700],
                          ),
                          itemCount: vendorStatement == null
                              ? 0
                              : vendorStatement!.length,
                          itemBuilder: (BuildContext context, int index) {
                            //print(vendorStatement[index]["paid_amt"]);
                            return Container(
                              child: Card(
                                elevation: 3.0,
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(
                                              left: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.01,
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01),
                                          child: Text(
                                            vendorStatement![index]["paid_amt"] != "0"
                                                ? (vendorStatement![index]["bill_no"] == '-'
                                                    ? (vendorStatement![index]["c_type"] == "cr"
                                                        ? "Payment received On Account " +
                                                            vendorStatement![index]
                                                                    ["recpt_no"]
                                                                .toString() +
                                                            " by " +
                                                            (vendorStatement![index]["payment_mode"] == null
                                                                ? "Cash"
                                                                : vendorStatement![index][
                                                                    "payment_mode"])
                                                        : "Payment paid On Account " +
                                                            vendorStatement![index]
                                                                    ["recpt_no"]
                                                                .toString()
                                                                .toString() +
                                                            " by " +
                                                            vendorStatement![index]["payment_mode"]
                                                                .toString())
                                                    : (vendorStatement![index]["recpt_no"] != "0" || vendorStatement![index]["bill_no"] != "0"
                                                        ? (vendorStatement![index]["c_type"] == "cr"
                                                            ? "Payment received against Receipt " +
                                                                vendorStatement![index]["recpt_no"]
                                                                    .toString() +
                                                                " by " +
                                                                (vendorStatement![index]["payment_mode"] == null
                                                                    ? "Cash"
                                                                    : vendorStatement![index]
                                                                        ["payment_mode"])
                                                            : "Payment paid to receipt " + vendorStatement![index]["recpt_no"].toString() + " by " + vendorStatement![index]["payment_mode"].toString())
                                                        : (vendorStatement![index]["recpt_no"] == "0" && vendorStatement![index]["bill_no"] == "0" && vendorStatement![index]["c_type"] == "cr" ? "Payment taken Opening Stock " : "Payment given Opening Stock")))
                                                : "invoice issued against " + vendorStatement![index]["bill_no"],
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        vendorStatement![index]["recpt_no"] ==
                                                    "-" &&
                                                vendorStatement![index]
                                                        ["c_type"] ==
                                                    "dr"
                                            ? Container(
                                                child: RaisedButton(
                                                  onPressed: () {
                                                    Navigator.of(context).push(
                                                        MaterialPageRoute(
                                                            builder: (context) {
                                                      return MapInvoiceList(
                                                        custId: vendorStatement![
                                                            index]["cust_id"],
                                                        salePurchase: 1,
                                                        bkId: vendorStatement![
                                                                index]
                                                            ["bahi_khata_id"],
                                                        date: vendorStatement![
                                                            index]["date"],
                                                      );
                                                    })).then((value) {
                                                      this.getJsonData();
                                                      this.getTotal();
                                                    });
                                                  },
                                                  child: Text("MAP"),
                                                  textColor: Colors.white,
                                                  color: Colors.lightBlue[700],
                                                ),
                                              )
                                            : Container()
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(
                                              left: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.01,
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01),
                                          child: Text(
                                            vendorStatement![index]["date"]
                                                .toString(),
                                            style: TextStyle(
                                                color: Colors.grey[700]),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              right: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.01,
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01),
                                          child: Text(
                                            vendorStatement![index]
                                                            ["paid_amt"]
                                                        .toString() !=
                                                    "0"
                                                ? "₹ " +
                                                    vendorStatement![index]
                                                            ["paid_amt"]
                                                        .toString()
                                                : "₹ " +
                                                    vendorStatement![index]
                                                        ["pay_amt"],
                                            style: TextStyle(
                                                color: vendorStatement![index]
                                                                ["paid_amt"]
                                                            .toString() !=
                                                        "0"
                                                    ? vendorStatement![index]
                                                                ["c_type"] ==
                                                            "cr"
                                                        ?Colors.green[600]
                                                        : Colors.deepOrange
                                                    : Colors.red[600]),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                        );
                      } else {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                  ),
          )
        ],
      ),
    );
  }
}
