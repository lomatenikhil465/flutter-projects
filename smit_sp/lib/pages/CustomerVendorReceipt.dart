import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:bhandari/pages/CustomerDetails.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'Customer.dart';
import 'VendorDetails.dart';

class CustomerVendorReceiptPage extends StatefulWidget {
  final int salePurchase, payReceive;
  CustomerVendorReceiptPage({this.salePurchase, this.payReceive});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CustomerVendorReceiptPage();
  }
}

List customerInfo;
List _searchResult;
Future<String> customerResponse;

class _CustomerVendorReceiptPage extends State<CustomerVendorReceiptPage> {
  int radio;
  TextEditingController _controller;

  Future<String> getJsonData() async {
    String url;
    if (widget.salePurchase == 0) {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=customer";
    } else {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=customerPurchase";
    }
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      customerInfo = convertDataToJson['data'];
    });

    return "success";
  }

  void onSearchTextChanged(String text) async {
    _searchResult.clear();
    if (text.isEmpty) {
      setState(() {});
      //return ;
    }

    customerInfo.forEach((userDetail) {
      print("search result : " + userDetail["cust_name"].toString());
      if (userDetail["cust_name"].toLowerCase().contains(text.toLowerCase())) {
        _searchResult.add(userDetail);
      }
    });

    setState(() {});
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _searchResult = new List();
    _controller = TextEditingController(text: "");
    customerResponse = getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Column(
      children: [
        Container(
          height: 55,
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            onChanged: (value) {
              onSearchTextChanged(value);
            },
            controller: _controller,
            decoration: InputDecoration(
                labelText: "Search",
                hintText: "Search",
                prefixIcon: Icon(Icons.search),
                contentPadding: EdgeInsets.only(top: 10),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5.0)))),
          ),
        ),
        Expanded(
          child: FutureBuilder<String>(
            future: customerResponse,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return customerInfo == null
                    ? Center(
                        child: Text("No Data Available"),
                      )
                    : (_searchResult == null
                                ? false
                                : _searchResult.length != 0) ||
                            _controller.text.isNotEmpty
                        ? ListView.separated(
                            separatorBuilder:
                                (BuildContext context, int index) => Divider(
                              thickness: 0.0,
                              color: Colors.lightBlue[900],
                            ),
                            itemCount: _searchResult == null
                                ? 0
                                : _searchResult.length,
                            itemBuilder: (BuildContext context, int index) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(builder: (context) {
                                    return MainReceiptPage(
                                      index: index,
                                      payReceive: widget.payReceive,
                                      salePurchase: widget.salePurchase,
                                      getData: 0,
                                    );
                                  }));
                                },
                                child: Container(
                                  height: 60,
                                  child: Card(
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.01,
                                                  top: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.01),
                                              child: Text(_searchResult[index]
                                                      ["cust_name"]
                                                  .toString()),
                                            )
                                          ],
                                        ),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Container(
                                              child: Icon(
                                                Icons.location_on,
                                                color: Colors.grey,
                                                size: 15,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.01,
                                                  top: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.01),
                                              child: Text(
                                                _searchResult[index]["address"],
                                                style: TextStyle(
                                                    color: Colors.grey),
                                              ),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          )
                        : ListView.separated(
                            separatorBuilder:
                                (BuildContext context, int index) => Divider(
                              thickness: 0.0,
                              color: Colors.lightBlue[900],
                            ),
                            itemCount:
                                customerInfo == null ? 0 : customerInfo.length,
                            itemBuilder: (BuildContext context, int index) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(builder: (context) {
                                    return MainReceiptPage(
                                      index: index,
                                      payReceive: widget.payReceive,
                                      salePurchase: widget.salePurchase,
                                      getData: 0,
                                    );
                                  }));
                                },
                                child: Container(
                                  height: 60,
                                  child: Card(
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              padding: EdgeInsets.only(
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.01,
                                                  top: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.01),
                                              child: Text(customerInfo[index]
                                                      ["cust_name"]
                                                  .toString()),
                                            )
                                          ],
                                        ),
                                        Row(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: <Widget>[
                                            Container(
                                              child: Icon(
                                                Icons.location_on,
                                                color: Colors.grey,
                                                size: 15,
                                              ),
                                            ),
                                            Container(
                                              padding: EdgeInsets.only(
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.01,
                                                  top: MediaQuery.of(context)
                                                          .size
                                                          .height *
                                                      0.01),
                                              child: Text(
                                                customerInfo[index]["address"],
                                                style: TextStyle(
                                                    color: Colors.grey),
                                              ),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              );
                            },
                          );
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ),
      ],
    );
  }
}

class MainReceiptPage extends StatefulWidget {
  final int? index, salePurchase, payReceive, getData;
  final String? custId;
  MainReceiptPage(
      {required this.index,
      required this.salePurchase,
      required this.payReceive,
      required this.getData,
       this.custId});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MainReceiptPage();
  }
}

class _MainReceiptPage extends State<MainReceiptPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  GlobalKey<AutoCompleteTextFieldState<Customer>> key = new GlobalKey();
  TextEditingController _dateController,
      _customerController,
      _firmController,
      _contactController,
      _emailController;
  TextEditingController _chequeDateController;
  String bankName, transId, chequeNo, bkId;
  SharedPreferences prefs;
  List indCustomer, temp, invoiceDetails;
  List<Map> saleBills, purchaseBills;
  var temp2;
  var billType = ['On Account', 'Bill Against', 'Advance'];
  int radio = 0;
  Future<String> indCustomerResponse;
  DateTime _dateTime;
  var _modeOfPayment = ['Cash', 'Cheque', 'Online'];
  String _currentItemSelected, amount, _currentBill, _currentBillNo;

  Future<String> getJsonData() async {
    prefs = await SharedPreferences.getInstance();
    String url;
    if (widget.salePurchase == 0) {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=individualCustomer&custId=" +
              widget.custId;
    } else {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=individualCustomerPurchase&custId=" +
              widget.custId;
    }
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      indCustomer = convertDataToJson['data'];
      
    });
    _customerController.text = indCustomer[0]["cust_name"];
    _firmController.text =
        indCustomer[0]["firm_name"] == null ? "" : indCustomer[0]["firm_name"];
    _contactController.text = widget.salePurchase == 0
        ? (indCustomer[0]["contact"] == null ? "" : indCustomer[0]["contact"])
        : (indCustomer[0]["contactno"] == null
            ? ""
            : indCustomer[0]["contactno"]);
    _emailController.text = widget.salePurchase == 0
        ? (indCustomer[0]["emailid"] == null ? "" : indCustomer[0]["emailid"])
        : (indCustomer[0]["email"] == null ? "" : indCustomer[0]["email"]);

    return "success";
  }

  Future<String> getCust() async {
    String url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=findCustomer&contact=" + _contactController.text + "&custName="+ _customerController.text;
    
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      customerInfo = convertDataToJson['data'];
    });

    return "success";
  }

  Future<String> getSaleBills() async {
    String url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=getSaleBills&custId="+ (widget.getData == 1 ? widget.custId : temp[0]["cid"]);
    
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      saleBills = List<Map>.from(convertDataToJson['data'] as List);
     
    });

    for (var i = 0; i < saleBills.length; i++) {
      if(saleBills[i]["current_balance"] == '0'){
        saleBills.removeAt(i);
        i = 0;
      }
    }
    

    return "success";
  }

  Future<String> getPurchaseBills() async {
    String url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=getSaleBills&custId="+ (widget.getData == 1 ? widget.custId : temp[0]["cid"]);
    
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      purchaseBills = convertDataToJson['data'];
    });

    return "success";
  }

  Future<String> getInvoiceDetails() async {
    prefs = await SharedPreferences.getInstance();
    String url;
    if (widget.salePurchase == 0) {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=invoiceDetails&custId=" +
             (widget.getData == 1 ?  widget.custId : temp[0]["cid"].toString()) +
              "&billNo=" +
              _currentBillNo;
    } else {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=invoiceDetailsPurchase&custId=" +
              (widget.getData == 1 ?  widget.custId : temp[0]["cid"].toString()) +
              "&billNo=" +
              _currentBillNo;
    }
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    
    setState(() {
      var convertDataToJson = json.decode(response.body);
      invoiceDetails = convertDataToJson['data'];
    });
    print("invoiceDetails " + invoiceDetails.toString());

    return "success";
  }


  Future<String> insertCustomer() async {
    String url=
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertCustomer&";
    
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'custName': _customerController.text,
          'firm_name': _firmController.text,
          'address': "-",
          'contact': _contactController.text,
          'email': _emailController.text,
          'cst': '-',
          'gst': '-',
          'state':'0'
        }).then((value) {
      print(value.statusCode);
    });
    

    //print(response.reasonPhrase);

    return "success";
  }

  Future<String> insertReceipt() async {
    var empId = prefs.getString('empId');
    print(temp[0]["cid"].toString());

    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertReceipt&";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'custId': (widget.getData == 1 ?  widget.custId : temp[0]["cid"].toString()),
          'bank': bankName == null ? "-" : bankName,
          'chk_no': chequeNo == null ? "-" :chequeNo,
          'cheq_dt': '',
          'amt': amount == null ? "-" :amount,
          'emp_id': empId,
          'ba': (double.parse(invoiceDetails[0]["grand_total"]) -
                  double.parse(invoiceDetails[0]["paid"]) -
                  double.parse(amount))
              .toString(),
          'dt': _dateController.text,
          'bill_no': invoiceDetails[0]["bill_no"].toString(),
          'firm_id': '1',
          'cash': _currentItemSelected == 'Cash'
              ? '1'
              : (_currentItemSelected == 'Cheque' ? '2' : '3'),
          'onlinepaymenttype': '',
          'transaction_id': transId == null ? "-" :transId,
          'narration': '',
        }).then((value) {
      print(value.statusCode);
    });

    //print(response.reasonPhrase);

    return "success";
  }

  Future<String> insertReceiptAdvance() async {
    var empId = prefs.getString('empId');
    print(widget.salePurchase);

    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertReceipt&";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'custId': (widget.getData == 1 ?  widget.custId : (temp.isEmpty ? customerInfo[0]["cid"].toString():temp[0]["cid"].toString())),
          'bank': bankName == null ? "-" :bankName,
          'chk_no': chequeNo == null ? "-" :chequeNo,
          'cheq_dt': '',
          'amt': amount == null ? "-" :amount,
          'emp_id': empId,
          'ba': '-',
          'dt': _dateController.text,
          'bill_no': '-',
          'firm_id': '1',
          'cash': _currentItemSelected == 'Cash'
              ? '1'
              : (_currentItemSelected == 'Cheque' ? '2' : '3'),
          'onlinepaymenttype': '',
          'transaction_id': transId == null ? "-" :transId,
          'narration': '',
        }).then((value) {
      print(value.statusCode);
    });

    //print(response.reasonPhrase);

    return "success";
  }

  Future<String> insertReceiptPurchase() async {
    var empId = prefs.getString('empId');
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertReceiptPurchase";
    print(url);
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'bill': (widget.getData == 1 ?  widget.custId : temp[0]["cid"].toString()),
          'bank':  bankName == null ? "-" :bankName,
          'chk_no': chequeNo == null ? "-" :chequeNo,
          'cheq_dt': _chequeDateController.text,
          'amt': amount == null ? "-" :amount,
          'emp_id': empId,
          'ba': (double.parse(invoiceDetails[0]["grand_total"]) -
                  double.parse(invoiceDetails[0]["paid"]) -
                  double.parse(amount))
              .toString(),
          'dt': _dateController.text,
          'bill_no': invoiceDetails[0]["bill_no"].toString(),
          'firm_name': '1',
          'cash': _currentItemSelected == 'Cash'
              ? '1'
              : (_currentItemSelected == 'Cheque' ? '2' : '3'),
          'total': invoiceDetails[0]["grand_total"],
          'onlinepaymenttype': '',
          'transaction_id': transId == null ? "-" :transId,
          'narration': '',
        });

    return "success";
  }

  Future<String> insertReceiptPurchaseAdvance() async {
    var empId = prefs.getString('empId');
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertReceiptPurchase";
    print(url);
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'bill': (widget.getData == 1 ?  widget.custId :(temp.isEmpty ? customerInfo[0]["cid"].toString():temp[0]["cid"].toString())),
          'bank': bankName == null ? "-" :bankName,
          'chk_no': chequeNo == null ? "-" :chequeNo,
          'cheq_dt': _chequeDateController.text,
          'amt': amount == null ? "-" :amount,
          'emp_id': empId,
          'ba': '-',
          'dt': _dateController.text,
          'bill_no': '-',
          'firm_name': '1',
          'cash': _currentItemSelected == 'Cash'
              ? '1'
              : (_currentItemSelected == 'Cheque' ? '2' : '3'),
          'total': '-',
          'onlinepaymenttype': '',
          'transaction_id': transId == null ? "-" :transId,
          'narration': '',
        });

    return "success";
  }

  Future<String> insertSaleBahiKhata() async {
    var empId = prefs.getString('empId');
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertSaleBahiKhata";
    print(url);
    //print(nameOfCustomer[widget.index]["cid"]);
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'date': _dateController.text,
          'custId':
              widget.getData == 1 ? widget.custId : (temp.isEmpty ? customerInfo[0]["cid"].toString():temp[0]["cid"].toString()),
          'empId': empId,
          'paid_amt': amount.toString(),
          'payment_mode': _currentItemSelected,
          'transaction_type': widget.payReceive == 0 ? "dr" : "cr",
        });

    return "success";
  }

  // Future<String> getBahiKhataId() async {
  //   var empId = prefs.getString('empId');
  //   String url;
  //   if (widget.salePurchase == 0) {
  //     url =
  //         "https://qot.constructionsmall.com/app/getquerycode.php?apicall=getBahiKhataId&custId=" +
  //             widget.custId + "&empId=" + empId;
  //   } else {
  //     url =
  //         "https://qot.constructionsmall.com/app/getquerycode.php?apicall=individualCustomerPurchase&custId=" +
  //             widget.custId;
  //   }
  //   print(url);
  //   var response = await http.get(
  //       //Encode the url
  //       Uri.encodeFull(url),
  //       //only accept Json response
  //       headers: {"Accept": "application/json"});

  //   print(response.body);
  //   setState(() {
  //     var convertDataToJson = json.decode(response.body);
  //     bkId = convertDataToJson['data'];
  //   });

  //   return "success";
  // }

  // Future<String> mapSaleBahiKhata() async {
  //   var empId = prefs.getString('empId');
  //   String url =
  //       "https://qot.constructionsmall.com/app/getquerycode.php?apicall=bahiKhataMapping-Sale";
  //   print(url);
  //   //print(nameOfCustomer[widget.index]["cid"]);
  //   var response = await http.post(Uri.encodeFull(url),
  //       // headers: {"Content-Type": "application/json"},
  //       body: {
  //         'bhaikhataId': bkId,
  //         'dt': DateFormat("yyyy-MM-dd").format(DateTime.now()),
  //         'custId': widget.custId,
  //         'uid': empId

  //       });

  //   return "success";
  // }

  Future<String> insertPurchaseBahiKhata() async {
    var empId = prefs.getString('empId');
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertPurchaseBahiKhata";
    print(url);

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},

        body: {
          'date': _dateController.text,
          'custId':
              widget.getData == 1 ? widget.custId : (temp.isEmpty ? customerInfo[0]["cid"].toString():temp[0]["cid"].toString()),
          'empId': empId,
          'paid_amt': amount.toString(),
          'payment_mode': _currentItemSelected.toString(),
          'transaction_type': widget.payReceive == 0 ? "dr" : "cr",
        });

    return "success";
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  void getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  void onRadioChanged(int val) {
    setState(() {
      radio = val;
    });
  }

  void onChange(){
    setState(() {
      
    });
  }

  

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    DateTime now = DateTime.now();
    String today = DateFormat("dd/MM/yyyy").format(now);
    _dateController = TextEditingController(text: today);
    temp = new List();
    saleBills = new List();
    purchaseBills = new List();
    _customerController = TextEditingController(text: "");
    _firmController = TextEditingController(text: "");
    _contactController = TextEditingController(text: "");
    _emailController = TextEditingController(text: "");
    CustomerViewModel.loadCategories();
    customerResponse = CustomerViewModel.getResopnse();
    if (widget.getData == 1) {
      indCustomerResponse = this.getJsonData();
      if(widget.payReceive == 0){
        getPurchaseBills();
      } else {
        getSaleBills();
      }
    } else {
      getEmpId();
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text((widget.payReceive == 0
            ? "Pay - Bahi Khata"
            : "Receive - Bahi Khata")),
        backgroundColor: Colors.lightBlue[700],
      ),
      body: FutureBuilder<String>(
          future: widget.getData == 1 ? indCustomerResponse : customerResponse,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                            Container(
                                padding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.width *
                                        0.01,
                                    right: MediaQuery.of(context).size.width *
                                        0.01,
                                    left: MediaQuery.of(context).size.width *
                                        0.01),
                                width: MediaQuery.of(context).size.width * 0.92,
                                child: DateTimeField(
                                    format: DateFormat('dd/MM/yyy'),
                                    initialValue: DateTime.now(),
                                    controller: _dateController,
                                    decoration: InputDecoration(
                                      labelText: "Date",
                                      prefixIcon: Container(
                                          margin: EdgeInsets.only(
                                              right: 20, top: 20),
                                          child: Icon(
                                            Icons.calendar_today,
                                          )),
                                      contentPadding: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.015),
                                    ),
                                    onShowPicker: (context, currentValue) {
                                      return showDatePicker(
                                          context: context,
                                          initialDate: _dateTime == null
                                              ? DateTime.now()
                                              : _dateTime,
                                          firstDate: DateTime(2001),
                                          lastDate: DateTime(2100));
                                    },
                                    validator: (val) {
                                      if (val != null) {
                                        return null;
                                      } else {
                                        return 'Date Field is Empty';
                                      }
                                    })),
                            // Container(
                            //   padding: EdgeInsets.only(
                            //       //top: MediaQuery.of(context).size.height * 0.02,
                            //       left: MediaQuery.of(context).size.width * 0.04,
                            //       right: MediaQuery.of(context).size.width * 0.04),
                            //   child: Row(
                            //     children: <Widget>[
                            //       Container(
                            //         width: MediaQuery.of(context).size.width * 0.92,
                            //         child: TextFormField(
                            //           textAlignVertical: TextAlignVertical.bottom,
                            //           autofocus: false,
                            //           readOnly: true,
                            //           initialValue: widget.getData == 0
                            //               ? customerInfo[widget.index]["cust_name"].toString()
                            //               : indCustomer == null ? "" : indCustomer[0]["cust_name"],
                            //           decoration: InputDecoration(
                            //               labelText: "Customer Name",
                            //               contentPadding: EdgeInsets.only(
                            //                   top: MediaQuery.of(context).size.height *
                            //                       0.015),
                            //               suffixIcon: Container(
                            //                 padding: EdgeInsets.only(
                            //                     top:
                            //                         MediaQuery.of(context).size.height *
                            //                             0.03,
                            //                     left:
                            //                         MediaQuery.of(context).size.width *
                            //                             0.07),
                            //                 child: Icon(Icons.perm_contact_calendar),
                            //               )),
                            //           keyboardType: TextInputType.text,
                            //           style: new TextStyle(
                            //             fontFamily: "Poppins",
                            //           ),
                            //         ),
                            //       )
                            //     ],
                            //   ),
                            // ),
                            Container(
                              width: MediaQuery.of(context).size.width * 0.92,
                              child: AutoCompleteTextField(
                                  itemSubmitted: (item) {
                                    temp.clear();
                                    temp.insert(0, {"cid":item.cid});

                                    print("Submit" + temp.toString());
                                    onChange();
                                    _customerController.text =
                                        item.cust_name.toString();
                                    _firmController.text =
                                        item.firm_name == null
                                            ? ""
                                            : item.firm_name.toString();
                                    _contactController.text =
                                        item.contact == null
                                            ? ""
                                            : item.contact.toString();
                                    _emailController.text = item.emailid == null
                                        ? ""
                                        : item.emailid.toString();

                                        if(widget.salePurchase == 0){
                                          getSaleBills();
                                        } else {
                                          getPurchaseBills();
                                        }
                                  },
                                  clearOnSubmit: false,
                                  key: key,
                                  decoration: InputDecoration(
                                      labelText: "Customer Name",
                                      contentPadding: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.015),
                                      suffixIcon: Container(
                                        padding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.03,
                                            left: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.07),
                                        child:
                                            Icon(Icons.perm_contact_calendar),
                                      )),
                                  suggestions: CustomerViewModel.customer,
                                  controller: _customerController,
                                  itemBuilder: (context, item) {
                                    //print(item.cust_name.toString());
                                    return Container(
                                      padding: EdgeInsets.all(10),
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                        children: [
                                          Container(
                                            child: Text(
                                              item.cust_name.toString(),
                                              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          Container(
                                            child: Text(
                                            item.firm_name == null ? "" :  item.firm_name.toString(),
                                              style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Colors.grey),
                                            ),
                                          )
                                        ],
                                      ),
                                    );
                                  },
                                  itemSorter: (a, b) =>
                                      a.cust_name.compareTo(b.cust_name),
                                  itemFilter: (suggestion, input) {
                                    return suggestion.cust_name
                                        .toLowerCase()
                                        .startsWith(input.toLowerCase());
                                  }),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  //top: MediaQuery.of(context).size.height * 0.02,
                                  left:
                                      MediaQuery.of(context).size.width * 0.04,
                                  right:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.92,
                                    child: TextFormField(
                                      autofocus: false,
                                      controller: _firmController,
                                      textAlignVertical:
                                          TextAlignVertical.bottom,
                               
                                      // initialValue: widget.getData == 0
                                      //     ? temp.isEmpty == true ? "" : temp[0]["cust_name"]
                                      //     : indCustomer[0]["firm_name"] == null ? "" : indCustomer[0]["firm_name"].toString(),
                                      decoration: InputDecoration(
                                        labelText: "Company Name",
                                        contentPadding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.015),
                                      ),
                                      keyboardType:
                                          TextInputType.text,
                                      style: new TextStyle(
                                        fontFamily: "Poppins",
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  //top: MediaQuery.of(context).size.height * 0.02,
                                  left:
                                      MediaQuery.of(context).size.width * 0.04,
                                  right:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.92,
                                    child: TextFormField(
                                      autofocus: false,
                                  
                                      controller: _contactController,
                                      // initialValue: widget.salePurchase == 0
                                      //     ? widget.getData == 0
                                      //         ? customerInfo[widget.index]["contact"].toString()
                                      //         : indCustomer == null ? "" : indCustomer[0]["contact"].toString()
                                      //     : widget.getData == 0
                                      //         ? customerInfo[widget.index]
                                      //             ["contactno"].toString()
                                      //         : indCustomer == null ? "": indCustomer[0]["contactno"].toString(),
                                      decoration: InputDecoration(
                                          labelText: "Contact No",
                                          contentPadding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.015),
                                          suffixIcon: Container(
                                            padding: EdgeInsets.only(
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.03,
                                                left: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.07),
                                            child: Icon(
                                                Icons.perm_contact_calendar),
                                          )),
                                          inputFormatters: [
                              LengthLimitingTextInputFormatter(10),
                              new WhitelistingTextInputFormatter(
                                  RegExp("[0-9]"))
                            ],
                                      keyboardType: TextInputType.numberWithOptions(decimal: true,signed: false),
                                      style: new TextStyle(
                                        fontFamily: "Poppins",
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  //top: MediaQuery.of(context).size.height * 0.02,
                                  left:
                                      MediaQuery.of(context).size.width * 0.04,
                                  right:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.92,
                                    child: TextFormField(
                                      autofocus: false,
                               
                                      controller: _emailController,
                                      // initialValue: widget.salePurchase == 0
                                      //     ? widget.getData == 0
                                      //         ? customerInfo[widget.index]["emailid"]
                                      //             .toString()
                                      //         : indCustomer == null ? "": indCustomer[0]["emailid"].toString()
                                      //     : widget.getData == 0
                                      //         ? customerInfo[widget.index]["email"]
                                      //             .toString()
                                      //         : indCustomer == null ? "": indCustomer[0]["email"].toString(),
                                      decoration: InputDecoration(
                                        labelText: "Email",
                                        contentPadding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.015),
                                      ),
                                      keyboardType: TextInputType.emailAddress,
                                      style: new TextStyle(
                                        fontFamily: "Poppins",
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  //top: MediaQuery.of(context).size.height * 0.02,
                                  left:
                                      MediaQuery.of(context).size.width * 0.04,
                                  right:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.92,
                                    child: TextFormField(
                                      autofocus: false,
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter(
                                            RegExp(r"^\d*\.?\d*"))
                                      ],
                                      validator: (value) {
                                        if (value == "") {
                                          return "Please Enter address";
                                        } else {
                                          return null;
                                        }
                                      },
                                      onSaved: (value) {
                                        setState(() {
                                          amount = value;
                                        });
                                      },
                                      decoration: InputDecoration(
                                        labelText: "₹ Amount",
                                        prefixText: "",
                                        contentPadding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.015),
                                      ),
                                      keyboardType:
                                          TextInputType.numberWithOptions(
                                              decimal: true, signed: false),
                                      style: new TextStyle(
                                        fontFamily: "Poppins",
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          
                      (temp.isEmpty || widget.getData != 0) && (temp.isEmpty && widget.getData != 1) ? Container(): Container(
                              padding: EdgeInsets.only(
                                  //top: MediaQuery.of(context).size.height * 0.02,
                                  left:
                                      MediaQuery.of(context).size.width * 0.04,
                                  right:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.01,
                                      right: MediaQuery.of(context).size.width *
                                          0.01,
                                    ),
                                    child: Container(
                                      height: 35,
                                      width: MediaQuery.of(context).size.width *
                                          0.90,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        border: Border.all(
                                            color: Colors.grey[700],
                                            style: BorderStyle.solid,
                                            width: 1.0),
                                      ),
                                      child: DropdownButtonHideUnderline(
                                        child: ButtonTheme(
                                          alignedDropdown: true,
                                          child: DropdownButton<String>(
                                            isDense: true,
                                            autofocus: true,
                                            focusColor: Colors.lightBlue[900],
                                            //style: Theme.of(context).textTheme.title,
                                            isExpanded: true,
                                            hint: Text("Choose a Bill Type"),
                                            items: billType.map(
                                                (String dropDownStringItem) {
                                              return DropdownMenuItem<String>(
                                                  value: dropDownStringItem,
                                                  child: Text(
                                                      dropDownStringItem,
                                                      style: TextStyle(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          color:
                                                              Colors.black)));
                                            }).toList(),
                                            onChanged:
                                                (String newValueSelected) {
                                              setState(() {
                                                _currentBill =
                                                    newValueSelected;
                                              });
                                            },
                                            value: _currentBill,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),

                           _currentBill == 'Bill Against'? Container(
                              padding: EdgeInsets.only(
                                  //top: MediaQuery.of(context).size.height * 0.02,
                                  left:
                                      MediaQuery.of(context).size.width * 0.04,
                                  right:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.01,
                                      right: MediaQuery.of(context).size.width *
                                          0.01,
                                    ),
                                    child: Container(
                                      height: 35,
                                      width: MediaQuery.of(context).size.width *
                                          0.90,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        border: Border.all(
                                            color: Colors.grey[700],
                                            style: BorderStyle.solid,
                                            width: 1.0),
                                      ),
                                      child: DropdownButtonHideUnderline(
                                        child: ButtonTheme(
                                          alignedDropdown: true,
                                          child: DropdownButton(
                                            isDense: true,
                                            autofocus: true,
                                            focusColor: Colors.lightBlue[900],
                                            //style: Theme.of(context).textTheme.title,
                                            isExpanded: true,
                                            hint: Text("Choose a Bill No."),
                                            items: (widget.salePurchase == 0 ? saleBills : purchaseBills).map(
                                                (dropDownStringItem) {
                                                  
                                              return  DropdownMenuItem(
                                                  value: '${dropDownStringItem["bill_no"].toString()}',
                                                  child: Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      Text(
                                                          "${dropDownStringItem['bill_no'].toString()}",
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight.w500,
                                                              color:
                                                                  Colors.black)),
                                                                  Text(
                                                          "₹ ${dropDownStringItem['current_balance'].toString()}",
                                                          style: TextStyle(
                                                              fontSize: 16,
                                                              fontWeight:
                                                                  FontWeight.w500,
                                                              color:
                                                                  Colors.green[600])),
                                                    ],
                                                  ));
                                            }).toList(),
                                            onChanged:
                                                (String newValueSelected) {
                                              setState(() {
                                                _currentBillNo =
                                                    newValueSelected;
                                              });
                                              getInvoiceDetails();
                                            },
                                            value: _currentBillNo,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ):Container(),
                  
                  
                            Container(
                              padding: EdgeInsets.only(
                                  //top: MediaQuery.of(context).size.height * 0.02,
                                  left:
                                      MediaQuery.of(context).size.width * 0.04,
                                  right:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.01,
                                      right: MediaQuery.of(context).size.width *
                                          0.01,
                                    ),
                                    child: Container(
                                      height: 35,
                                      width: MediaQuery.of(context).size.width *
                                          0.90,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        border: Border.all(
                                            color: Colors.grey[700],
                                            style: BorderStyle.solid,
                                            width: 1.0),
                                      ),
                                      child: DropdownButtonHideUnderline(
                                        child: ButtonTheme(
                                          alignedDropdown: true,
                                          child: DropdownButton<String>(
                                            isDense: true,
                                            autofocus: true,
                                            focusColor: Colors.lightBlue[900],
                                            //style: Theme.of(context).textTheme.title,
                                            isExpanded: true,
                                            hint: Text("Mode of Payment"),
                                            items: _modeOfPayment.map(
                                                (String dropDownStringItem) {
                                              return DropdownMenuItem<String>(
                                                  value: dropDownStringItem,
                                                  child: Text(
                                                      dropDownStringItem,
                                                      style: TextStyle(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          color:
                                                              Colors.black)));
                                            }).toList(),
                                            onChanged:
                                                (String newValueSelected) {
                                              setState(() {
                                                _currentItemSelected =
                                                    newValueSelected;
                                              });
                                            },
                                            value: _currentItemSelected,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            _currentItemSelected == 'Cheque' &&
                                    _currentItemSelected != null
                                ? Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.01,
                                            left: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.01),
                                        child: Text(
                                          "Bank Name : ",
                                          style: TextStyle(
                                              fontSize: 17,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      Container(
                                        //height: 40,
                                        padding: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.01,
                                          right: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02,
                                        ),
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.7,
                                        child: TextFormField(
                                          focusNode: null,
                                          autofocus: false,
                                          validator: (value) {
                                            if (value == "") {
                                              return "Please Enter Bank Name";
                                            } else {
                                              return null;
                                            }
                                          },
                                          onSaved: (value) {
                                            setState(() {
                                              bankName = value;
                                            });
                                          },
                                          decoration: InputDecoration(
                                              hintText: "Bank Name",
                                              suffixText: "₹",
                                              border: new OutlineInputBorder(
                                                borderSide: new BorderSide(),
                                              ),
                                              contentPadding: EdgeInsets.only(
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.015,
                                                left: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.015,
                                              )),
                                          keyboardType: TextInputType.text,
                                          style: new TextStyle(
                                            fontFamily: "Poppins",
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                : Container(),
                            _currentItemSelected == 'Cheque' &&
                                    _currentItemSelected != null
                                ? Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.01,
                                            left: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.01),
                                        child: Text(
                                          "Cheque No. : ",
                                          style: TextStyle(
                                              fontSize: 17,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      Container(
                                        //height: 40,
                                        padding: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.01,
                                          right: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02,
                                        ),
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.7,
                                        child: TextFormField(
                                          focusNode: null,
                                          autofocus: false,
                                          validator: (value) {
                                            if (value == "") {
                                              return "Please Enter Cheque No.";
                                            } else {
                                              return null;
                                            }
                                          },
                                          onSaved: (value) {
                                            setState(() {
                                              chequeNo = value;
                                            });
                                          },
                                          decoration: InputDecoration(
                                              hintText: "Cheque No.",
                                              suffixText: "₹",
                                              border: new OutlineInputBorder(
                                                borderSide: new BorderSide(),
                                              ),
                                              contentPadding: EdgeInsets.only(
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.015,
                                                left: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.015,
                                              )),
                                          keyboardType:
                                              TextInputType.numberWithOptions(
                                                  decimal: true, signed: false),
                                          style: new TextStyle(
                                            fontFamily: "Poppins",
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                : Container(),
                            _currentItemSelected == 'Cheque' &&
                                    _currentItemSelected != null
                                ? Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.01,
                                            left: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.01),
                                        child: Text(
                                          "Cheque Date : ",
                                          style: TextStyle(
                                              fontSize: 17,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      Container(
                                          //height: 40,
                                          padding: EdgeInsets.only(
                                              right: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.02,
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.01),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.69,
                                          child: DateTimeField(
                                              format: DateFormat('dd/MM/yyy'),
                                              style: TextStyle(fontSize: 15),
                                              initialValue: DateTime.now(),
                                              controller: _chequeDateController,
                                              decoration: InputDecoration(
                                                  contentPadding:
                                                      EdgeInsets.only(
                                                    top: MediaQuery.of(context)
                                                            .size
                                                            .height *
                                                        0.02,
                                                  ),
                                                  border: OutlineInputBorder(
                                                      borderSide: BorderSide()),
                                                  prefixIcon: Icon(
                                                      Icons.calendar_today)),
                                              resetIcon: null,
                                              onShowPicker:
                                                  (context, currentValue) {
                                                return showDatePicker(
                                                    context: context,
                                                    initialDate:
                                                        _dateTime == null
                                                            ? DateTime.now()
                                                            : _dateTime,
                                                    firstDate: DateTime(2001),
                                                    lastDate: DateTime(2100));
                                              },
                                              validator: (val) {
                                                if (val != null) {
                                                  return null;
                                                } else {
                                                  return 'Date Field is Empty';
                                                }
                                              }))
                                    ],
                                  )
                                : Container(),
                            _currentItemSelected == 'Online' &&
                                    _currentItemSelected != null
                                ? Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        padding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.01,
                                            left: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.01),
                                        child: Text(
                                          "Trans Id : ",
                                          style: TextStyle(
                                              fontSize: 17,
                                              fontWeight: FontWeight.w500),
                                        ),
                                      ),
                                      Container(
                                        //height: 40,
                                        padding: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.01,
                                          right: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02,
                                        ),
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.7,
                                        child: TextFormField(
                                          focusNode: null,
                                          autofocus: false,
                                          validator: (value) {
                                            if (value == "") {
                                              return "Please Enter Trans Id";
                                            } else {
                                              return null;
                                            }
                                          },
                                          onSaved: (value) {
                                            setState(() {
                                              transId = value;
                                            });
                                          },
                                          decoration: InputDecoration(
                                              hintText: "Trans Id",
                                              suffixText: "₹",
                                              border: new OutlineInputBorder(
                                                borderSide: new BorderSide(),
                                              ),
                                              contentPadding: EdgeInsets.only(
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.015,
                                                left: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.015,
                                              )),
                                          keyboardType: TextInputType.text,
                                          style: new TextStyle(
                                            fontFamily: "Poppins",
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                : Container(),
                            widget.payReceive == 0
                                ? Row(
                                    children: [
                                      Radio(
                                          value: 0,
                                          groupValue: radio,
                                          onChanged: onRadioChanged),
                                      Container(
                                        child: Text("Map Purchase Invoice"),
                                      ),
                                      Radio(
                                          value: 1,
                                          groupValue: radio,
                                          onChanged: onRadioChanged),
                                      Container(
                                        child: Text("Pay only On Account"),
                                      ),
                                    ],
                                  )
                                : Container()
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width * 1,
                      color: Colors.lightBlue[700],
                      child: FlatButton(
                          onPressed: () {
                            if (_formKey.currentState.validate() &&
                                _currentItemSelected != null) {
                              waiting(context);

                              _formKey.currentState.save();

                              if (widget.payReceive != 0) {
                                if(temp.isEmpty){

                                  
                                  insertCustomer().then((value){
                                    getCust().then((value) {
                                  insertReceiptAdvance().then((value) {
                                  Navigator.of(_keyLoader.currentContext).pop();
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(builder: (context) {
                                    return CustomerDetails(
                                      custId: widget.getData == 0
                                          ? customerInfo[0]["cid"]
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cid"],
                                      name: widget.getData == 0
                                          ? _customerController.text
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cust_name"],
                                    );
                                  }));
                                });
                                    });
                                   
                                  });
                                } else {

                                  if(_currentBill == 'Bill Against'){
                                     insertReceipt().then((value) {
                                  Navigator.of(_keyLoader.currentContext).pop();
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(builder: (context) {
                                    return CustomerDetails(
                                      custId: widget.getData == 0
                                          ? temp[0]["cid"]
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cid"],
                                      name: widget.getData == 0
                                          ? _customerController.text
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cust_name"],
                                    );
                                  }));
                                });
                                  } else if(_currentBill == 'On Account'){
                                  insertSaleBahiKhata().then((value) {
                                  Navigator.of(_keyLoader.currentContext).pop();
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(builder: (context) {
                                    return CustomerDetails(
                                      custId: widget.getData == 0
                                          ? temp[0]["cid"]
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cid"],
                                      name: widget.getData == 0
                                          ? _customerController.text
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cust_name"],
                                    );
                                  }));
                                });
                                  } else {
                                  
                                insertReceiptAdvance().then((value) {
                                  Navigator.of(_keyLoader.currentContext).pop();
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(builder: (context) {
                                    return CustomerDetails(
                                      custId: widget.getData == 0
                                          ? temp[0]["cid"]
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cid"],
                                      name: widget.getData == 0
                                          ? _customerController.text
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cust_name"],
                                    );
                                  }));
                                });
                                  }
                                }
                              } else {
                                if(temp.isEmpty){
                                  insertCustomer().then((value) {
                                    getCust().then((value) {
                                      insertReceiptPurchaseAdvance().then((value) {
                                  Navigator.of(_keyLoader.currentContext).pop();
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(builder: (context) {
                                    return CustomerDetails(
                                      custId: widget.getData == 0
                                          ? customerInfo[0]["cid"]
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cid"],
                                      name: widget.getData == 0
                                          ? _customerController.text
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cust_name"],
                                    );
                                  }));
                                });
                                    });
                                    
                                  });
                                } else {
                                  if(_currentBill == 'Bill Against'){
                                    insertReceiptPurchase().then((value) {
                                  Navigator.of(_keyLoader.currentContext).pop();
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(builder: (context) {
                                    return CustomerDetails(
                                      custId: widget.getData == 0
                                          ? temp[0]["cid"]
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cid"],
                                      name: widget.getData == 0
                                          ? _customerController.text
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cust_name"],
                                    );
                                  }));
                                });
                                  }else if(_currentBill == 'On Account'){
                                    insertPurchaseBahiKhata().then((value) {
                                  Navigator.of(_keyLoader.currentContext).pop();
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(builder: (context) {
                                    return CustomerDetails(
                                      custId: widget.getData == 0
                                          ? temp[0]["cid"]
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cid"],
                                      name: widget.getData == 0
                                          ? _customerController.text
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cust_name"],
                                    );
                                  }));
                                });
                                  } else {
                              insertReceiptPurchaseAdvance().then((value) {
                                  Navigator.of(_keyLoader.currentContext).pop();
                                  Navigator.of(context).pushReplacement(
                                      MaterialPageRoute(builder: (context) {
                                    return CustomerDetails(
                                      custId: widget.getData == 0
                                          ? temp[0]["cid"]
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cid"],
                                      name: widget.getData == 0
                                          ? _customerController.text
                                          : indCustomer == null
                                              ? ""
                                              : indCustomer[0]["cust_name"],
                                    );
                                  }));
                                });
                                  }
                                
                                }
                              }
                            } else {
                              print("4");

                              if (_currentItemSelected == null) {
                                _showDialogBox(
                                    context, "Warning", "Please Choose Option");
                              }
                            }
                          },
                          child: Text(
                            (widget.payReceive == 0 ? "Pay" : "Receive"),
                            style: TextStyle(color: Colors.white),
                          )),
                    )
                  ],
                ),
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          }),
    );
  }
}
