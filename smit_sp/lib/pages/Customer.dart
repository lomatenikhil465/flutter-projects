import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
class Customer {
   String? cid;
  String? cust_name;
  String? emailid,firm_name,contact;
 
  Customer({this.cid, this.cust_name, this.emailid, this.firm_name, this.contact});
 
  factory Customer.fromJson(Map<String, dynamic> parsedJson) {
    return Customer(
      cid: parsedJson["cid"],
      cust_name: parsedJson["cust_name"] as String,
      emailid: parsedJson["emailid"] as String,
      firm_name: parsedJson["firm_name"] as String,
      contact: parsedJson["contact"] as String,
    );
  }
}

class CustomerViewModel {
  static List<Customer>? customer;

  static Future loadCategories() async {
    try {
      customer = [];
      //String jsonString = await rootBundle.loadString('assets/categories.json');
      final response =
            await http.get("https://qot.constructionsmall.com/app/getquerycode.php?apicall=customer");
      Map parsedJson = json.decode(response.body);
      var customerJson = parsedJson['data'] as List;
      for (int i = 0; i < customerJson.length; i++) {
        customer!.add(new Customer.fromJson(customerJson[i]));
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<String?> getResopnse() async{
    final response =  await http.get("https://qot.constructionsmall.com/app/getquerycode.php?apicall=customer");

    if(response.statusCode == 200){
      return "success";
    } else {
      return null;
    }
  }
}