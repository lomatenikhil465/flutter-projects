import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class ChangePassPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ChangePassPage();
  }
}

class _ChangePassPage extends State<ChangePassPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  SharedPreferences prefs;
  TextEditingController _newPassController, _confirmPassController;
  bool _obscureText = true;
   bool _autoValidate = false;

   waiting(BuildContext context) {
    return showDialog(
      
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
                  child: AlertDialog(
            key: _keyLoader,
              content: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                child: CircularProgressIndicator(),
              ),
              Container(
                child: Text("Loading..."),
              )
            ],
          )),
        );
        
      },
      
    );
  }

  Future<String> updatePass() async {
    var empId = prefs.getString('empId');
    String url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=changePass&id=" +
             empId +
              "&pass=" +
              _confirmPassController.text;
   
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    
    
    print(response.body);

    return "success";
  }

  void _validateInputs() {
    //checkConnection();
    if (_formKey.currentState.validate() ) {
      waiting(context);
//    If all data are correct then save data to out variables


      _formKey.currentState.save();
      updatePass().then((value) {
        Navigator.of(_keyLoader.currentContext).pop();
        
          
           prefs.setString('pass', _confirmPassController.text);
          
          // Navigator.of(context)
          //     .pushReplacement(MaterialPageRoute(builder: (context) {
          //   return Menu(username: username,);
          // }));
       
      });
      _formKey.currentState.reset();
      
    } else {
//    If all data are not valid then start auto validation.

      setState(() {
        _autoValidate = true;
      });
    }
  }

  Future getEmpId() async{
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEmpId();
    _newPassController = TextEditingController(text: "");
    _confirmPassController = TextEditingController(text: "");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Change Password"),
        backgroundColor: Colors.lightBlue[700],
      ),
          body: Form(
        key: _formKey,
         autovalidate: _autoValidate,
        child: Center(
          child: SingleChildScrollView(
        
                       
                          child: Column(
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    child: TextFormField(
                              controller: _newPassController,
                              autofocus: false,
                            
                              validator: (value){
                                if(value == ""){
                                  return "please enter password";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                suffixIcon: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      _obscureText = !_obscureText;
                                    });
                                  },
                                  child: Icon(
                                    _obscureText
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                  ),
                                ),
                                labelText: "New Password",
                                contentPadding:
                                    EdgeInsets.only(left: 10.0, right: 2.0),
                                fillColor: Colors.white,
                                border: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(10.0),
                                  borderSide: new BorderSide(),
                                ),
                              ),
                              obscureText: _obscureText,
                              style: new TextStyle(
                                fontFamily: "Poppins",
                              ),
                            ),
                  ),
                  Container(
                    padding: EdgeInsets.all(10),
                    child: TextFormField(
                              controller: _confirmPassController,
                              autofocus: false,
                              validator: (value){
                                if(value.compareTo(_newPassController.text) != 0){
                                  return "please enter exact password";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                suffixIcon: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      _obscureText = !_obscureText;
                                    });
                                  },
                                  child: Icon(
                                    _obscureText
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                  ),
                                ),
                                labelText: "Confirm Password",
                                contentPadding:
                                    EdgeInsets.only(left: 10.0, right: 2.0),
                                fillColor: Colors.white,
                                border: new OutlineInputBorder(
                                  borderRadius: new BorderRadius.circular(10.0),
                                  borderSide: new BorderSide(),
                                ),
                              ),
                              obscureText: _obscureText,
                              style: new TextStyle(
                                fontFamily: "Poppins",
                              ),
                            ),
                  ),
                  Padding(padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.15)),
                  RaisedButton(
                      elevation: 3.0,
                      onPressed: (){
                        
                         //_showDialogBox(context, "", "");
                        _validateInputs();
                      },
                      
                      shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(
                            width: 1.5, color: Colors.lightBlue[700]),
                      ),
                      textColor: Colors.lightBlue[700],
                      padding: EdgeInsets.only(
                          right: 100.0, left: 100.0, top: 10.0, bottom: 10.0),
                      color: Colors.white,
                      child: Container(
                        
                        child: Text(
                          "Change",
                          style: TextStyle(
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                              textBaseline: TextBaseline.alphabetic),
                        ),
                      ),
                    ),
                ],
              ),
           
            ),

        ),
      ),
    );
  }
}
