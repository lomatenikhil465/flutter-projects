import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_full_pdf_viewer/full_pdf_viewer_scaffold.dart';
import 'package:path_provider/path_provider.dart';

class PdfViewerPage extends StatelessWidget {
  final String? path;
  const PdfViewerPage({required Key key, this.path}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PDFViewerScaffold(
      path: path,
      appBar: AppBar(
        title: Text("PDF"),
        actions: [
          Container(
            child: Center(
              child: IconButton(icon: Icon(Icons.file_download), onPressed: () async{
                Dio dio = new Dio();
                                Directory dir =
                                    await getApplicationDocumentsDirectory();
                                String path = dir.path;
                                await dio.download(path + "/example.pdf","example.pdf");
              }),
            ),
          )
        ],
      ),
    );
  }
}