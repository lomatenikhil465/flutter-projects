import 'package:smit_sp/pages/InvoiceDetails.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class InvoiceList extends StatefulWidget {
  final String? custId;
  final int? salePurchase;
  InvoiceList({this.custId, this.salePurchase});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _InvoiceList();
  }
}

class _InvoiceList extends State<InvoiceList> {
  List? invoiceList;
  Future<String>? invoiceListResponse;

  Future<String> getJsonData() async {
    String url;
    if (widget.salePurchase == 0) {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=invoiceList&custId=" +
              widget.custId.toString();
    } else {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=invoiceList&custId=" +
              widget.custId.toString();
    }

    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      invoiceList = convertDataToJson['data'];
    });

    return "success";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    invoiceListResponse = getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Invoice List"),
        backgroundColor: Colors.lightBlue[700],
      ),
      body: FutureBuilder<String>(
        future: invoiceListResponse,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
                child: ListView.separated(
              separatorBuilder: (BuildContext context, int index) => Divider(
                thickness: 0.0,
                color: Colors.lightBlue[900],
              ),
              itemCount: invoiceList == null ? 0 : invoiceList!.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(
                  onTap: () {
                    if(invoiceList![index]["lType"] == 's'){
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return InvoiceDetails(
                        custId: widget.custId.toString(),
                        billNo: invoiceList![index]["bill_no"],
                        custName: invoiceList![index]["cust_name"],
                        salePurchase: widget.salePurchase,
                      );
                    })).then((value){
                      this.getJsonData();
                    });
                    } else {
                       Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return InvoiceDetails(
                        custId: widget.custId.toString(),
                        billNo: invoiceList![index]["bill_no"],
                        custName: invoiceList![index]["cust_name"],
                        salePurchase: 1,
                      );
                    })).then((value){
                      this.getJsonData();
                    });
                    }
                  },
                  child: Container(
                    child: Card(
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.01,
                                    top: MediaQuery.of(context).size.height *
                                        0.01),
                                child: Text(
                                  invoiceList![index]["cust_name"].toString(),
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.lightBlue[700]),
                                ),
                              ),

                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.01,
                                    top: MediaQuery.of(context).size.height *
                                        0.01),
                                child: Text(
                                  invoiceList![index]["lType"] == 's'
                                  ? "Sale"
                                  :"Purchase",
                                  style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.lightBlue[700]),
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.01,
                                    top: MediaQuery.of(context).size.height *
                                        0.01),
                                child: Text(
                                  invoiceList![index]["grand_total"].toString() +
                                      "₹",
                                ),
                              )
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.01,
                                    top: MediaQuery.of(context).size.height *
                                        0.01),
                                child: Text(
                                  invoiceList![index]["bill_no"].toString(),
                                ),
                              )
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                        left:
                                            MediaQuery.of(context).size.width *
                                                0.01,
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      widget.salePurchase == 0?
                                      invoiceList![index]["dt"].toString()
                                      :invoiceList![index]["date"].toString(),
                                      style: TextStyle(color: Colors.grey),
                                    ),
                                  ),
                                ],
                              ),
                              Spacer(),
                              Container(
                                height: 20,
                                width: 60,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10.0),
                                    color:( widget.salePurchase == 1 ? (double.parse(invoiceList![index]
                                                        ["grand_total"]) -
                                                    double.parse(invoiceList![
                                                                    index]
                                                                ["paid_amt"] ==
                                                            "0"
                                                        ? invoiceList![index]
                                                            ["pay_amt"]
                                                        : invoiceList![index]
                                                            ["paid_amt"]))
                                                .toString() !=
                                            "0.0" : invoiceList![index]["current_balance"] != "0")
                                        ? Colors.deepOrange
                                        : Colors.green[600]),
                                child: Center(
                                  child: Text(
                                    ( widget.salePurchase == 1 ? (double.parse(invoiceList![index]
                                                        ["grand_total"]) -
                                                    double.parse(invoiceList![
                                                                    index]
                                                                ["paid_amt"] ==
                                                            "0"
                                                        ? invoiceList![index]
                                                            ["pay_amt"]
                                                        : invoiceList![index]
                                                            ["paid_amt"]))
                                                .toString() !=
                                            "0.0" : invoiceList![index]["current_balance"] != "0")
                                        ? "Unpaid"
                                        : "Paid",
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  ),
                );
              },
            ));
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}
