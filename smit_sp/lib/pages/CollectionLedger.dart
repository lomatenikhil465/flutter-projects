import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CollectionLedger extends StatefulWidget {
  final int index;

  CollectionLedger({this.index});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CollectionLedger();
  }
}

class _CollectionLedger extends State<CollectionLedger> {
  Future<String> collectionLedger;
  List ledgerData;
  SharedPreferences prefs;
  double totalAmount = 0.0;

  Future<String> getJsonData() async {
    var empId = prefs.getString('empId');
    print(empId);
    String today = DateFormat("yyyy-MM-dd").format(DateTime.now());
    String url;
    if (widget.index == 0) {
       url=
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=saleCollection&fdt=" +
            today +
            "&uid=" +
            empId;
    } else {
      url=
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=purchaseCollection&fdt=" +
            today +
            "&uid=" +
            empId;
    }
    
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      ledgerData = convertDataToJson['data'];
    });

    if(ledgerData != null){

      for (var i = 0; i < ledgerData.length; i++) {

        totalAmount = totalAmount + double.parse(ledgerData[i]["total_bill"]);
        
      }
    }

    return "success";
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEmpId().then((value) {
      collectionLedger = this.getJsonData();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlue[700],
        title: Text(widget.index == 0 ? "Sale" : "Purchase"),
      ),
      body: Column(
        children: <Widget>[
          Container(
            child: Text(
              "Total Amount : " + totalAmount.toString(),
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
            ),
          ),
          Divider(
            thickness: 2,
            color: Colors.grey,
          ),
          Container(
            child: Expanded(
                child: ledgerData == null
                    ? Center(
                        child: Text("No Data Available..."),
                      )
                    : FutureBuilder<String>(
                        future: collectionLedger,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return ListView.separated(
                                separatorBuilder:
                                    (BuildContext context, int index) =>
                                        Divider(
                                          thickness: 0.0,
                                          color: Colors.lightBlue[700],
                                        ),
                                itemCount:
                                    ledgerData == null ? 0 : ledgerData.length,
                                itemBuilder: (BuildContext context, int index) {
                                  return Container(
                              child: Card(
                                elevation: 0,
                                child: Column(
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Container(
                                                child: Text(  ledgerData[index]["cust_name"],
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Flexible(
                                                                                  child: Container(
                                            padding: EdgeInsets.only(
                                                left: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.01,
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.01),
                                            child: Text(widget.index == 0 ? "Sale" : "Purchase",
                                              // customerStatement[index]["chal_type"] == "Rcpt"
                                              //     ? (customerStatement[index]["c_type"] == "cr"
                                              //         ? "Payment received against Receipt " +
                                              //             customerStatement[index]
                                              //                 ["recpt_no"] +
                                              //             " by " +
                                              //             (customerStatement[index]["payment_mode"] == null
                                              //                 ? "Cash"
                                              //                 : customerStatement[index][
                                              //                     "payment_mode"])
                                              //         : "Payment paid to receipt " +
                                              //             customerStatement[index]
                                              //                     ["recpt_no"]
                                              //                 .toString() +
                                              //             " by " +
                                              //             (customerStatement[index]["payment_mode"] == null
                                              //                 ? "Cash"
                                              //                 : customerStatement[index][
                                              //                     "payment_mode"]))
                                              //     : (customerStatement[index]
                                              //                 ["chal_type"] ==
                                              //             "OnAccount"
                                              //         ? (customerStatement[index]["c_type"] == "cr"
                                              //             ? "Payment received On Account " +
                                              //                 customerStatement[index]
                                              //                     ["recpt_no"] +
                                              //                 " by " +
                                              //                 (customerStatement[index]["payment_mode"] == null
                                              //                     ? "Cash"
                                              //                     : customerStatement[index]
                                              //                         ["payment_mode"])
                                              //             : "Payment paid On Account " + customerStatement[index]["recpt_no"].toString() + " by " + (customerStatement[index]["payment_mode"] == null ? "Cash" : customerStatement[index]["payment_mode"]))
                                              //         : (customerStatement[index]["chal_type"] == "OpeningStock" ? (customerStatement[index]["c_type"] == "cr" ? "Payment taken for Opening Stock " : "Payment given for opening stock") : "invoice issued against " + customerStatement[index]["chall_no"])),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                        ),
                                        
                                        Container(
                                                child: Text(  ledgerData[index]["bill_no"],
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(
                                              right: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.01,
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01),
                                          child: Text("Total : " + ledgerData[index]["total_bill"] ,
                                            // customerStatement[index]["chal_type"] == "Rcpt" ||
                                            //         customerStatement[index]
                                            //                 ["chal_type"] ==
                                            //             "OpeningStock"
                                            //     ? "₹ " +
                                            //         customerStatement[index]
                                            //             ["amt"]
                                            //     : (customerStatement[index]
                                            //                 ["chal_type"] ==
                                            //             "OnAccount"
                                            //         ? (customerStatement[index]["remain_amt"] != "0"
                                            //             ? "₹ " +
                                            //                 customerStatement[index]
                                            //                     ["remain_amt"]
                                            //             : "₹ " +
                                            //                 customerStatement[index]
                                            //                     ["amt"])
                                            //         : (customerStatement[index]
                                            //                     ["pay_amt"] ==
                                            //                 "0"
                                            //             ? "₹ " +
                                            //                 customerStatement[index]
                                            //                     ["amt"]
                                            //             : "₹ " +
                                            //                 customerStatement[index]
                                            //                     ["pay_amt"])),
                                            style: TextStyle(
                                                color: 
                                                         Colors.green[600]),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              left: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.01,
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01),
                                          child: Text(
                                            ledgerData[index]["chdt"],
                                            style: TextStyle(
                                                color: Colors.grey[700]),
                                          ),
                                        ),
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            );
                                });
                          } else {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                        },
                      )),
          )
        ],
      ),
    );
  }
}
