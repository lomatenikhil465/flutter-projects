import 'package:smit_sp/pages/PurchasePage.dart';
import 'package:smit_sp/pages/QuotationPage.dart';
import 'package:smit_sp/pages/SalePage.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';
import 'dart:async';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';

class InvoiceDetails extends StatefulWidget {
  final String? custId, billNo, custName;
  final int? salePurchase;
  InvoiceDetails({this.custId, this.billNo, this.custName, this.salePurchase});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _InvoiceDetails();
  }
}

List? invoiceDetails;

class _InvoiceDetails extends State<InvoiceDetails> {
  Future<String>? invoiceDetailsResponse;
  String? _currentItemSelected,
      amount,
      bankName = "",
      chequeNo = "",
      transId = "";
  TextEditingController? _dateController, _chequeDateController;
  SharedPreferences? prefs;
  DateTime? _dateTime;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  var _modeOfPayment = ['Cash', 'Cheque', 'Online'];

  void save() {
    if (_formKey.currentState!.validate()) {
//    If all data are correct then save data to out variables

      _formKey.currentState!.save();
      if (widget.salePurchase == 0) {
        insertReceipt().then((value) {
          Navigator.of(context).pop();
          Fluttertoast.showToast(
              msg: 'Successfully Added',
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              textColor: Colors.black,
              fontSize: 16.0);
          this.getJsonData();
        });
      } else {
        insertReceiptPurchase().then((value) {
          Navigator.of(context).pop();
          Fluttertoast.showToast(
              msg: 'Successfully Added',
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              textColor: Colors.black,
              fontSize: 16.0);
          this.getJsonData();
        });
      }
    }
  }

  Future<String> getJsonData() async {
    prefs = await SharedPreferences.getInstance();
    String url;
    print(widget.custId.toString());
    print(widget.billNo.toString());
    if (widget.salePurchase == 0) {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=invoiceDetails&custId=" +
              widget.custId.toString() +
              "&billNo=" +
              widget.billNo.toString();
    } else if (widget.salePurchase == 1) {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=invoiceDetailsPurchase&custId=" +
              widget.custId.toString() +
              "&billNo=" +
              widget.billNo.toString();
    } else {
      url =
          "https://qot.constructionsmall.com/app/getquerycode.php?apicall=invoiceDetailsQuotation&custId=" +
              widget.custId.toString() +
              "&billNo=" +
              widget.billNo.toString();
    }
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      invoiceDetails = convertDataToJson['data'];
    });

    return "success";
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  Future<String> deleteQuotation() async {
    print(widget.salePurchase);

    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=deleteQuotation&";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'cust_id': widget.custId.toString(),
          'date': invoiceDetails![0]["dt"],
          'bill_no': invoiceDetails![0]["bill_no"].toString(),
          'firm_id': '1',
          'emp_id': '1',
        }).then((value) {
      print(value.statusCode);
      Navigator.of(context).pop();
    });

    //print(response.reasonPhrase);

    return "success";
  }

  Future<String> deleteSale() async {
    print(widget.salePurchase);

    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=deleteSale&";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'cust_id': widget.custId.toString(),
          'date': invoiceDetails![0]["dt"],
          'bill_no': invoiceDetails![0]["bill_no"].toString(),
          'firm_id': '1',
          'emp_id': '1',
        }).then((value) {
      print(value.statusCode);
      Navigator.of(context).pop();
    });

    //print(response.reasonPhrase);

    return "success";
  }

  Future<String> deletePurchase() async {
    print(widget.salePurchase);

    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=deletePurchase&";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'cust_id': widget.custId.toString(),
          'date': invoiceDetails![0]["dt"],
          'bill_no': invoiceDetails![0]["bill_no"].toString(),
          'firm_id': '1',
          'emp_id': '1',
        }).then((value) {
      print(value.statusCode);
      Navigator.of(context).pop();
    });

    //print(response.reasonPhrase);

    return "success";
  }

  Future<String> insertReceipt() async {
    var empId = prefs!.getString('empId');
    print(widget.salePurchase);

    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertReceipt&";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'custId': widget.custId.toString(),
          'bank': bankName,
          'chk_no': chequeNo,
          'cheq_dt': '',
          'amt': amount,
          'emp_id': empId,
          'ba': (double.parse(invoiceDetails![0]["grand_total"]) -
                  double.parse(invoiceDetails![0]["paid"]) -
                  double.parse(amount.toString()))
              .toString(),
          'dt': _dateController!.text,
          'bill_no': invoiceDetails![0]["bill_no"].toString(),
          'firm_id': '1',
          'cash': _currentItemSelected,
          'onlinepaymenttype': '',
          'transaction_id': transId,
          'narration': '',
        }).then((value) {
      print(value.statusCode);
    });

    //print(response.reasonPhrase);

    return "success";
  }

  Future<String> insertReceiptPurchase() async {
    var empId = prefs!.getString('empId');
    String url =
        "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertReceiptPurchase";
    print(url);
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'bill': widget.custId.toString(),
          'bank': bankName,
          'chk_no': chequeNo,
          'cheq_dt': _chequeDateController!.text,
          'amt': amount,
          'emp_id': empId,
          'ba': (double.parse(invoiceDetails![0]["grand_total"]) -
                  double.parse(invoiceDetails![0]["paid"]) -
                  double.parse(amount.toString()))
              .toString(),
          'dt': _dateController!.text,
          'bill_no': invoiceDetails![0]["bill_no"].toString(),
          'firm_name': '1',
          'cash': _currentItemSelected == 'Cash'
              ? '1'
              : (_currentItemSelected == 'Cheque' ? '2' : '1'),
          'total': invoiceDetails![0]["grand_total"],
          'onlinepaymenttype': '',
          'transaction_id': transId,
          'narration': '',
        });

    return "success";
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  Future _paidReceipt(BuildContext context) {
    String? temp;
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatefulBuilder(
            builder: (context, setState) {
              return Dialog(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.60,
                  child: Form(
                    key: _formKey,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Column(
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01,
                                        left:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      "Paid Amount",
                                      style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ],
                              ),
                              Divider(
                                thickness: 3.0,
                                color: Colors.grey.shade700,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01,
                                        left:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      "Bill No.",
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01,
                                        right:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      invoiceDetails![0]['bill_no'].toString(),
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01,
                                        left:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      "Bill Date",
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01,
                                        right:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      invoiceDetails![0]["dt"],
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01,
                                        left:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      "Amount",
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01,
                                        right:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      invoiceDetails![0]["grand_total"],
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01,
                                        left:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      "Paid ",
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01,
                                        right:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      (invoiceDetails![0]["paid"].toString()),
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01,
                                        left:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      "Balance Due ",
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01,
                                        right:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      (double.parse(invoiceDetails![0]
                                                  ["grand_total"]) -
                                              double.parse(
                                                  invoiceDetails![0]["paid"]))
                                          .toString(),
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.01,
                                      left: MediaQuery.of(context).size.height *
                                          0.01,
                                    ),
                                    child: Text(
                                      "Mode of Payment",
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Container(
                                    padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.01,
                                      right: MediaQuery.of(context).size.width *
                                          0.01,
                                    ),
                                    child: Container(
                                      height: 35,
                                      width: MediaQuery.of(context).size.width *
                                          0.4,
                                      decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        border: Border.all(
                                            color: Colors.grey.shade700,
                                            style: BorderStyle.solid,
                                            width: 1.0),
                                      ),
                                      child: DropdownButtonHideUnderline(
                                        child: ButtonTheme(
                                          alignedDropdown: true,
                                          child: DropdownButton<String>(
                                            isDense: true,
                                            autofocus: true,
                                            focusColor: Colors.lightBlue[900],
                                            //style: Theme.of(context).textTheme.title,
                                            isExpanded: true,
                                            hint: Text("Mode"),
                                            items: _modeOfPayment.map(
                                                (String dropDownStringItem) {
                                              return DropdownMenuItem<String>(
                                                  value: dropDownStringItem,
                                                  child: Text(
                                                      dropDownStringItem,
                                                      style: TextStyle(
                                                          fontSize: 16,
                                                          fontWeight:
                                                              FontWeight.w500,
                                                          color:
                                                              Colors.black)));
                                            }).toList(),
                                            onChanged:
                                                (String? newValueSelected) {
                                              setState(() {
                                                temp = newValueSelected.toString();
                                              });
                                            },
                                            value: temp,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01,
                                        left:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      "Date of Payment",
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Container(
                                      height: 40,
                                      padding: EdgeInsets.only(
                                          right: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.01,
                                          top: MediaQuery.of(
                                                      context)
                                                  .size
                                                  .width *
                                              0.01),
                                      width: MediaQuery.of(context).size.width *
                                          0.4,
                                      child: DateTimeField(
                                          format: DateFormat('dd/MM/yyy'),
                                          style: TextStyle(fontSize: 15),
                                          initialValue: DateTime.now(),
                                          controller: _dateController,
                                          decoration: InputDecoration(
                                              contentPadding: EdgeInsets.only(
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.02,
                                              ),
                                              border: OutlineInputBorder(
                                                  borderSide: BorderSide()),
                                              prefixIcon:
                                                  Icon(Icons.calendar_today)),
                                          resetIcon: null,
                                          onShowPicker:
                                              (context, currentValue) {
                                            return showDatePicker(
                                                context: context,
                                                initialDate: _dateTime == null
                                                    ? DateTime.now()
                                                    : _dateTime!.toUtc(),
                                                firstDate: DateTime(2001),
                                                lastDate: DateTime(2100));
                                          },
                                          validator: (val) {
                                            if (val != null) {
                                              return null;
                                            } else {
                                              return 'Date Field is Empty';
                                            }
                                          }))
                                ],
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.01,
                                        left:
                                            MediaQuery.of(context).size.height *
                                                0.01),
                                    child: Text(
                                      "Amount",
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Container(
                                    height: 40,
                                    padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.width *
                                          0.01,
                                      right: MediaQuery.of(context).size.width *
                                          0.01,
                                    ),
                                    width:
                                        MediaQuery.of(context).size.width * 0.4,
                                    child: TextFormField(
                                      focusNode: null,
                                      autofocus: false,
                                      validator: (value) {
                                        if (value == "") {
                                          return "Please Enter Amount";
                                        } else {
                                          return null;
                                        }
                                      },
                                      onSaved: (value) {
                                        setState(() {
                                          amount = value;
                                        });
                                      },
                                      decoration: InputDecoration(
                                          hintText: "Amount",
                                          suffixText: "₹",
                                          border: new OutlineInputBorder(
                                            borderSide: new BorderSide(),
                                          ),
                                          contentPadding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.015,
                                              left: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.015,
                                              right: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.015)),
                                      inputFormatters: [
                                        WhitelistingTextInputFormatter(
                                            RegExp(r"^\d*\.?\d*"))
                                      ],
                                      keyboardType:
                                          TextInputType.numberWithOptions(
                                              decimal: true, signed: false),
                                      style: new TextStyle(
                                        fontFamily: "Poppins",
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              temp == 'Cheque' && temp != null
                                  ? Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01,
                                              left: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01),
                                          child: Text(
                                            "Bank Name",
                                            style: TextStyle(
                                                fontSize: 17,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Container(
                                          height: 40,
                                          padding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.01,
                                            right: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.01,
                                          ),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.4,
                                          child: TextFormField(
                                            focusNode: null,
                                            autofocus: false,
                                            validator: (value) {
                                              if (value == "") {
                                                return "Please Enter Bank Name";
                                              } else {
                                                return null;
                                              }
                                            },
                                            onSaved: (value) {
                                              setState(() {
                                                bankName = value;
                                              });
                                            },
                                            decoration: InputDecoration(
                                                hintText: "Bank Name",
                                                suffixText: "₹",
                                                border: new OutlineInputBorder(
                                                  borderSide: new BorderSide(),
                                                ),
                                                contentPadding: EdgeInsets.only(
                                                    top: MediaQuery.of(context)
                                                            .size
                                                            .height *
                                                        0.015,
                                                    left: MediaQuery.of(context)
                                                            .size
                                                            .height *
                                                        0.015,
                                                    right:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.015)),
                                            keyboardType: TextInputType.text,
                                            style: new TextStyle(
                                              fontFamily: "Poppins",
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  : Container(),
                              temp == 'Cheque' && temp != null
                                  ? Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01,
                                              left: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01),
                                          child: Text(
                                            "Cheque No.",
                                            style: TextStyle(
                                                fontSize: 17,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Container(
                                          height: 40,
                                          padding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.01,
                                            right: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.01,
                                          ),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.4,
                                          child: TextFormField(
                                            focusNode: null,
                                            autofocus: false,
                                            validator: (value) {
                                              if (value == "") {
                                                return "Please Enter Cheque No.";
                                              } else {
                                                return null;
                                              }
                                            },
                                            onSaved: (value) {
                                              setState(() {
                                                chequeNo = value;
                                              });
                                            },
                                            decoration: InputDecoration(
                                                hintText: "Cheque No.",
                                                suffixText: "₹",
                                                border: new OutlineInputBorder(
                                                  borderSide: new BorderSide(),
                                                ),
                                                contentPadding: EdgeInsets.only(
                                                    top: MediaQuery.of(context)
                                                            .size
                                                            .height *
                                                        0.015,
                                                    left: MediaQuery.of(context)
                                                            .size
                                                            .height *
                                                        0.015,
                                                    right:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.015)),
                                            keyboardType:
                                                TextInputType.numberWithOptions(
                                                    decimal: true,
                                                    signed: false),
                                            style: new TextStyle(
                                              fontFamily: "Poppins",
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  : Container(),
                              temp == 'Cheque' && temp != null
                                  ? Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01,
                                              left: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01),
                                          child: Text(
                                            "Cheque Date",
                                            style: TextStyle(
                                                fontSize: 17,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Container(
                                            height: 40,
                                            padding: EdgeInsets.only(
                                                right: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.01,
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.01),
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.4,
                                            child: DateTimeField(
                                                format: DateFormat('dd/MM/yyy'),
                                                style: TextStyle(fontSize: 15),
                                                initialValue: DateTime.now(),
                                                controller:
                                                    _chequeDateController,
                                                decoration: InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.only(
                                                      top:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .height *
                                                              0.02,
                                                    ),
                                                    border: OutlineInputBorder(
                                                        borderSide:
                                                            BorderSide()),
                                                    prefixIcon: Icon(
                                                        Icons.calendar_today)),
                                                resetIcon: null,
                                                onShowPicker:
                                                    (context, currentValue) {
                                                  return showDatePicker(
                                                      context: context,
                                                      initialDate:
                                                          _dateTime == null
                                                              ? DateTime.now()
                                                              : _dateTime!.toUtc(),
                                                      firstDate: DateTime(2001),
                                                      lastDate: DateTime(2100));
                                                },
                                                validator: (val) {
                                                  if (val != null) {
                                                    return null;
                                                  } else {
                                                    return 'Date Field is Empty';
                                                  }
                                                }))
                                      ],
                                    )
                                  : Container(),
                              temp == 'Online' && temp != null
                                  ? Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01,
                                              left: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01),
                                          child: Text(
                                            "Trans Id",
                                            style: TextStyle(
                                                fontSize: 17,
                                                fontWeight: FontWeight.w500),
                                          ),
                                        ),
                                        Container(
                                          height: 40,
                                          padding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.01,
                                            right: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.01,
                                          ),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.4,
                                          child: TextFormField(
                                            focusNode: null,
                                            autofocus: false,
                                            validator: (value) {
                                              if (value == "") {
                                                return "Please Enter Trans Id";
                                              } else {
                                                return null;
                                              }
                                            },
                                            onSaved: (value) {
                                              setState(() {
                                                transId = value;
                                              });
                                            },
                                            decoration: InputDecoration(
                                                hintText: "Trans Id",
                                                suffixText: "₹",
                                                border: new OutlineInputBorder(
                                                  borderSide: new BorderSide(),
                                                ),
                                                contentPadding: EdgeInsets.only(
                                                    top: MediaQuery.of(context)
                                                            .size
                                                            .height *
                                                        0.015,
                                                    left: MediaQuery.of(context)
                                                            .size
                                                            .height *
                                                        0.015,
                                                    right:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height *
                                                            0.015)),
                                            keyboardType: TextInputType.text,
                                            style: new TextStyle(
                                              fontFamily: "Poppins",
                                            ),
                                          ),
                                        ),
                                      ],
                                    )
                                  : Container()
                            ],
                          ),
                        ),
                        Container(
                          height: 50,
                          width: MediaQuery.of(context).size.width * 1,
                          color: Colors.lightBlue[700],
                          child: FlatButton(
                              onPressed: () {
                                this._currentItemSelected = temp;
                                Navigator.of(context).pop();
                                waiting(context);

                                save();
                              },
                              child: Text(
                                "Save",
                                style: TextStyle(color: Colors.white),
                              )),
                        )
                      ],
                    ),
                  ),
                ),
              );
            },
          );
          ;
        });
  }

  // List<List<String>> _list() {
  //   return invoiceDetails!.map((prod) {
  //     return ['1', prod.newitem, '', prod.qty, prod.rate, prod.grand_total];
  //   }).toList();
  // }

  writeOnPdf() async {
    var empName = prefs!.getString('user');
    int counter = 0;
    final pdf = pw.Document();
    Printing.layoutPdf(
        name: "Invoice_" + widget.billNo.toString(),
        onLayout: (file) async {
          pdf.addPage(pw.MultiPage(
            pageFormat: PdfPageFormat.a4,
            margin: pw.EdgeInsets.all(32),
            header: (pw.Context context) {
              return pw.Container(
                  alignment: pw.Alignment.centerRight,
                  margin:
                      const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
                  padding:
                      const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
                  decoration: const pw.BoxDecoration(
                      border: pw.Border(bottom: pw.BorderSide(color: PdfColors.grey, width: 0.5))),
                  child: pw.Column(children: [
                    pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.center,
                        children: [
                          pw.Text('ABM Cons',
                              style: pw.Theme.of(context)
                                  .defaultTextStyle
                                  .copyWith(
                                      color: PdfColors.lightBlue700,
                                      fontSize: 18,
                                      fontWeight: pw.FontWeight.bold)),
                        ]),
                    pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.center,
                        children: [
                          pw.Text('Address : DAMANI NAGAR ,SUN CITY, SOLAPUR',
                              style: pw.Theme.of(context)
                                  .defaultTextStyle
                                  .copyWith(
                                      color: PdfColors.black, fontSize: 15))
                        ])
                  ]));
            },
            footer: (pw.Context context) {
              return pw.Container(
                  alignment: pw.Alignment.centerRight,
                  margin: const pw.EdgeInsets.only(top: 1.0 * PdfPageFormat.cm),
                  child: pw.Text(
                      'Page ${context.pageNumber} of ${context.pagesCount}',
                      style: pw.Theme.of(context)
                          .defaultTextStyle
                          .copyWith(color: PdfColors.grey)));
            },
            build: (pw.Context context) {
              return <pw.Widget>[
                pw.Column(children: [
                  pw.Row(children: [
                    pw.Container(
                        child: pw.Text('Bill To:',
                            style: pw.TextStyle(
                                color: PdfColors.black,
                                fontWeight: pw.FontWeight.normal,
                                fontSize: 10))),
                  ]),
                  pw.Row(children: [
                    pw.Container(
                        child: pw.Text(widget.custName,
                            style: pw.TextStyle(
                                color: PdfColors.lightBlue700,
                                fontWeight: pw.FontWeight.bold,
                                fontSize: 17)))
                  ]),
                  pw.Row(children: [
                    pw.Container(
                        child: pw.Text(
                            "Phone No. : " + invoiceDetails![0]["ph_no"],
                            style: pw.TextStyle(color: PdfColors.black)))
                  ]),
                  pw.Row(children: [
                    pw.Container(
                        child: pw.Text(
                            "Email Address : " + invoiceDetails![0]["email"],
                            style: pw.TextStyle(color: PdfColors.black)))
                  ]),
                  pw.Container(
                      child: pw.Center(
                          child: pw.Text(
                              (invoiceDetails![0]['cgst'] != '0' ||
                                      invoiceDetails![0]['cgst'] != ''
                                  ? 'Tax Invoice'
                                  : 'Sale Invoice'),
                              style: pw.TextStyle(
                                  color: PdfColors.black, fontSize: 20)))),
                  pw.Row(
                      mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                      children: [
                        pw.Container(
                            child: pw.Text("Bill From : ",
                                style: pw.TextStyle(color: PdfColors.black))),
                        pw.Container(
                            child: pw.Text("Bill No. : " + widget.billNo.toString(),
                                style: pw.TextStyle(
                                  color: PdfColors.black,
                                  fontWeight: pw.FontWeight.bold,
                                )))
                      ]),
                  pw.Row(
                      mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                      children: [
                        pw.Container(
                            child: pw.Text(empName,
                                style: pw.TextStyle(color: PdfColors.black))),
                        pw.Container(
                            child: pw.Text("Date : " + invoiceDetails![0]["dt"],
                                style: pw.TextStyle(
                                  color: PdfColors.black,
                                  fontWeight: pw.FontWeight.bold,
                                )))
                      ]),
                  pw.Container(
                    padding: pw.EdgeInsets.only(top: 10),
                    child: pw.Table.fromTextArray(
                        headerDecoration:
                            pw.BoxDecoration(color: PdfColors.lightBlue700),
                        headerStyle:
                            pw.TextStyle(color: PdfColors.white, fontSize: 15),
                        context: context,
                        cellStyle: pw.TextStyle(fontSize: 13),
                        cellAlignment: pw.Alignment.center,
                        data: <List>[
                          <String>[
                            'Sr',
                            'Particulars',
                            'HSN',
                            'Qty',
                            'Price',
                            'Amount'
                          ],
                          ...invoiceDetails!.map((e) {
                            counter++;
                            //print("invoice : " +e["ph_no"]);
                            return [
                              counter.toString(),
                              e['newitem'],
                              '',
                              e['qty'].toString(),
                              e['rate'].toString(),
                              (double.parse(e['rate']) * double.parse(e['qty']))
                                  .toString()
                            ];
                          })

                          //  invoiceDetails.map((prod) =>  ['1',prod.newitem, '', prod.qty, prod.rate, prod.grand_total] )
                        ]),
                  ),
                  pw.Container(
                    padding: pw.EdgeInsets.only(top: 10),
                    child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.end,
                        children: [
                          pw.Container(
                              width: 160,
                              child: pw.Row(
                                  mainAxisAlignment:
                                      pw.MainAxisAlignment.spaceBetween,
                                  children: [
                                    pw.Text("Total",
                                        style: pw.TextStyle(
                                          color: PdfColors.black,
                                          fontSize: 14,
                                        )),
                                    pw.Text(
                                        "Rs. " +
                                            invoiceDetails![0]["total_bill"],
                                        style: pw.TextStyle(
                                          color: PdfColors.black,
                                          fontSize: 14,
                                        ))
                                  ])),
                        ]),
                  ),
                  pw.Container(
                          //padding: pw.EdgeInsets.only(top: 10),
                          child: pw.Row(
                              mainAxisAlignment: pw.MainAxisAlignment.end,
                              children: [
                                pw.Container(
                                    width: 160,
                                    child: pw.Row(
                                        mainAxisAlignment:
                                            pw.MainAxisAlignment.spaceBetween,
                                        children: [
                                          pw.Text("GST ",
                                              style: pw.TextStyle(
                                                color: PdfColors.black,
                                                fontSize: 14,
                                              )),
                                          pw.Text(
                                              "Rs. " +
                                                  (double.parse(
                                                              invoiceDetails![0]
                                                                  ["cgst"]) +
                                                          double.parse(
                                                              invoiceDetails![0]
                                                                  ["sgst"]))
                                                      .toString(),
                                              style: pw.TextStyle(
                                                color: PdfColors.black,
                                                fontSize: 14,
                                              ))
                                        ])),
                              ]),
                        )
                    ,
                  pw.Container(
                    //padding: pw.EdgeInsets.only(top: 10),
                    child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.end,
                        children: [
                          pw.Container(
                              alignment: pw.Alignment.centerRight,
                              width: 160,
                              decoration: pw.BoxDecoration(
                                  color: PdfColors.lightBlue700),
                              child: pw.Row(
                                  mainAxisAlignment:
                                      pw.MainAxisAlignment.spaceBetween,
                                  children: [
                                    pw.Text("Subtotal ",
                                        style: pw.TextStyle(
                                            color: PdfColors.white,
                                            fontSize: 14,
                                            fontWeight: pw.FontWeight.bold)),
                                    pw.Text(
                                        "Rs. " +
                                            invoiceDetails![0]["grand_total"],
                                        style: pw.TextStyle(
                                            color: PdfColors.white,
                                            fontSize: 14,
                                            fontWeight: pw.FontWeight.bold))
                                  ])),
                        ]),
                  ),
                  pw.Container(
                    //padding: pw.EdgeInsets.only(top: 10),
                    child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.end,
                        children: [
                          pw.Container(
                              alignment: pw.Alignment.centerRight,
                              width: 160,
                              child: pw.Row(
                                  mainAxisAlignment:
                                      pw.MainAxisAlignment.spaceBetween,
                                  children: [
                                    pw.Text("Paid ",
                                        style: pw.TextStyle(
                                          fontSize: 14,
                                        )),
                                    pw.Text("Rs. " + invoiceDetails![0]["paid"],
                                        style: pw.TextStyle(
                                          fontSize: 14,
                                        ))
                                  ])),
                        ]),
                  ),
                  pw.Container(
                    //padding: pw.EdgeInsets.only(top: 10),
                    child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.end,
                        children: [
                          pw.Container(
                              alignment: pw.Alignment.centerRight,
                              width: 160,
                              child: pw.Row(
                                  mainAxisAlignment:
                                      pw.MainAxisAlignment.spaceBetween,
                                  children: [
                                    pw.Text("Balance",
                                        style: pw.TextStyle(
                                            fontSize: 14,
                                            fontWeight: pw.FontWeight.bold)),
                                    pw.Text(
                                        "Rs. " +
                                            (double.parse(invoiceDetails![0]
                                                        ["grand_total"]) -
                                                    double.parse(
                                                        invoiceDetails![0]
                                                            ["paid"]))
                                                .toString(),
                                        style: pw.TextStyle(
                                            fontSize: 14,
                                            fontWeight: pw.FontWeight.bold))
                                  ])),
                        ]),
                  ),
                  pw.Container(
                    padding: pw.EdgeInsets.only(top: 10, right: 60),
                    child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.end,
                        children: [
                          pw.Container(
                            child: pw.Text("For, " + empName,
                                style: pw.TextStyle(
                                    fontWeight: pw.FontWeight.normal)),
                          ),
                        ]),
                  ),
                  pw.Container(
                    padding: pw.EdgeInsets.only(top: 40),
                    child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.end,
                        children: [
                          pw.Container(
                            child: pw.Text("Authorized Signatory ",
                                style: pw.TextStyle(
                                    fontWeight: pw.FontWeight.normal)),
                          ),
                        ]),
                  ),
                ])
              ];
            },
          ));

          return pdf.save();
        });
  }

  shareOnPdf() async {
    var empName = prefs!.getString('user');
    int counter = 0;
    final pdf = pw.Document();

    pdf.addPage(pw.MultiPage(
      pageFormat: PdfPageFormat.a4,
      margin: pw.EdgeInsets.all(32),
      header: (pw.Context context) {
        return pw.Container(
            alignment: pw.Alignment.centerRight,
            margin: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
            padding: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
            decoration: const pw.BoxDecoration(
                border: pw.Border(bottom: pw.BorderSide(color: PdfColors.grey, width:0.5))),
            child: pw.Column(children: [
              pw.Row(mainAxisAlignment: pw.MainAxisAlignment.center, children: [
                pw.Text('ABM Cons',
                    style: pw.Theme.of(context).defaultTextStyle.copyWith(
                        color: PdfColors.lightBlue700,
                        fontSize: 18,
                        fontWeight: pw.FontWeight.bold))
              ]),
              pw.Row(mainAxisAlignment: pw.MainAxisAlignment.center, children: [
                pw.Text('Address : DAMANI NAGAR ,SUN CITY, SOLAPUR',
                    style: pw.Theme.of(context)
                        .defaultTextStyle
                        .copyWith(color: PdfColors.black, fontSize: 15))
              ])
            ]));
      },
      footer: (pw.Context context) {
        return pw.Container(
            alignment: pw.Alignment.centerRight,
            margin: const pw.EdgeInsets.only(top: 1.0 * PdfPageFormat.cm),
            child: pw.Text(
                'Page ${context.pageNumber} of ${context.pagesCount}',
                style: pw.Theme.of(context)
                    .defaultTextStyle
                    .copyWith(color: PdfColors.grey)));
      },
      build: (pw.Context context) {
        return <pw.Widget>[
          pw.Column(children: [
            pw.Row(children: [
              pw.Container(
                  child: pw.Text('Bill To:',
                      style: pw.TextStyle(
                          color: PdfColors.black,
                          fontWeight: pw.FontWeight.normal,
                          fontSize: 10))),
            ]),
            pw.Row(children: [
              pw.Container(
                  child: pw.Text(widget.custName,
                      style: pw.TextStyle(
                          color: PdfColors.lightBlue700,
                          fontWeight: pw.FontWeight.bold,
                          fontSize: 17)))
            ]),
            pw.Row(children: [
              pw.Container(
                  child: pw.Text("Phone No. : " + invoiceDetails![0]["ph_no"],
                      style: pw.TextStyle(color: PdfColors.black)))
            ]),
            pw.Row(children: [
              pw.Container(
                  child: pw.Text(
                      "Email Address : " + invoiceDetails![0]["email"],
                      style: pw.TextStyle(color: PdfColors.black)))
            ]),
            pw.Container(
                child: pw.Center(
                    child: pw.Text(
                        (invoiceDetails![0]['cgst'] != '0' ||
                                invoiceDetails![0]['cgst'] != ''
                            ? 'Tax Invoice'
                            : 'Sale Invoice'),
                        style: pw.TextStyle(
                            color: PdfColors.black, fontSize: 20)))),
            pw.Row(
                mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                children: [
                  pw.Container(
                      child: pw.Text("Bill From : ",
                          style: pw.TextStyle(color: PdfColors.black))),
                  pw.Container(
                      child: pw.Text("Invoice No. : " + widget.billNo.toString(),
                          style: pw.TextStyle(color: PdfColors.black)))
                ]),
            pw.Row(
                mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                children: [
                  pw.Container(
                      child: pw.Text(empName,
                          style: pw.TextStyle(color: PdfColors.black))),
                  pw.Container(
                      child: pw.Text("Date : " + invoiceDetails![0]["dt"],
                          style: pw.TextStyle(color: PdfColors.black)))
                ]),
            pw.Container(
              padding: pw.EdgeInsets.only(top: 10),
              child: pw.Table.fromTextArray(
                  headerDecoration:
                      pw.BoxDecoration(color: PdfColors.lightBlue700),
                  headerStyle:
                      pw.TextStyle(color: PdfColors.white, fontSize: 15),
                  context: context,
                  cellStyle: pw.TextStyle(fontSize: 13),
                  cellAlignment: pw.Alignment.center,
                  data: <List>[
                    <String>[
                      'Sr',
                      'Particulars',
                      'HSN',
                      'Qty',
                      'Price',
                      'Amount'
                    ],
                    ...invoiceDetails!.map((e) {
                      counter++;
                      //print("invoice : " +e["ph_no"]);
                      return [
                        counter.toString(),
                        e['newitem'],
                        '',
                        e['qty'].toString(),
                        e['rate'].toString(),
                        (double.parse(e['rate']) * double.parse(e['qty']))
                            .toString()
                      ];
                    })

                    //  invoiceDetails.map((prod) =>  ['1',prod.newitem, '', prod.qty, prod.rate, prod.grand_total] )
                  ]),
            ),
            pw.Container(
              padding: pw.EdgeInsets.only(top: 10),
              child: pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.end,
                  children: [
                    pw.Container(
                        width: 160,
                        child: pw.Row(
                            mainAxisAlignment:
                                pw.MainAxisAlignment.spaceBetween,
                            children: [
                              pw.Text("Total",
                                  style: pw.TextStyle(
                                    color: PdfColors.black,
                                    fontSize: 14,
                                  )),
                              pw.Text("Rs. " + invoiceDetails![0]["total_bill"],
                                  style: pw.TextStyle(
                                    color: PdfColors.black,
                                    fontSize: 14,
                                  ))
                            ])),
                  ]),
            ),
             pw.Container(
                    //padding: pw.EdgeInsets.only(top: 10),
                    child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.end,
                        children: [
                          pw.Container(
                              width: 160,
                              child: pw.Row(
                                  mainAxisAlignment:
                                      pw.MainAxisAlignment.spaceBetween,
                                  children: [
                                    pw.Text("GST ",
                                        style: pw.TextStyle(
                                          color: PdfColors.black,
                                          fontSize: 14,
                                        )),
                                    pw.Text(
                                        "Rs. " +
                                            (double.parse(invoiceDetails![0]
                                                        ["cgst"]) +
                                                    double.parse(
                                                        invoiceDetails![0]
                                                            ["sgst"]))
                                                .toString(),
                                        style: pw.TextStyle(
                                          color: PdfColors.black,
                                          fontSize: 14,
                                        ))
                                  ])),
                        ]),
                  )
             ,
            pw.Container(
              //padding: pw.EdgeInsets.only(top: 10),
              child: pw
                  .Row(mainAxisAlignment: pw.MainAxisAlignment.end, children: [
                pw.Container(
                    alignment: pw.Alignment.centerRight,
                    width: 160,
                    decoration: pw.BoxDecoration(color: PdfColors.lightBlue700),
                    child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                        children: [
                          pw.Text("Subtotal ",
                              style: pw.TextStyle(
                                  color: PdfColors.white,
                                  fontSize: 14,
                                  fontWeight: pw.FontWeight.bold)),
                          pw.Text("Rs. " + invoiceDetails![0]["grand_total"],
                              style: pw.TextStyle(
                                  color: PdfColors.white,
                                  fontSize: 14,
                                  fontWeight: pw.FontWeight.bold))
                        ])),
              ]),
            ),
            pw.Container(
              //padding: pw.EdgeInsets.only(top: 10),
              child: pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.end,
                  children: [
                    pw.Container(
                        alignment: pw.Alignment.centerRight,
                        width: 160,
                        child: pw.Row(
                            mainAxisAlignment:
                                pw.MainAxisAlignment.spaceBetween,
                            children: [
                              pw.Text("Paid ",
                                  style: pw.TextStyle(
                                    fontSize: 14,
                                  )),
                              pw.Text("Rs. " + invoiceDetails![0]["paid"],
                                  style: pw.TextStyle(
                                    fontSize: 14,
                                  ))
                            ])),
                  ]),
            ),
            pw.Container(
              //padding: pw.EdgeInsets.only(top: 10),
              child: pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.end,
                  children: [
                    pw.Container(
                        alignment: pw.Alignment.centerRight,
                        width: 160,
                        child: pw.Row(
                            mainAxisAlignment:
                                pw.MainAxisAlignment.spaceBetween,
                            children: [
                              pw.Text("Balance",
                                  style: pw.TextStyle(
                                      fontSize: 14,
                                      fontWeight: pw.FontWeight.bold)),
                              pw.Text(
                                  "Rs. " +
                                      (double.parse(invoiceDetails![0]
                                                  ["grand_total"]) -
                                              double.parse(
                                                  invoiceDetails![0]["paid"]))
                                          .toString(),
                                  style: pw.TextStyle(
                                      fontSize: 14,
                                      fontWeight: pw.FontWeight.bold))
                            ])),
                  ]),
            ),
            pw.Container(
              padding: pw.EdgeInsets.only(top: 10, right: 60),
              child: pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.end,
                  children: [
                    pw.Container(
                      child: pw.Text("For, " + empName,
                          style:
                              pw.TextStyle(fontWeight: pw.FontWeight.normal)),
                    ),
                  ]),
            ),
            pw.Container(
              padding: pw.EdgeInsets.only(
                top: 40,
              ),
              child: pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.end,
                  children: [
                    pw.Container(
                      child: pw.Text("Authorized Signatory ",
                          style:
                              pw.TextStyle(fontWeight: pw.FontWeight.normal)),
                    ),
                  ]),
            ),
          ])
        ];
      },
    ));

    String bill = widget.billNo.toString().replaceAll(new RegExp(r'\/'), '_');

    await Printing.sharePdf(
        bytes: pdf.save(),
        filename: (widget.salePurchase == 0
                ? 'Invoice_'
                : (widget.salePurchase == 0 ? 'Purchase_' : 'Quotation_')) +
            bill.toString() +
            '.pdf');
  }

  @override
  void initState() {
    super.initState();
    DateTime now = DateTime.now();

    String date = DateFormat("dd/MM/yyyy").format(now);
    _dateController = TextEditingController(text: date);
    _chequeDateController = TextEditingController(text: date);
    invoiceDetailsResponse = getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Invoice"),
        centerTitle: true,
        elevation: 0,
        backgroundColor: Colors.lightBlue[700],
        actions: [
          Container(
              child: IconButton(
            icon: Icon(Icons.print),
            onPressed: () async {
              if (widget.salePurchase == 0) {
                writeOnPdf();
              }
            },
            color: Colors.white,
          )),
        ],
      ),
      floatingActionButton: widget.salePurchase == 0
          ? FloatingActionButton(
              onPressed: () async {
                shareOnPdf();
              },
              child: Icon(Icons.share),
            )
          : Container(),
      body: Column(
        children: <Widget>[
          Container(
            height: 80,
            width: MediaQuery.of(context).size.width,
            color: Colors.lightBlue[700],
            child: Column(
              children: <Widget>[
                Container(
                    child: Text(
                  invoiceDetails == null ? "" : invoiceDetails![0]["bill_no"],
                  style: TextStyle(color: Colors.white, fontSize: 17),
                )),
                Container(
                    child: Icon(
                  Icons.assignment,
                  size: 40,
                  color: Colors.white,
                )),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                          right: MediaQuery.of(context).size.width * 0.09),
                      child: Text(
                        widget.custName.toString(),
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 17,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Container(
                      child: Text(
                          invoiceDetails![0]["dt"] == null
                              ? ''
                              : invoiceDetails![0]["dt"].toString(),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.normal)),
                    )
                  ],
                )
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.16,
            child: GridView.count(
              crossAxisCount: 3,
              primary: false,
              childAspectRatio: 6 / 5,
              padding: EdgeInsets.only(top: 10.0),
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              children: <Widget>[
                GestureDetector(
                    onTap: () {
                      //waiting(context);
                      if (widget.salePurchase == 0) {
                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) {
                          return SaleTabPage(
                            billNo: invoiceDetails![0]["bill_no"].toString(),
                            customerId: invoiceDetails![0]["cust_id"],
                            editFlag: 1,
                          );
                        })).then((value) {
                          this.getJsonData();
                        });
                      } else if (widget.salePurchase == 1) {
                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) {
                          return PurchaseTabPage(
                            billNo: invoiceDetails![0]["bill_no"].toString(),
                            customerId: invoiceDetails![0]["cust_id"],
                            editFlag: 1,
                          );
                        })).then((value) {
                          this.getJsonData();
                        });
                      } else {
                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) {
                          return QuotationPage(
                            billNo: invoiceDetails![0]["bill_no"].toString(),
                            customerId: invoiceDetails![0]["cust_id"],
                            editFlag: 1,
                          );
                        })).then((value) {
                          this.getJsonData();
                        });
                      }
                    },
                    child: Card(
                      elevation: 5.0,
                      child: Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Colors.green, Colors.lightGreen])),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.03),
                              child: new Center(
                                  child: Icon(
                                Icons.edit,
                                size: 20,
                                color: Colors.white,
                              )),
                            ),
                            new Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Text(
                                "Edit",
                                style: new TextStyle(
                                    color: Colors.white, fontSize: 15.0),
                              )),
                            ),
                          ],
                        ),
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      if (widget.salePurchase != 2) {
                        _paidReceipt(context);
                      }
                    },
                    child: Card(
                      elevation: 5.0,
                      child: Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Colors.blue, Colors.lightBlue])),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.03),
                              child: new Center(
                                  child: Icon(
                                Icons.payment,
                                size: 20,
                                color: Colors.white,
                              )),
                            ),
                            new Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Text(
                                "Paid",
                                style: new TextStyle(
                                    color: Colors.white, fontSize: 15.0),
                              )),
                            ),
                          ],
                        ),
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      if (widget.salePurchase == 2) {
                        waiting(context);
                        deleteQuotation().then((value) {
                          Navigator.of(context).pop();
                          _showDialogBox(
                              context, "Message", "Successfully Deleted!!!");
                        });
                      } else if (widget.salePurchase == 0) {
                        waiting(context);
                        deleteSale().then((value) {
                          Navigator.of(context).pop();
                          _showDialogBox(
                              context, "Message", "Successfully Deleted!!!");
                        });
                      } else {
                        waiting(context);
                        deletePurchase().then((value) {
                          Navigator.of(context).pop();
                          _showDialogBox(
                              context, "Message", "Successfully Deleted!!!");
                        });
                      }
                    },
                    child: Card(
                      elevation: 5.0,
                      child: Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                colors: [Colors.deepOrange, Colors.orange])),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.03),
                              child: new Center(
                                  child: Icon(Icons.delete_forever,
                                      size: 20, color: Colors.white)),
                            ),
                            new Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: new Center(
                                  child: Text(
                                "Delete",
                                style: new TextStyle(
                                    color: Colors.white, fontSize: 15.0),
                              )),
                            ),
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
          SingleChildScrollView(
            child: Column(
              children: <Widget>[
                FutureBuilder<String>(
                  future: invoiceDetailsResponse,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Column(
                        children: List.generate(
                            invoiceDetails == null ? 0 : invoiceDetails!.length,
                            (int index) {
                          return ProductDetails(
                            index: index,
                            change: null,
                          );
                        }),
                      );
                    } else {
                      return Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      child: Text(
                        "Subtotal ₹ " +
                            (invoiceDetails == null
                                ? ""
                                : invoiceDetails![0]["grand_total"].toString()),
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      child: Text(
                        "Total Paid ₹ " +
                            (invoiceDetails == null
                                ? ""
                                : invoiceDetails![0]["paid"].toString()),
                        style: TextStyle(
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ],
                ),
                Divider(
                  thickness: 3.0,
                  color: Colors.grey,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      child: Text(
                        "Balance Due ₹ " +
                            (invoiceDetails == null
                                ? ""
                                : (double.parse(
                                            invoiceDetails![0]["grand_total"]) -
                                        double.parse(invoiceDetails![0]["paid"]
                                            .toString()))
                                    .toString()),
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.w500),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

typedef CallBack = void Function();

class ProductDetails extends StatefulWidget {
  final int? index;
  final CallBack? change;
  ProductDetails({this.index, required this.change});
  @override
  State<StatefulWidget> createState() {
    return _ProductDetails();
  }
}

class _ProductDetails extends State<ProductDetails> {
  double? total, temp;
  @override
  void initState() {
    super.initState();
    total = 0;
    temp = 0;
    print(total);
  }

  callback() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[200],
      padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                height: 75,
                width: 60,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(color: Colors.grey, width: 1)),
                child: Image.network(
                  "https://qot.constructionsmall.com/" +
                      invoiceDetails![widget.index!.toInt()]["img"],
                  loadingBuilder: (context, child, progress) {
                    return progress == null ? child : LinearProgressIndicator();
                  },
                  fit: BoxFit.scaleDown,
                ),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.01),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.01),
                            child: Text(
                              invoiceDetails![widget.index!.toInt()]["newitem"]
                                  .toString(),
                              style: TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.01),
                            child: Text(
                              "Item Sub Total( " +
                                  invoiceDetails![widget.index!.toInt()]["qty"]
                                      .toString() +
                                  " X " +
                                  invoiceDetails![widget.index!.toInt()]["rate"]
                                      .toString() +
                                  " )",
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w500),
                            ),
                          ),
                          Container(
                            child: Text(
                              "₹ " +
                                  (double.parse(invoiceDetails![widget.index!.toInt()]
                                              ["qty"]) *
                                          double.parse(
                                              invoiceDetails![widget.index!.toInt()]
                                                  ["rate"]))
                                      .toString(),
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w500),
                            ),
                          )
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.01),
                            child: Text(
                              "Discount",
                              style: TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            child: Text(
                              "₹ " +
                                  (invoiceDetails![widget.index!.toInt()]
                                              ["discount_amt"] ==
                                          null
                                      ? "0"
                                      : invoiceDetails![widget.index!.toInt()]
                                          ["discount_amt"]),
                              style: TextStyle(
                                  fontSize: 17, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Divider(
            thickness: 1.0,
          )
        ],
      ),
    );
  }
}
