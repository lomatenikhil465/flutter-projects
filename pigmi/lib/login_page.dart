import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pigmi/home_page.dart';
import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import 'changePassword.dart';
import 'utils/const.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _Login();
  }
}

List? userData;

class _Login extends State<Login> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  bool _obscureText = true;
  SharedPreferences? prefs;
  String? username, passwd, otp;
  TextEditingController? otpController;
  final GlobalKey<State> _internetCheck = new GlobalKey<State>();
  // Map _source = {ConnectivityResult.none: false};
  // InternetConnectivity _connectivity = InternetConnectivity.instance;

  Future<String> getJsonData() async {
    prefs = await SharedPreferences.getInstance();
    String url = Constants.url +
        "app/pigmiAll.php?apicall=login&user=" +
        username.toString() +
        "&pass=" +
        passwd.toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      userData = convertDataToJson['user'];
    });

    //print(userData);
    return "success";
  }

  // waiting(BuildContext context) {
  //   return showDialog(
  //     context: context,
  //     builder: (BuildContext context) {
  //       // return object of type Dialog
  //       return WillPopScope(
  //         onWillPop: () async => false,
  //         child: AlertDialog(
  //             key: _keyLoader,
  //             content: Column(
  //               mainAxisAlignment: MainAxisAlignment.center,
  //               mainAxisSize: MainAxisSize.min,
  //               children: <Widget>[
  //                 Container(
  //                   child: CircularProgressIndicator(),
  //                 ),
  //                 Container(
  //                   child: Text("Loading..."),
  //                 )
  //               ],
  //             )),
  //       );
  //     },
  //   );
  // }

  Future enterOTPDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02),
                    color: Colors.blue[800],
                    height: 50,
                    child: Row(
                      children: [
                        Container(
                            child: Text("Enter OTP",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white)))
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02),
                    child: Text(
                      "OTP sent to your registered mobile No.",
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                        top: MediaQuery.of(context).size.height * 0.02),
                    child: Row(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.4,
                          child: TextFormField(
                            controller: otpController,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter OTP";
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                                labelText: "Enter OTP",
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15)),
                                contentPadding: EdgeInsets.only(
                                    top: 0, left: 10, bottom: 0, right: 0)),
                            keyboardType: TextInputType.numberWithOptions(
                                signed: false, decimal: true),
                            style: new TextStyle(
                                fontFamily: "Poppins", fontSize: 18),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          child: TextButton(
                              style: TextButton.styleFrom(
                                  //side: BorderSide(color: Colors.green),
                                  primary: Colors.white,
                                  backgroundColor: Colors.green,
                                  textStyle: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                  )),
                              onPressed: () {
                                //Navigator.of(context).pop();
                                
                                if (otpController!.text == otp.toString()) {
                                  Navigator.of(context).pop(true);
                                } else {
                                  //Navigator.of(context).pop(false);
                                  Constants.showDialogBox(
                                      context, "Error!", "Enter valid OTP.");
                                }
                              },
                              child: Text("Submit")),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.04,
                              right: MediaQuery.of(context).size.width * 0.02),
                          child: TextButton(
                              style: TextButton.styleFrom(
                                  primary: Colors.white,
                                  backgroundColor: Colors.blue[800],
                                  textStyle: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                  )),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text("Close")),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  void _validateInputs() {
    //checkConnection();
    if (_formKey.currentState!.validate()) {
      Constants.waiting(context);
//    If all data are correct then save data to out variables
      _formKey.currentState!.save();
      getJsonData().then((value) {
        Navigator.of(context).pop();
        if (userData != null) {
          print(userData![0]["userId"]);
          getOtp().then((value) {
            enterOTPDialog(context).then((value) {
              if(value == true){
              //Constants.waiting(context);
              Constants.showDialogBox(context, "Successfully", "");
              prefs!.setString('user', username.toString());
              prefs!.setString('pass', passwd.toString());
              prefs!.setString('empId', userData![0]["userId"]);
              prefs!.setString('pType', userData![0]["pType"].toString());
              Navigator.of(context)
                  .pushReplacement(MaterialPageRoute(builder: (context) {
                return HomePage(
                  username: username.toString(),
                );
              }));
              } else{

              }
            });
            
          });
        } else {
          Constants.showDialogBox(
              context, "Error!", "Please Check your Username and Password.");
        }
      });
      _formKey.currentState!.reset();
    } else {
//    If all data are not valid then start auto validation.

      setState(() {
        _autoValidate = true;
      });
    }
  }

  String? validateUsername(String? value) {
    if (value!.length < 3)
      return 'Username must be more than 2 charater';
    else
      return null;
  }

  Future _showDialogBox(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              backgroundColor: Colors.transparent,
              key: _internetCheck,
              content: Container(
                height: 500,
                decoration: BoxDecoration(
                  border: Border.all(width: 5, color: Colors.white),
                  borderRadius: BorderRadius.circular(10),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.topCenter,
                      child: Image.asset("assets\\images\\no-internet.png"),
                    ),
                    Container(
                      child: Text(
                        "Please Check Your \nInternet Connection",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.w500),
                      ),
                    ),
                    Container(
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        color: Color.fromRGBO(255, 104, 90, 1),
                        child: Text(
                          "Try Again",
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                    )
                  ],
                ),
              )),
        );
      },
    );
  }

//   void checkConnection(){
// switch (_source.keys.toList()[0]) {
//       case ConnectivityResult.none:

//         _showDialogBox(context,"Warning","Check your Internet Connection");
//         break;
// }
//   }

  Future<String> getOtp() async {
    String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=getOTP&userId=" +
        empId.toString() +
        "&contact=" +
        userData![0]["ph"].toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      otp = convertDataToJson['user'];
    });
    return "success";
  }

  @override
  void initState() {
    super.initState();
// _connectivity.initialise();
//     _connectivity.myStream.listen((source) {
//       setState(() => _source = source);
//     });
    otpController = TextEditingController(text: "");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: Form(
            key: _formKey,
            autovalidate: _autoValidate,
            child: Container(
              padding: EdgeInsets.all(30.0),
              child: Center(
                  child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.center,
                      child: Image.asset(
                        "assets\\images\\jf log1.jpg",
                        width: MediaQuery.of(context).size.width / 1.5,
                      ),
                    ),
                    // Container(
                    //   child:Text("JF" , style: TextStyle(
                    //     color: Colors.blue[800],
                    //     fontSize: 20,
                    //     fontWeight: FontWeight.bold
                    //   ),)
                    // ),
                    Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.02),
                      child: TextFormField(
                        validator: validateUsername,
                        onSaved: (String? user) {
                          username = user.toString();
                        },
                        decoration: InputDecoration(
                          labelText: "Username",
                          contentPadding:
                              EdgeInsets.only(left: 10.0, right: 2.0),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.03),
                      child: TextFormField(
                        onSaved: (String? pass) {
                          passwd = pass.toString();
                        },
                        validator: (value) {
                          if (value == "") {
                            return "please enter password";
                          } else {
                            return null;
                          }
                        },
                        decoration: InputDecoration(
                          suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                _obscureText = !_obscureText;
                              });
                            },
                            child: Icon(
                              _obscureText
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                            ),
                          ),
                          labelText: "Password",
                          contentPadding:
                              EdgeInsets.only(left: 10.0, right: 2.0),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        obscureText: _obscureText,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 50.0),
                    ),
                    RaisedButton(
                      elevation: 3.0,
                      onPressed: () {
                        //_showDialogBox(context, "", "");
                        _validateInputs();
                        // Navigator.of(context).pushReplacement(
                        //     MaterialPageRoute(builder: (context) {
                        //   return HomePage(
                        //     username: username.toString(),
                        //   );
                        // }));
                      },
                      shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side:
                            BorderSide(width: 1.5, color: Colors.blue.shade800),
                      ),
                      textColor: Colors.blue[800],
                      padding: EdgeInsets.only(
                          right: 100.0, left: 100.0, top: 10.0, bottom: 10.0),
                      color: Colors.white,
                      child: Text(
                        "Login",
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            textBaseline: TextBaseline.alphabetic),
                      ),
                    ),
                    Container(
                      child: TextButton(
                        onPressed: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return ChangePassword(
                              inOut: 2,
                            );
                          }));
                        },
                        style: TextButton.styleFrom(primary: Colors.blue[800]),
                        child: Text("Forgot Password?"),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.25),
                      child: Text(
                        "© Designed by SMiT Solutions",
                        style: TextStyle(fontSize: 10),
                      ),
                    )
                  ],
                ),
              )),
            )));
  }
}
