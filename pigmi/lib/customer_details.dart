import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:pigmi/profile_page.dart';
import 'package:pigmi/report/pending_customer_info.dart';
import 'package:pigmi/utils/const.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'dart:convert';

import 'receive_payment.dart';

class CustomerDetails extends StatefulWidget {
  final String? custId, name, total, accNo, startDt, endDt;
  final int? dWM, index;
  final List? customer;
  CustomerDetails(
      {this.custId,
      this.name,
      this.total,
      this.accNo,
      this.dWM,
      this.startDt,
      this.endDt,
      this.customer,
      this.index});
  @override
  State<StatefulWidget> createState() {
    return _CustomerDetails();
  }
}

class _CustomerDetails extends State<CustomerDetails> {
  List? customerStatement, customerTotalInvoice;
  Future<String>? customerStatementResponse;
  int flag = 0;

  SharedPreferences? prefs;
  double totalPayment = 0.0, totalInterest = 0.0;
  var temp = 0.0;
  String? amount, intAmount;
  TextEditingController? amountController, intAmountController;

  Future<String> getJsonData() async {
    String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiReport.php?apicall=customerReport&empid=" +
        empId.toString() +
        "&pType=" +
        widget.dWM.toString() +
        "&custId=" +
        widget.custId.toString() +
        "&accNo=" +
        widget.accNo.toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      customerStatement = convertDataToJson['user'];
    });

    if (customerStatement != null) {
      for (var i = 0; i < customerStatement!.length; i++) {
        totalPayment =
            totalPayment + double.parse(customerStatement![i]["amt"]);
        if (widget.dWM == 3) {
          totalInterest += double.parse(customerStatement![i]["int_Amt"]);
        }
      }
    }
    return "success";
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  Future<String> updateAmount(String date, int index) async {
    String url = Constants.url +
        "app/pigmiReport.php?apicall=" +
        (widget.dWM == 3 ? "UpdateAmtMonthly&" : "updateCustomer&");
    String? empId = prefs!.getString("empId");
    await http.post(Uri.parse(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'accNo': widget.accNo.toString(),
          'pType': widget.dWM.toString(),
          'amt': amountController!.text,
          if (widget.dWM == 3) 'intamt': intAmountController!.text,
          'userId': empId.toString(),
          'custId': widget.custId.toString(),
          if (widget.dWM != 3) 'dt': date,
          'ID': customerStatement![index]["ID"].toString()
        }).then((value) {
      print(value.body);
    });

    //print(response.reasonPhrase);

    return "success";
  }

  Future updateAmountDialog(BuildContext context, int index) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02),
                    color: Colors.blue[800],
                    height: 50,
                    child: Row(
                      children: [
                        Container(
                            child: Text("Update Amount",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white)))
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            "Date : ",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          child: Text(
                            DateFormat("dd MMM yyyy").format(DateTime.parse(
                                customerStatement![index]["dt"])),
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                        top: MediaQuery.of(context).size.height * 0.02),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            "Amount :",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.45,
                          child: TextFormField(
                            controller: amountController,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter Amount";
                              } else {
                                return null;
                              }
                            },
                            onSaved: (String? value) {
                              setState(() {
                                amount = value;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: "Enter Amount",
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15)),
                                contentPadding: EdgeInsets.only(
                                    top: 0, left: 10, bottom: 0, right: 0)),
                            keyboardType: TextInputType.numberWithOptions(
                                signed: false, decimal: true),
                            style: new TextStyle(
                                fontFamily: "Poppins", fontSize: 18),
                          ),
                        ),
                      ],
                    ),
                  ),
                  if (widget.dWM == 3)
                    Container(
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.02,
                          right: MediaQuery.of(context).size.width * 0.02,
                          top: MediaQuery.of(context).size.height * 0.02),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text(
                              "Int. Amount :",
                              style: TextStyle(
                                  fontSize: 18, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                            width: MediaQuery.of(context).size.width * 0.45,
                            child: TextFormField(
                              controller: intAmountController,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Please Enter Amount";
                                } else {
                                  return null;
                                }
                              },
                              onSaved: (String? value) {
                                setState(() {
                                  intAmount = value;
                                });
                              },
                              decoration: InputDecoration(
                                  labelText: "Enter Int Amount",
                                  border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15)),
                                  contentPadding: EdgeInsets.only(
                                      top: 0, left: 10, bottom: 0, right: 0)),
                              keyboardType: TextInputType.numberWithOptions(
                                  signed: false, decimal: true),
                              style: new TextStyle(
                                  fontFamily: "Poppins", fontSize: 18),
                            ),
                          ),
                        ],
                      ),
                    ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          child: TextButton(
                              style: TextButton.styleFrom(
                                  //side: BorderSide(color: Colors.green),
                                  primary: Colors.white,
                                  backgroundColor: Colors.green,
                                  textStyle: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                  )),
                              onPressed: () {
                                //Navigator.of(context).pop();
                                Constants.waiting(context);
                                updateAmount(
                                        customerStatement![index]["dt"]
                                            .toString(),
                                        index)
                                    .then((value) {
                                  Navigator.of(context).pop();
                                  setState(() {
                                    totalPayment = 0.0;
                                    temp = 0.0;
                                    amountController =
                                        TextEditingController(text: "");
                                    customerStatementResponse = getJsonData();
                                  });
                                });
                                Navigator.of(context).pop();
                              },
                              child: Text("Update")),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.04,
                              right: MediaQuery.of(context).size.width * 0.02),
                          child: TextButton(
                              style: TextButton.styleFrom(
                                  primary: Colors.white,
                                  backgroundColor: Colors.blue[800],
                                  textStyle: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                  )),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text("Close")),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    totalPayment = 0.0;
    temp = 0.0;
    amountController = TextEditingController(text: "");
    intAmountController = TextEditingController(text: "");
    print(widget.customer);
    getEmpId().then((value) {
      customerStatementResponse = getJsonData();
    });

    //getTotal();
  }

  Future pendingInfo(BuildContext context, int index) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02),
                    color: Colors.blue[800],
                    height: 50,
                    child: Row(
                      children: [
                        Container(
                            child: Text("Pending Adjustment",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white)))
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            "Entry Date : ",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                          child: Text(
                            DateFormat("dd MMM yyyy").format(DateTime.parse(
                                customerStatement![index]["pdt"])),
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                        top: MediaQuery.of(context).size.height * 0.02),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            "Total Pending Amt. :",
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "₹ " + customerStatement![index]["pAmt"],
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.red),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                        top: MediaQuery.of(context).size.height * 0.02),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            "Adjusted Date : ",
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            DateFormat("dd MMM yyyy").format(DateTime.parse(
                                customerStatement![index]["dt"])),
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                        top: MediaQuery.of(context).size.height * 0.02),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(
                            "Principal Amt. :",
                            style: TextStyle(
                              fontSize: 18,
                            ),
                          ),
                        ),
                        Container(
                          child: Text(
                            "₹ " + customerStatement![index]["amt"],
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.green),
                          ),
                        )
                      ],
                    ),
                  ),
                  if (widget.dWM == 3)
                    Container(
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.02,
                          right: MediaQuery.of(context).size.width * 0.02,
                          top: MediaQuery.of(context).size.height * 0.02),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Text(
                              "Interest Amt. :",
                              style: TextStyle(
                                fontSize: 18,
                              ),
                            ),
                          ),
                          Container(
                            child: Text(
                              "₹ " + customerStatement![index]["int_Amt"],
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.green),
                            ),
                          )
                        ],
                      ),
                    ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.04,
                              right: MediaQuery.of(context).size.width * 0.02),
                          child: TextButton(
                              style: TextButton.styleFrom(
                                  primary: Colors.white,
                                  backgroundColor: Colors.blue[800],
                                  textStyle: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                  )),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text("Close")),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: GestureDetector(
          onTap: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return ProfilePage(accNo: widget.accNo, custId: widget.custId);
            }));
          },
          child: Container(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.01,
            ),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                // Container(
                //       child:  CircleAvatar(
                //             radius: 25,
                //             backgroundColor: Colors.grey[200],
                //             child:(
                //                     widget.customer![0]["pf_img_path"] != "" && widget.customer![0][widget.index]["pf_img_path"] != "0"

                //                 ? ClipRRect(
                //                     borderRadius: BorderRadius.circular(100),
                //                     child: Image.network(
                //                      Constants.url +   widget.customer![0]["pf_img_path"].toString(),
                //                       width: 300,
                //                       height: 300,
                //                       fit: BoxFit.fitHeight,
                //                     ),
                //                   )
                //                 : Container(
                //                     decoration: BoxDecoration(
                //                         color: Colors.grey[200],
                //                         borderRadius: BorderRadius.circular(100)),
                //                     width: 300,
                //                     height: 300,
                //                     child: Icon(
                //                       Icons.person,
                //                       color: Colors.grey[800],
                //                     ),
                //                   ))
                //           ),
                //     ),
                Row(
                  children: [
                    Container(
                      child: CircleAvatar(
                        radius: 20,
                        backgroundColor: Colors.grey[200],
                        child: (widget.customer![widget.index!.toInt()]
                                        ["pf_img_path"] !=
                                    "" &&
                                widget.customer![widget.index!.toInt()]
                                        ["pf_img_path"] !=
                                    "0")
                            ? ClipRRect(
                                borderRadius: BorderRadius.circular(100),
                                child: Image.network(
                                  Constants.url +
                                      (widget.customer![widget.index!.toInt()]
                                              ["pf_img_path"])
                                          .toString(),
                                  width: 100,
                                  height: 100,
                                  fit: BoxFit.fitHeight,
                                ),
                              )
                            : Container(
                                decoration: BoxDecoration(
                                    color: Colors.grey[200],
                                    borderRadius: BorderRadius.circular(100)),
                                width: 300,
                                height: 300,
                                child: Icon(
                                  Icons.person,
                                  color: Colors.grey[800],
                                ),
                              ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.04),
                      child: Column(
                        children: [
                          Text(
                            widget.name.toString(),
                            style: TextStyle(fontSize: 18),
                          ),
                          Text(
                            widget.accNo.toString(),
                            style: TextStyle(fontSize: 14),
                          ),
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
        backgroundColor: Colors.blue[800],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (customerStatement == null) {
            final snackBar = SnackBar(
              content: Text('No any statements to be print...'),
            );
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          } else {
            Constants.shareOnPdf(
                customerStatement,
                widget.accNo,
                totalPayment,
                widget.endDt,
                widget.name,
                widget.startDt,
                widget.total,
                widget.customer,
                widget.dWM,
                totalInterest);
          }
        },
        backgroundColor: Colors.blue[800],
        heroTag: "Sharing",
        child: Icon(Icons.share),
      ),
      body: Column(
        children: <Widget>[
          Container(
            //height: 80,
            width: MediaQuery.of(context).size.width,
            color: Colors.blue[800],
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.08,
                      right: MediaQuery.of(context).size.width * 0.08),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          child: Text(
                        "Total Amt",
                        style: TextStyle(color: Colors.grey[300], fontSize: 14),
                      )),
                      Container(
                          child: Text(
                        "Total Received",
                        style: TextStyle(color: Colors.grey[300], fontSize: 14),
                      )),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.08,
                      right: MediaQuery.of(context).size.width * 0.08),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          "₹ " +
                              double.parse(widget.total.toString())
                                  .toStringAsFixed(2),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 22,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                        child: Text(
                          "₹ " + totalPayment.toStringAsFixed(2),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 22,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
                // if (widget.dWM == 3)
                //   Container(
                //     padding: EdgeInsets.only(
                //         left: MediaQuery.of(context).size.width * 0.08,
                //         right: MediaQuery.of(context).size.width * 0.08),
                //     child: Row(
                //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //       children: [
                //         Container(
                //             child: Text(
                //           "Total Interest",
                //           style:
                //               TextStyle(color: Colors.grey[300], fontSize: 14),
                //         )),
                //         Container(
                //             child: Text(
                //           "Total Received",
                //           style:
                //               TextStyle(color: Colors.grey[300], fontSize: 14),
                //         )),
                //       ],
                //     ),
                //   ),
                // if (widget.dWM == 3)
                //   Container(
                //     padding: EdgeInsets.only(
                //         left: MediaQuery.of(context).size.width * 0.08,
                //         right: MediaQuery.of(context).size.width * 0.08),
                //     child: Row(
                //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //       children: [
                //         Container(
                //           child: Text(
                //             "₹ " +
                //                 (double.parse(widget.customer![widget.index!
                //                             .toInt()]["interest_amt"]) *
                //                         double.parse(widget.customer![
                //                             widget.index!.toInt()]["duration"]))
                //                     .toStringAsFixed(2),
                //             style: TextStyle(
                //                 color: Colors.white,
                //                 fontSize: 22,
                //                 fontWeight: FontWeight.w500),
                //           ),
                //         ),
                //         Container(
                //           child: Text(
                //             "₹ " + totalInterest.toStringAsFixed(2),
                //             style: TextStyle(
                //                 color: Colors.white,
                //                 fontSize: 22,
                //                 fontWeight: FontWeight.w500),
                //           ),
                //         ),
                //       ],
                //     ),
                //   ),
                Container(
                  padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.08,
                      right: MediaQuery.of(context).size.width * 0.08,
                      top: MediaQuery.of(context).size.height * 0.01),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        child: Text(
                          "Start Dt.: " +
                              DateFormat("dd MMM yyyy").format(
                                  DateTime.parse(widget.startDt.toString())),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Container(
                        child: Text(
                          "End Dt.: " +
                              DateFormat("dd MMM yyyy").format(
                                  DateTime.parse(widget.endDt.toString())),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.16,
            child: GridView.count(
              crossAxisCount: 3,
              primary: false,
              childAspectRatio: 10 / 9,
              padding: EdgeInsets.only(top: 10.0, left: 10, right: 10),
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              children: <Widget>[
                GestureDetector(
                    onTap: () {
                      // Future<void> sheetController = showModalBottomSheet<void>(
                      //     context: context,
                      //     builder: (context) => BottomSheetWidget(
                      //           custId: widget.custId.toString(),
                      //           sale: 0,
                      //           purchase: 1,
                      //         ),
                      //     backgroundColor: Colors.transparent);

                      // sheetController.then((value) {
                      //   print("sheet closed");
                      // });
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return ReceivePayment(
                          dWM: widget.dWM,
                          customerName: widget.accNo,
                        );
                      }));
                    },
                    child: Card(
                      elevation: 5.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            gradient: LinearGradient(
                                colors: [Colors.deepOrange, Colors.orange])),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          // crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              child: Icon(
                                Icons.payment,
                                color: Colors.white,
                                size: 30,
                              ),
                            ),
                            new Container(
                                // padding: EdgeInsets.only(
                                //     top: MediaQuery.of(context).size.height *
                                //         0.035),

                                child: Text(
                              "     New \nPayment",
                              style: new TextStyle(
                                  color: Colors.white,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w500),
                            )),
                          ],
                        ),
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      // final snackBar = SnackBar(
                      //   content: Text('Coming Soon...'),
                      // );
                      // ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      if (customerStatement == null) {
                        final snackBar = SnackBar(
                          content: Text('No any statements to be print...'),
                        );
                        ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      } else {
                        Constants.writeOnPdf(
                            customerStatement,
                            widget.accNo,
                            totalPayment,
                            widget.endDt,
                            widget.name,
                            widget.startDt,
                            widget.total,
                            widget.customer,
                            widget.dWM,
                            totalInterest);
                      }
                    },
                    child: Card(
                      elevation: 5.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            gradient: LinearGradient(
                                colors: [Colors.green, Colors.lightGreen])),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Container(
                              child: Icon(
                                Icons.print,
                                color: Colors.white,
                                size: 30,
                              ),
                            ),
                            new Container(
                              child: new Center(
                                  child: Text(
                                "     Print\nStatement",
                                style: new TextStyle(
                                    color: Colors.white,
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w500),
                              )),
                            ),
                          ],
                        ),
                      ),
                    )),
                GestureDetector(
                    onTap: () {
                      // final snackBar = SnackBar(
                      //   content: Text('Coming Soon...'),
                      // );
                      // ScaffoldMessenger.of(context).showSnackBar(snackBar);
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return PendingCustomerInfo(
                          custId: customerStatement![0]["regId"],
                          dWM: widget.dWM,
                          fromDt: widget.startDt,
                          toDt: DateFormat("yyyy-MM-dd").format(DateTime.now()),
                        );
                      }));
                    },
                    child: Card(
                      elevation: 5.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      child: Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.03),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            gradient: LinearGradient(colors: [
                              Colors.blue.shade800,
                              Colors.lightBlue
                            ])),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            new Container(
                                child: Text(
                              "  Pending",
                              style: new TextStyle(
                                  color: Colors.white,
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.w500),
                            )),
                            Container(
                              child: Text(
                                "₹ " +
                                    (double.parse(widget.total.toString()) -
                                            double.parse(
                                                totalPayment.toString()))
                                        .toStringAsFixed(2),
                                style: TextStyle(
                                    fontSize: 16,
                                    color: Colors.white,
                                    fontWeight: FontWeight.w500),
                              ),
                            )
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
          Expanded(
            child: customerStatement == null
                ? Center(
                    child: Text("No Transaction Data..."),
                  )
                : FutureBuilder<String>(
                    future: customerStatementResponse,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                          itemCount: customerStatement == null
                              ? 0
                              : customerStatement!.length,
                          itemBuilder: (BuildContext context, int index) {
                            return GestureDetector(
                              onTap: customerStatement![index]["psts"] == '1'
                                  ? () {
                                      pendingInfo(context, index);
                                    }
                                  : null,
                              child: Container(
                                height: 40,
                                child: Card(
                                  elevation: 3.0,
                                  child: Container(
                                    padding: EdgeInsets.only(
                                        left:
                                            MediaQuery.of(context).size.width *
                                                0.05),
                                    child: Row(
                                      mainAxisAlignment: (widget.dWM == 3
                                          ? MainAxisAlignment.spaceBetween
                                          : MainAxisAlignment.start),
                                      children: <Widget>[
                                        if (customerStatement![index]["psts"] ==
                                            '1')
                                          Container(
                                            child: Text(
                                              "*",
                                              style: TextStyle(
                                                  color: Colors.red,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20),
                                            ),
                                          ),
                                        Flexible(
                                          child: Container(
                                            child: Text(
                                              DateFormat("dd MMM yyyy").format(
                                                  DateTime.parse(
                                                      customerStatement![index]
                                                          ["dt"])),
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                        ),
                                        if (widget.dWM == 3)
                                          Container(
                                            child: Text(
                                              customerStatement![index]
                                                              ["amt"] !=
                                                          "0" &&
                                                      customerStatement![index]
                                                              ["int_Amt"] !=
                                                          "0"
                                                  ? "Int. + Principal"
                                                  : customerStatement![index]
                                                              ["int_Amt"] !=
                                                          "0"
                                                      ? "Interest"
                                                      : "Principal",
                                              style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                        if (widget.dWM != 3)
                                          Spacer(
                                            flex: 2,
                                          ),
                                        Container(
                                          child: Text(
                                            "₹ " +
                                                (widget.dWM == 3
                                                    ? (double.parse(
                                                                customerStatement![
                                                                        index]
                                                                    ["amt"]) +
                                                            double.parse(
                                                                customerStatement![
                                                                        index][
                                                                    "int_Amt"]))
                                                        .toStringAsFixed(2)
                                                    : customerStatement![index]
                                                            ["amt"]
                                                        .toString()),
                                            style: TextStyle(
                                              fontWeight: FontWeight.w500,
                                            ),
                                          ),
                                        ),
                                        Container(
                                            // height: 20,
                                            child: IconButton(
                                          onPressed: () {
                                            amountController!.text =
                                                customerStatement![index]["amt"]
                                                    .toString();

                                            intAmountController!.text =
                                                customerStatement![index]
                                                        ["int_Amt"]
                                                    .toString();
                                            updateAmountDialog(context, index);
                                          },
                                          alignment: Alignment.center,
                                          iconSize: 20,
                                          icon: Icon(Icons.edit),
                                          color: Colors.blue[800],
                                        )),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        );
                      } else {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                  ),
          )
        ],
      ),
    );
  }
}
