import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pdf/pdf.dart';
import 'package:printing/printing.dart';
import 'package:pdf/widgets.dart' as pw;

class Constants {
  static String url = "https://pigmi.smitsolutions.co.in/";

  static waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              content: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                child: CircularProgressIndicator(),
              ),
              Container(
                child: Text("Loading..."),
              )
            ],
          )),
        );
      },
    );
  }

  static Future showDialogBox(
      BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Container(
            width: 50,
            height: 50,
            child: AlertDialog(
              title: Text(
                title,
                style: TextStyle(color: Colors.red),
              ),
              content: Container(
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  if (content != "") Text(content),
                  if (content == "")
                    title != 'Already Exists'
                        ? Image.asset(
                            "assets\\images\\correct.gif",
                            width: 200,
                            height: 200,
                          )
                        : Icon(
                            Icons.error,
                            size: 50,
                          )
                ],
              )),
              actions: <Widget>[
                FlatButton(
                    onPressed: () => Navigator.of(context).pop(),
                    child: Text("Close"))
              ],
            ),
          );
        });
  }

  static Future confirmationMsg(
      BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Container(
            width: 50,
            height: 50,
            child: AlertDialog(
              title: Text(
                title,
                style: TextStyle(color: Colors.red),
              ),
              content: Container(
                  child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(content),
                ],
              )),
              actions: <Widget>[
                TextButton(
                    onPressed: () => Navigator.of(context).pop(true),
                    child: Text("Yes")),
                TextButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: Text("No")),
              ],
            ),
          );
        });
  }

  static writeOnPdf(
      List? invoiceDetails,
      var accNo,
      var totalPayment,
      var endDt,
      var name,
      var startDt,
      var total,
      var customer,
      var dWM,
      var totalInterest) async {
    int counter = 0;
    final pdf = pw.Document();
    final image = await imageFromAssetBundle('assets\\images\\jf log1.jpg');
    Printing.layoutPdf(
        name: "Invoice_" + accNo.toString(),
        onLayout: (file) async {
          pdf.addPage(pw.MultiPage(
            pageFormat: PdfPageFormat.a4,
            margin: pw.EdgeInsets.all(32),
            header: (pw.Context context) {
              return pw.Container(
                  alignment: pw.Alignment.centerRight,
                  margin:
                      const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
                  padding:
                      const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
                  decoration: const pw.BoxDecoration(
                      border: pw.Border(
                          bottom: pw.BorderSide(
                              color: PdfColors.grey, width: 0.5))),
                  child: pw.Column(children: [
                    pw.Row(
                        //mainAxisAlignment: pw.MainAxisAlignment.center,
                        children: [
                          pw.Container(
                            width: 100,
                            height: 100,
                            child: pw.Image(
                              image,
                            ),
                          ),
                          pw.Container(
                            child: pw.Column(children: [
                              pw.Text('JOGISIDDHA FINANCE',
                                  style: pw.Theme.of(context)
                                      .defaultTextStyle
                                      .copyWith(
                                          color: PdfColors.lightBlue700,
                                          fontSize: 18,
                                          fontWeight: pw.FontWeight.bold)),
                              pw.Text('ADDRESS :  SOLAPUR',
                                  style: pw.Theme.of(context)
                                      .defaultTextStyle
                                      .copyWith(
                                          color: PdfColors.black, fontSize: 15))
                            ]),
                          ),
                        ]),
                    // pw.Row(
                    //     mainAxisAlignment: pw.MainAxisAlignment.center,
                    //     children: [

                    //     ])
                  ]));
            },
            footer: (pw.Context context) {
              return pw.Container(
                  alignment: pw.Alignment.centerRight,
                  margin: const pw.EdgeInsets.only(top: 1.0 * PdfPageFormat.cm),
                  child: pw.Text(
                      'Page ${context.pageNumber} of ${context.pagesCount}',
                      style: pw.Theme.of(context)
                          .defaultTextStyle
                          .copyWith(color: PdfColors.grey)));
            },
            build: (pw.Context context) {
              return <pw.Widget>[
                pw.Column(children: [
                  pw.Row(children: [
                    pw.Container(
                        child: pw.Text('Bill To:',
                            style: pw.TextStyle(
                                color: PdfColors.black,
                                fontWeight: pw.FontWeight.normal,
                                fontSize: 10))),
                  ]),
                  pw.Row(children: [
                    pw.Container(
                        child: pw.Text(name.toString(),
                            style: pw.TextStyle(
                                color: PdfColors.lightBlue700,
                                fontWeight: pw.FontWeight.bold,
                                fontSize: 17)))
                  ]),
                  pw.Row(children: [
                    pw.Container(
                        child: pw.Text("Phone No. : " + customer[0]["mob_No"],
                            style: pw.TextStyle(color: PdfColors.black)))
                  ]),
                  // pw.Row(children: [
                  //   pw.Container(
                  //       child: pw.Text(
                  //           "Email Address : " + invoiceDetails?[0]["email"],
                  //           style: pw.TextStyle(color: PdfColors.black)))
                  // ]),
                  pw.Container(
                      child: pw.Center(
                          child: pw.Text('Statement',
                              style: pw.TextStyle(
                                  color: PdfColors.black, fontSize: 20)))),
                  pw.Row(
                      mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                      children: [
                        pw.Container(
                            child: pw.Text("Acc No. : " + accNo.toString(),
                                style: pw.TextStyle(
                                  color: PdfColors.black,
                                  fontWeight: pw.FontWeight.bold,
                                )))
                      ]),
                  pw.Row(
                      mainAxisAlignment: pw.MainAxisAlignment.end,
                      children: [
                        // pw.Container(
                        //     child: pw.Text(empName,
                        //         style: pw.TextStyle(color: PdfColors.black))),
                        pw.Container(
                            child: pw.Text(
                                "Date : " +
                                    DateFormat("dd-MM-yyyy")
                                        .format(DateTime.now()),
                                style: pw.TextStyle(
                                  color: PdfColors.black,
                                  fontWeight: pw.FontWeight.bold,
                                )))
                      ]),
                  pw.Container(
                    padding: pw.EdgeInsets.only(top: 10),
                    child: pw.Table.fromTextArray(
                        headerDecoration:
                            pw.BoxDecoration(color: PdfColors.lightBlue700),
                        headerStyle:
                            pw.TextStyle(color: PdfColors.white, fontSize: 15),
                        context: context,
                        cellStyle: pw.TextStyle(fontSize: 13),
                        cellAlignment: pw.Alignment.center,
                        data: <List>[
                          <String>[
                            'Sr',
                            'Date',
                            'Amount',
                            if (dWM == 3) 'Int. Amt'
                          ],
                          ...invoiceDetails!.map((e) {
                            counter++;
                            //print("invoice : " +e["ph_no"]);
                            return [
                              counter.toString(),
                              DateFormat("dd-MM-yyyy")
                                  .format(DateTime.parse(e['dt'].toString())),
                              (e['amt']).toString(),
                              if (dWM == 3) (e['int_Amt']).toString()
                            ];
                          })

                          //  invoiceDetails.map((prod) =>  ['1',prod.newitem, '', prod.qty, prod.rate, prod.grand_total] )
                        ]),
                  ),
                  pw.Container(
                    padding: pw.EdgeInsets.only(top: 20),
                    child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.end,
                        children: [
                          pw.Container(
                              child: pw.Row(
                                  mainAxisAlignment:
                                      pw.MainAxisAlignment.spaceBetween,
                                  children: [
                                pw.Text("Tot. Given Amt.  ",
                                    style: pw.TextStyle(
                                        color: PdfColors.black,
                                        fontSize: 14,
                                        fontWeight: pw.FontWeight.bold)),
                                pw.Text("Rs." + total.toString(),
                                    style: pw.TextStyle(
                                        color: PdfColors.black,
                                        fontSize: 14,
                                        fontWeight: pw.FontWeight.bold))
                              ])),
                        ]),
                  ),
                  pw.Container(
                    //padding: pw.EdgeInsets.only(top: 10),
                    child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.end,
                        children: [
                          pw.Container(
                              child: pw.Row(
                                  mainAxisAlignment:
                                      pw.MainAxisAlignment.spaceBetween,
                                  children: [
                                pw.Text("Total Received   ",
                                    style: pw.TextStyle(
                                        color: PdfColors.green,
                                        fontSize: 14,
                                        fontWeight: pw.FontWeight.bold)),
                                pw.Text(
                                    "Rs." +
                                        (totalPayment + totalInterest)
                                            .toString(),
                                    style: pw.TextStyle(
                                        color: PdfColors.green,
                                        fontSize: 14,
                                        fontWeight: pw.FontWeight.bold))
                              ])),
                        ]),
                  ),
                  pw.Container(
                    //padding: pw.EdgeInsets.only(top: 10),
                    child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.end,
                        children: [
                          pw.Container(
                              alignment: pw.Alignment.centerRight,
                              child: pw.Row(
                                  mainAxisAlignment:
                                      pw.MainAxisAlignment.spaceBetween,
                                  children: [
                                    pw.Text("Tot. Pending   ",
                                        style: pw.TextStyle(
                                            color: PdfColors.red,
                                            fontSize: 14,
                                            fontWeight: pw.FontWeight.bold)),
                                    pw.Text(
                                        "Rs." +
                                            (double.parse(total.toString()) -
                                                    double.parse(totalPayment
                                                        .toString()))
                                                .toStringAsFixed(2),
                                        style: pw.TextStyle(
                                            color: PdfColors.red,
                                            fontSize: 14,
                                            fontWeight: pw.FontWeight.bold))
                                  ])),
                        ]),
                  ),
                  // pw.Container(
                  //   //padding: pw.EdgeInsets.only(top: 10),
                  //   child: pw.Row(
                  //       mainAxisAlignment: pw.MainAxisAlignment.end,
                  //       children: [
                  //         pw.Container(
                  //             alignment: pw.Alignment.centerRight,
                  //             width: 160,
                  //             child: pw.Row(
                  //                 mainAxisAlignment:
                  //                     pw.MainAxisAlignment.spaceBetween,
                  //                 children: [
                  //                   pw.Text("Paid ",
                  //                       style: pw.TextStyle(
                  //                         fontSize: 14,
                  //                       )),
                  //                   pw.Text("Rs. " + invoiceDetails[0]["paid"],
                  //                       style: pw.TextStyle(
                  //                         fontSize: 14,
                  //                       ))
                  //                 ])),
                  //       ]),
                  // ),
                  // pw.Container(
                  //   //padding: pw.EdgeInsets.only(top: 10),
                  //   child: pw.Row(
                  //       mainAxisAlignment: pw.MainAxisAlignment.end,
                  //       children: [
                  //         pw.Container(
                  //             alignment: pw.Alignment.centerRight,
                  //             width: 160,
                  //             child: pw.Row(
                  //                 mainAxisAlignment:
                  //                     pw.MainAxisAlignment.spaceBetween,
                  //                 children: [
                  //                   pw.Text("Balance",
                  //                       style: pw.TextStyle(
                  //                           fontSize: 14,
                  //                           fontWeight: pw.FontWeight.bold)),
                  //                   pw.Text(
                  //                       "Rs. " +
                  //                           (double.parse(invoiceDetails[0]
                  //                                       ["grand_total"]) -
                  //                                   double.parse(
                  //                                       invoiceDetails[0]
                  //                                           ["paid"]))
                  //                               .toString(),
                  //                       style: pw.TextStyle(
                  //                           fontSize: 14,
                  //                           fontWeight: pw.FontWeight.bold))
                  //                 ])),
                  //       ]),
                  // ),
                  pw.Container(
                    padding: pw.EdgeInsets.only(top: 10),
                    child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.end,
                        children: [
                          pw.Container(
                            child: pw.Text("From, " + "JOGISIDDHA FINANCE",
                                style: pw.TextStyle(
                                    fontWeight: pw.FontWeight.normal)),
                          ),
                        ]),
                  ),
                  pw.Container(
                    padding: pw.EdgeInsets.only(top: 40),
                    child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.end,
                        children: [
                          pw.Container(
                            child: pw.Text("Authorized Signatory ",
                                style: pw.TextStyle(
                                    fontWeight: pw.FontWeight.normal)),
                          ),
                        ]),
                  ),
                ])
              ];
            },
          ));

          return pdf.save();
        });
  }

  static shareOnPdf(
      List? invoiceDetails,
      var accNo,
      var totalPayment,
      var endDt,
      var name,
      var startDt,
      var total,
      var customer,
      var dWM,
      var totalInterest) async {
    int counter = 0;
    final pdf = pw.Document();
    final image = await imageFromAssetBundle('assets\\images\\jf log1.jpg');
    pdf.addPage(pw.MultiPage(
      pageFormat: PdfPageFormat.a4,
      margin: pw.EdgeInsets.all(32),
      header: (pw.Context context) {
        return pw.Container(
            alignment: pw.Alignment.centerRight,
            margin: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
            padding: const pw.EdgeInsets.only(bottom: 3.0 * PdfPageFormat.mm),
            decoration: const pw.BoxDecoration(
                border: pw.Border(
                    bottom: pw.BorderSide(color: PdfColors.grey, width: 0.5))),
            child: pw.Column(children: [
              pw.Row(
                  //mainAxisAlignment: pw.MainAxisAlignment.center,
                  children: [
                    pw.Container(
                      width: 100,
                      height: 100,
                      child: pw.Image(
                        image,
                      ),
                    ),
                    pw.Container(
                      child: pw.Column(children: [
                        pw.Text('JOGISIDDHA FINANCE',
                            style: pw.Theme.of(context)
                                .defaultTextStyle
                                .copyWith(
                                    color: PdfColors.lightBlue700,
                                    fontSize: 18,
                                    fontWeight: pw.FontWeight.bold)),
                        pw.Text('ADDRESS :  SOLAPUR',
                            style: pw.Theme.of(context)
                                .defaultTextStyle
                                .copyWith(color: PdfColors.black, fontSize: 15))
                      ]),
                    ),
                  ]),
              // pw.Row(
              //     mainAxisAlignment: pw.MainAxisAlignment.center,
              //     children: [

              //     ])
            ]));
      },
      footer: (pw.Context context) {
        return pw.Container(
            alignment: pw.Alignment.centerRight,
            margin: const pw.EdgeInsets.only(top: 1.0 * PdfPageFormat.cm),
            child: pw.Text(
                'Page ${context.pageNumber} of ${context.pagesCount}',
                style: pw.Theme.of(context)
                    .defaultTextStyle
                    .copyWith(color: PdfColors.grey)));
      },
      build: (pw.Context context) {
        return <pw.Widget>[
          pw.Column(children: [
            pw.Row(children: [
              pw.Container(
                  child: pw.Text('Bill To:',
                      style: pw.TextStyle(
                          color: PdfColors.black,
                          fontWeight: pw.FontWeight.normal,
                          fontSize: 10))),
            ]),
            pw.Row(children: [
              pw.Container(
                  child: pw.Text(name.toString(),
                      style: pw.TextStyle(
                          color: PdfColors.lightBlue700,
                          fontWeight: pw.FontWeight.bold,
                          fontSize: 17)))
            ]),
            pw.Row(children: [
              pw.Container(
                  child: pw.Text("Phone No. : " + customer[0]["mob_No"],
                      style: pw.TextStyle(color: PdfColors.black)))
            ]),
            // pw.Row(children: [
            //   pw.Container(
            //       child: pw.Text(
            //           "Email Address : " + invoiceDetails?[0]["email"],
            //           style: pw.TextStyle(color: PdfColors.black)))
            // ]),
            pw.Container(
                child: pw.Center(
                    child: pw.Text('Statement',
                        style: pw.TextStyle(
                            color: PdfColors.black, fontSize: 20)))),
            pw.Row(
                mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                children: [
                  pw.Container(
                      child: pw.Text("Acc No. : " + accNo.toString(),
                          style: pw.TextStyle(
                            color: PdfColors.black,
                            fontWeight: pw.FontWeight.bold,
                          )))
                ]),
            pw.Row(mainAxisAlignment: pw.MainAxisAlignment.end, children: [
              // pw.Container(
              //     child: pw.Text(empName,
              //         style: pw.TextStyle(color: PdfColors.black))),
              pw.Container(
                  child: pw.Text(
                      "Date : " +
                          DateFormat("dd-MM-yyyy").format(DateTime.now()),
                      style: pw.TextStyle(
                        color: PdfColors.black,
                        fontWeight: pw.FontWeight.bold,
                      )))
            ]),
            pw.Container(
              padding: pw.EdgeInsets.only(top: 10),
              child: pw.Table.fromTextArray(
                  headerDecoration:
                      pw.BoxDecoration(color: PdfColors.lightBlue700),
                  headerStyle:
                      pw.TextStyle(color: PdfColors.white, fontSize: 15),
                  context: context,
                  cellStyle: pw.TextStyle(fontSize: 13),
                  cellAlignment: pw.Alignment.center,
                  data: <List>[
                    <String>['Sr', 'Date', 'Amount', if (dWM == 3) 'Int. Amt'],
                    ...invoiceDetails!.map((e) {
                      counter++;
                      //print("invoice : " +e["ph_no"]);
                      return [
                        counter.toString(),
                        DateFormat("dd-MM-yyyy")
                            .format(DateTime.parse(e['dt'].toString())),
                        (e['amt']).toString(),
                        if (dWM == 3) (e['int_Amt']).toString()
                      ];
                    })

                    //  invoiceDetails.map((prod) =>  ['1',prod.newitem, '', prod.qty, prod.rate, prod.grand_total] )
                  ]),
            ),
            pw.Container(
              padding: pw.EdgeInsets.only(top: 20),
              child: pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.end,
                  children: [
                    pw.Container(
                        child: pw.Row(
                            mainAxisAlignment:
                                pw.MainAxisAlignment.spaceBetween,
                            children: [
                          pw.Text("Tot. Given Amt.  ",
                              style: pw.TextStyle(
                                  color: PdfColors.black,
                                  fontSize: 14,
                                  fontWeight: pw.FontWeight.bold)),
                          pw.Text("Rs." + total.toString(),
                              style: pw.TextStyle(
                                  color: PdfColors.black,
                                  fontSize: 14,
                                  fontWeight: pw.FontWeight.bold))
                        ])),
                  ]),
            ),
            pw.Container(
              //padding: pw.EdgeInsets.only(top: 10),
              child: pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.end,
                  children: [
                    pw.Container(
                        child: pw.Row(
                            mainAxisAlignment:
                                pw.MainAxisAlignment.spaceBetween,
                            children: [
                          pw.Text("Total Received   ",
                              style: pw.TextStyle(
                                  color: PdfColors.green,
                                  fontSize: 14,
                                  fontWeight: pw.FontWeight.bold)),
                          pw.Text(
                              "Rs." + (totalPayment + totalInterest).toString(),
                              style: pw.TextStyle(
                                  color: PdfColors.green,
                                  fontSize: 14,
                                  fontWeight: pw.FontWeight.bold))
                        ])),
                  ]),
            ),
            pw.Container(
              //padding: pw.EdgeInsets.only(top: 10),
              child: pw
                  .Row(mainAxisAlignment: pw.MainAxisAlignment.end, children: [
                pw.Container(
                    alignment: pw.Alignment.centerRight,
                    child: pw.Row(
                        mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
                        children: [
                          pw.Text("Tot. Pending   ",
                              style: pw.TextStyle(
                                  color: PdfColors.red,
                                  fontSize: 14,
                                  fontWeight: pw.FontWeight.bold)),
                          pw.Text(
                              "Rs." +
                                  (double.parse(total.toString()) -
                                          double.parse(totalPayment.toString()))
                                      .toStringAsFixed(2),
                              style: pw.TextStyle(
                                  color: PdfColors.red,
                                  fontSize: 14,
                                  fontWeight: pw.FontWeight.bold))
                        ])),
              ]),
            ),
            // pw.Container(
            //   //padding: pw.EdgeInsets.only(top: 10),
            //   child: pw.Row(
            //       mainAxisAlignment: pw.MainAxisAlignment.end,
            //       children: [
            //         pw.Container(
            //             alignment: pw.Alignment.centerRight,
            //             width: 160,
            //             child: pw.Row(
            //                 mainAxisAlignment:
            //                     pw.MainAxisAlignment.spaceBetween,
            //                 children: [
            //                   pw.Text("Paid ",
            //                       style: pw.TextStyle(
            //                         fontSize: 14,
            //                       )),
            //                   pw.Text("Rs. " + invoiceDetails[0]["paid"],
            //                       style: pw.TextStyle(
            //                         fontSize: 14,
            //                       ))
            //                 ])),
            //       ]),
            // ),
            // pw.Container(
            //   //padding: pw.EdgeInsets.only(top: 10),
            //   child: pw.Row(
            //       mainAxisAlignment: pw.MainAxisAlignment.end,
            //       children: [
            //         pw.Container(
            //             alignment: pw.Alignment.centerRight,
            //             width: 160,
            //             child: pw.Row(
            //                 mainAxisAlignment:
            //                     pw.MainAxisAlignment.spaceBetween,
            //                 children: [
            //                   pw.Text("Balance",
            //                       style: pw.TextStyle(
            //                           fontSize: 14,
            //                           fontWeight: pw.FontWeight.bold)),
            //                   pw.Text(
            //                       "Rs. " +
            //                           (double.parse(invoiceDetails[0]
            //                                       ["grand_total"]) -
            //                                   double.parse(
            //                                       invoiceDetails[0]
            //                                           ["paid"]))
            //                               .toString(),
            //                       style: pw.TextStyle(
            //                           fontSize: 14,
            //                           fontWeight: pw.FontWeight.bold))
            //                 ])),
            //       ]),
            // ),
            pw.Container(
              padding: pw.EdgeInsets.only(top: 10),
              child: pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.end,
                  children: [
                    pw.Container(
                      child: pw.Text("From, " + "JOGISIDDHA FINANCE",
                          style:
                              pw.TextStyle(fontWeight: pw.FontWeight.normal)),
                    ),
                  ]),
            ),
            pw.Container(
              padding: pw.EdgeInsets.only(top: 40),
              child: pw.Row(
                  mainAxisAlignment: pw.MainAxisAlignment.end,
                  children: [
                    pw.Container(
                      child: pw.Text("Authorized Signatory ",
                          style:
                              pw.TextStyle(fontWeight: pw.FontWeight.normal)),
                    ),
                  ]),
            ),
          ])
        ];
      },
    ));

    await Printing.sharePdf(
        bytes: await pdf.save(),
        filename: "Statement_" + accNo.toString() + '.pdf');
  }
}
