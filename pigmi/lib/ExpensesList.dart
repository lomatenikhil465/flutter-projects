import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:pigmi/utils/const.dart';
import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class ExpensesList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ExpensesList();
  }
}

class _ExpensesList extends State<ExpensesList> {
  TextEditingController? _fromDateController, _toDateController;
  DateTime? _dateTime;
  List? expensesList;
  int customerVendor = 0;
  SharedPreferences? prefs;
  Future<String>? expensesResponse;
  double total = 0.0;

  Future<String> getJsonData() async {
    var empId = prefs!.getString('empId');
    var splittedFromDate = _fromDateController!.text.split("/");
    var newFromDate = splittedFromDate[2] +
        "-" +
        splittedFromDate[1] +
        "-" +
        splittedFromDate[0];
    var splittedToDate = _toDateController!.text.split("/");
    var newToDate =
        splittedToDate[2] + "-" + splittedToDate[1] + "-" + splittedToDate[0];
    String url = Constants.url +
        "app/pigmiAll.php?apicall=getExpenseList&userId=" +
        empId.toString() +
        "&fdt=" +
        newFromDate.toString() +
        "&tdt=" +
        newToDate.toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      expensesList = convertDataToJson['user'];
      if (expensesList != null) {
        for (var item in expensesList!) {
          total += double.parse(item["amount"]);
        }
      }
    });
    return "success";
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    super.initState();
    DateTime now = DateTime.now();
    String today = DateFormat("dd/MM/yyyy").format(now);
    _fromDateController = TextEditingController(text: today);
    _toDateController = TextEditingController(text: today);
    getEmpId().then((value) {
      expensesResponse = this.getJsonData();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Expenses"),
        backgroundColor: Colors.blue[800],
      ),
      body: Column(
        children: <Widget>[
          Container(
            child: Row(
              children: <Widget>[
                Container(
                  child: Text(
                    " From : ",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                Container(
                    height: 50,
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.01),
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: DateTimeField(
                        format: DateFormat('dd/MM/yyyy'),
                        style: TextStyle(fontSize: 15),
                        onChanged: (value) {
                          total = 0.0;
                          this.getJsonData();
                        },
                        initialValue: DateTime.now(),
                        controller: _fromDateController,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.02,
                            ),
                            // border: OutlineInputBorder(borderSide: BorderSide()),
                            prefixIcon: Icon(Icons.calendar_today)),
                        resetIcon: null,
                        onShowPicker: (context, currentValue) {
                          return showDatePicker(
                              context: context,
                              initialDate: _dateTime == null
                                  ? DateTime.now()
                                  : _dateTime!.toUtc(),
                              firstDate: DateTime(2001),
                              lastDate: DateTime(2025));
                        },
                        validator: (val) {
                          if (val != null) {
                            return null;
                          } else {
                            return 'Date Field is Empty';
                          }
                        })),
                Spacer(),
                Container(
                  child: Text(
                    "To : ",
                    style: TextStyle(fontSize: 16),
                  ),
                ),
                Container(
                    height: 50,
                    margin: EdgeInsets.only(
                      right: MediaQuery.of(context).size.width * 0.01,
                    ),
                    width: MediaQuery.of(context).size.width * 0.35,
                    child: DateTimeField(
                        format: DateFormat('dd/MM/yyyy'),
                        onChanged: (value) {
                          total = 0.0;
                          this.getJsonData();
                        },
                        style: TextStyle(fontSize: 15),
                        initialValue: DateTime.now(),
                        controller: _toDateController,
                        decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.02,
                            ),
                            //border: OutlineInputBorder(borderSide: BorderSide()),
                            prefixIcon: Icon(Icons.calendar_today)),
                        resetIcon: null,
                        onShowPicker: (context, currentValue) {
                          return showDatePicker(
                              context: context,
                              initialDate: _dateTime == null
                                  ? DateTime.now()
                                  : _dateTime!.toUtc(),
                              firstDate: DateTime(2001),
                              lastDate: DateTime(2025));
                        },
                        validator: (val) {
                          if (val != null) {
                            return null;
                          } else {
                            return 'Date Field is Empty';
                          }
                        }))
              ],
            ),
          ),
          Container(
            child: Expanded(
                child: expensesList == null
                    ? Center(
                        child: Text("No Data Available..."),
                      )
                    : FutureBuilder<String>(
                        future: expensesResponse,
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            return ListView.builder(
                              itemCount: expensesList == null
                                  ? 0
                                  : expensesList!.length,
                              itemBuilder: (BuildContext context, int index) {
                                return Container(
                                  child: Card(
                                    elevation: 3.0,
                                    child: Column(
                                      children: <Widget>[
                                        Container(
                                          padding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.01),
                                          child: Row(
                                            // mainAxisAlignment:
                                            //     MainAxisAlignment.spaceBetween,
                                            children: <Widget>[
                                              Container(
                                                padding: EdgeInsets.only(
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.01,
                                                ),
                                                child: Text(
                                                  DateFormat("dd MMM yyyy")
                                                      .format(DateTime.parse(
                                                          expensesList![index]
                                                              ["date"])),
                                                  style: TextStyle(
                                                      color: Colors.grey[700]),
                                                ),
                                              ),
                                              Container(
                                                padding: EdgeInsets.only(
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.02,
                                                ),
                                                child: Text(
                                                  expensesList![index]["name"],
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                              Container(
                                                padding: EdgeInsets.only(
                                                  left: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.05,
                                                ),
                                                child: Text(
                                                  expensesList![index]
                                                      ["reason"],
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.w500),
                                                ),
                                              ),
                                              Spacer(),
                                              Container(
                                                padding: EdgeInsets.only(
                                                  right: MediaQuery.of(context)
                                                          .size
                                                          .width *
                                                      0.01,
                                                ),
                                                child: Text(
                                                  "₹ " +
                                                      expensesList![index]
                                                          ["amount"],
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.red),
                                                ),
                                              )
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                );
                              },
                            );
                          } else {
                            return Center(
                              child: CircularProgressIndicator(),
                            );
                          }
                        },
                      )),
          ),
          Container(
            height: 50,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(
              right: MediaQuery.of(context).size.width * 0.04,
            ),
            color: Colors.blue[800],
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Text(
                  " Total : " + total.toStringAsFixed(2),
                  style: TextStyle(
                      fontSize: 16,
                      color: Colors.white,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
