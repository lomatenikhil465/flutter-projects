import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'customer/ExistingCustomerPage.dart';

class CancelledCustomers extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CancelledCustomers();
  }
  
}

class _CancelledCustomers extends State<CancelledCustomers> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Cancelled Customers"),
          backgroundColor: Colors.blue[800],
          bottom: TabBar(
                  labelColor: Colors.blue[800],
                  unselectedLabelColor: Colors.white,
                  indicatorSize: TabBarIndicatorSize.label,
                  indicator: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10)),
                      color: Colors.white),
                  tabs: [
                    Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text("Daily"),
                      ),
                    ),
                    
                    Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text("Weekly"),
                      ),
                    ),
                     Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text("Monthly"),
                      ),
                    ),
                  ]
              ),
        ),
        body: Container(
          child: TabBarView(
           
          children: [
            ExistingCustomerPage(dWM: 1,cat: "CancelledCustomers",),
            ExistingCustomerPage(dWM: 2,cat: "CancelledCustomers",),
             ExistingCustomerPage(dWM: 3,cat: "CancelledCustomers",),
          ]
          ),
        ),
      ),
    );
  }
  
}