import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pigmi/ind_cust_receive_payment.dart';
import 'dart:async';
import 'dart:convert';

import 'package:pigmi/utils/const.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ReceivePayment extends StatefulWidget {
  final int? dWM;
  final String? customerName;
  ReceivePayment({
    this.dWM,
    this.customerName,
  });

  @override
  State<StatefulWidget> createState() {
    return _ReceivePayment();
  }
}

List? customers, searchResult = [];

class _ReceivePayment extends State<ReceivePayment> {
  String? _currentItemSelected, amount;
  TextEditingController? _nameController;
  SharedPreferences? prefs;
  var empId;

  Future<String> getJsonData() async {
    String? empId = prefs!.getString("empId");

    //String? pType = prefs!.getString("pType");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=search&empid=" +
        empId.toString() +
        "&pType=" +
        widget.dWM.toString() + "&sts=0";
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      customers = convertDataToJson['user'];
    });

    return "success";
  }

  // Future<String> getTotalAmt(int index) async {
  //   String? empId = prefs!.getString("empId");
  //   String url = Constants.url +
  //       "app/pigmiAll.php?apicall=getTotalAmt&regId=" +
  //       searchResult![index]["ID"] +
  //       "&userId=" +
  //       empId.toString();
  //   print(url);
  //   var response = await http.get(
  //       //Encode the url
  //       Uri.parse(url),
  //       //only accept Json response
  //       headers: {"Accept": "application/json"});

  //   print(response.body);
  //   setState(() {
  //     var convertDataToJson = json.decode(response.body);
  //     customers = convertDataToJson['user'];
  //   });

  //   return "success";
  // }

  void onSearchTextChanged(String text) async {
    searchResult!.clear();
    if (text.isEmpty) {
      setState(() {});
      //return ;
    } else {
      customers!.forEach((userDetail) {
        //("search result : " + userDetail["newitem"].toString());
        if (userDetail["cName"].toLowerCase().contains(text.toLowerCase()) ||
            userDetail["accNo"].toLowerCase().contains(text.toLowerCase())) {
          _currentItemSelected = userDetail["accNo"].toString();

          searchResult!.add(userDetail);
        }
      });
    }
    setState(() {});
  }

  void _onDropDownItemSelected(String newValueSelected) {
    searchResult!.clear();
    if (newValueSelected == "") {
      setState(() {});
    } else {
      this._currentItemSelected = newValueSelected;
      print(newValueSelected);
      customers!.forEach((userDetail) {
        //print("search result : " + userDetail["newitem"].toString());
        if (userDetail["accNo"]
            .toLowerCase()
            .contains(newValueSelected.toLowerCase())) {
          _currentItemSelected = userDetail["accNo"].toString();
          searchResult!.add(userDetail);
        }
      });
      setState(() {
       
      });
    }
  }

  // Widget particularRow(String? heading, String? value) {
  //   return Container(
  //     padding: EdgeInsets.only(
  //         left: MediaQuery.of(context).size.width * 0.01,
  //         right: MediaQuery.of(context).size.width * 0.01,
  //         top: MediaQuery.of(context).size.height * 0.01),
  //     child: Row(
  //       mainAxisAlignment: MainAxisAlignment.spaceBetween,
  //       children: [
  //         Container(
  //           child: Text(
  //             heading.toString(),
  //             style: TextStyle(
  //               fontSize: 16,
  //             ),
  //           ),
  //         ),
  //         Container(
  //           child: Text(
  //             value.toString(),
  //             style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }

  Widget paymentCustomerList() {
    //print("current user : " + searchResult.toString());
    return ListView.builder(
        itemCount: searchResult == null ? 0 : searchResult!.length,
        itemBuilder: (BuildContext context, int index) {
          // getTotalAmt(index);
          return IndCustReceivePayment(
            index: index,
            empId: empId,
            accNo: searchResult![index]["accNo"].toString(),
            refreshCallback: () => getJsonData(),
          );
        });
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      empId = prefs!.getString("empId");
    });
  }

  @override
  void initState() {
    super.initState();
    _nameController = TextEditingController(text: "");
    searchResult!.clear();

    getEmpId().then((value) {
      getJsonData().then((value) {
        _currentItemSelected = widget.customerName;
        if (_currentItemSelected != null)
          _onDropDownItemSelected(_currentItemSelected.toString());
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text(widget.dWM == 1
              ? "Daily"
              : (widget.dWM == 2 ? "Weekly" : "Monthly")),
          backgroundColor: Colors.blue[800]),
      body: Column(
        children: [
          Row(
            children: [
              Container(
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.03,
                    top: MediaQuery.of(context).size.height * 0.01),
                width: MediaQuery.of(context).size.width * 0.97,
                child: TextFormField(
                  validator: null,
                  // onChanged: onSearchTextChanged,
                  decoration: InputDecoration(
                      labelText: "Name",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10)),
                      contentPadding: const EdgeInsets.only(
                          top: 0, right: 10, bottom: 0, left: 10),
                      suffixIcon: Container(
                        width: 30,
                        height: 30,
                        padding: EdgeInsets.all(0),
                        decoration: BoxDecoration(
                            color: Colors.blue[800],
                            borderRadius: BorderRadius.only(
                                bottomRight: Radius.circular(10),
                                topRight: Radius.circular(10))),
                        child: Center(
                          child: IconButton(
                              color: Colors.white,
                              onPressed: () {
                                onSearchTextChanged(_nameController!.text);
                              },
                              icon: Icon(
                                Icons.search,
                                size: 25,
                              )),
                        ),
                      )),
                  controller: _nameController,
                  keyboardType: TextInputType.text,
                  style: new TextStyle(fontFamily: "Poppins", fontSize: 18),
                ),
              ),
            ],
          ),
          Container(
            height: MediaQuery.of(context).size.height * 0.07,
            width: MediaQuery.of(context).size.width * 0.96,
            margin: EdgeInsets.only(
                right: MediaQuery.of(context).size.width * 0.03,
                top: MediaQuery.of(context).size.height * 0.01,
                left: MediaQuery.of(context).size.width * 0.03,
                bottom: MediaQuery.of(context).size.height * 0.02),
            decoration: BoxDecoration(
                border: Border.all(width: 1.0),
                borderRadius: BorderRadius.circular(10)),
            child: DropdownButtonHideUnderline(
              child: ButtonTheme(
                alignedDropdown: true,
                child: DropdownButton<String>(
                  itemHeight: 80,
                  elevation: 1,

                  autofocus: true,
                  focusColor: Colors.lightBlue[900],
                  //style: Theme.of(context).textTheme.title,
                  isExpanded: true,
                  hint: Text("Choose a Customer"),
                  items: customers == null
                      ? null
                      : customers!
                          .map<DropdownMenuItem<String>>((dropDownStringItem) {
                          return DropdownMenuItem<String>(
                              value: dropDownStringItem["accNo"].toString(),
                              child: Text(
                                  dropDownStringItem["accNo"].toString() +
                                      " " +
                                      dropDownStringItem["cName"].toString(),
                                  style: TextStyle(
                                      fontSize: 17,
                                      fontWeight: FontWeight.normal,
                                      color: Colors.blue[800])));
                        }).toList(),
                  onChanged: (String? newValueSelected) =>
                      _onDropDownItemSelected(newValueSelected.toString()),
                  value: _currentItemSelected,
                ),
              ),
            ),
          ),
          Expanded(child: paymentCustomerList()),
        ],
      ),
    );
  }
}
