import 'package:flutter/material.dart';

class BottomSheetWidget extends StatefulWidget {

  final String? custId;
  final int? sale , purchase;
  BottomSheetWidget({ this.custId, this.sale,this.purchase});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _BottomSheetWidget();
  }
}

class _BottomSheetWidget extends State<BottomSheetWidget> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      margin: const EdgeInsets.only(top: 5, left: 15, right: 15),
      height: 275,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            
            width: MediaQuery.of(context).size.width * 1,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey)
                ]),
            child: Column(
              children: <Widget>[
                Container(
                  height: 50,
                  child: Center(
                    
                    child: Text(
                      "Bahi Khata Receipt",
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
                Divider(thickness: 2,),
                SheetButton(
                  buttonColor: Colors.deepOrange,
                  text: "Pay",
                  payReceive:0,
                  custId: widget.custId.toString().toString(),
                  salePurchase: widget.sale!.toInt(),
                  getData: widget.sale!.toInt() == null ? 0 : 1,
                ),
                SheetButton(
                  buttonColor: Colors.green,
                  text: "Receive",
                  payReceive:1,
                  custId: widget.custId.toString(),
                  salePurchase: widget.sale!.toInt(),
                  getData: widget.sale!.toInt() == null ? 0 : 1,
                ),
               widget.sale!.toInt() != null && widget.purchase != null? SheetButton(
                  buttonColor: Colors.lightBlue.shade700,
                  text:  "Sale Entry" ,
                  payReceive:1,
                  custId: widget.custId.toString(),
                  salePurchase: widget.sale!.toInt(),
                  getData: widget.sale!.toInt() == null ? 0 : 1,
                ) : Container(),
                widget.sale!.toInt() != null && widget.purchase != null? SheetButton(
                  buttonColor: Colors.lightBlue.shade700,
                  text:  "Purchase Entry" ,
                  payReceive:1,
                  custId: widget.custId.toString(),
                  salePurchase: widget.purchase!.toInt(),
                  getData: widget.purchase == null ? 0 : 1,
                ) : Container()
              ],
            ),
          )
        ],
      ),
    );
  }
}

class SheetButton extends StatefulWidget {
  final String text, custId;
  final Color buttonColor;
  final int payReceive, salePurchase, getData;

  SheetButton(
      {required this.text,
      required this.buttonColor,
     required this.payReceive,
     required this.custId,
     required this.salePurchase,
     required this.getData});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SheetButton();
  }
}

class _SheetButton extends State<SheetButton> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: MediaQuery.of(context).size.width * 0.85,
      child: RaisedButton(
        onPressed: () async {
         
        },
        color: widget.buttonColor,
        child: Text(
          widget.text,
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }
}
