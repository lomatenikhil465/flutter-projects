import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'dart:async';
import 'dart:convert';

import 'package:pigmi/utils/const.dart';
import 'receive_payment.dart';

class IndCustReceivePayment extends StatefulWidget {
  final String? empId, accNo;
  final int? index;
  final VoidCallback refreshCallback;
  IndCustReceivePayment(
      {this.empId, this.index, required this.refreshCallback, this.accNo});
  @override
  State<StatefulWidget> createState() {
    return _IndCustReceivePayment();
  }
}

class _IndCustReceivePayment extends State<IndCustReceivePayment> {
  String? total, currentInt = "0";
  String? amount, intAmount;
  double totalPayment = 0;
  int? radio, anRadio;
  List? dates;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Future<String> getTotalAmt(int index) async {
    //String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=getTotalAmt&regId=" +
        searchResult![index]["ID"] +
        "&userId=" +
        widget.empId.toString() +
        "&pType=" +
        searchResult![index]["pType"];
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      total = convertDataToJson['user'][0]['total'];
    });

    return "success";
  }

  Future<String> getCurrentInt(int index) async {
    //String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=getCurrentInt&regId=" +
        searchResult![index]["ID"] +
        "&accNo=" +
        searchResult![index]["accNo"].toString();
    print("get current int : " + url.toString());
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      if (convertDataToJson != null) {
        currentInt = convertDataToJson['user'][0]['current_intAmt'];
      }
    });

    return "success";
  }

  Future<String> insertAmt(int index) async {
    String url = Constants.url +
        "app/pigmiAll.php?apicall=" +
        (searchResult![index]["pType"] == '3'
            ? "insertAmtMonthly"
            : "insertAmt") +
        "&";
    print(url);
    // var intAmt;
    // if (searchResult![index]["pType"] == '3') {
    //   if (radio == 0) {
    //     intAmt = amount;
    //     amount = "0";
    //   } else if (radio == 1) {
    //     intAmt = currentInt;
    //     amount = (double.parse(amount.toString()) -
    //             double.parse(currentInt.toString()))
    //         .toStringAsFixed(2);
    //   } else {}

    //   print(intAmt + " " + amount);
    // }
    var response = await http.post(Uri.parse(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'pType': searchResult![index]["pType"],
          'regId': searchResult![index]["ID"],
          'userId': widget.empId.toString(),
          'accNo': searchResult![index]["accNo"],
          'amt': amount == null ? "0" : amount.toString(),
          if (searchResult![index]["pType"] == '3')
            'intamt': intAmount == null ? "0" : intAmount.toString(),
        });

    var convertDataToJson = json.decode(response.body);

    //print(response.reasonPhrase);

    return convertDataToJson['message'];
  }

  Future<String> getPendingData() async {
    // String? empId = prefs!.getString("empId");

    String url = Constants.url +
        "app/pigmiAll.php?apicall=" +
        (searchResult![widget.index!.toInt()]["pType"] == '1'
            ? "pendingListCustomerDaily"
            : searchResult![widget.index!.toInt()]["pType"] == '2'
                ? "pendingListCustomerWeekly"
                : "pendingListCustomerMonthly") +
        "&userId=" +
        widget.empId.toString() +
        "&pType=" +
        searchResult![widget.index!.toInt()]["pType"] +
        "&regId=" +
        searchResult![widget.index!.toInt()]["ID"].toString() +
        "&fdt=" +
        searchResult![widget.index!.toInt()]["startdt"].toString() +
        "&tdt=" +
        DateFormat("yyyy-MM-dd").format(DateTime.now());
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    var convertDataToJson;
    convertDataToJson = json.decode(response.body);
    setState(() {
      dates!.clear();
      for (var item in convertDataToJson['user']!) {
        dates!.add(item["dt"]);
        totalPayment = totalPayment +
           // double.parse(item["amt"]) +
            double.parse(item["int"]);
      }
      print("Dates : " + dates.toString());
    });

    return "success";
  }

  Future<String> updatePending(int index) async {
    String url = Constants.url +
        "app/pigmiReport_T.php?apicall=" +
        "insertAmtMonthlyPending" +
        "&";
    print(url);

    var response = await http.post(Uri.parse(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'pType': searchResult![index]["pType"],
          'regId': searchResult![index]["ID"],
          'userId': widget.empId.toString(),
          'accNo': searchResult![index]["accNo"],
          'amt': amount == null ? "0" : amount.toString(),
          'intAmt': intAmount == null ? "0" : intAmount.toString(),
          'dt': dates.toString(),
          //if (searchResult![index]["pType"] == '3') 'intamt': intAmt.toString(),
        });

    var convertDataToJson = json.decode(response.body);

    //print(response.reasonPhrase);

    return convertDataToJson['message'];
  }

  Widget particularRow(String? heading, String? value, Color color) {
    return Container(
      padding: EdgeInsets.only(
          left: MediaQuery.of(context).size.width * 0.02,
          right: MediaQuery.of(context).size.width * 0.02,
          top: MediaQuery.of(context).size.height * 0.01),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Text(
              heading.toString(),
              style: TextStyle(fontSize: 16, color: color),
            ),
          ),
          Container(
            child: Text(
              value.toString(),
              style: TextStyle(
                  fontSize: 16, fontWeight: FontWeight.bold, color: color),
            ),
          ),
        ],
      ),
    );
  }

  void submitAmount() {
    if (_formKey.currentState!.validate()) {
      print("enter");
      _formKey.currentState!.save();
      
        if (anRadio == 0) {
          insertAmt(widget.index!.toInt()).then((value) {
            Navigator.of(context).pop();
            amount = null;
            intAmount = null;
            searchResult!.clear();
            print(value);
            if (value == 'Record Already Exists...!') {
              Constants.showDialogBox(context, "Already Exists", "");
            } else {
              Constants.showDialogBox(context, "Successfully", "");
            }

            widget.refreshCallback();
          });
        } else {
          updatePending(widget.index!.toInt()).then((value) {
            Navigator.of(context).pop();
            amount = null;
            intAmount = null;
            searchResult!.clear();
            print(value);
            if (value == 'Some Field missing') {
              Constants.showDialogBox(context, "Some Field is Missing", "");
            } else {
              Constants.showDialogBox(context, "Successfully", "");
            }

            widget.refreshCallback();
          });
        
      }
    } else {
      Navigator.of(context).pop();
      Constants.showDialogBox(context, "Error!", "Amount Not Submitted");
    }
  }
   void change(){
    setState(() {
     
    });
  }

  void radioButtonChanges(int? val) {
    setState(() {
      radio = val!.toInt();
    });
  }

  void anRadioButtonChanges(int? val) {
    setState(() {
      anRadio = val!.toInt();
    });
  }

  @override
  void didUpdateWidget(IndCustReceivePayment oldwidget) {
    print("called didUpdateWidget");
    print(oldwidget.accNo);
    if (amount != null || intAmount != null) {
      total = "0";
      totalPayment = 0;
      print("Id  : " + searchResult.toString());
      getTotalAmt(widget.index!.toInt());
      getPendingData();
      getCurrentInt(widget.index!.toInt());
    } else {
      if (widget.accNo != oldwidget.accNo) {
        total = "0";
        totalPayment = 0;
        print("Id  : " + searchResult.toString());
        getTotalAmt(widget.index!.toInt());
        getPendingData();
        getCurrentInt(widget.index!.toInt());
      }
    }
    super.didUpdateWidget(widget);
  }

  @override
  void initState() {
    super.initState();
    total = "0";
    totalPayment = 0;
    radio = 0;
    anRadio = 0;
    dates = [];
    print(widget.accNo);
    print("Id  : " + searchResult!.length.toString());
    getTotalAmt(widget.index!.toInt());

    getCurrentInt(widget.index!.toInt());
    getPendingData();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Container(
        padding: EdgeInsets.only(
          left: MediaQuery.of(context).size.width * 0.02,
          right: MediaQuery.of(context).size.width * 0.02,
        ),
        child: Card(
          elevation: 3.0,
          // color: Colors.grey[200],
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
            side: BorderSide(color: Colors.blue.shade800, width: 2),
          ),
          child: Column(
            children: [
              particularRow("Acc. No.",
                  searchResult![widget.index!.toInt()]['accNo'], Colors.black),
              particularRow("Cust Name",
                  searchResult![widget.index!.toInt()]['cName'], Colors.black),

              particularRow(
                  "Total Amt",
                  (double.parse(
                          searchResult![widget.index!.toInt()]['credit_amt']))
                      .toString(),
                  Colors.black),
              if (searchResult![widget.index!.toInt()]["pType"] == '3')
                particularRow(
                    "Interest Amt",
                    (double.parse(currentInt.toString())).toString(),
                    Colors.black),
              // particularRow(
              //   "Per " + (searchResult![widget.index!.toInt()]["pType"] == '1' ? "Day" : searchResult![widget.index!.toInt()]["pType"] == '2' ? "Week" : "Month") + " Amt",
              //   (double.parse(
              //           searchResult![widget.index!.toInt()]['emi_Amt']))
              //       .toString(),
              //   Colors.black),
              particularRow(
                  "Total Received Amt",
                  (double.parse(total == null ? "0" : total.toString()))
                      .toString(),
                  Colors.green),
              particularRow(
                  "Total Pending Amt",
                  (double.parse(totalPayment.toString())).toString(),
                  Colors.red),
              if (searchResult![widget.index!.toInt()]["pType"] == '3' &&
                  totalPayment != 0.0)
                Container(
                  child: Row(
                    children: <Widget>[
                      Radio(
                        value: 0,
                        groupValue: anRadio,
                        onChanged: anRadioButtonChanges,
                      ),
                      Container(
                        child: Text(
                          "Current",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      Radio(
                        value: 1,
                        groupValue: anRadio,
                        onChanged: anRadioButtonChanges,
                      ),
                      Container(
                        child: Text(
                          "Pending",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                    ],
                  ),
                ),
              // if (searchResult![widget.index!.toInt()]["pType"] == '3' &&
              //     anRadio == 0)
              //   Container(
              //     child: Row(
              //       children: <Widget>[
              //         Radio(
              //           value: 0,
              //           groupValue: radio,
              //           onChanged: radioButtonChanges,
              //         ),
              //         Container(
              //           child: Text(
              //             "Interest",
              //             style: TextStyle(fontSize: 16),
              //           ),
              //         ),
              //         Radio(
              //           value: 1,
              //           groupValue: radio,
              //           onChanged: radioButtonChanges,
              //         ),
              //         Container(
              //           child: Text(
              //             "Interest + Principal",
              //             style: TextStyle(fontSize: 16),
              //           ),
              //         ),
              //       ],
              //     ),
              //   ),

              Container(
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.02,
                    right: MediaQuery.of(context).size.width * 0.02,
                    top: MediaQuery.of(context).size.height * 0.01),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Principal Amt",
                        style: TextStyle(fontSize: 16, color: Colors.black),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.45,
                      child: TextFormField(
                         initialValue: "0",
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Please Enter Amount";
                          } else {
                            return null;
                          }
                        },
                        onSaved: (String? value) {
                          setState(() {
                            amount = value;
                          });
                        },
                        decoration: InputDecoration(
                            labelText: "Enter Amount",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)),
                            contentPadding: EdgeInsets.only(
                                top: 0, left: 10, bottom: 0, right: 0)),
                        keyboardType: TextInputType.numberWithOptions(
                            signed: false, decimal: true),
                        style: new TextStyle(
                            fontFamily: "Poppins",
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
              if (searchResult![widget.index!.toInt()]["pType"] == '3')
              Container(
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.02,
                    right: MediaQuery.of(context).size.width * 0.02,
                    top: MediaQuery.of(context).size.height * 0.01),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Interest Amt",
                        style: TextStyle(fontSize: 16, color: Colors.black),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.45,
                      child: TextFormField(
                        initialValue: "0",
                        validator: (value) {
                          if (value!.isEmpty) {
                            return "Please Enter Int Amount";
                          } else {
                            return null;
                          }
                        },
                        onSaved: (String? value) {
                          setState(() {
                            intAmount = value;
                          });
                        },
                        decoration: InputDecoration(
                            labelText: "Enter Int Amount",
                            border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(10)),
                            contentPadding: EdgeInsets.only(
                                top: 0, left: 10, bottom: 0, right: 0)),
                        keyboardType: TextInputType.numberWithOptions(
                            signed: false, decimal: true),
                        style: new TextStyle(
                            fontFamily: "Poppins",
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width * 0.02,
                    right: MediaQuery.of(context).size.width * 0.02,
                    top: MediaQuery.of(context).size.height * 0.01,
                    bottom: MediaQuery.of(context).size.height * 0.02),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.3,
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              primary: Colors.blue[800],
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10))),
                          onPressed: () {
                            Constants.waiting(context);
                            submitAmount();
                          },
                          child: Text(
                            "Submit",
                            style: TextStyle(color: Colors.white),
                          )),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
