import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:http/http.dart' as http;
import 'package:pigmi/utils/const.dart';
import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

class Expenses extends StatefulWidget {
  final String? custId;
  Expenses({this.custId});
  @override
  State<StatefulWidget> createState() {
    return _Expenses();
  }
}

class _Expenses extends State<Expenses> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  TextEditingController? _dateController, _custController, _contactController;
  DateTime? _dateTime;
  SharedPreferences? prefs;
  String? amount, custName, reason;

  Future<String> insertExpenses() async {
    var empId = prefs!.getString('empId');
    String url = Constants.url + "app/addCust.php?apicall=insertExpenses";
    print(url);
    var splittedDate = _dateController!.text.split("/");
    var newDate = splittedDate[2] +"-" + splittedDate[1] + "-" + splittedDate[0];
    await http.post(Uri.parse(url),
        // headers: {"Content-Type": "application/json"},

        body: {
          'empId': empId,
          'date': newDate.toString(),
          'name': custName.toString(),
          'contact': _contactController!.text,
          'amount': amount.toString(),
          'reason': reason,
        });

    return "success";
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  // Future<String> insertCustomer() async {
  //   String url=
  //         "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertCustomer&";

  //   var response = await http.post(Uri.parse(url),
  //       // headers: {"Content-Type": "application/json"},
  //       body: {
  //         'custName': _custController!.text,
  //         'firm_name': '-',
  //         'address': "-",
  //         'contact': _contactController!.text,
  //         'email': '-',
  //         'cst': '-',
  //         'gst': '-',
  //         'state':'0'
  //       }).then((value) {
  //     print(value.statusCode);
  //   });

  //   //print(response.reasonPhrase);

  //   return "success";
  // }

  // Future<String> getCust() async {
  //   String url =
  //         "https://qot.constructionsmall.com/app/getquerycode.php?apicall=findCustomer&contact=" + _contactController!.text + "&custName="+ _custController!.text;

  //   print(url);
  //   var response = await http.get(
  //       //Encode the url
  //       Uri.parse(url),
  //       //only accept Json response
  //       headers: {"Accept": "application/json"});

  //   print(response.body);
  //   setState(() {
  //     var convertDataToJson = json.decode(response.body);
  //     expensesCustomer = convertDataToJson['data'];
  //   });

  //   return "success";
  // }

  @override
  void initState() {
    super.initState();
    getEmpId();
    DateTime now = DateTime.now();
    String today = DateFormat("dd/MM/yyyy").format(now);

    _custController = TextEditingController(text: "");
    _contactController = TextEditingController(text: "");
    _dateController = TextEditingController(text: today);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Expenses"),
        backgroundColor: Colors.blue[800],
      ),
      body: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.width * 0.01,
                            right: MediaQuery.of(context).size.width * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01),
                        width: MediaQuery.of(context).size.width * 0.92,
                        child: DateTimeField(
                            format: DateFormat('dd/MM/yyy'),
                            initialValue: DateTime.now(),
                            controller: _dateController,
                            decoration: InputDecoration(
                              labelText: "Date",
                              prefixIcon: Container(
                                  margin: EdgeInsets.only(right: 20, top: 20),
                                  child: Icon(
                                    Icons.calendar_today,
                                  )),
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            onShowPicker: (context, currentValue) {
                              return showDatePicker(
                                  context: context,
                                  initialDate: _dateTime == null
                                      ? DateTime.now()
                                      : _dateTime!.toUtc(),
                                  firstDate: DateTime(2001),
                                  lastDate: DateTime(2100));
                            },
                            validator: (val) {
                              if (val != null) {
                                return null;
                              } else {
                                return 'Date Field is Empty';
                              }
                            })),

                    
                         
                                Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.92,
                                  child: TextFormField(
                                    textAlignVertical: TextAlignVertical.bottom,
                                    autofocus: false,
                                    onSaved: (String? custName) {
                                      setState(() {
                                        this.custName = custName;
                                      });
                                    },
                                    validator: (value) {
                                      if (value == "") {
                                        return "Please Enter name";
                                      } else {
                                        return null;
                                      }
                                    },
                                    decoration: InputDecoration(
                                        labelText: "Name",
                                        contentPadding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.015),
                                        suffixIcon: Container(
                                          padding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.03,
                                              left: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.07),
                                          child:
                                              Icon(Icons.perm_contact_calendar),
                                        )),
                                    keyboardType: TextInputType.text,
                                    style: new TextStyle(
                                      fontFamily: "Poppins",
                                    ),
                                  ),
                                ),
                            
                          Container(
                            width: MediaQuery.of(context).size.width * 0.92,
                            child: TextFormField(
                              autofocus: false,

                              controller: _contactController,
                             
                              decoration: InputDecoration(
                                  labelText: "Contact No",
                                  contentPadding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.015),
                                  suffixIcon: Container(
                                    padding: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.03,
                                        left:
                                            MediaQuery.of(context).size.width *
                                                0.07),
                                    child: Icon(Icons.perm_contact_calendar),
                                  )),
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(10),
                                new FilteringTextInputFormatter.allow(
                                    RegExp("[0-9]"))
                              ],
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true, signed: false),
                              style: new TextStyle(
                                fontFamily: "Poppins",
                              ),
                            ),
                          ),
                    
                    Container(
                      padding: EdgeInsets.only(
                          //top: MediaQuery.of(context).size.height * 0.02,
                          left: MediaQuery.of(context).size.width * 0.04,
                          right: MediaQuery.of(context).size.width * 0.04),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.92,
                            child: TextFormField(
                              autofocus: false,
                              textAlignVertical: TextAlignVertical.bottom,
                              onSaved: (value) {
                                setState(() {
                                  reason = value;
                                });
                              },
                              validator: (value) {
                                if (value == "") {
                                  return "Please enter reason";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                labelText: "Reason",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                              ),
                              keyboardType: TextInputType.text,
                              style: new TextStyle(
                                fontFamily: "Poppins",
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    //  Container(
                    //     padding: EdgeInsets.only(
                    //         //top: MediaQuery.of(context).size.height * 0.02,
                    //         left: MediaQuery.of(context).size.width * 0.04,
                    //         right: MediaQuery.of(context).size.width * 0.04),
                    //     child: Row(
                    //       children: <Widget>[
                    //         Container(
                    //           width: MediaQuery.of(context).size.width * 0.92,
                    //           child: TextFormField(
                    //             autofocus: false,
                    //             textAlignVertical: TextAlignVertical.bottom,
                    //             onSaved: (value){
                    //               setState(() {
                    //                 description = value;
                    //               });
                    //             },
                    //    validator: (value){
                    //               if (value == "") {
                    //                 return "Please enter description";
                    //               } else {
                    //                 return null;
                    //               }
                    //             },
                    //             decoration: InputDecoration(
                    //               labelText: "Description",
                    //               contentPadding: EdgeInsets.only(
                    //                   top: MediaQuery.of(context).size.height *
                    //                       0.015),
                    //             ),
                    //             keyboardType: TextInputType.text,
                    //             style: new TextStyle(
                    //               fontFamily: "Poppins",
                    //             ),
                    //           ),
                    //         )
                    //       ],
                    //     ),
                    //   ),
                    Container(
                      padding: EdgeInsets.only(
                          //top: MediaQuery.of(context).size.height * 0.02,
                          left: MediaQuery.of(context).size.width * 0.04,
                          right: MediaQuery.of(context).size.width * 0.04),
                      child: Row(
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.92,
                            child: TextFormField(
                              autofocus: false,
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(
                                    RegExp(r"^\d*\.?\d*"))
                              ],
                              validator: (value) {
                                if (value == "") {
                                  return "Please Enter amount";
                                } else {
                                  return null;
                                }
                              },
                              onSaved: (value) {
                                setState(() {
                                  amount = value;
                                });
                              },
                              decoration: InputDecoration(
                                labelText: "₹ Amount",
                                prefixText: "",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                              ),
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true, signed: false),
                              style: new TextStyle(
                                fontFamily: "Poppins",
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              height: 50,
              width: MediaQuery.of(context).size.width * 1,
              color: Colors.blue[800],
              child: TextButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      Constants.waiting(context);
                      _formKey.currentState!.save();

                      insertExpenses().then((value) {
                        Navigator.of(context).pop();
                        _formKey.currentState!.reset();

                        Navigator.of(context).pop();
                        Constants.showDialogBox(
                            context, "Successfully Saved in Expenses", "");
                      });
                    } else {
                      Navigator.of(context).pop();
                      Constants.showDialogBox(
                          context, "Error", "Please Enter Correct Details");
                    }
                  },
                  child: Text(
                    "Save",
                    style: TextStyle(color: Colors.white),
                  )),
            )
          ],
        ),
      ),
    );
  }
}
