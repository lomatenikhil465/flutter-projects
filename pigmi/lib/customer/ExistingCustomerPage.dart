import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:pigmi/customer_details.dart';
import 'package:pigmi/profile_page.dart';
import 'dart:async';
import 'dart:convert';

import 'package:pigmi/utils/const.dart';
import 'package:shared_preferences/shared_preferences.dart';

//import '../CustomerDetails.dart';

class ExistingCustomerPage extends StatefulWidget {
  final int? dWM;
  final String? cat;

  ExistingCustomerPage({this.dWM, this.cat});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ExistingCustomerPage();
  }
}

List? nameOfCustomer;
List? _searchResult;
var empId;

class _ExistingCustomerPage extends State<ExistingCustomerPage> {
  TextEditingController? _controller;
  SharedPreferences? prefs;
  Future<String>? customerResponse;

  Future<String> getJsonData() async {
    String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=search&empid=" +
        empId.toString() +
        "&pType=" +
        widget.dWM.toString() + "&sts=" + (widget.cat != "CancelledCustomers" ? "0" : "1");
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      nameOfCustomer = convertDataToJson['user'];
    });

    return "success";
  }

   Future<String> getCompletedCustomers() async {
    String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=completedCustomers&empid=" +
        empId.toString() +
        "&pType=" +
        widget.dWM.toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      nameOfCustomer = convertDataToJson['user'];
    });

    return "success";
  }

  

  void onSearchTextChanged(String text) async {
    if (_searchResult != null) {
      _searchResult!.clear();
    }
    if (text.isEmpty) {
      setState(() {});
      //return ;
    }

    nameOfCustomer!.forEach((userDetail) {
      print("search result : " + userDetail["cName"].toString());
      if (userDetail["cName"].toLowerCase().contains(text.toLowerCase()) ||
          userDetail["accNo"].toLowerCase().contains(text.toLowerCase())) {
        _searchResult!.add(userDetail);
      }
    });

    setState(() {});
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      empId = prefs!.getString("empId");
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _searchResult = [];
    getEmpId().then((value) {
      customerResponse = widget.cat == 'CompletedCustomers' ? getCompletedCustomers() : getJsonData();
    });

    _controller = TextEditingController(text: "");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: Column(
      children: <Widget>[
        Container(
          height: 55,
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            onChanged: (value) {
              onSearchTextChanged(value);
            },
            controller: _controller,
            decoration: InputDecoration(
                labelText: "Search",
                hintText: "Search",
                contentPadding: EdgeInsets.only(top: 10),
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5.0)))),
          ),
        ),
        Expanded(
          child: FutureBuilder<String>(
            future: customerResponse,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return nameOfCustomer == null
                    ? Center(
                        child: Text("No Data Available"),
                      )
                    : ((_searchResult == null
                                ? false
                                : _searchResult!.length != 0) ||
                            _controller!.text.isNotEmpty
                        ? ListView.separated(
                            separatorBuilder:
                                (BuildContext context, int index) => Divider(
                              thickness: 0.0,
                              color: Colors.lightBlue[900],
                            ),
                            itemCount: _searchResult == null
                                ? 0
                                : _searchResult!.length,
                            itemBuilder: (BuildContext context, int index) {
                              return IndCustomer(
                                  index: index,
                                  dWM: widget.dWM!.toInt(),
                                  cat: widget.cat,
                                  callback: getJsonData);
                            },
                          )
                        : ListView.builder(
                            // separatorBuilder:
                            //     (BuildContext context, int index) => Divider(
                            //   thickness: 0.0,
                            //   color: Colors.white,
                            // ),
                            itemCount: nameOfCustomer == null
                                ? 0
                                : nameOfCustomer!.length,
                            itemBuilder: (BuildContext context, int index) {
                              return IndCustomer(
                                  index: index,
                                  dWM: widget.dWM!.toInt(),
                                  cat: widget.cat,
                                  callback: getJsonData);
                            },
                          ));
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ),
      ],
    ));
  }
}

typedef Callback = void Function();

class IndCustomer extends StatefulWidget {
  final int index, dWM;
  final String? cat;
  final Callback callback;
  IndCustomer(
      {required this.index,
      required this.dWM,
      this.cat,
      required this.callback});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _IndCustomer();
  }
}

class _IndCustomer extends State<IndCustomer> {
  List? customerStatement;
  double totalPayment = 0.0;

  Future<String> getTotal() async {
    // String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiReport.php?apicall=customerReport&empid=" +
        empId.toString() +
        "&pType=" +
        widget.dWM.toString() +
        "&custId=" +
        ((_searchResult == null ? false : _searchResult!.length != 0)
                ? _searchResult![widget.index]["ID"]
                : nameOfCustomer![widget.index]["ID"])
            .toString() +
        "&accNo=" +
        ((_searchResult == null ? false : _searchResult!.length != 0)
                ? _searchResult![widget.index]["accNo"]
                : nameOfCustomer![widget.index]["accNo"])
            .toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      customerStatement = convertDataToJson['user'];

      if (customerStatement != null) {
        for (var i = 0; i < customerStatement!.length; i++) {
          totalPayment = totalPayment +
              double.parse(customerStatement![i]["amt"]) +
              double.parse(
                  widget.dWM == 3 ? customerStatement![i]["int_Amt"] : "0");
        }
      }
    });
    return "success";
  }

  Future<String> cancelCustomer() async {
    //String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiReport.php?apicall=cancelCustomer&empid=" +
        empId.toString() +
        "&pType=" +
        widget.dWM.toString()  + "&accNo=" 
        +((_searchResult == null ? false : _searchResult!.length != 0)
                ? _searchResult![widget.index]["accNo"]
                : nameOfCustomer![widget.index]["accNo"])
            .toString()+"&sts=" + (widget.cat != "CancelledCustomers"? "1" : "0");
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    

    return "success";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getTotal();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        if (widget.cat == 'Profile') {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
            return ProfilePage(
              custId:
                  (_searchResult == null ? false : _searchResult!.length != 0)
                      ? _searchResult![widget.index]["ID"]
                      : nameOfCustomer![widget.index]["ID"],
              accNo:
                  (_searchResult == null ? false : _searchResult!.length != 0)
                      ? _searchResult![widget.index]["accNo"]
                      : nameOfCustomer![widget.index]["accNo"],
            );
          })).then((value) {
            widget.callback();
          });
        } else if (widget.cat == 'Ledger') {
          print(nameOfCustomer![widget.index]["ID"]);
          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
            return CustomerDetails(
                custId:
                    (_searchResult == null ? false : _searchResult!.length != 0)
                        ? _searchResult![widget.index]["ID"]
                        : nameOfCustomer![widget.index]["ID"],
                name:
                    (_searchResult == null ? false : _searchResult!.length != 0)
                        ? _searchResult![widget.index]["cName"]
                        : nameOfCustomer![widget.index]["cName"],
                accNo:
                    (_searchResult == null ? false : _searchResult!.length != 0)
                        ? _searchResult![widget.index]["accNo"]
                        : nameOfCustomer![widget.index]["accNo"],
                total:
                    (_searchResult == null ? false : _searchResult!.length != 0)
                        ? _searchResult![widget.index]["credit_amt"]
                        : nameOfCustomer![widget.index]["credit_amt"],
                dWM: widget.dWM,
                startDt:
                    (_searchResult == null ? false : _searchResult!.length != 0)
                        ? _searchResult![widget.index]["startdt"]
                        : nameOfCustomer![widget.index]["startdt"],
                endDt:
                    (_searchResult == null ? false : _searchResult!.length != 0)
                        ? _searchResult![widget.index]["enddt"]
                        : nameOfCustomer![widget.index]["enddt"],
                customer:
                    (_searchResult == null ? false : _searchResult!.length != 0)
                        ? _searchResult
                        : nameOfCustomer,
                index: widget.index);
          })).then((value) {
            widget.callback();
          });
        } else if (widget.cat == 'Report') {}
      },
      child: Container(
        // height: 60,
        child: Card(
          elevation: 3.0,
          child: Column(
            children: <Widget>[
              Row(
                children: [
                  Container(
                    child: CircleAvatar(
                      radius: 25,
                      backgroundColor: Colors.grey[200],
                      child: ((_searchResult == null
                                  ? false
                                  : _searchResult!.length != 0)
                              ? (_searchResult![widget.index]["pf_img_path"] !=
                                      "" &&
                                  _searchResult![widget.index]["pf_img_path"] !=
                                      "0")
                              : (nameOfCustomer![widget.index]["pf_img_path"] !=
                                      "" &&
                                  nameOfCustomer![widget.index]
                                          ["pf_img_path"] !=
                                      "0"))
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: Image.network(
                                Constants.url +
                                    ((_searchResult == null
                                                ? false
                                                : _searchResult!.length != 0)
                                            ? _searchResult![widget.index]
                                                ["pf_img_path"]
                                            : nameOfCustomer![widget.index]
                                                ["pf_img_path"])
                                        .toString(),
                                width: 300,
                                height: 300,
                                fit: BoxFit.fitHeight,
                              ),
                            )
                          : Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(100)),
                              width: 300,
                              height: 300,
                              child: Icon(
                                Icons.person,
                                color: Colors.grey[800],
                              ),
                            ),
                    ),
                  ),
                  Expanded(
                      child: Container(
                    child: Column(
                      children: [
                        Row(
                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: Text(
                                ((_searchResult == null
                                                ? false
                                                : _searchResult!.length != 0)
                                            ? _searchResult![widget.index]
                                                ["accNo"]
                                            : nameOfCustomer![widget.index]
                                                ["accNo"])
                                        .toString() +
                                    " ",
                                style: TextStyle(
                                    color: Colors.blue[800],
                                    fontWeight: FontWeight.w500,
                                    fontSize: 15),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: Text(
                                ((_searchResult == null
                                            ? false
                                            : _searchResult!.length != 0)
                                        ? _searchResult![widget.index]["cName"]
                                        : nameOfCustomer![widget.index]
                                            ["cName"])
                                    .toString(),
                                style: TextStyle(
                                    color: Colors.blue[800],
                                    fontWeight: FontWeight.w500,
                                    fontSize: 15),
                              ),
                            ),
                            Spacer(),
                            if(widget.cat != "CancelledCustomers")
                            Container(
                              // padding: EdgeInsets.only(
                              //     top: MediaQuery.of(context).size.height * 0.01),
                              child: Text(
                                "Tot. Received",
                                style: TextStyle(color: Colors.green[600]),
                              ),
                            )
                          ],
                        ),
                        if(widget.cat != "CancelledCustomers")
                        Row(
                          //crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            // Container(
                            //   child: Icon(
                            //     Icons.location_on,
                            //     color: Colors.grey,
                            //     size: 15,
                            //   ),
                            // ),
                            // Container(
                            //   padding: EdgeInsets.only(
                            //       left: MediaQuery.of(context).size.width * 0.01),
                            //   child: Text(
                            //     ((_searchResult == null
                            //                 ? false
                            //                 : _searchResult!.length != 0)
                            //             ? _searchResult![widget.index]["addrs"]
                            //             : nameOfCustomer![widget.index]["addrs"])
                            //         .toString(),
                            //     style: TextStyle(color: Colors.grey),
                            //   ),
                            // ),
                            Spacer(),
                            Container(
                              child: Text(
                                "₹ " + (totalPayment).toStringAsFixed(2),
                                style: TextStyle(
                                    fontWeight: FontWeight.w500, fontSize: 18),
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  )),
                 
                  Container(
                   // padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02,right: MediaQuery.of(context).size.width * 0.02,),
                   // width: 75,
                    child: IconButton(
                      color:widget.cat != "CancelledCustomers" ?Colors.red : Colors.green,
                      iconSize: 30,
                      icon: Icon(widget.cat != "CancelledCustomers" ? Icons.cancel_presentation_outlined : Icons.person_add),
                      
                     
                      onPressed: () async {   
                      var msg = await Constants.confirmationMsg(context, "Confirmation", widget.cat != "CancelledCustomers" ? "Do you really want to Cancel?\n\n(All Transaction made for this Customer will also CANCELLED.)" : "Do you want to add in the customers?");
                      if(msg){
                        Constants.waiting(context);
                        cancelCustomer().then((value) {
                          Navigator.of(context).pop();
                          widget.callback();
                        });
                      }
                      }, 
                    
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
