//import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'dart:io';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
//import '../Customer.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:pigmi/utils/const.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CreateNewCustomerPage extends StatefulWidget {
  final int dWM;
  CreateNewCustomerPage({required this.dWM});
  @override
  State<StatefulWidget> createState() {
    return _CreateNewCustomerPage();
  }
}

class _CreateNewCustomerPage extends State<CreateNewCustomerPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  //static List<Customer> customer = new List<Customer>();
  final picker = ImagePicker();
  int? radio, crde = 0;
  File? profileImg;
  List<File?>? docImg;
  String? compName,
      custName,
      contactNo,
      email,
      gst,
      address,
      changedRadio,
      totalBalance,
      amountDWM,
      duration,
      interest;
  SharedPreferences? prefs;
  late bool _autoValidate, _switch;
  var temp;
  DateTime? _dateTime, newDate;
  TextEditingController? _accController,
      _dateController,
      _endDateController,
      interestController,
      _totalBalanceController,
      totalInterestController,
      durationController,
      totalProfitController,
      _totalGivenController,
      perDaysAmtController;
  //GlobalKey<AutoCompleteTextFieldState<Customer>> key = new GlobalKey();

  Future<String> insertCustomer() async {
    String url = Constants.url + "app/addCust.php?apicall=signup&";
    var splittedStartDate = _dateController!.text.split("-");
    var rearrangedStartDate = splittedStartDate[2] +
        "-" +
        splittedStartDate[1] +
        "-" +
        splittedStartDate[0];
    print(_dateController!.text);
    print(rearrangedStartDate);
    var splittedEndDate = _endDateController!.text.split("-");
    var rearrangedEndDate = splittedEndDate[2] +
        "-" +
        splittedEndDate[1] +
        "-" +
        splittedEndDate[0];
    String? empId = prefs!.getString("empId");
    String? base64ProfileImage;
    List<String?>? base64DocImage = [];
    if (profileImg != null)
      base64ProfileImage = base64Encode(profileImg!.readAsBytesSync());
    if (docImg != null && docImg!.length != 0){
      for (var i = 0; i < docImg!.length; i++) {
        base64DocImage.add(base64Encode(docImg![i]!.readAsBytesSync()));
      }
    }
    var response = await http.post(Uri.parse(url),
        // headers: {"Content-Type": "application/json"},
        body:{
          'startdt': rearrangedStartDate,
          'enddt': rearrangedEndDate,
          'accNo': _accController!.text,
          'cust_nm': custName.toString(),
          'addrs': address.toString(),
          'mobileNo': contactNo.toString(),
          'aType': (radio! + 1).toString(),
          'docpath': docImg!.length == 0 ? "" : base64DocImage.toString(),
          'profile_pic':
              profileImg == null ? "" : base64ProfileImage.toString(),
          'period': duration.toString(),
          'amt': _totalBalanceController!.text,
          'userId': empId.toString(),
          'interest': interestController!.text,
          'interest_amt': totalInterestController!.text,
          'perDays': perDaysAmtController!.text
        }).then((value) {
      print(value.body);
    });

    //print(response.reasonPhrase);

    return "success";
  }

//   Future<String> getJsonData() async {
//     String url=
//           "https://qot.constructionsmall.com/app/getquerycode.php?apicall=customer";

//     print(url);
//     var response = await http.get(
//         //Encode the url
//         Uri.encodeFull(url),
//         //only accept Json response
//         headers: {"Accept": "application/json"});

//     setState(() {
//       var convertDataToJson = json.decode(response.body);
//       customer = convertDataToJson['data'];
//     });
// print(customer.toString());
//     return "success";
//   }

//   void getUsers() async {
//     try {
//         final response =
//             await http.get("https://qot.constructionsmall.com/app/getquerycode.php?apicall=customer");
//             print(response.statusCode == 200);
//         if (response.statusCode == 200) {
//         customer = loadUsers(response.body);
//         print('Users: ${customer.length}');

//         } else {
//         print("Error getting users.1");
//         }
//     } catch (e) {
//         print("Error getting users.2" + e.toString());
//     }
// }

// static List<Customer> loadUsers(String jsonString) {
//     final parsed = json.decode(jsonString);
//     return parsed['data'].map<Customer>((json) => Customer.fromJson(json)).toList();
// }

  // Future<String> insertOpeningStock() async {
  //   String url;

  //   print(widget.dWM);
  //   if (widget.dWM == 0) {
  //     url =
  //         "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertOpeningStock";
  //   } else {
  //     url =
  //         "https://qot.constructionsmall.com/app/getquerycode.php?apicall=insertOpeningStockPurchase";
  //   }
  //   print(url);

  //   var response = await http.post(Uri.parse(url),
  //       // headers: {"Content-Type": "application/json"},
  //       body: {
  //         'date': DateFormat("dd/MM/yyyy").format(DateTime.now()),
  //         'amount': totalBalance,
  //         'firm_id': '1',
  //         'emp_id': '1',
  //         'crde': crde == 0 ? 'cr' : 'dr'
  //       }).then((value) {
  //     print(value.statusCode);
  //   });

  //   //print(response.reasonPhrase);

  //   return "success";
  // }

  Future<String> getAccNo() async {
    String url = Constants.url +
        "app/addCust.php?apicall=AccNo&aType=" +
        (radio! + 1).toString();

    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      _accController!.text = convertDataToJson['user']["ac_no"];
    });

    return "success";
  }

  void _showPicker(context, int choose) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        if (choose == 1)
                          getProfileImage(context, ImageSource.gallery);
                        else
                          getDocImage(context, ImageSource.gallery);
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      if (choose == 1)
                        getProfileImage(context, ImageSource.camera);
                      else
                        getDocImage(context, ImageSource.camera);
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future getProfileImage(BuildContext context, ImageSource source) async {
    final pickedFile = await picker.pickImage(source: source);

    setState(() {
      if (pickedFile != null) {
        profileImg = File(pickedFile.path);
        final kb = profileImg!.lengthSync() / 1024;
        print(kb);
        if (kb / 1024 > 2.0 && source != ImageSource.camera) {
          profileImg = null;
          _showDialogBox(
              context, "Error!", "File size must be less than 2.0 MB");
        }
      } else {
        print('No image selected.');
      }
    });
  }

  Future getDocImage(BuildContext context, ImageSource source) async {
    final pickedFile = await picker.pickMultiImage();

    setState(() {
      if (pickedFile != null) {
        for (var i = 0; i < pickedFile.length; i++) {
          docImg!.add(File(pickedFile[i].path));
        }
       
      } else {
        print('No image selected.');
      }
    });
  }

  // Future<String> uploadImage() async {
  //   String url =
  //       "https://www.veershaivamatch.com/app/uploadImage.php?apicall=uploadpic";
  //   String base64Image = base64Encode(profileImg!.readAsBytesSync());
  //   var response = await http.post(Uri.parse(url),
  //       // headers: {"Content-Type": "application/json"},
  //       body: {
  //         //'custId': widget.matchId,
  //         'pic': base64Image,
  //       });
  //   //  setState(() {
  //   //   var convertDataToJson = json.decode(response.body);
  //   //   homeSearchData = convertDataToJson['user'];
  //   // });
  //   print(response.body);
  //   return "success";
  // }

  String? validateEmail(String? value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern.toString());
    if (!regex.hasMatch(value.toString()))
      return 'Enter Valid Email';
    else
      return null;
  }

  String? validateMobile(String? value) {
// Indian Mobile number are of 10 digit only
    if (value!.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }

  void radioButtonChanges(int? val) {
    setState(() {
      radio = val!.toInt();
    });
    getAccNo();
  }

  void radioButtonCreDeb(int? val) {
    setState(() {
      if (crde == 0) {
        crde = 1;
      } else {
        crde = 0;
      }
    });
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  void saveCustomer() {
    if (_formKey.currentState!.validate()) {
      print("enter");
      _formKey.currentState!.save();

      insertCustomer().then((value) {
        _formKey.currentState!.reset();
        getAccNo();
        profileImg = null;
        docImg = [];
        Navigator.of(context).pop();
        _dateController = TextEditingController(
            text: DateFormat("dd-MM-yyyy").format(_dateTime!.toUtc()));
        _endDateController = TextEditingController(
            text: DateFormat("dd-MM-yyyy").format(_dateTime!.toUtc()));
        interestController = TextEditingController(text: "0");
        _totalBalanceController = TextEditingController(text: "0");
        totalInterestController = TextEditingController(text: "0");
        durationController = TextEditingController(text: "0");
        totalProfitController = TextEditingController(text: "0");
        _totalGivenController = TextEditingController(text: "0");
        perDaysAmtController = TextEditingController(text: "0");
        _showDialogBox(context, "Saved to Customer List Successfully", "");
        
      });
    } else {
      Navigator.of(context).pop();
      setState(() {
        _autoValidate = true;
      });
      _showDialogBox(context, "Error!", "Please fill all the fields");
    }
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
docImg = [];
    _autoValidate = false;
    _switch = false;
    crde = 0;
    radio = 0;
    getEmpId();
    getAccNo();
    _dateTime = DateTime.now();
    newDate = DateTime.now();
    _totalBalanceController = TextEditingController(text: "0");
    interestController = TextEditingController(text: "0");
    _accController = TextEditingController(text: "0");
    totalInterestController = TextEditingController(text: "0");
    durationController = TextEditingController(text: "0");
    totalProfitController = TextEditingController(text: "0");
    _totalGivenController = TextEditingController(text: "0");
    perDaysAmtController = TextEditingController(text: "0");
    _dateController = TextEditingController(
        text: DateFormat("dd-MM-yyyy").format(_dateTime!.toUtc()));
    _endDateController = TextEditingController(
        text: DateFormat("dd-MM-yyyy").format(newDate!.toUtc()));
    // CustomerViewModel.loadCategories();
    // getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.03),
                    child: Center(
                      child: GestureDetector(
                        onTap: () {
                          _showPicker(context, 1);
                        },
                        child: CircleAvatar(
                          radius: 50,
                          backgroundColor: Color(0xffFDCF09),
                          child: profileImg != null
                              ? ClipRRect(
                                  borderRadius: BorderRadius.circular(100),
                                  child: Image.file(
                                    profileImg!.absolute,
                                    width: 300,
                                    height: 300,
                                    fit: BoxFit.fitHeight,
                                  ),
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                      color: Colors.grey[200],
                                      borderRadius: BorderRadius.circular(100)),
                                  width: 300,
                                  height: 300,
                                  child: Icon(
                                    Icons.camera_alt,
                                    color: Colors.grey[800],
                                  ),
                                ),
                        ),
                      ),
                    ),
                  ),
                  if (widget.dWM == 0)
                    Container(
                      child: Row(
                        children: <Widget>[
                          Radio(
                            value: 0,
                            groupValue: radio,
                            onChanged: radioButtonChanges,
                          ),
                          Container(
                            child: Text(
                              "Daily",
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          Radio(
                            value: 1,
                            groupValue: radio,
                            onChanged: radioButtonChanges,
                          ),
                          Container(
                            child: Text(
                              "Weekly",
                              style: TextStyle(fontSize: 16),
                            ),
                          ),
                          Radio(
                            value: 2,
                            groupValue: radio,
                            onChanged: radioButtonChanges,
                          ),
                          Container(
                            child: Text(
                              "Monthly",
                              style: TextStyle(fontSize: 16),
                            ),
                          )
                        ],
                      ),
                    ),
                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            autofocus: false,
                            textAlignVertical: TextAlignVertical.bottom,
                            controller: _accController,
                            readOnly: true,
                            enabled: false,
                            decoration: InputDecoration(
                              labelText: "Account No.",
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            textAlignVertical: TextAlignVertical.bottom,
                            autofocus: false,
                            onSaved: (String? custName) {
                              setState(() {
                                this.custName = custName;
                              });
                            },
                            validator: (value) {
                              if (value == "") {
                                return "Please Enter name";
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                                labelText: "Customer Name",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                                suffixIcon: Container(
                                  padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.03,
                                      left: MediaQuery.of(context).size.width *
                                          0.07),
                                  child: Icon(Icons.perm_contact_calendar),
                                )),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),

                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            autofocus: false,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(10),
                              new WhitelistingTextInputFormatter(
                                  RegExp("[0-9]"))
                            ],
                            onSaved: (String? contact) {
                              setState(() {
                                this.contactNo = contact;
                              });
                            },
                            decoration: InputDecoration(
                                labelText: "Contact No",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                                suffixIcon: Container(
                                  padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.03,
                                      left: MediaQuery.of(context).size.width *
                                          0.07),
                                  child: Icon(Icons.perm_contact_calendar),
                                )),
                            validator: validateMobile,
                            keyboardType: TextInputType.phone,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  //           Container(
                  //             child: AutoCompleteTextField(
                  //               itemSubmitted: (item){

                  //                   print("Submit" + item.cust_name.toString());
                  //                   _customerController.text = item.cust_name.toString();

                  //               },
                  //               clearOnSubmit: false,
                  //               key: key,
                  //               suggestions: CustomerViewModel.customer,
                  //               controller: _customerController,
                  //               itemBuilder: (context, item){
                  //                print( item.cust_name.toString());
                  //                 return Container(
                  //                   child: Row(
                  //                     children: [
                  //                       Container(
                  //                         child: Text(
                  //                           item.cust_name.toString(),

                  //                         ),
                  //                       )
                  //                     ],
                  //                   ),
                  //                 );
                  //               },
                  //               itemSorter: (a, b) =>
                  //   a.cust_name.compareTo(b.cust_name),
                  //               itemFilter: (suggestion, input) {

                  //  return  suggestion.cust_name.toLowerCase().startsWith(input.toLowerCase());
                  //               }
                  //   ),
                  //           ),
                  // Container(
                  //   padding: EdgeInsets.only(
                  //       //top: MediaQuery.of(context).size.height * 0.02,
                  //       left: MediaQuery.of(context).size.width * 0.04,
                  //       right: MediaQuery.of(context).size.width * 0.04),
                  //   child: Row(
                  //     children: <Widget>[
                  //       Container(
                  //         width: MediaQuery.of(context).size.width * 0.92,
                  //         child: TextFormField(
                  //           autofocus: false,
                  //           //inputFormatters: [new BlacklistingTextInputFormatter(RegExp(r"/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/")),],
                  //           onSaved: (value) {
                  //             setState(() {
                  //               this.email = value;
                  //             });
                  //           },

                  //           decoration: InputDecoration(
                  //             labelText: "Email",
                  //             contentPadding: EdgeInsets.only(
                  //                 top: MediaQuery.of(context).size.height *
                  //                     0.015),
                  //           ),

                  //           keyboardType: TextInputType.emailAddress,
                  //           style: new TextStyle(
                  //             fontFamily: "Poppins",
                  //           ),
                  //         ),
                  //       )
                  //     ],
                  //   ),
                  // ),

                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.92,
                          child: TextFormField(
                            autofocus: false,
                            maxLines: 2,
                            onSaved: (String? address) {
                              setState(() {
                                this.address = address;
                              });
                            },
                            decoration: InputDecoration(
                              labelText: "Address",
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        )
                      ],
                    ),
                  ),

                  Container(
                    height: 50,
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.01,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.25,
                          child: TextFormField(
                            autofocus: false,
                            controller: _totalBalanceController,
                            validator: (value) {
                              if (value == "") {
                                return "Please Enter Opening Stock";
                              } else {
                                return null;
                              }
                            },
                            onSaved: (String? value) {
                              setState(() {
                                this.totalBalance = value;
                              });
                            },
                            onChanged: (value) {
                              setState(() {
                                totalBalance = value;
                              });
                            },
                            decoration: InputDecoration(
                              labelText: "Total Amt",
                              contentPadding: EdgeInsets.only(
                                  top: 10, left: 10, bottom: 0, right: 0),
                              border: new OutlineInputBorder(
                                borderSide: new BorderSide(),
                              ),
                            ),
                            keyboardType: TextInputType.numberWithOptions(
                                decimal: true, signed: false),
                            style: new TextStyle(
                                fontFamily: "Poppins",
                                color: Colors.blue[800],
                                fontSize: 18,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.3,
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.04),
                          child: TextFormField(
                            autofocus: false,
                            controller: durationController,
                            validator: (value) {
                              if (value == "") {
                                return "Please Enter Duration";
                              } else {
                                return null;
                              }
                            },
                            onChanged: (value) {
                              setState(() {
                                // var tpdate = DateTime.parse(_dateController!.text);
                                print(durationController!.text);
                                var splittedDate =
                                    _dateController!.text.split("-");
                                var rearrangedDate = splittedDate[2] +
                                    "-" +
                                    splittedDate[1] +
                                    "-" +
                                    splittedDate[0];
                                print(rearrangedDate);
                                DateTime date = DateTime.parse(rearrangedDate);
                                // var newDate  = DateTime.pa
                                print(date.day);
                                if (radio == 0) {
                                  newDate = date.add(
                                      Duration(days: int.parse(value) + 1));
                                } else if (radio == 1) {
                                  newDate = date.add(Duration(
                                      days: ((int.parse(value)) * 7) + 1));
                                } else {
                                  newDate = new DateTime(
                                      date.year,
                                      date.month + int.parse(value),
                                      date.day + 1);
                                }
                                _endDateController!.text =
                                    DateFormat("dd-MM-yyyy")
                                        .format(newDate!.toUtc());
                                if (radio == 0) {
                                  _totalGivenController!.text = (double.parse(
                                              _totalBalanceController!.text) -
                                          ((((double.parse(interestController!
                                                              .text) /
                                                          100) *
                                                      double.parse(
                                                          _totalBalanceController!
                                                              .text)) /
                                                  30) *
                                              double.parse(durationController!
                                                          .text ==
                                                      '0'
                                                  ? "1"
                                                  : durationController!.text)))
                                      .toStringAsFixed(2);

                                  totalProfitController!
                                      .text = ((((double.parse(
                                                          interestController!
                                                              .text) /
                                                      100) *
                                                  double.parse(
                                                      _totalBalanceController!
                                                          .text)) /
                                              30) *
                                          double.parse(
                                              durationController!.text == '0'
                                                  ? "1"
                                                  : durationController!.text))
                                      .toStringAsFixed(2);
                                  perDaysAmtController!.text = (double.parse(
                                              _totalBalanceController!.text) /
                                          double.parse(
                                              durationController!.text))
                                      .toStringAsFixed(2);
                                }

                                if (radio == 1) {
                                  _totalGivenController!.text = (double.parse(
                                              _totalBalanceController!.text) -
                                          ((((double.parse("20") / 100) *
                                                  double.parse(
                                                      _totalBalanceController!
                                                          .text)) *
                                              double.parse(durationController!
                                                          .text ==
                                                      '0'
                                                  ? "1"
                                                  : durationController!.text) /
                                              10)))
                                      .toStringAsFixed(2);

                                  totalProfitController!
                                      .text = ((((double.parse("20") / 100) *
                                              double.parse(
                                                  _totalBalanceController!
                                                      .text))) *
                                          double.parse(
                                              durationController!.text == '0'
                                                  ? "1"
                                                  : durationController!.text) /
                                          10)
                                      .toStringAsFixed(2);
                                  perDaysAmtController!.text = (double.parse(
                                              _totalBalanceController!.text) /
                                          double.parse(
                                              durationController!.text))
                                      .toStringAsFixed(2);
                                }
                                if (radio == 2 && value != "") {
                                  totalInterestController!
                                      .text = (((double.parse(
                                                  interestController!.text) /
                                              100) *
                                          double.parse(
                                              _totalBalanceController!.text)))
                                      .toStringAsFixed(2);
                                  totalInterestController!
                                      .text = (double.parse(value) *
                                          double.parse(
                                              totalInterestController!.text))
                                      .toStringAsFixed(2);
                                } else if (radio != 2 || value == "") {
                                  totalInterestController!
                                      .text = (((double.parse(value) / 100) *
                                          double.parse(
                                              _totalBalanceController!.text)))
                                      .toStringAsFixed(2);
                                }
                              });
                            },
                            onSaved: (String? value) {
                              setState(() {
                                this.duration = value;
                              });
                            },
                            decoration: InputDecoration(
                              labelText:
                                  (radio == 0
                                      ? "Days"
                                      : (radio == 1 ? "Weeks" : "Months")),
                              contentPadding: EdgeInsets.only(
                                  top: 10, left: 10, bottom: 0, right: 0),
                              border: new OutlineInputBorder(
                                borderSide: new BorderSide(),
                              ),
                            ),
                            keyboardType: TextInputType.numberWithOptions(
                                decimal: true, signed: false),
                            style: new TextStyle(
                              fontFamily: "Poppins",
                              fontSize: 18,
                            ),
                          ),
                        ),
                        radio != 1 ?
                        Container(
                            padding: EdgeInsets.only(
                              //top: MediaQuery.of(context).size.height * 0.02,
                              left: MediaQuery.of(context).size.width * 0.05,
                            ),
                            width: MediaQuery.of(context).size.width * 0.3,
                            child: TextFormField(
                              autofocus: false,
                              
                              controller: interestController,
                              validator: (value) {
                                if (value == "") {
                                  return "Please Enter Interest";
                                } else {
                                  return null;
                                }
                              },
                              onChanged: (value) {
                                print(durationController!.text);
                                setState(() {
                                  if (radio == 0) {
                                    _totalGivenController!.text = (double.parse(
                                                _totalBalanceController!.text) -
                                            ((((double.parse(value) / 100) *
                                                        double.parse(
                                                            _totalBalanceController!
                                                                .text)) /
                                                    30) *
                                                double.parse(
                                                    durationController!.text ==
                                                            '0'
                                                        ? "1"
                                                        : durationController!
                                                            .text)))
                                        .toStringAsFixed(2);

                                    totalProfitController!
                                        .text = ((((double.parse(value) / 100) *
                                                    double.parse(
                                                        _totalBalanceController!
                                                            .text)) /
                                                30) *
                                            double.parse(
                                                durationController!.text == '0'
                                                    ? "1"
                                                    : durationController!.text))
                                        .toStringAsFixed(2);
                                    perDaysAmtController!.text = (double.parse(
                                                _totalBalanceController!.text) /
                                            double.parse(
                                                durationController!.text))
                                        .toStringAsFixed(2);
                                  }

                                  if (radio == 2 && value != '') {
                                    perDaysAmtController!.text = (double.parse(
                                                _totalBalanceController!.text) /
                                            double.parse(
                                                durationController!.text))
                                        .toStringAsFixed(2);
                                    totalInterestController!
                                        .text = (((double.parse(value) / 100) *
                                            double.parse(
                                                _totalBalanceController!.text)))
                                        .toStringAsFixed(2);
                                  } else {
                                    totalInterestController!.text = "0";
                                  }
                                });
                              },
                              onSaved: (String? value) {
                                setState(() {
                                  this.interest = value;
                                });
                              },
                              decoration: InputDecoration(
                                  labelText: "Int. (in %)",
                                   contentPadding: EdgeInsets.only(
                                  top: 10, left: 10, bottom: 0, right: 0),
                              border: new OutlineInputBorder(
                                borderSide: new BorderSide(),
                              ),
                                  prefixStyle: TextStyle(
                                      color: Colors.black, fontSize: 18)),
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true, signed: false),
                              style: new TextStyle(
                                fontFamily: "Poppins",
                                fontSize: 18
                              ),
                            ),
                          ) :
                           Container(
                              padding: EdgeInsets.only(
                              //top: MediaQuery.of(context).size.height * 0.02,
                              left: MediaQuery.of(context).size.width * 0.05,
                            ),
                          child: Text(" Interest 20% ",style: new TextStyle(
                                fontFamily: "Poppins",
                                fontSize: 16
                              ),),
                        )
                         
                      ],
                    ),
                  ),

                  Container(
                    padding: EdgeInsets.only(
                        //top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.04,
                        right: MediaQuery.of(context).size.width * 0.04),
                    child: Row(
                      children: <Widget>[
                        if (radio != 2)
                          Container(
                            width: MediaQuery.of(context).size.width * 0.3,
                            child: TextFormField(
                              autofocus: false,
                              readOnly: true,
                              controller: _totalGivenController,
                              onChanged: (value) {
                                setState(() {
                                  totalBalance = value;
                                });
                              },
                              decoration: InputDecoration(
                                labelText: "Total Given Amt",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                              ),
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true, signed: false),
                              style: new TextStyle(
                                  fontFamily: "Poppins",
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        if (radio != 2)
                          Container(
                            padding: EdgeInsets.only(
                              //top: MediaQuery.of(context).size.height * 0.02,
                              left: MediaQuery.of(context).size.width * 0.05,
                            ),
                            width: MediaQuery.of(context).size.width * 0.3,
                            child: TextFormField(
                              autofocus: false,
                              readOnly: true,
                              controller: totalProfitController,
                              validator: (value) {
                                if (value == "") {
                                  return "Please Enter Interest";
                                } else {
                                  return null;
                                }
                              },
                              onChanged: (value) {
                                print(durationController!.text);
                                setState(() {});
                              },
                              decoration: InputDecoration(
                                  labelText: "Total Profit",
                                  contentPadding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height *
                                          0.015),
                                  prefixStyle: TextStyle(
                                      color: Colors.black, fontSize: 18)),
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true, signed: false),
                              style: new TextStyle(
                                  fontFamily: "Poppins",
                                  color: Colors.green,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        Container(
                          padding: EdgeInsets.only(
                            //top: MediaQuery.of(context).size.height * 0.02,
                            left: radio != 2
                                ? MediaQuery.of(context).size.width * 0.05
                                : 0,
                          ),
                          width: MediaQuery.of(context).size.width * 0.3,
                          child: TextFormField(
                            autofocus: false,
                            readOnly: true,
                            controller: perDaysAmtController,
                            validator: (value) {
                              if (value == "") {
                                return "Please Enter Interest";
                              } else {
                                return null;
                              }
                            },
                            onChanged: (value) {
                              print(durationController!.text);
                              setState(() {});
                            },
                            decoration: InputDecoration(
                                labelText: "Amt (per " +
                                    (radio == 0
                                        ? "Day"
                                        : (radio == 1 ? "Week" : "Month")) +
                                    ")",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                                prefixStyle: TextStyle(
                                    color: Colors.black, fontSize: 18)),
                            keyboardType: TextInputType.numberWithOptions(
                                decimal: true, signed: false),
                            style: new TextStyle(
                                fontFamily: "Poppins",
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        if (radio == 2)
                          Container(
                            padding: EdgeInsets.only(
                              //top: MediaQuery.of(context).size.height * 0.02,
                              left: MediaQuery.of(context).size.width * 0.1,
                            ),
                            width: MediaQuery.of(context).size.width * 0.46,
                            child: TextFormField(
                              
                              autofocus: false,
                              controller: totalInterestController,
                              readOnly: true,
                              enabled: false,
                              validator: (value) {
                                if (value == "") {
                                  return "Please Enter Interest";
                                } else {
                                  return null;
                                }
                              },
                              onChanged: (value) {},
                              decoration: InputDecoration(
                                labelText: "Tot Int.(per Month)",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                              ),
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true, signed: false),
                              style: new TextStyle(
                                  fontFamily: "Poppins",
                                  color: Colors.blue[800],
                                  fontWeight: FontWeight.w500),
                            ),
                          )
                      ],
                    ),
                  ),

                  // Container(
                  //   padding: EdgeInsets.only(
                  //       //top: MediaQuery.of(context).size.height * 0.02,
                  //       left: MediaQuery.of(context).size.width * 0.04,
                  //       right: MediaQuery.of(context).size.width * 0.04),
                  //   child: Row(
                  //     children: <Widget>[
                  //       Container(
                  //         width: MediaQuery.of(context).size.width * 0.92,
                  //         child: TextFormField(
                  //           autofocus: false,
                  //           validator: (value) {
                  //             if (value == "") {
                  //               return "Please Enter Amount";
                  //             } else {
                  //               return null;
                  //             }
                  //           },
                  //           onSaved: (String? value) {
                  //             setState(() {
                  //               this.amountDWM = value;
                  //             });
                  //           },
                  //           decoration: InputDecoration(
                  //             labelText: "Amount Collect " +
                  //                 (radio == 0
                  //                     ? "Daily"
                  //                     : (radio == 1 ? "Weekly" : "Monthly")),
                  //             contentPadding: EdgeInsets.only(
                  //                 top: MediaQuery.of(context).size.height *
                  //                     0.015),
                  //           ),
                  //           keyboardType: TextInputType.numberWithOptions(
                  //               decimal: true, signed: false),
                  //           style: new TextStyle(
                  //             fontFamily: "Poppins",
                  //           ),
                  //         ),
                  //       )
                  //     ],
                  //   ),
                  // ),

                  // Container(
                  //   padding: EdgeInsets.only(
                  //       //top: MediaQuery.of(context).size.height * 0.02,
                  //       left: MediaQuery.of(context).size.width * 0.04,
                  //       right: MediaQuery.of(context).size.width * 0.04),
                  //   child: Row(
                  //     children: <Widget>[

                  //     ],
                  //   ),
                  // ),
                  Row(
                    children: [
                      Container(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.01),
                          width: MediaQuery.of(context).size.width * 0.46,
                          child: DateTimeField(
                              format: DateFormat('dd-MM-yyyy'),
                              controller: _dateController,
                              initialValue: DateTime.now(),
                              resetIcon: null,
                              decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.calendar_today),
                                  labelText: "Opening Date"),
                              onShowPicker: (context, currentValue) {
                                return showDatePicker(
                                    context: context,
                                    initialDate: _dateTime == null
                                        ? DateTime.now()
                                        : _dateTime!.toUtc(),
                                    firstDate: DateTime(2001),
                                    lastDate: DateTime(2100));
                              },
                              validator: (val) {
                                if (val != null) {
                                  return null;
                                } else {
                                  return 'Date Field is Empty';
                                }
                              })),
                      Container(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.04),
                          width: MediaQuery.of(context).size.width * 0.46,
                          child: DateTimeField(
                              format: DateFormat('dd-MM-yyyy'),
                              controller: _endDateController,
                              initialValue: newDate,
                              enabled: false,
                              readOnly: true,
                              resetIcon: null,
                              decoration: InputDecoration(
                                  prefixIcon: Icon(Icons.calendar_today),
                                  labelText: "End Date"),
                              onShowPicker: (context, currentValue) {
                                return showDatePicker(
                                    context: context,
                                    initialDate: newDate!.toUtc(),
                                    firstDate: DateTime(2001),
                                    lastDate: DateTime(2100));
                              },
                              validator: (val) {
                                if (val != null) {
                                  return null;
                                } else {
                                  return 'Date Field is Empty';
                                }
                              })),
                    ],
                  ),

                  Container(
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.04,
                          top: MediaQuery.of(context).size.height * 0.01),
                      child: Row(
                        children: [
                          Text(
                            "Upload Document : ",
                            style: TextStyle(fontSize: 18),
                          ),
                          docImg != null && docImg!.length != 0
                              ? Expanded(
                                child: Container(
                                  //height: 300,
                                  child: ListView.builder(
                                    shrinkWrap: true,
                                    itemCount: docImg!.length,
                                    
                                    itemBuilder: (BuildContext context, int index){
                                    return Container(
                                      
                                      child: Text(docImg![index]!.path.split("/").last));
                                  }),
                                ),
                              )
                              : TextButton(
                                  onPressed: () {
                                    _showPicker(context, 2);
                                  },
                                  child: Row(
                                    children: [
                                      Icon(Icons.photo_library),
                                      Text(" Select File",
                                          style: TextStyle(fontSize: 18)),
                                    ],
                                  ))
                        ],
                      )),
                ],
              ),
            ),
          ),
          Container(
            height: 50,
            width: MediaQuery.of(context).size.width * 1,
            color: Colors.blue[800],
            child: FlatButton(
                onPressed: () {
                  Constants.waiting(context);
                  saveCustomer();
                },
                child: Text(
                  "Save",
                  style: TextStyle(color: Colors.white),
                )),
          )
        ],
      ),
    );
  }
}
