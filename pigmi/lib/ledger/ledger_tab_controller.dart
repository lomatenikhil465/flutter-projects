import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pigmi/customer/ExistingCustomerPage.dart';
//import 'package:pigmi/ledger/LedgerPage.dart';

class LedgerTabController extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LedgerTabController();
  }
  
}

class _LedgerTabController extends State<LedgerTabController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Ledger"),
          backgroundColor: Colors.blue[800],
          bottom: TabBar(
                  labelColor: Colors.blue[800],
                  unselectedLabelColor: Colors.white,
                  indicatorSize: TabBarIndicatorSize.label,
                  indicator: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10)),
                      color: Colors.white),
                  tabs: [
                    Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text("Daily"),
                      ),
                    ),
                    
                    Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text("Weekly"),
                      ),
                    ),
                     Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text("Monthly"),
                      ),
                    ),
                  ]
              ),
        ),
        body: Container(
          child: TabBarView(
           
          children: [
            ExistingCustomerPage(dWM: 1,cat: "Ledger",),
            ExistingCustomerPage(dWM: 2,cat: "Ledger",),
             ExistingCustomerPage(dWM: 3,cat: "Ledger",),
          ]
          ),
        ),
      ),
    );
  }
  
}