import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:pigmi/customer_details.dart';
import 'dart:async';
import 'dart:convert';

import 'package:pigmi/utils/const.dart';
import 'package:shared_preferences/shared_preferences.dart';

//import '../CustomerDetails.dart';

class LedgerPage extends StatefulWidget {
  final int? dWM;

  LedgerPage({this.dWM});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LedgerPage();
  }
}

List? nameOfCustomer;
List? _searchResult;
var empId;

class _LedgerPage extends State<LedgerPage> {
  TextEditingController? _controller;

  SharedPreferences? prefs;
  Future<String>? customerResponse;

  Future<String> getJsonData() async {
    String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=search&empid=" +
        empId.toString() +
        "&pType=" +
        widget.dWM.toString()
         ;
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      nameOfCustomer = convertDataToJson['user'];
    });

    return "success";
  }

  void onSearchTextChanged(String text) async {
    if (_searchResult != null) {
      _searchResult!.clear();
    }
    if (text.isEmpty) {
      setState(() {});
      //return ;
    }

    nameOfCustomer!.forEach((userDetail) {
      print("search result : " + userDetail["cName"].toString());
      if (userDetail["cName"].toLowerCase().contains(text.toLowerCase()) ||
          userDetail["accNo"].toLowerCase().contains(text.toLowerCase())) {
        _searchResult!.add(userDetail);
      }
    });

    setState(() {});
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      empId = prefs!.getString("empId");
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _searchResult = [];
    getEmpId().then((value) {
      
      customerResponse = getJsonData();
    });
    
    _controller = TextEditingController(text: "");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: Column(
      children: <Widget>[
        Container(
          height: 55,
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            onChanged: (value) {
              onSearchTextChanged(value);
            },
            controller: _controller,
            decoration: InputDecoration(
                labelText: "Search",
                hintText: "Search",
                contentPadding: EdgeInsets.only(top: 10),
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5.0)))),
          ),
        ),
        
           Expanded(
          child: FutureBuilder<String>(
            future: customerResponse,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return nameOfCustomer == null
                    ? Center(
                        child: Text("No Data Available"),
                      )
                    : ((_searchResult == null
                                ? false
                                : _searchResult!.length != 0) ||
                            _controller!.text.isNotEmpty
                        ? ListView.separated(
                            separatorBuilder:
                                (BuildContext context, int index) => Divider(
                              thickness: 0.0,
                              color: Colors.lightBlue[900],
                            ),
                            itemCount: _searchResult == null
                                ? 0
                                : _searchResult!.length,
                            itemBuilder: (BuildContext context, int index) {
                              return LedgerCustomer(
                                index: index,
                                dWM: widget.dWM!.toInt(),
                              );
                            },
                          )
                        : ListView.builder(
                            // separatorBuilder:
                            //     (BuildContext context, int index) => Divider(
                            //   thickness: 0.0,
                            //   color: Colors.white,
                            // ),
                            itemCount: nameOfCustomer == null
                                ? 0
                                : nameOfCustomer!.length,
                            itemBuilder: (BuildContext context, int index) {
                              return LedgerCustomer(
                                index: index,
                                dWM: widget.dWM!.toInt(),
                              );
                            },
                          ));
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ),
      ],
    ));
  }
}

class LedgerCustomer extends StatefulWidget {
  final int index, dWM;
  LedgerCustomer({required this.index, required this.dWM});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LedgerCustomer();
  }
}

class _LedgerCustomer extends State<LedgerCustomer> {
  List? customerStatement;
  double totalPayment = 0.0;

  Future<String> getTotal() async {
    // String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiReport.php?apicall=customerReport&empid=" +
        empId.toString() +
        "&pType=" +
        widget.dWM.toString() +
        "&custId=" +
        ((_searchResult == null ? false : _searchResult!.length != 0)
                ? _searchResult![widget.index]["ID"]
                : nameOfCustomer![widget.index]["ID"])
            .toString() +
        "&accNo=" +
        ((_searchResult == null ? false : _searchResult!.length != 0)
                ? _searchResult![widget.index]["accNo"]
                : nameOfCustomer![widget.index]["accNo"])
            .toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      customerStatement = convertDataToJson['user'];

      if (customerStatement != null) {
        for (var i = 0; i < customerStatement!.length; i++) {
          print("hi :  " + customerStatement![i]["int_Amt"]);
          totalPayment =
              totalPayment + double.parse(customerStatement![i]["amt"]) + double.parse(widget.dWM == 3 ?  customerStatement![i]["int_Amt"] : "0");
        }
      }
    });
    return "success";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getTotal();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return CustomerDetails(
            custId: (_searchResult == null ? false : _searchResult!.length != 0)
                ? _searchResult![widget.index]["ID"]
                : nameOfCustomer![widget.index]["ID"],
            name: (_searchResult == null ? false : _searchResult!.length != 0)
                ? _searchResult![widget.index]["cName"]
                : nameOfCustomer![widget.index]["cName"],
            accNo: (_searchResult == null ? false : _searchResult!.length != 0)
                ? _searchResult![widget.index]["accNo"]
                : nameOfCustomer![widget.index]["accNo"],
            total: (_searchResult == null ? false : _searchResult!.length != 0)
                ? _searchResult![widget.index]["credit_amt"]
                : nameOfCustomer![widget.index]["credit_amt"],
            dWM: widget.dWM,
            startDt:
                (_searchResult == null ? false : _searchResult!.length != 0)
                    ? _searchResult![widget.index]["startdt"]
                    : nameOfCustomer![widget.index]["startdt"],
            endDt: (_searchResult == null ? false : _searchResult!.length != 0)
                ? _searchResult![widget.index]["enddt"]
                : nameOfCustomer![widget.index]["enddt"],
          );
        }));
      },
      child: Container(
        // height: 60,
        child: Card(
          elevation: 3.0,
          child: Column(
            children: <Widget>[
              Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.01,
                        top: MediaQuery.of(context).size.height * 0.01),
                    child: Text(
                      ((_searchResult == null
                                      ? false
                                      : _searchResult!.length != 0)
                                  ? _searchResult![widget.index]["accNo"]
                                  : nameOfCustomer![widget.index]["accNo"])
                              .toString() +
                          " ",
                      style: TextStyle(
                          color: Colors.blue[800], fontWeight: FontWeight.w500, fontSize: 15),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.01,
                        top: MediaQuery.of(context).size.height * 0.01),
                    child: Text(
                      ((_searchResult == null
                                  ? false
                                  : _searchResult!.length != 0)
                              ? _searchResult![widget.index]["cName"]
                              : nameOfCustomer![widget.index]["cName"])
                          .toString(),
                      style: TextStyle(
                          color: Colors.blue[800], fontWeight: FontWeight.w500, fontSize: 15),
                    ),
                  ),
                  Spacer(),
                  Container(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.01),
                    child: Text(
                      "Tot. Received",
                      style: TextStyle(color: Colors.green[600]),
                    ),
                  )
                ],
              ),
              Row(
                //crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Container(
                    child: Icon(
                      Icons.location_on,
                      color: Colors.grey,
                      size: 15,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.01),
                    child: Text(
                      ((_searchResult == null
                                  ? false
                                  : _searchResult!.length != 0)
                              ? _searchResult![widget.index]["addrs"]
                              : nameOfCustomer![widget.index]["addrs"])
                          .toString(),
                      style: TextStyle(color: Colors.grey),
                    ),
                  ),
                  Spacer(),
                  Container(
                    child: Text(
                      "₹ " + (totalPayment).toStringAsFixed(2),
                      style:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 18),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
