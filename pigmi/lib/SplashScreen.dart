import 'dart:async';

//import 'package:pigmi/pages/menuPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pigmi/home_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login_page.dart';

//import 'Login.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SplashScreenState();
  }
}

class _SplashScreenState extends State<SplashScreen> {
  SharedPreferences? prefs;

  @override
  void initState() {
    super.initState();

    _mockCheckForSession();
  }

  Future<bool> _mockCheckForSession() async {
    Timer(Duration(seconds: 5), _navigateToHome);
    prefs = await SharedPreferences.getInstance();
    return true;
  }

  void _navigateToHome() {
    var user = prefs?.getString('user');
    var pass = prefs?.getString('pass');
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (BuildContext context) {
      if (user == null && pass == null) {
        return Login();
      } else {
        return HomePage(
          username: user,
        );
      }
    }));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        body: Container(
      color: Colors.white,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Opacity(
              opacity: 1,
              child: Image.asset(
                'assets\\images\\jf log1.jpg',
              )),
          // Center(
          //   child: Text("JF",
          //       style: TextStyle(
          //           fontSize: 30,
          //           color: Colors.blue[800],
          //           fontWeight: FontWeight.bold)),
          // )
        ],
      ),
    ));
  }
}
