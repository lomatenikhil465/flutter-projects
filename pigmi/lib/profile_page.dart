import 'dart:io';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:pigmi/utils/const.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:url_launcher/url_launcher.dart';

class ProfilePage extends StatefulWidget {
  final String? custId, accNo;

  ProfilePage({this.custId, this.accNo});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProfilePage();
  }
}

class _ProfilePage extends State<ProfilePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final picker = ImagePicker();
  List? profileData, images;
  int? radio, crde = 0;
  bool? status = false;
  File? profileImg, docImg;
  String? compName,
      custName,
      contactNo,
      email,
      gst,
      address,
      changedRadio,
      totalBalance,
      amountDWM,
      duration,
      interest;
  SharedPreferences? prefs;
  Future<String>? profileResponse;
  var temp;
  DateTime? _dateTime, newDate;
  TextEditingController? _accController,
      _dateController,
      _endDateController,
      custNameController,
      contactController,
      addressController,
      _totalBalanceController,
      durationController,
      interestController,
      totalInterestController;
  TextEditingController? totalProfitController,
      _totalGivenController,
      perDaysAmtController;
  CarouselController buttonCarouselController = new CarouselController();

  Future<String> getJsonData() async {
    var empId = prefs!.getString('empId');
    String url = Constants.url +
        "app/pigmiAll.php?apicall=profile&custId=" +
        widget.custId.toString() +
        "&empid=" +
        empId.toString();

    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      profileData = convertDataToJson['user'];
      var splittedDate = profileData![0]["startdt"].split("-");
      var rearrangedDate =
          splittedDate[2] + "-" + splittedDate[1] + "-" + splittedDate[0];
      var splittedEndDate = profileData![0]["enddt"].split("-");
      var rearrangedEndDate = splittedEndDate[2] +
          "-" +
          splittedEndDate[1] +
          "-" +
          splittedEndDate[0];
      _accController!.text = profileData![0]["accNo"] == null
          ? ""
          : profileData![0]["accNo"].toString();
      custNameController!.text =
          profileData![0]["cName"] == null ? "" : profileData![0]["cName"];
      contactController!.text =
          profileData![0]["mob_No"] == null ? "" : profileData![0]["mob_No"];
      addressController!.text =
          profileData![0]["addrs"] == null ? "" : profileData![0]["addrs"];
      _totalBalanceController!.text = profileData![0]["credit_amt"] == null
          ? ""
          : profileData![0]["credit_amt"];
      _dateController!.text = rearrangedDate;
      interestController!.text = profileData![0]["interest"] == null
          ? ""
          : profileData![0]["interest"];
      totalInterestController!.text = profileData![0]["interest_amt"] == null
          ? ""
          : profileData![0]["interest_amt"];
      durationController!.text = profileData![0]["duration"] == null
          ? ""
          : profileData![0]["duration"];
      _endDateController!.text = rearrangedEndDate;

      if (profileData![0]["pType"] == '1') {
        print(_totalBalanceController!.text);
        _totalGivenController!.text =
            (double.parse(_totalBalanceController!.text) -
                    ((((double.parse(interestController!.text) / 100) *
                                double.parse(_totalBalanceController!.text)) /
                            30) *
                        double.parse(durationController!.text == '0'
                            ? "1"
                            : durationController!.text)))
                .toStringAsFixed(2);

        totalProfitController!.text =
            ((((double.parse(interestController!.text) / 100) *
                            double.parse(_totalBalanceController!.text)) /
                        30) *
                    double.parse(durationController!.text == '0'
                        ? "1"
                        : durationController!.text))
                .toStringAsFixed(2);
        perDaysAmtController!.text =
            (double.parse(_totalBalanceController!.text) /
                    double.parse(durationController!.text))
                .toStringAsFixed(2);
      }

      if (profileData![0]["pType"] == '2') {
        _totalGivenController!.text =
            (double.parse(_totalBalanceController!.text) -
                    ((((double.parse("20") / 100) *
                            double.parse(_totalBalanceController!.text)) *
                        double.parse(durationController!.text == '0'
                            ? "1"
                            : durationController!.text) /
                        10)))
                .toStringAsFixed(2);

        totalProfitController!.text = ((((double.parse("20") / 100) *
                    double.parse(_totalBalanceController!.text))) *
                double.parse(durationController!.text == '0'
                    ? "1"
                    : durationController!.text) /
                10)
            .toStringAsFixed(2);
        perDaysAmtController!.text =
            (double.parse(_totalBalanceController!.text) /
                    double.parse(durationController!.text))
                .toStringAsFixed(2);
      }
      if (profileData![0]["pType"] == '3') {
        perDaysAmtController!.text =
            (double.parse(_totalBalanceController!.text) /
                    double.parse(durationController!.text))
                .toStringAsFixed(2);
        totalInterestController!.text =
            (((double.parse(interestController!.text) / 100) *
                    double.parse(_totalBalanceController!.text)))
                .toStringAsFixed(2);
      }
      print(profileData![0]["pf_img_path"].toString());
    });

    return "success";
  }

  Future<String> getImages() async {
    // var empId = prefs!.getString('empId');
    String url = Constants.url +
        "app/pigmiAll.php?apicall=getImages&accNo=" +
        widget.accNo.toString();

    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print("thisi is : " + response.body.toString());
    setState(() {
      var convertDataToJson = json.decode(response.body);
      images = convertDataToJson['user'];
    });

    return "success";
  }

  Future<String> updateCustomer() async {
    String url = Constants.url + "app/addCust.php?apicall=updateCustomer&";
    String? base64ProfileImage, base64DocImage;
    var splittedStartDate = _dateController!.text.split("-");
    var rearrangedStartDate = splittedStartDate[2] +
        "-" +
        splittedStartDate[1] +
        "-" +
        splittedStartDate[0];
    print(_dateController!.text);
    print(rearrangedStartDate);
    var splittedEndDate = _endDateController!.text.split("-");
    var rearrangedEndDate = splittedEndDate[2] +
        "-" +
        splittedEndDate[1] +
        "-" +
        splittedEndDate[0];
    String? empId = prefs!.getString("empId");
    if (profileImg != null &&
        (profileData![0]["pf_img_path"] == '' ||
            profileData![0]["pf_img_path"] == '0'))
      base64ProfileImage = base64Encode(profileImg!.readAsBytesSync());
    if (docImg != null &&
        (profileData![0]["doc_path"] == '' ||
            profileData![0]["doc_path"] == '0'))
      base64DocImage = base64Encode(docImg!.readAsBytesSync());
    print("Profile page" + base64ProfileImage.toString());
    var response = await http.post(Uri.parse(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'startdt': rearrangedStartDate,
          'enddt': rearrangedEndDate,
          'accNo': _accController!.text,
          'cust_nm': custName.toString(),
          'addrs': address.toString(),
          'mobileNo': contactNo.toString(),
          'aType': profileData![0]["pType"].toString(),
          'docpath': docImg == null
              ? (profileData![0]["doc_path"] == null ||
                      profileData![0]["doc_path"] == '' ||
                      profileData![0]["doc_path"] == '0')
                  ? ""
                  : profileData![0]["doc_path"]
              : base64DocImage.toString(),
          'profile_pic': profileImg == null
              ? (profileData![0]["pf_img_path"] == null ||
                      profileData![0]["pf_img_path"] == '' ||
                      profileData![0]["pf_img_path"] == '0')
                  ? ""
                  : profileData![0]["pf_img_path"]
              : base64ProfileImage.toString(),
          'period': duration.toString(),
          'amt': _totalBalanceController!.text,
          'userId': empId.toString(),
          'interest': interestController!.text,
          'custId': widget.custId.toString(),
          'interest_amt': totalInterestController!.text,
          'perDays': perDaysAmtController!.text
        }).then((value) {
      print(value.body);
    });

    //print(response.reasonPhrase);

    return "success";
  }

  showDocumentImage(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  (images != null)
                      ? CarouselSlider(
                          options: CarouselOptions(
                            height: 400,
                            autoPlay: false,
                            enlargeCenterPage: true,
                            viewportFraction: 0.9,
                            aspectRatio: 2.0,
                            initialPage: 2,
                          ),
                          items: images!.map((i) {
                            return Builder(
                              builder: (BuildContext context) {
                                return Container(
                                    width: MediaQuery.of(context).size.width,
                                    // margin:
                                    //     EdgeInsets.symmetric(horizontal: 5.0),
                                    // decoration:
                                    //     BoxDecoration(color: Colors.amber),
                                    child: Image.network(
                                        Constants.url + i["img_path"]));
                              },
                            );
                          }).toList(),
                        )
                      // Container(
                      //     child: Image.network(
                      //       Constants.url + profileData![0]["pf_img_path"],
                      //       loadingBuilder: (BuildContext context, Widget child,
                      //           ImageChunkEvent? loadingProgress) {
                      //         if (loadingProgress == null) return child;
                      //         return Center(
                      //           child: CircularProgressIndicator(
                      //             value: loadingProgress.expectedTotalBytes !=
                      //                     null
                      //                 ? loadingProgress.cumulativeBytesLoaded /
                      //                     loadingProgress.expectedTotalBytes!
                      //                         .toInt()
                      //                 : null,
                      //           ),
                      //         );
                      //       },
                      //     ),
                      //   )
                      : Container(
                          child: Text(
                            "No Documents are uploaded...",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: TextButton(
                        style: TextButton.styleFrom(
                            textStyle: TextStyle(
                          fontSize: 16,
                        )),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text("Close")),
                  )
                ],
              ),
            ),
          );
        });
  }

  showProfileImage(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.white,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  (profileData != null &&
                          profileData![0]["pf_img_path"] != '0' &&
                          profileData![0]["pf_img_path"] != '')
                      ? Container(
                          child: Image.network(
                            Constants.url + profileData![0]["pf_img_path"],
                            loadingBuilder: (BuildContext context, Widget child,
                                ImageChunkEvent? loadingProgress) {
                              if (loadingProgress == null) return child;
                              return Center(
                                child: CircularProgressIndicator(
                                  value: loadingProgress.expectedTotalBytes !=
                                          null
                                      ? loadingProgress.cumulativeBytesLoaded /
                                          loadingProgress.expectedTotalBytes!
                                              .toInt()
                                      : null,
                                ),
                              );
                            },
                          ),
                        )
                      : Container(
                          child: Text(
                            "No Documents are uploaded...",
                            style: TextStyle(fontSize: 18),
                          ),
                        ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: TextButton(
                        style: TextButton.styleFrom(
                            textStyle: TextStyle(
                          fontSize: 16,
                        )),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text("Close")),
                  )
                ],
              ),
            ),
          );
        });
  }

  String? validateMobile(String? value) {
// Indian Mobile number are of 10 digit only
    if (value!.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }

  void saveCustomer() {
    if (_formKey.currentState!.validate()) {
      print("enter");
      _formKey.currentState!.save();

      updateCustomer().then((value) {
        // _formKey.currentState!.reset();
        Navigator.of(context).pop();
        profileImg = null;
        docImg = null;
        this.getJsonData();
        Constants.showDialogBox(
            context, "Saved to Customer List Successfully", "");
      });
    } else {
      // setState(() {
      //   _autoValidate = true;
      // });
      Navigator.of(context).pop();
      Constants.showDialogBox(context, "Error!", "Please fill all the fields");
    }
  }

  // Widget _getActionButtons() {
  //   return Padding(
  //     padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 45.0),
  //     child: new Row(
  //       mainAxisSize: MainAxisSize.max,
  //       mainAxisAlignment: MainAxisAlignment.start,
  //       children: <Widget>[
  //         Expanded(
  //           child: Padding(
  //             padding: EdgeInsets.only(right: 10.0),
  //             child: Container(
  //                 child: new RaisedButton(
  //               child: new Text("Save"),
  //               textColor: Colors.white,
  //               color: Colors.green,
  //               onPressed: () {
  //                 _validateInputs();
  //                 setState(() {
  //                   status = true;
  //                   FocusScope.of(context).requestFocus(new FocusNode());
  //                 });
  //               },
  //               shape: new RoundedRectangleBorder(
  //                   borderRadius: new BorderRadius.circular(20.0)),
  //             )),
  //           ),
  //           flex: 2,
  //         ),
  //         Expanded(
  //           child: Padding(
  //             padding: EdgeInsets.only(left: 10.0),
  //             child: Container(
  //                 child: new RaisedButton(
  //               child: new Text("Cancel"),
  //               textColor: Colors.white,
  //               color: Colors.red,
  //               onPressed: () {
  //                 setState(() {
  //                   status = true;
  //                   FocusScope.of(context).requestFocus(new FocusNode());
  //                 });
  //               },
  //               shape: new RoundedRectangleBorder(
  //                   borderRadius: new BorderRadius.circular(20.0)),
  //             )),
  //           ),
  //           flex: 2,
  //         ),
  //       ],
  //     ),
  //   );
  // }

  Future<String> cancelCustomer() async {
    String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiReport.php?apicall=cancelCustomer&empid=" +
        empId.toString() +
        "&pType=" +
        profileData![0]["pType"] +
        "&accNo=" +
        widget.accNo.toString() +
        "&sts=1";
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);

    return "success";
  }

  void _showPicker(context, int choose) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        if (choose == 1)
                          getProfileImage(context, ImageSource.gallery);
                        else
                          getDocImage(context, ImageSource.gallery);
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      if (choose == 1)
                        getProfileImage(context, ImageSource.camera);
                      else
                        getDocImage(context, ImageSource.camera);
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future getProfileImage(BuildContext context, ImageSource source) async {
    var image = await picker.pickImage(source: source);
    setState(() {
      profileImg = File(image!.path);
    });
  }

  Future getDocImage(BuildContext context, ImageSource source) async {
    var image = await picker.pickImage(source: source);
    setState(() {
      docImg = File(image!.path);
    });
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  _makingPhoneCall() async {
    const url = 'tel:9823942767';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    status = true;
    _accController = TextEditingController(text: "");
    _dateController = TextEditingController(text: "");
    _endDateController = TextEditingController(text: "");
    custNameController = TextEditingController(text: "");
    contactController = TextEditingController(text: "");
    addressController = TextEditingController(text: "");
    _totalBalanceController = TextEditingController(text: "0");
    durationController = TextEditingController(text: "0");
    interestController = TextEditingController(text: "0");
    totalInterestController = TextEditingController(text: "0");
    totalProfitController = TextEditingController(text: "0");
    _totalGivenController = TextEditingController(text: "0");
    perDaysAmtController = TextEditingController(text: "0");
    getEmpId().then((value) {
      profileImg = null;
      docImg = null;
      Constants.waiting(context);
      getImages();
      profileResponse = this.getJsonData().then((value) {
        Navigator.of(context).pop();
        return value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Profile"),
          backgroundColor: Colors.blue[800],
          actions: [
            Container(
              // padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02,right: MediaQuery.of(context).size.width * 0.02,),
              // width: 75,
              height: 10,
              child: TextButton(
                child: Text("Cancel"),
                style: TextButton.styleFrom(
                    primary: Colors.deepOrangeAccent,
                    textStyle: TextStyle(fontWeight: FontWeight.bold)
                    //side: BorderSide(color: Colors.red, width: 1)
                    ),
                onPressed: () async {
                  var msg = await Constants.confirmationMsg(
                      context,
                      "Confirmation",
                      "Do you really want to Cancel?\n\n(All Transaction made for this Customer will also CANCELLED.)");
                  if (msg) {
                    Constants.waiting(context);
                    cancelCustomer().then((value) {
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    });
                  }
                },
              ),
            )
          ],
        ),
        body: profileData == null
            ? Container(
                child: Center(
                child: Text("No Profile Data Available..."),
              ))
            : Form(
                key: _formKey,
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: SingleChildScrollView(
                        child: Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.03),
                              child: Center(
                                child: GestureDetector(
                                  onTap: () {
                                    _showPicker(context, 1);
                                  },
                                  child: CircleAvatar(
                                    radius: 50,
                                    backgroundColor: Color(0xffFDCF09),
                                    child: profileImg != null ||
                                            (profileData == null
                                                ? false
                                                : (profileData![0]
                                                            ["pf_img_path"] !=
                                                        "" &&
                                                    profileData![0]
                                                            ["pf_img_path"] !=
                                                        "0"))
                                        ? ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(100),
                                            child: profileImg != null
                                                ? Image.file(
                                                    profileImg!.absolute,
                                                    width: 300,
                                                    height: 300,
                                                    fit: BoxFit.fitHeight,
                                                  )
                                                : Image.network(
                                                    Constants.url +
                                                        profileData![0]
                                                            ["pf_img_path"],
                                                    width: 300,
                                                    height: 300,
                                                    fit: BoxFit.fitHeight,
                                                    loadingBuilder: (BuildContext
                                                            context,
                                                        Widget child,
                                                        ImageChunkEvent?
                                                            loadingProgress) {
                                                      if (loadingProgress ==
                                                          null) return child;
                                                      return Center(
                                                        child:
                                                            CircularProgressIndicator(
                                                          value: loadingProgress
                                                                      .expectedTotalBytes !=
                                                                  null
                                                              ? loadingProgress
                                                                      .cumulativeBytesLoaded /
                                                                  loadingProgress
                                                                      .expectedTotalBytes!
                                                                      .toInt()
                                                              : null,
                                                        ),
                                                      );
                                                    },
                                                  ),
                                          )
                                        : Container(
                                            decoration: BoxDecoration(
                                                color: Colors.grey[200],
                                                borderRadius:
                                                    BorderRadius.circular(100)),
                                            width: 300,
                                            height: 300,
                                            child: Icon(
                                              Icons.camera_alt,
                                              color: Colors.grey[800],
                                            ),
                                          ),
                                  ),
                                ),
                              ),
                            ),
                            Container(
                              alignment: Alignment.center,
                              child: TextButton(
                                  onPressed: () {
                                    // _showPicker(context, 2);
                                    showProfileImage(context);
                                  },
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.photo_library,
                                        color: Colors.blue[800],
                                      ),
                                      Text(" View",
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: Colors.blue[800])),
                                    ],
                                  )),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  //top: MediaQuery.of(context).size.height * 0.02,
                                  left:
                                      MediaQuery.of(context).size.width * 0.04,
                                  right:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.92,
                                    child: TextFormField(
                                      autofocus: false,
                                      textAlignVertical:
                                          TextAlignVertical.bottom,
                                      controller: _accController,
                                      readOnly: true,
                                      enabled: false,
                                      decoration: InputDecoration(
                                        labelText: "Account No.",
                                        contentPadding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.015),
                                      ),
                                      keyboardType: TextInputType.text,
                                      style: new TextStyle(
                                        fontFamily: "Poppins",
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  //top: MediaQuery.of(context).size.height * 0.02,
                                  left:
                                      MediaQuery.of(context).size.width * 0.04,
                                  right:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.92,
                                    child: TextFormField(
                                      textAlignVertical:
                                          TextAlignVertical.bottom,
                                      autofocus: false,
                                      controller: custNameController,
                                      onSaved: (String? custName) {
                                        setState(() {
                                          this.custName = custName;
                                        });
                                      },
                                      validator: (value) {
                                        if (value == "") {
                                          return "Please Enter name";
                                        } else {
                                          return null;
                                        }
                                      },
                                      decoration: InputDecoration(
                                          labelText: "Customer Name",
                                          contentPadding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.015),
                                          suffixIcon: Container(
                                            padding: EdgeInsets.only(
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.03,
                                                left: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.07),
                                            child: Icon(
                                                Icons.perm_contact_calendar),
                                          )),
                                      keyboardType: TextInputType.text,
                                      style: new TextStyle(
                                        fontFamily: "Poppins",
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),

                            Container(
                              padding: EdgeInsets.only(
                                  //top: MediaQuery.of(context).size.height * 0.02,
                                  left:
                                      MediaQuery.of(context).size.width * 0.04,
                                  right:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.92,
                                    child: TextFormField(
                                      autofocus: false,
                                      inputFormatters: [
                                        LengthLimitingTextInputFormatter(10),
                                        new WhitelistingTextInputFormatter(
                                            RegExp("[0-9]"))
                                      ],
                                      controller: contactController,
                                      onSaved: (String? contact) {
                                        setState(() {
                                          this.contactNo = contact;
                                        });
                                      },
                                      decoration: InputDecoration(
                                          labelText: "Contact No",
                                          contentPadding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.015),
                                          suffixIcon: Container(
                                            padding: EdgeInsets.only(
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.03,
                                                left: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    0.07),
                                            child: Container(
                                              height: 30,
                                              child: IconButton(
                                                  onPressed: () {
                                                    _makingPhoneCall();
                                                  },
                                                  icon: Icon(
                                                    Icons.call,
                                                    size: 20,
                                                  )),
                                            ),
                                          )),
                                      validator: validateMobile,
                                      keyboardType: TextInputType.phone,
                                      style: new TextStyle(
                                        fontFamily: "Poppins",
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                            //           Container(
                            //             child: AutoCompleteTextField(
                            //               itemSubmitted: (item){

                            //                   print("Submit" + item.cust_name.toString());
                            //                   _customerController.text = item.cust_name.toString();

                            //               },
                            //               clearOnSubmit: false,
                            //               key: key,
                            //               suggestions: CustomerViewModel.customer,
                            //               controller: _customerController,
                            //               itemBuilder: (context, item){
                            //                print( item.cust_name.toString());
                            //                 return Container(
                            //                   child: Row(
                            //                     children: [
                            //                       Container(
                            //                         child: Text(
                            //                           item.cust_name.toString(),

                            //                         ),
                            //                       )
                            //                     ],
                            //                   ),
                            //                 );
                            //               },
                            //               itemSorter: (a, b) =>
                            //   a.cust_name.compareTo(b.cust_name),
                            //               itemFilter: (suggestion, input) {

                            //  return  suggestion.cust_name.toLowerCase().startsWith(input.toLowerCase());
                            //               }
                            //   ),
                            //           ),
                            // Container(
                            //   padding: EdgeInsets.only(
                            //       //top: MediaQuery.of(context).size.height * 0.02,
                            //       left: MediaQuery.of(context).size.width * 0.04,
                            //       right: MediaQuery.of(context).size.width * 0.04),
                            //   child: Row(
                            //     children: <Widget>[
                            //       Container(
                            //         width: MediaQuery.of(context).size.width * 0.92,
                            //         child: TextFormField(
                            //           autofocus: false,
                            //            initialValue:email ,
                            //           //inputFormatters: [new BlacklistingTextInputFormatter(RegExp(r"/^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/")),],
                            //           onSaved: (value) {
                            //             setState(() {
                            //               this.email = value;
                            //             });
                            //           },

                            //           decoration: InputDecoration(
                            //             labelText: "Email",
                            //             contentPadding: EdgeInsets.only(
                            //                 top: MediaQuery.of(context).size.height *
                            //                     0.015),
                            //           ),

                            //           keyboardType: TextInputType.emailAddress,
                            //           style: new TextStyle(
                            //             fontFamily: "Poppins",
                            //           ),
                            //         ),
                            //       )
                            //     ],
                            //   ),
                            // ),

                            Container(
                              padding: EdgeInsets.only(
                                  //top: MediaQuery.of(context).size.height * 0.02,
                                  left:
                                      MediaQuery.of(context).size.width * 0.04,
                                  right:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.92,
                                    child: TextFormField(
                                      autofocus: false,
                                      maxLines: 2,
                                      controller: addressController,
                                      onSaved: (String? address) {
                                        setState(() {
                                          this.address = address;
                                        });
                                      },
                                      decoration: InputDecoration(
                                        labelText: "Address",
                                        contentPadding: EdgeInsets.only(
                                            top: MediaQuery.of(context)
                                                    .size
                                                    .height *
                                                0.015),
                                      ),
                                      keyboardType: TextInputType.text,
                                      style: new TextStyle(
                                        fontFamily: "Poppins",
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),

                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.04,
                                  right:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    width: MediaQuery.of(context).size.width *
                                        0.25,
                                    child: TextFormField(
                                      autofocus: false,
                                      controller: _totalBalanceController,
                                      onChanged: (value) {
                                        setState(() {
                                          totalBalance = value;
                                        });
                                      },
                                      decoration: InputDecoration(
                                        labelText: "Total Amt",
                                        contentPadding: EdgeInsets.only(
                                            top: 10,
                                            left: 10,
                                            bottom: 0,
                                            right: 0),
                                        border: new OutlineInputBorder(
                                          borderSide: new BorderSide(),
                                        ),
                                      ),
                                      keyboardType:
                                          TextInputType.numberWithOptions(
                                              decimal: true, signed: false),
                                      style: new TextStyle(
                                          fontFamily: "Poppins",
                                          color: Colors.blue[800],
                                          fontSize: 18,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.3,
                                    padding: EdgeInsets.only(
                                        left:
                                            MediaQuery.of(context).size.width *
                                                0.04),
                                    child: TextFormField(
                                      autofocus: false,
                                      controller: durationController,
                                      onChanged: (value) {
                                        setState(() {
                                          // var tpdate = DateTime.parse(_dateController!.text);
                                          print(durationController!.text);
                                          var splittedDate =
                                              _dateController!.text.split("-");
                                          var rearrangedDate = splittedDate[2] +
                                              "-" +
                                              splittedDate[1] +
                                              "-" +
                                              splittedDate[0];
                                          print(rearrangedDate);
                                          DateTime date =
                                              DateTime.parse(rearrangedDate);
                                          // var newDate  = DateTime.pa
                                          print(date.day);
                                          if (profileData![0]["pType"] == '1') {
                                            newDate = date.add(Duration(
                                                days: int.parse(value) + 1));
                                          } else if (profileData![0]["pType"] ==
                                              '2') {
                                            newDate = date.add(Duration(
                                                days: ((int.parse(value)) * 7) +
                                                    1));
                                          } else {
                                            newDate = new DateTime(
                                                date.year,
                                                date.month + int.parse(value),
                                                date.day + 1);
                                          }
                                          _endDateController!.text =
                                              DateFormat("dd-MM-yyyy")
                                                  .format(newDate!.toUtc());
                                          if (profileData![0]["pType"] == '1') {
                                            _totalGivenController!
                                                .text = (double.parse(
                                                        _totalBalanceController!
                                                            .text) -
                                                    ((((double.parse(interestController!
                                                                        .text) /
                                                                    100) *
                                                                double.parse(
                                                                    _totalBalanceController!
                                                                        .text)) /
                                                            30) *
                                                        double.parse(
                                                            durationController!
                                                                        .text ==
                                                                    '0'
                                                                ? "1"
                                                                : durationController!
                                                                    .text)))
                                                .toStringAsFixed(2);

                                            totalProfitController!
                                                .text = ((((double.parse(
                                                                    interestController!
                                                                        .text) /
                                                                100) *
                                                            double.parse(
                                                                _totalBalanceController!
                                                                    .text)) /
                                                        30) *
                                                    double.parse(
                                                        durationController!
                                                                    .text ==
                                                                '0'
                                                            ? "1"
                                                            : durationController!
                                                                .text))
                                                .toStringAsFixed(2);
                                            perDaysAmtController!
                                                .text = (double.parse(
                                                        _totalBalanceController!
                                                            .text) /
                                                    double.parse(
                                                        durationController!
                                                            .text))
                                                .toStringAsFixed(2);
                                          }

                                          if (profileData![0]["pType"] == '2') {
                                            _totalGivenController!
                                                .text = (double.parse(
                                                        _totalBalanceController!
                                                            .text) -
                                                    ((((double.parse("20") /
                                                                100) *
                                                            double.parse(
                                                                _totalBalanceController!
                                                                    .text)) *
                                                        double.parse(
                                                            durationController!
                                                                        .text ==
                                                                    '0'
                                                                ? "1"
                                                                : durationController!
                                                                    .text) /
                                                        10)))
                                                .toStringAsFixed(2);

                                            totalProfitController!
                                                .text = ((((double.parse("20") /
                                                            100) *
                                                        double.parse(
                                                            _totalBalanceController!
                                                                .text))) *
                                                    double.parse(
                                                        durationController!
                                                                    .text ==
                                                                '0'
                                                            ? "1"
                                                            : durationController!
                                                                .text) /
                                                    10)
                                                .toStringAsFixed(2);
                                            perDaysAmtController!
                                                .text = (double.parse(
                                                        _totalBalanceController!
                                                            .text) /
                                                    double.parse(
                                                        durationController!
                                                            .text))
                                                .toStringAsFixed(2);
                                          }
                                          if (profileData![0]["pType"] == '3' &&
                                              value != "") {
                                            totalInterestController!
                                                .text = (((double.parse(
                                                            interestController!
                                                                .text) /
                                                        100) *
                                                    double.parse(
                                                        _totalBalanceController!
                                                            .text)))
                                                .toStringAsFixed(2);
                                            totalInterestController!
                                                .text = (double.parse(value) *
                                                    double.parse(
                                                        totalInterestController!
                                                            .text))
                                                .toStringAsFixed(2);
                                          } else if (radio != 2 ||
                                              value == "") {
                                            totalInterestController!
                                                .text = (((double.parse(value) /
                                                        100) *
                                                    double.parse(
                                                        _totalBalanceController!
                                                            .text)))
                                                .toStringAsFixed(2);
                                          }
                                        });
                                      },
                                      onSaved: (String? value) {
                                        setState(() {
                                          this.duration = value;
                                        });
                                      },
                                      decoration: InputDecoration(
                                        labelText: "Duration (" +
                                            (profileData![0]["pType"] == '1'
                                                ? "Days"
                                                : (profileData![0]["pType"] ==
                                                        '2'
                                                    ? "Weeks"
                                                    : "Months")) +
                                            ")",
                                        contentPadding: EdgeInsets.only(
                                            top: 10,
                                            left: 10,
                                            bottom: 0,
                                            right: 0),
                                        border: new OutlineInputBorder(
                                          borderSide: new BorderSide(),
                                        ),
                                      ),
                                      keyboardType:
                                          TextInputType.numberWithOptions(
                                              decimal: true, signed: false),
                                      style: new TextStyle(
                                        fontFamily: "Poppins",
                                        fontSize: 18,
                                      ),
                                    ),
                                  ),
                                  profileData![0]["pType"] != '2'
                                      ? Container(
                                          padding: EdgeInsets.only(
                                            //top: MediaQuery.of(context).size.height * 0.02,
                                            left: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.05,
                                          ),
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.3,
                                          child: TextFormField(
                                            autofocus: false,
                                            controller: interestController,
                                            onChanged: (value) {
                                              print(durationController!.text);
                                              setState(() {
                                                if (profileData![0]["pType"] ==
                                                    '1') {
                                                  _totalGivenController!
                                                      .text = (double.parse(
                                                              _totalBalanceController!
                                                                  .text) -
                                                          ((((double.parse(value) /
                                                                          100) *
                                                                      double.parse(
                                                                          _totalBalanceController!
                                                                              .text)) /
                                                                  30) *
                                                              double.parse(durationController!
                                                                          .text ==
                                                                      '0'
                                                                  ? "1"
                                                                  : durationController!
                                                                      .text)))
                                                      .toStringAsFixed(2);

                                                  totalProfitController!
                                                      .text = ((((double.parse(
                                                                          value) /
                                                                      100) *
                                                                  double.parse(
                                                                      _totalBalanceController!
                                                                          .text)) /
                                                              30) *
                                                          double.parse(
                                                              durationController!
                                                                          .text ==
                                                                      '0'
                                                                  ? "1"
                                                                  : durationController!
                                                                      .text))
                                                      .toStringAsFixed(2);
                                                  perDaysAmtController!
                                                      .text = (double.parse(
                                                              _totalBalanceController!
                                                                  .text) /
                                                          double.parse(
                                                              durationController!
                                                                  .text))
                                                      .toStringAsFixed(2);
                                                }

                                                if (profileData![0]["pType"] ==
                                                        '3' &&
                                                    value != '') {
                                                  perDaysAmtController!
                                                      .text = (double.parse(
                                                              _totalBalanceController!
                                                                  .text) /
                                                          double.parse(
                                                              durationController!
                                                                  .text))
                                                      .toStringAsFixed(2);
                                                  totalInterestController!
                                                      .text = (((double.parse(
                                                                  value) /
                                                              100) *
                                                          double.parse(
                                                              _totalBalanceController!
                                                                  .text)))
                                                      .toStringAsFixed(2);
                                                } else {
                                                  totalInterestController!
                                                      .text = "0";
                                                }
                                              });
                                            },
                                            onSaved: (String? value) {
                                              setState(() {
                                                this.interest = value;
                                              });
                                            },
                                            decoration: InputDecoration(
                                                labelText: "Int. (in %)",
                                                contentPadding: EdgeInsets.only(
                                                    top: 10,
                                                    left: 10,
                                                    bottom: 0,
                                                    right: 0),
                                                border: new OutlineInputBorder(
                                                  borderSide: new BorderSide(),
                                                ),
                                                prefixStyle: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 18)),
                                            keyboardType:
                                                TextInputType.numberWithOptions(
                                                    decimal: true,
                                                    signed: false),
                                            style: new TextStyle(
                                                fontFamily: "Poppins",
                                                fontSize: 18),
                                          ),
                                        )
                                      : Container(
                                          padding: EdgeInsets.only(
                                            //top: MediaQuery.of(context).size.height * 0.02,
                                            left: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.05,
                                          ),
                                          child: Text(
                                            " Interest 20% ",
                                            style: new TextStyle(
                                                fontFamily: "Poppins",
                                                fontSize: 16),
                                          ),
                                        ),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  //top: MediaQuery.of(context).size.height * 0.02,
                                  left:
                                      MediaQuery.of(context).size.width * 0.04,
                                  right:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Row(
                                children: <Widget>[
                                  if (profileData![0]["pType"] != '3')
                                    Container(
                                      width: MediaQuery.of(context).size.width *
                                          0.3,
                                      child: TextFormField(
                                        autofocus: false,
                                        readOnly: true,
                                        controller: _totalGivenController,
                                        onChanged: (value) {
                                          setState(() {
                                            totalBalance = value;
                                          });
                                        },
                                        decoration: InputDecoration(
                                          labelText: "Total Given Amt",
                                          contentPadding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.015),
                                        ),
                                        keyboardType:
                                            TextInputType.numberWithOptions(
                                                decimal: true, signed: false),
                                        style: new TextStyle(
                                            fontFamily: "Poppins",
                                            color: Colors.red,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  if (profileData![0]["pType"] != '3')
                                    Container(
                                      padding: EdgeInsets.only(
                                        //top: MediaQuery.of(context).size.height * 0.02,
                                        left:
                                            MediaQuery.of(context).size.width *
                                                0.05,
                                      ),
                                      width: MediaQuery.of(context).size.width *
                                          0.3,
                                      child: TextFormField(
                                        autofocus: false,
                                        controller: totalProfitController,
                                        readOnly: true,
                                        onChanged: (value) {
                                          print(durationController!.text);
                                          setState(() {});
                                        },
                                        decoration: InputDecoration(
                                            labelText: "Total Profit",
                                            contentPadding: EdgeInsets.only(
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.015),
                                            prefixStyle: TextStyle(
                                                color: Colors.black,
                                                fontSize: 18)),
                                        keyboardType:
                                            TextInputType.numberWithOptions(
                                                decimal: true, signed: false),
                                        style: new TextStyle(
                                            fontFamily: "Poppins",
                                            color: Colors.green,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  Container(
                                    padding: EdgeInsets.only(
                                      //top: MediaQuery.of(context).size.height * 0.02,
                                      left: profileData![0]["pType"] != '3'
                                          ? MediaQuery.of(context).size.width *
                                              0.05
                                          : 0,
                                    ),
                                    width:
                                        MediaQuery.of(context).size.width * 0.3,
                                    child: TextFormField(
                                      autofocus: false,
                                      controller: perDaysAmtController,
                                      readOnly: true,
                                      onChanged: (value) {
                                        print(durationController!.text);
                                        setState(() {});
                                      },
                                      decoration: InputDecoration(
                                          labelText: "Amt (per " +
                                              (profileData![0]["pType"] == '1'
                                                  ? "Day"
                                                  : (profileData![0]["pType"] ==
                                                          '2'
                                                      ? "Week"
                                                      : "Month")) +
                                              ")",
                                          contentPadding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.015),
                                          prefixStyle: TextStyle(
                                              color: Colors.black,
                                              fontSize: 18)),
                                      keyboardType:
                                          TextInputType.numberWithOptions(
                                              decimal: true, signed: false),
                                      style: new TextStyle(
                                          fontFamily: "Poppins",
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ),
                                  if (profileData![0]["pType"] == '3')
                                    Container(
                                      padding: EdgeInsets.only(
                                        //top: MediaQuery.of(context).size.height * 0.02,
                                        left:
                                            MediaQuery.of(context).size.width *
                                                0.1,
                                      ),
                                      width: MediaQuery.of(context).size.width *
                                          0.46,
                                      child: TextFormField(
                                        autofocus: false,
                                        controller: totalInterestController,
                                        readOnly: true,
                                        enabled: false,
                                        onChanged: (value) {},
                                        decoration: InputDecoration(
                                          labelText: "Tot Int.(per Month)",
                                          contentPadding: EdgeInsets.only(
                                              top: MediaQuery.of(context)
                                                      .size
                                                      .height *
                                                  0.015),
                                        ),
                                        keyboardType:
                                            TextInputType.numberWithOptions(
                                                decimal: true, signed: false),
                                        style: new TextStyle(
                                            fontFamily: "Poppins",
                                            color: Colors.blue[800],
                                            fontWeight: FontWeight.w500),
                                      ),
                                    )
                                ],
                              ),
                            ),

                            // Container(
                            //   padding: EdgeInsets.only(
                            //       //top: MediaQuery.of(context).size.height * 0.02,
                            //       left: MediaQuery.of(context).size.width * 0.04,
                            //       right: MediaQuery.of(context).size.width * 0.04),
                            //   child: Row(
                            //     children: <Widget>[
                            //       Container(
                            //         width: MediaQuery.of(context).size.width * 0.92,
                            //         child: TextFormField(
                            //           autofocus: false,
                            //           validator: (value) {
                            //             if (value == "") {
                            //               return "Please Enter Amount";
                            //             } else {
                            //               return null;
                            //             }
                            //           },
                            //           onSaved: (String? value) {
                            //             setState(() {
                            //               this.amountDWM = value;
                            //             });
                            //           },
                            //           decoration: InputDecoration(
                            //             labelText: "Amount Collect " +
                            //                 (radio == 0
                            //                     ? "Daily"
                            //                     : (radio == 1 ? "Weekly" : "Monthly")),
                            //             contentPadding: EdgeInsets.only(
                            //                 top: MediaQuery.of(context).size.height *
                            //                     0.015),
                            //           ),
                            //           keyboardType: TextInputType.numberWithOptions(
                            //               decimal: true, signed: false),
                            //           style: new TextStyle(
                            //             fontFamily: "Poppins",
                            //           ),
                            //         ),
                            //       )
                            //     ],
                            //   ),
                            // ),
                            Row(
                              children: [
                                Container(
                                    padding: EdgeInsets.only(
                                        left:
                                            MediaQuery.of(context).size.width *
                                                0.01),
                                    width: MediaQuery.of(context).size.width *
                                        0.46,
                                    child: DateTimeField(
                                      format: DateFormat('dd-MM-yyyy'),
                                      controller: _dateController,
                                      initialValue: DateTime.now(),
                                      resetIcon: null,
                                      decoration: InputDecoration(
                                          prefixIcon:
                                              Icon(Icons.calendar_today),
                                          labelText: "Opening Date"),
                                      onShowPicker: (context, currentValue) {
                                        return showDatePicker(
                                            context: context,
                                            initialDate: _dateTime == null
                                                ? DateTime.now()
                                                : _dateTime!.toUtc(),
                                            firstDate: DateTime(2001),
                                            lastDate: DateTime(2100));
                                      },
                                    )),
                                Container(
                                    padding: EdgeInsets.only(
                                        left:
                                            MediaQuery.of(context).size.width *
                                                0.04),
                                    width: MediaQuery.of(context).size.width *
                                        0.46,
                                    child: DateTimeField(
                                      format: DateFormat('dd-MM-yyyy'),
                                      controller: _endDateController,
                                      initialValue: newDate,
                                      enabled: false,
                                      readOnly: true,
                                      resetIcon: null,
                                      decoration: InputDecoration(
                                          prefixIcon:
                                              Icon(Icons.calendar_today),
                                          labelText: "End Date"),
                                      onShowPicker: (context, currentValue) {
                                        return showDatePicker(
                                            context: context,
                                            initialDate: newDate!.toUtc(),
                                            firstDate: DateTime(2001),
                                            lastDate: DateTime(2100));
                                      },
                                    )),
                              ],
                            ),

                            Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.04,
                                    top: MediaQuery.of(context).size.height *
                                        0.01),
                                child: Row(
                                  children: [
                                    Text(
                                      "Upload Document : ",
                                      style: TextStyle(fontSize: 18),
                                    ),
                                    docImg != null
                                        ? Flexible(
                                            child: Text(
                                                docImg!.path.split("/").last))
                                        : TextButton(
                                            onPressed: () {
                                              // _showPicker(context, 2);
                                              showDocumentImage(context);
                                            },
                                            child: Row(
                                              children: [
                                                Icon(
                                                  Icons.photo_library,
                                                  color: Colors.blue[800],
                                                ),
                                                Text(" View",
                                                    style: TextStyle(
                                                        fontSize: 18,
                                                        color:
                                                            Colors.blue[800])),
                                              ],
                                            )),
                                    Spacer(),
                                    Container(
                                      child: IconButton(
                                          onPressed: () {
                                            // _showPicker(context, 2);
                                            final snackBar = SnackBar(
                                              content: Text(
                                                  'We will give you this option Soon...'),
                                            );
                                            ScaffoldMessenger.of(context)
                                                .showSnackBar(snackBar);
                                          },
                                          icon: Icon(
                                            Icons.edit,
                                            color: Colors.blue[800],
                                          )),
                                    )
                                  ],
                                )),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: 50,
                      width: MediaQuery.of(context).size.width * 1,
                      color: Colors.blue[800],
                      child: FlatButton(
                          onPressed: () {
                            Constants.waiting(context);
                            saveCustomer();
                          },
                          child: Text(
                            "Save",
                            style: TextStyle(color: Colors.white),
                          )),
                    )
                  ],
                ),
              ));
  }
}
