import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pigmi/customer/CreateNewCustomerPage.dart';

class CustomerRegistration extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CustomerRegistration();
  }
  
}

class _CustomerRegistration extends State<CustomerRegistration> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Customer Registration"),
        backgroundColor: Colors.blue[800],
      ),
      body: CreateNewCustomerPage(dWM: 0),
    );
  }
  
}