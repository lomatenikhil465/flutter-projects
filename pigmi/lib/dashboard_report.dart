import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pigmi/utils/const.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class DashboardReport extends StatefulWidget {
  final int? takeGive;
  final String? fromDt, toDt, custId;
  DashboardReport({this.takeGive, this.fromDt, this.toDt, this.custId});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DashboardReport();
  }
}

class _DashboardReport extends State<DashboardReport> {
  SharedPreferences? prefs;
  List? takeGiveData;
  double daily = 0, weekly = 0, monthly = 0;
  Future<String>? pendingResponse;
  static int count = 0;
  double total = 0;

  Future<String> getJsonData() async {
    String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=" + (widget.takeGive == 1 ? "totTakenList": "totGivenList")+
        "&userId=" +
        empId.toString() +
        "&fdt=" +
        widget.fromDt.toString() +
        "&tdt=" +
        widget.toDt.toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print( "monthly " + response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      takeGiveData = convertDataToJson['user'];
     for (var i = 0; i < takeGiveData!.length; i++) {
       //print(takeGiveData![i]["accType"]);
       if(takeGiveData![i]["accType"] == 'Daily'){
         print("Enter Daily " + i.toString());
         daily += double.parse(takeGiveData![i][ widget.takeGive == 1 ? widget.takeGive == 1 ? "amt" : "credit_amt" : "credit_amt"]);
       }

       if(takeGiveData![i]["accType"] == 'Weekly'){
          print("Enter Weekly " + i.toString());
         weekly += double.parse(takeGiveData![i][widget.takeGive == 1 ? "amt" : "credit_amt"]);
       }

       if(takeGiveData![i]["accType"] == 'Monthly'){
         print("Enter Monthly " + i.toString());
         monthly += double.parse(takeGiveData![i][widget.takeGive == 1 ? "amt" : "credit_amt"])+ double.parse(widget.takeGive == 1 ? takeGiveData![i]["int_Amt"] : "0");
       }
     }
    });

    return "success";
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  Widget accType(var title, var value){
    return Container(
      padding: EdgeInsets.only(top: 5,left: 20,right: 20,),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text(title, style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold
                          ),),
                        ),
                        Container(
                          child: Text("₹ " + value, style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Colors.green
                          )),
                        )
                      ],
                    ),
                  );
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEmpId().then((value) {
      pendingResponse = this.getJsonData();
    });
  }

  @override
  Widget build(BuildContext context) {
    count = 0;
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.takeGive == 1 ?"Total Taken" : "Total Given"),
        backgroundColor: Colors.blue[800],
      ),
      body: (takeGiveData == null) ?
      Container(
        child: Center(
          child: Text("No Pending Data..."),
        ),
      ):
      FutureBuilder<String>(
        future: pendingResponse,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return takeGiveData!.length == 0?
            Container(
              child: Center(
                child: Text("No Pending Data..."),
              ),
            )
            :Container(
              child: Column(
                
                children: [
                  accType("Daily : ", daily.toStringAsFixed(2)),
                  accType("Weekly : ", weekly.toStringAsFixed(2)),
                  accType("Monthly : ", monthly.toStringAsFixed(2)),
                 // accType("Total : ", (daily + weekly + monthly).toStringAsFixed(2)),
                  Expanded(
                   
                    child: SingleChildScrollView(
                      
                      child: DataTable(
                        columnSpacing: 10,
                          columns: <DataColumn>[
                            DataColumn(
                              label: Text(
                                'No.',
                                style: TextStyle(fontStyle: FontStyle.italic),
                              ),
                            ),
                             DataColumn(
                              label: Text(
                                 widget.takeGive == 1 ? 'Dt.':'Acc No',
                                style: TextStyle(fontStyle: FontStyle.italic),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                'Name',
                                style: TextStyle(fontStyle: FontStyle.italic),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                'Acc Type',
                                style: TextStyle(fontStyle: FontStyle.italic),
                              ),
                            ),
                            // DataColumn(
                            //   label: Text(
                            //     'Acc No.',
                            //     style: TextStyle(fontStyle: FontStyle.italic),
                            //   ),
                            // ),
                            //  DataColumn(
                            //   label: Text(
                            //     'Cust Name',
                            //     style: TextStyle(fontStyle: FontStyle.italic),
                            //   ),
                            // ),
                             DataColumn(
                              label: Text(
                                'Amt',
                                style: TextStyle(fontStyle: FontStyle.italic),
                              ),
                            ),
                          ],
                          dataTextStyle: TextStyle(
                           color: Colors.black,
                            fontSize: 16,
                            //fontWeight: FontWeight.bold
                          ),
                          rows: takeGiveData!.map((data) {
                            
                            count = count + 1;
                            return DataRow(cells: [
                              DataCell(Text(count.toString())),
                              DataCell(Text( widget.takeGive == 1 ?DateFormat("dd-MM-yyyy").format(DateTime.parse(data["dt"].toString())) : data["accNo"] )),
                              // DataCell(Text(data["acNo"])),
                               DataCell(Text(data["cName"])),
                               DataCell(Text(data["accType"])),
                                DataCell(Text((double.parse(data[widget.takeGive == 1 ? "amt" : "credit_amt"]) + double.parse(widget.takeGive == 1 ? data["int_Amt"] : "0")).toStringAsFixed(2), style: TextStyle(
                            color: Colors.green[700],
                           
                            //fontWeight: FontWeight.bold
                          ),)),
                            ]);
                          }).toList(),
                        ),
                    ),
                  ),
                  Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width,
                    color:Colors.blue[800],
                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text("Total : " + (daily + weekly + monthly).toStringAsFixed(2), style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold
                        ),)
                      ],
                    ),
                  )
                ],
              )
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
