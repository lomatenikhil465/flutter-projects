import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pigmi/ExpensesList.dart';
import 'package:pigmi/cancelledCustomers.dart';
import 'package:pigmi/customer/customer_list.dart';
import 'package:pigmi/dashboard_report.dart';
import 'package:pigmi/ledger/ledger_tab_controller.dart';
import 'package:pigmi/receive_payment.dart';
import 'package:pigmi/utils/const.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'ExpensesPage.dart';
import 'changePassword.dart';
import 'completed_customers.dart';
import 'customer_registration.dart';
import 'login_page.dart';
import 'report/report_tab_controller.dart';

class HomePage extends StatefulWidget {
  final String? username;
  HomePage({required this.username});
  @override
  State<StatefulWidget> createState() {
    return _HomePage();
  }
}

class _HomePage extends State<HomePage> {
  List? todayCollection, todayCollection1, users;
  SharedPreferences? prefs;
  double saleCollection = 0.0,
      purchaseCollection = 0.0,
      receiptCollection = 0.0,
      cashCollection = 0.0,
      outstandingReceivable = 0.0,
      outstandingPayable = 0.0,
      tempOutstandingPayable = 0.0;
  String _currentItemSelected = "Today";
  String today = DateFormat("yyyy-MM-dd").format(DateTime.now());
  String destination = DateFormat("yyyy-MM-dd").format(DateTime.now());
  var _dueInterval = [
    'Today',
    'Yesterday',
    'Last Week',
    'Last Month',
    'Last 3 Months',
    'Last 6 Months',
    'Last Year'
  ];
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();


  Future<String> getUser() async {
    String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=getUser&userId=" +
        empId.toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      users = convertDataToJson['user'];
    });
    return "success";
  }

  Future<String> getJsonData() async {
    var empId = prefs!.getString('empId');
    print(empId);

    String url = Constants.url +
        "app/pigmiReport.php?apicall=pigmiReport&fdt=" +
        today +
        "&tdt=" +
        destination +
        "&empid=" +
        empId.toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      todayCollection = convertDataToJson['user'];
      if (todayCollection != null) {
        print("enter");
        for (var i = 0; i < todayCollection!.length; i++) {
          if (todayCollection![i]["eType"] == "DAILY") {
            saleCollection =
                saleCollection + double.parse(todayCollection![i]["amt"]);
          }

          if (todayCollection![i]["eType"] == "WEEKLY") {
            purchaseCollection =
                purchaseCollection + double.parse(todayCollection![i]["amt"]);
          }

          if (todayCollection![i]["eType"] == "MONTHLY") {
            receiptCollection = receiptCollection +
                double.parse(todayCollection![i]["amt"]) +
                double.parse(todayCollection![i]["int_Amt"]);
          }

          tempOutstandingPayable = tempOutstandingPayable +
              double.parse(todayCollection![i]["credit_amt"]);
        }

        outstandingReceivable =
            saleCollection + receiptCollection + purchaseCollection;
      }

      print(outstandingReceivable);
    });

    return "success";
  }

  Future<String> getTotalGivenAmt() async {
    var empId = prefs!.getString('empId');
    print(empId);

    String url = Constants.url +
        "app/pigmiAll.php?apicall=totGivenAmt&fdt=" +
        today +
        "&tdt=" +
        destination +
        "&userId=" +
        empId.toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      if (convertDataToJson['user'][0]["totgivenAmt"] != null) {
        outstandingPayable =
            double.parse(convertDataToJson['user'][0]["totgivenAmt"]);
      }

      print(outstandingPayable);
    });

    return "success";
  }

  // Future<String> getAllTime() async {
  //   var saleCollection1 = 0.0,
  //       receiptCollection1 = 0.0,
  //       purchaseCollection1 = 0.0,
  //       cashCollection1 = 0.0;
  //   var empId = prefs!.getString('empId');
  //   print("get all time" + empId.toString());

  //   String url =
  //       "https://qot.constructionsmall.com/app/getquerycode.php?apicall=saledayclose2&uid=" +
  //           empId.toString();
  //   print(url);
  //   var response = await http.get(
  //       //Encode the url
  //       Uri.parse(url),
  //       //only accept Json response
  //       headers: {"Accept": "application/json"});

  //   print(response.body);
  //   setState(() {
  //     var convertDataToJson = json.decode(response.body);
  //     todayCollection1 = convertDataToJson['data'];
  //   });

  //   if (todayCollection1 != null) {
  //     for (var i = 0; i < todayCollection1!.length; i++) {
  //       if (todayCollection1![i]["eType"] == "SALE") {
  //         saleCollection1 = saleCollection1 +
  //             double.parse(todayCollection1![i]["grand_total"]);
  //       }

  //       if (todayCollection1![i]["eType"] == "RCPT") {
  //         receiptCollection1 =
  //             receiptCollection1 + double.parse(todayCollection1![i]["cr"]);
  //       }

  //       if (todayCollection1![i]["eType"] == "ON ACCOUNT" &&
  //           todayCollection1![i]["cr"] != "0") {
  //         receiptCollection1 =
  //             receiptCollection1 + double.parse(todayCollection1![i]["cr"]);
  //       } else if (todayCollection1![i]["eType"] == "ON ACCOUNT" &&
  //           todayCollection1![i]["dr"] != "0") {
  //         receiptCollection1 =
  //             receiptCollection1 + double.parse(todayCollection1![i]["dr"]);
  //       }

  //       if (todayCollection1![i]["eType"] == "PURCHASE") {
  //         purchaseCollection1 = purchaseCollection1 +
  //             double.parse(todayCollection1![i]["grand_total"]);
  //       }

  //       if (todayCollection1![i]["eType"] == "VCH") {
  //         cashCollection1 =
  //             cashCollection1 + double.parse(todayCollection1![i]["dr"]);
  //       }
  //     }

  //     outstandingReceivable = saleCollection1 - receiptCollection1;
  //     outstandingPayable = purchaseCollection1 - cashCollection1;
  //   }
  //   return "success";
  // }

  void _onDropDownItemSelected(String newValueSelected) {
    setState(() {
      this._currentItemSelected = newValueSelected;
      if (_currentItemSelected == 'Today') {
        today = DateFormat("yyyy-MM-dd").format(DateTime.now());
        destination = DateFormat("yyyy-MM-dd").format(DateTime.now());
        saleCollection = 0.0;
        purchaseCollection = 0.0;
        receiptCollection = 0.0;
        cashCollection = 0.0;
        outstandingReceivable = 0.0;
        outstandingPayable = 0.0;
      } else if (_currentItemSelected == 'Yesterday') {
        today = DateFormat("yyyy-MM-dd")
            .format(DateTime.now().subtract(Duration(days: 1)));
        destination = DateFormat("yyyy-MM-dd")
            .format(DateTime.now().subtract(Duration(days: 1)));
        saleCollection = 0.0;
        purchaseCollection = 0.0;
        receiptCollection = 0.0;
        cashCollection = 0.0;
        outstandingReceivable = 0.0;
        outstandingPayable = 0.0;
        print(today);
      } else if (_currentItemSelected == 'Last Week') {
        today = DateFormat("yyyy-MM-dd")
            .format(DateTime.now().subtract(Duration(days: 7)));
        destination = DateFormat("yyyy-MM-dd").format(DateTime.now());
        saleCollection = 0.0;
        purchaseCollection = 0.0;
        receiptCollection = 0.0;
        cashCollection = 0.0;
        outstandingReceivable = 0.0;
        outstandingPayable = 0.0;
        print(today);
      } else if (_currentItemSelected == 'Last Month') {
        DateTime now = DateTime.now();
        today = DateFormat("yyyy-MM-dd")
            .format(DateTime(now.year, now.month - 1, now.day));

        print(today);
        destination = DateFormat("yyyy-MM-dd").format(DateTime.now());
        saleCollection = 0.0;
        purchaseCollection = 0.0;
        receiptCollection = 0.0;
        cashCollection = 0.0;
        outstandingReceivable = 0.0;
        outstandingPayable = 0.0;
      } else if (_currentItemSelected == 'Last 3 Months') {
        DateTime now = DateTime.now();
        today = DateFormat("yyyy-MM-dd")
            .format(DateTime(now.year, now.month - 3, now.day));
        destination = DateFormat("yyyy-MM-dd").format(DateTime.now());
        saleCollection = 0.0;
        purchaseCollection = 0.0;
        receiptCollection = 0.0;
        cashCollection = 0.0;
        outstandingReceivable = 0.0;
        outstandingPayable = 0.0;
        print(today);
      } else if (_currentItemSelected == 'Last 6 Months') {
        DateTime now = DateTime.now();
        today = DateFormat("yyyy-MM-dd")
            .format(DateTime(now.year, now.month - 6, now.day));
        destination = DateFormat("yyyy-MM-dd").format(DateTime.now());
        saleCollection = 0.0;
        purchaseCollection = 0.0;
        receiptCollection = 0.0;
        cashCollection = 0.0;
        outstandingReceivable = 0.0;
        outstandingPayable = 0.0;
        print(today);
      } else if (_currentItemSelected == 'Last Year') {
        DateTime now = DateTime.now();
        today = DateFormat("yyyy-MM-dd")
            .format(DateTime(now.year - 1, now.month, now.day));
        destination = DateFormat("yyyy-MM-dd").format(DateTime.now());
        saleCollection = 0.0;
        purchaseCollection = 0.0;
        receiptCollection = 0.0;
        cashCollection = 0.0;
        outstandingReceivable = 0.0;
        outstandingPayable = 0.0;
        print(today);
      }
    });
    tempOutstandingPayable = 0.0;
    this.getTotalGivenAmt();
    this.getJsonData().then((value) {
      onChange();
    });
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  void onChange() {
    setState(() {});
  }

  Widget gridCard(String text, var icon) {
    return Card(
      elevation: 5.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
            child: new Center(
                child: Icon(
              icon,
              size: 30,
              color: Colors.blue[800],
            )),
          ),
          new Container(
            padding:
                EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
            child: new Center(
                child: Text(
              text,
              style: new TextStyle(color: Colors.black, fontSize: 20.0),
            )),
          ),
        ],
      ),
    );
  }

  Widget reportWidget(String firstTitle, String secondTitle) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 0.04,
            width: MediaQuery.of(context).size.width * 0.4,
            child: Container(
              padding:
                  EdgeInsets.all(MediaQuery.of(context).size.height * 0.008),
              child: Center(
                child: Text(
                  firstTitle,
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
              ),
            ),
          ),
          if (secondTitle != "-")
            GestureDetector(
              onTap: () {
                //           Navigator.of(context)
                //               .push(MaterialPageRoute(builder: (context) {
                //             return CollectionLedger(
                //               index: 1,
                //             );
                //           })).then((value) {
                //             saleCollection = 0.0;
                //             purchaseCollection = 0.0;
                //             receiptCollection = 0.0;
                //             cashCollection = 0.0;
                //             outstandingReceivable = 0.0;
                //             outstandingPayable = 0.0;
                //             this.getJsonData().then((value) {
                //   this.getAllTime();
                // });
                //           });
              },
              child: Container(
                height: MediaQuery.of(context).size.height * 0.04,
                width: MediaQuery.of(context).size.width * 0.4,
                child: Container(
                  padding: EdgeInsets.all(
                      MediaQuery.of(context).size.height * 0.008),
                  child: Center(
                    child: Text(
                      secondTitle,
                      style: TextStyle(color: Colors.white, fontSize: 14),
                    ),
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }

  Widget particularReportContainer(var collection, Color? color, String title) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
      height: MediaQuery.of(context).size.height * 0.07,
      width: MediaQuery.of(context).size.width * 0.45,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(5)),
      child: Row(
        children: [
          Container(
            width: 10,
            decoration: BoxDecoration(
                color: color,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(5),
                    bottomLeft: Radius.circular(5))),
            height: MediaQuery.of(context).size.height,
          ),
          SizedBox(
            width: 20,
          ),
          Column(
            children: [
              Container(
                // height: MediaQuery.of(context).size.height * 0.04,
                // width: MediaQuery.of(context).size.width * 0.4,
                child: Container(
                  // padding:
                  //     EdgeInsets.all(MediaQuery.of(context).size.height * 0.008),
                  child: Center(
                    child: Text(
                      title,
                      style: TextStyle(color: color, fontSize: 14),
                    ),
                  ),
                ),
              ),
              Container(
                child: Center(
                    child: RichText(
                  text: TextSpan(
                    text: 'Rs. ',
                    style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                    children: <TextSpan>[
                      TextSpan(
                          text: collection!.toString(),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20,
                              color: Colors.black)),
                    ],
                  ),
                )),
              ),
            ],
          ),
          Spacer(),
          Container(
            child: Text(
              "₹ ",
              style: TextStyle(
                  color: Colors.grey,
                  fontSize: 30,
                  fontWeight: FontWeight.w500),
            ),
          )
        ],
      ),
    );
  }

  Future showReportOptions(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.blue.shade800, width: 2.0),
                borderRadius: BorderRadius.circular(10.0)), //this right here
            child: Container(
              height: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.height * 0.01),
                        child: Text(
                          "Choose Option : ",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          // companyFlag = 1;
                          // categoryUrl = "https://cp.nexenlubes.com/Application/getDealerList.php?apicall=getCategory";
                          // productUrl =
                          //     "https://cp.nexenlubes.com/Application/getDealerList.php?apicall=getProductDetails&index=";
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return ReportTabController(
                              colPen: 1,
                              initialIndex: 0,
                            );
                          }));
                          // final snackBar = SnackBar(
                          //   content: Text('Coming Soon...'),
                          // );
                          // ScaffoldMessenger.of(context).showSnackBar(snackBar);
                        },
                        child: Container(
                            height: 35,
                            width: 200,
                            margin: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.02,
                                left:
                                    MediaQuery.of(context).size.height * 0.01),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                color: Colors.green),
                            child: Center(
                              child: Text(
                                "Collections",
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            )),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          // companyFlag = 0;
                          // categoryUrl = "http://eannolautomotive.com/app/getDealerList.php?apicall=getEannolCategory";
                          // productUrl =
                          //     "http://eannolautomotive.com/app/getDealerList.php?apicall=getProductDetails&index=";
                          // Navigator.of(context)
                          //  Navigator.of(context)
                          //       .push(MaterialPageRoute(builder: (context) {
                          //     return ReportTabController(colPen: 2,);
                          //   }));
                          // final snackBar = SnackBar(
                          //   content: Text('Coming Soon...'),
                          // );
                          // ScaffoldMessenger.of(context).showSnackBar(snackBar);
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return ReportTabController(
                              colPen: 2,
                              initialIndex: 0,
                            );
                          }));
                        },
                        child: Container(
                            height: 35,
                            width: 200,
                            margin: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.02,
                                left:
                                    MediaQuery.of(context).size.height * 0.01),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                color: Colors.deepOrange),
                            child: Center(
                              child: Text(
                                "Pending",
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            )),
                      )
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    getEmpId().then((value) {
      this.getJsonData();
      this.getTotalGivenAmt();
      this.getUser();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
                decoration: BoxDecoration(color: Colors.blue[800]),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            // Navigator.of(context).push(MaterialPageRoute(builder: (context){
                            //   return ProfilePage();
                            // }));
                          },
                          child: Text(
                            "Hello, " +
                                (users == null
                                    ? "Guest"
                                    : users![0]["nm"]),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.normal),
                          ),
                        ),
                      ],
                    ),
                    GestureDetector(
                      onTap: () {
                        // Navigator.of(context).push(MaterialPageRoute(builder: (context){
                        //   return ProfilePage();
                        // }));
                      },
                      child: Container(
                        padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01,
                      ),
                        child: Center(
                          child: Image.asset(
                            "assets\\images\\jf log1.jpg",
                            height: 100,
                          ),
                        ),
                      ),
                    ),
                  ],
                )),
            ListTile(
              leading: Icon(
                Icons.home,
                color: Colors.blue[800],
              ),
              title: Text("Home",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () async {
                Navigator.of(context)
                    .pushReplacement(MaterialPageRoute(builder: (context) {
                  return HomePage(username: widget.username);
                }));
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.groups, color: Colors.blue[800]),
              title: Text("Customers",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () async {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return CustomerList();
                }));
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.monetization_on, color: Colors.blue[800]),
              title: Text("Expenses",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () async {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return ExpensesList();
                }));
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.person_off_sharp, color: Colors.blue[800]),
              title: Text("Cancelled Customers",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () async {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return CancelledCustomers();
                }));
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.check_circle, color: Colors.blue[800]),
              title: Text("Completed Customers",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () async {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return CompletedCustomers();
                }));
              },
            ),
             Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.password_outlined, color: Colors.blue[800]),
              title: Text("Change Password",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () async {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return ChangePassword(inOut: 1,);
                }));
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.power_settings_new, color: Colors.blue[800]),
              title: Text("Logout",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () async {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.remove('user');
                prefs.remove('pass');
                Navigator.of(context)
                    .pushReplacement(MaterialPageRoute(builder: (context) {
                  return Login();
                }));
              },
            )
          ],
        ),
      ),
      appBar: AppBar(
        elevation: 0,
        title: Text("Finance"),
        backgroundColor: Colors.blue[800],
        actions: <Widget>[
          Container(
            child: Center(
                child: Text(
              DateFormat("dd/MM/yyyy").format(DateTime.now()),
              style: TextStyle(fontWeight: FontWeight.w500),
            )),
          ),
        ],
      ),
      body: Column(
        children: <Widget>[
          Container(
            //height: MediaQuery.of(context).size.height * 0.25,
            padding: EdgeInsets.only(
              bottom: MediaQuery.of(context).size.height * 0.01,
            ),
            width: MediaQuery.of(context).size.width,
            color: Colors.blue[800],
            child: Column(
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.03,
                      right: MediaQuery.of(context).size.width * 0.03),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    color: Colors.white,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                        color: Colors.white,
                        child: Text(
                          "Report",
                          style:
                              TextStyle(fontSize: 16, color: Colors.blue[800]),
                        ),
                      ),
                      Container(
                        height: MediaQuery.of(context).size.height * 0.04,
                        width: MediaQuery.of(context).size.width * 0.4,
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,
                              iconEnabledColor: Colors.blue[800],
                              autofocus: true,
                              focusColor: Colors.blue[800],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text(
                                "Choose a Due",
                                style: TextStyle(color: Colors.blue[800]),
                              ),
                              items:
                                  _dueInterval.map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.normal,
                                            color: Colors.blue[800])));
                              }).toList(),
                              onChanged: (String? newValueSelected) =>
                                  _onDropDownItemSelected(
                                      newValueSelected.toString()),
                              value: _currentItemSelected,
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return ReportTabController(
                              colPen: 1,
                              initialIndex: 0,
                              fromDt: destination,
                              toDt: today,
                            );
                          }));
                        },
                        child: particularReportContainer(
                            saleCollection, Colors.red, "Daily"),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return ReportTabController(
                              colPen: 1,
                              initialIndex: 1,
                              fromDt: destination,
                              toDt: today,
                            );
                          }));
                        },
                        child: particularReportContainer(
                            purchaseCollection, Colors.brown, "Weekly"),
                      ),
                    ],
                  ),
                ),
                // GestureDetector(
                //     onTap: () {
                //       //   Navigator.of(context)
                //       //       .push(MaterialPageRoute(builder: (context) {
                //       //     return CollectionLedger(
                //       //       index: 0,
                //       //     );
                //       //   })).then((value) {
                //       //     saleCollection = 0.0;
                //       //     purchaseCollection = 0.0;
                //       //     receiptCollection = 0.0;
                //       //     cashCollection = 0.0;
                //       //     outstandingReceivable = 0.0;
                //       //     outstandingPayable = 0.0;
                //       //     this.getJsonData().then((value) {
                //       //   this.getAllTime();
                //       // });
                //       //   });
                //     },
                //     child: reportWidget("Daily", "Weekly")),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) {
                          return ReportTabController(
                            colPen: 1,
                            initialIndex: 2,
                            fromDt: destination,
                            toDt: today,
                          );
                        }));
                      },
                      child: particularReportContainer(
                          receiptCollection, Colors.purple, "Monthly"),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) {
                          return ReportTabController(
                            colPen: 2,
                            initialIndex: 0,
                            fromDt: destination,
                            toDt: today,
                          );
                        }));
                      },
                      child: particularReportContainer(
                          tempOutstandingPayable - outstandingReceivable,
                          Colors.amber,
                          "Pending Amt"),
                    ),

                    // GestureDetector(
                    //   onTap: () {
                    //     //           Navigator.of(context)
                    //     //               .push(MaterialPageRoute(builder: (context) {
                    //     //             return CollectionLedgerCash();
                    //     //           })).then((value) {
                    //     //             saleCollection = 0.0;
                    //     //             purchaseCollection = 0.0;
                    //     //             receiptCollection = 0.0;
                    //     //             cashCollection = 0.0;
                    //     //             outstandingReceivable = 0.0;
                    //     //             outstandingPayable = 0.0;
                    //     //             this.getJsonData().then((value) {
                    //     //   this.getAllTime();
                    //     // });
                    //     //           });
                    //   },
                    //   child: Container(
                    //     width: MediaQuery.of(context).size.width * 0.4,
                    //     height: MediaQuery.of(context).size.height * 0.04,
                    //     decoration: BoxDecoration(
                    //         border:
                    //             Border.all(color: Colors.white, width: 1),
                    //         borderRadius: BorderRadius.circular(5)),
                    //     child: Container(
                    //       padding: EdgeInsets.all(
                    //           MediaQuery.of(context).size.height * 0.009),
                    //       child: Center(
                    //         child: Text(
                    //           "₹ " + cashCollection.toString(),
                    //           style: TextStyle(
                    //               color: Colors.white, fontSize: 16),
                    //         ),
                    //       ),
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
                // GestureDetector(
                //     onTap: () {}, child: reportWidget("Monthly", "-")),
                Container(
                  margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.02),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return DashboardReport(
                              takeGive: 2,
                              fromDt: destination,
                              toDt: today,
                            );
                          }));
                        },
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.07,
                          width: MediaQuery.of(context).size.width * 0.45,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.white, width: 1),
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.white),
                          child: Column(
                            children: [
                              Container(
                                // height: MediaQuery.of(context).size.height * 0.04,
                                child: Container(
                                  // padding: EdgeInsets.all(
                                  //     MediaQuery.of(context).size.height * 0.008),
                                  child: Center(
                                    child: Text(
                                      "Total Given",
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 14),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                child: Center(
                                  child: Text(
                                    "₹ " +
                                        outstandingPayable.toStringAsFixed(2),
                                    style: TextStyle(
                                        color: Colors.deepOrange,
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return DashboardReport(
                              takeGive: 1,
                              fromDt: destination,
                              toDt: today,
                            );
                          }));
                        },
                        child: Container(
                          width: MediaQuery.of(context).size.width * 0.45,
                          height: MediaQuery.of(context).size.height * 0.07,
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.white, width: 1),
                              borderRadius: BorderRadius.circular(5),
                              color: Colors.white),
                          child: Column(
                            children: [
                              Container(
                                child: Container(
                                  child: Center(
                                    child: Text(
                                      "Total Taken",
                                      style: TextStyle(
                                          color: Colors.grey, fontSize: 14),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                child: Center(
                                  child: Text(
                                    "₹ " +
                                        outstandingReceivable
                                            .toStringAsFixed(2),
                                    style: TextStyle(
                                        color: Colors.green[700],
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

                // GestureDetector(
                //   onTap: () {},
                //   child: Container(
                //     child: Row(
                //       mainAxisAlignment: MainAxisAlignment.spaceAround,
                //       children: <Widget>[
                //         Container(
                //           height: MediaQuery.of(context).size.height * 0.04,
                //           child: Container(
                //             padding: EdgeInsets.all(
                //                 MediaQuery.of(context).size.height * 0.008),
                //             child: Center(
                //               child: Text(
                //                 "Total Given",
                //                 style: TextStyle(
                //                     color: Colors.white, fontSize: 14),
                //               ),
                //             ),
                //           ),
                //         ),
                //         GestureDetector(
                //           onTap: () {},
                //           child: Container(
                //             height: MediaQuery.of(context).size.height * 0.04,
                //             child: Container(
                //               padding: EdgeInsets.all(
                //                   MediaQuery.of(context).size.height * 0.009),
                //               child: Center(
                //                 child: Text(
                //                   "Total Taken",
                //                   style: TextStyle(
                //                       color: Colors.white, fontSize: 14),
                //                 ),
                //               ),
                //             ),
                //           ),
                //         ),
                //       ],
                //     ),
                //   ),
                // ),
              ],
            ),
          ),
          Expanded(
            child: GridView.count(
              crossAxisCount: 2,
              primary: false,
              childAspectRatio: 16 / 9,
              padding: EdgeInsets.all(20.0),
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              children: <Widget>[
                GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return ReceivePayment(dWM: 1);
                      })).then((value) {
                        setState(() {
                          saleCollection = 0.0;
                          purchaseCollection = 0.0;
                          receiptCollection = 0.0;

                          outstandingReceivable = 0.0;
                          outstandingPayable = 0.0;
                          this.getJsonData();
                          this.getTotalGivenAmt();
                        });
                      });
                    },
                    child: gridCard("Daily", Icons.account_balance_wallet)),
                GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return ReceivePayment(dWM: 2);
                      })).then((value) {
                        setState(() {
                          saleCollection = 0.0;
                          purchaseCollection = 0.0;
                          receiptCollection = 0.0;

                          outstandingReceivable = 0.0;
                          outstandingPayable = 0.0;
                          this.getJsonData();
                          this.getTotalGivenAmt();
                        });
                      });
                    },
                    child: gridCard("Weekly", Icons.account_balance_wallet)),
                GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return ReceivePayment(dWM: 3);
                      })).then((value) {
                        setState(() {
                          saleCollection = 0.0;
                          purchaseCollection = 0.0;
                          receiptCollection = 0.0;

                          outstandingReceivable = 0.0;
                          outstandingPayable = 0.0;
                          this.getJsonData();
                          this.getTotalGivenAmt();
                        });
                      });
                    },
                    child: gridCard("Monthly", Icons.account_balance_wallet)),
                GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return CustomerRegistration();
                      })).then((value) {
                        setState(() {
                          saleCollection = 0.0;
                          purchaseCollection = 0.0;
                          receiptCollection = 0.0;

                          outstandingReceivable = 0.0;
                          outstandingPayable = 0.0;
                          this.getJsonData();
                          this.getTotalGivenAmt();
                        });
                      });
                    },
                    child: gridCard("Registration", Icons.assignment)),
                GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return LedgerTabController();
                      })).then((value) {
                        setState(() {
                          saleCollection = 0.0;
                          purchaseCollection = 0.0;
                          receiptCollection = 0.0;

                          outstandingReceivable = 0.0;
                          outstandingPayable = 0.0;
                          this.getJsonData();
                          this.getTotalGivenAmt();
                        });
                      });
                    },
                    child: gridCard("Ledger", Icons.list)),
                GestureDetector(
                    onTap: () {
                      showReportOptions(context);
                    },
                    child: gridCard("Report", Icons.reorder_sharp)),
                GestureDetector(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return Expenses();
                      }));
                    },
                    child: gridCard("Expenses", Icons.note_add)),
                GestureDetector(
                    onTap: () {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: const Text('Coming Soon...!!'),
                      ));
                    },
                    child: gridCard("PL", Icons.menu_book_rounded))
              ],
            ),
          )
        ],
      ),
    );
  }
}
