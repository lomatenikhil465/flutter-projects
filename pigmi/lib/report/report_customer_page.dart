import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:pigmi/customer_details.dart';
import 'package:pigmi/profile_page.dart';
import 'package:pigmi/report/pending_customer_info.dart';
import 'dart:async';
import 'dart:convert';

import 'package:pigmi/utils/const.dart';
import 'package:shared_preferences/shared_preferences.dart';

//import '../CustomerDetails.dart';

class ReportCustomerPage extends StatefulWidget {
  final int? dWM, colPen;
  final String? cat, fromDt, toDt;

  ReportCustomerPage({this.dWM, this.cat, this.colPen, this.fromDt, this.toDt});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ReportCustomerPage();
  }
}

List? nameOfCustomer;
List? _searchResult;
var empId;
double totalPending = 0.0;
class _ReportCustomerPage extends State<ReportCustomerPage> {
  TextEditingController _controller = TextEditingController(),
      _toDateController = TextEditingController(),
      _fromDateController = TextEditingController();
  SharedPreferences? prefs;
  DateTime? _dateTime = DateTime.now();
  Future<String>? customerResponse;
  double total = 0.0;

  Future<String> getJsonData() async {
    var splittedFromDate =_fromDateController.text.split("-");
          var rearrangedFromDate =splittedFromDate[2] +
        "-" +
       splittedFromDate[1] +
        "-" +
       splittedFromDate[0];
       var splittedToDate =_toDateController.text.split("-");
          var rearrangedTDate =splittedToDate[2] +
        "-" +
       splittedToDate[1] +
        "-" +
       splittedToDate[0];
    String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=getReport&userId=" +
        empId.toString() +
        "&pType=" +
        widget.dWM.toString() +
        "&from_dt=" +
       rearrangedFromDate +
        "&to_dt=" +
        rearrangedTDate;
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      nameOfCustomer = convertDataToJson['user'];
      if(nameOfCustomer != null){
        for (var i = 0; i < nameOfCustomer!.length; i++) {
         total += double.parse(nameOfCustomer![i]["tot"]);
        }
      }
    });

    

    return "success";
  }

  Future<String> getPendingData() async {
    String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=pendingList&userId=" +
        empId.toString() +
        "&pType=" +
        widget.dWM.toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      nameOfCustomer = convertDataToJson['user'];
    });

    return "success";
  }

  void onSearchTextChanged(String text) async {
    if (_searchResult != null) {
      _searchResult!.clear();
    }
    if (text.isEmpty) {
      setState(() {});
      //return ;
    }

    nameOfCustomer!.forEach((userDetail) {
      print("search result : " + userDetail["cName"].toString());
      if (userDetail["cName"].toLowerCase().contains(text.toLowerCase()) ||
          userDetail["accNo"].toLowerCase().contains(text.toLowerCase())) {
        _searchResult!.add(userDetail);
      }
    });

    setState(() {});
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
    setState(() {
      empId = prefs!.getString("empId");
    });
  }

  void updateReport(){
    setState(() {
      
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _searchResult = [];
    totalPending = 0.0;
    total = 0.0;
    getEmpId().then((value) {
      if(widget.fromDt == null && widget.toDt == null){
      _toDateController = TextEditingController(
          text: DateFormat('dd-MM-yyyy').format(_dateTime!.toUtc()));
      _fromDateController = TextEditingController(
          text: DateFormat('dd-MM-yyyy').format(_dateTime!.toUtc()));
      } else {
        _toDateController = TextEditingController(
          text: DateFormat('dd-MM-yyyy').format(DateTime.parse(widget.fromDt.toString())));
      _fromDateController = TextEditingController(
          text: DateFormat('dd-MM-yyyy').format(DateTime.parse(widget.toDt.toString())));
      }
      if (widget.colPen == 1) {
        customerResponse = getJsonData();
      } else {
        customerResponse = getPendingData();
      }
    });

    _controller = TextEditingController(text: "");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: Column(
      children: <Widget>[
        Container(
          height: 55,
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            onChanged: (value) {
              onSearchTextChanged(value);
            },
            controller: _controller,
            decoration: InputDecoration(
                labelText: "Search",
                hintText: "Search",
                contentPadding: EdgeInsets.only(top: 10),
                prefixIcon: Icon(Icons.search),
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5.0)))),
          ),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          // height: 100,
          child: Row(
            children: [
              Text(
                " From : ",
                style: TextStyle(fontSize: 16),
              ),
              Container(
                  width: MediaQuery.of(context).size.width * 0.35,
                  child: DateTimeField(
                      format: DateFormat('dd-MM-yyyy'),
                      controller: _fromDateController,
                      initialValue: DateTime.now(),
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.calendar_today),
                          labelText: "From Date"),
                      resetIcon: null,
                      onShowPicker: (context, currentValue) {
                        return showDatePicker(
                            context: context,
                            initialDate: _dateTime == null
                                ? DateTime.now()
                                : _dateTime!.toUtc(),
                            firstDate: DateTime(2001),
                            lastDate: DateTime(2100));
                      },
                      onChanged: (value){
                          setState(() {
                          if(widget.colPen == 1) {
                            total = 0.0;
                            this.getJsonData();
                            
                          }
                        });
                      },
                      validator: (val) {
                        if (val != null) {
                          return null;
                        } else {
                          return 'Date Field is Empty';
                        }
                      })),
              Text(
                " To : ",
                style: TextStyle(fontSize: 16),
              ),
              Container(
                  width: MediaQuery.of(context).size.width * 0.35,
                  child: DateTimeField(
                      format: DateFormat('dd-MM-yyyy'),
                      controller: _toDateController,
                      initialValue: DateTime.now(),
                      onChanged: (value){
                        setState(() {
                          if(widget.colPen == 2){
                          this.getPendingData().then((value) {
                              IndCustomer obj = new IndCustomer(callback: getJsonData,);
                                obj.createState();
                          });
                          } else {
                            total = 0.0;
                            this.getJsonData();
                          }
                        });
                      },
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.calendar_today),
                          labelText: "To Date"),
                      resetIcon: null,
                      onShowPicker: (context, currentValue) {
                        return showDatePicker(
                            context: context,
                            initialDate: _dateTime == null
                                ? DateTime.now()
                                : _dateTime!.toUtc(),
                            firstDate: DateTime(2001),
                            lastDate: DateTime(2100));
                      },
                      validator: (val) {
                        if (val != null) {
                          return null;
                        } else {
                          return 'Date Field is Empty';
                        }
                      })),
            ],
          ),
        ),
        Expanded(
          child: FutureBuilder<String>(
            future: customerResponse,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return nameOfCustomer == null
                    ? Center(
                        child: Text("No Data Available"),
                      )
                    : ((_searchResult == null
                                ? false
                                : _searchResult!.length != 0) ||
                            _controller.text.isNotEmpty
                        ? ListView.separated(
                            separatorBuilder:
                                (BuildContext context, int index) => Divider(
                              thickness: 0.0,
                              color: Colors.lightBlue[900],
                            ),
                            itemCount: _searchResult == null
                                ? 0
                                : _searchResult!.length,
                            itemBuilder: (BuildContext context, int index) {
                              return IndCustomer(
                                index: index,
                                dWM: widget.dWM!.toInt(),
                                cat: widget.cat,
                                callback: updateReport,
                                colPen: widget.colPen!.toInt(),
                              );
                            },
                          )
                        : ListView.builder(
                            // separatorBuilder:
                            //     (BuildContext context, int index) => Divider(
                            //   thickness: 0.0,
                            //   color: Colors.white,
                            // ),
                            itemCount: nameOfCustomer == null
                                ? 0
                                : nameOfCustomer!.length,
                            itemBuilder: (BuildContext context, int index) {
                              return IndCustomer(
                                index: index,
                                dWM: widget.dWM!.toInt(),
                                cat: widget.cat,
                                callback: updateReport,
                                colPen: widget.colPen!.toInt(),
                                toDt: _toDateController.text,
                              );
                            },
                          ));
              } else {
                return Center(
                  child: CircularProgressIndicator(),
                );
              }
            },
          ),
        ),
        if(widget.colPen == 1)
         Container(
           height: 50,
                width: MediaQuery.of(context).size.width,
                color:Colors.blue[800]  ,
                padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.04),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      child: Text("Total : ₹ "+ (widget.colPen == 1? total.toStringAsFixed(2) : totalPending.toStringAsFixed(2)), style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.white
                      ),),
                    )
                  ],
                ),
                  )
      ],
    ));
  }
}

typedef Callback = void Function();

class IndCustomer extends StatefulWidget {
  final int? index, dWM, colPen;
  final String? cat, toDt;
  final Callback callback;
  IndCustomer(
      { this.index,
       this.dWM,
      this.cat,
      required this.callback,
       this.colPen,
      this.toDt});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _IndCustomer();
  }
}

class _IndCustomer extends State<IndCustomer> {
  List? customerStatement;
  double totalPayment = 0.0;

  Future<String> getJsonData() async {
   // String? empId = prefs!.getString("empId");
    var splittedStartDate =widget.toDt!.split("-");
          var rearrangedStartDate =splittedStartDate[2] +
        "-" +
       splittedStartDate[1] +
        "-" +
       splittedStartDate[0];
    String url = Constants.url +
        "app/pigmiAll.php?apicall=" +
        (widget.dWM == 1
            ? "pendingListCustomerDaily"
            : widget.dWM == 2
                ? "pendingListCustomerWeekly"
                : "pendingListCustomerMonthly") +
        "&userId=" +
        empId.toString() +
        "&pType=" +
        widget.dWM.toString() +
        "&regId=" +
        ((_searchResult == null ? false : _searchResult!.length != 0)
                      ? _searchResult![widget.index!.toInt()]["ID"]
                      : nameOfCustomer![widget.index!.toInt()]["ID"]).toString() +
        "&fdt=" +
        ((_searchResult == null ? false : _searchResult!.length != 0)
                      ? _searchResult![widget.index!.toInt()]["startdt"]
                      : nameOfCustomer![widget.index!.toInt()]["startdt"]).toString() +
        "&tdt=" +
        rearrangedStartDate;
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
     
      for (var item in convertDataToJson['user']!) {
        totalPayment = totalPayment + double.parse(item["amt"]);
      }
     
    });
    //  totalPending += totalPayment;
    // widget.callback();

    return "success";
  }
  @override
  void dispose(){
    super.dispose();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    //this.getTotal();
   // totalPending = 0.0;
    if(widget.colPen == 2){
      print(totalPending);
    this.getJsonData();
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return 
    totalPayment == 0.0 && widget.colPen == 2
    ? Container()
    : GestureDetector(
      onTap: () {
        if (widget.colPen == 2) {
          var splittedStartDate =widget.toDt!.split("-");
          var rearrangedStartDate =splittedStartDate[2] +
        "-" +
       splittedStartDate[1] +
        "-" +
       splittedStartDate[0];
       print(rearrangedStartDate);
       print(nameOfCustomer![widget.index!.toInt()]["startdt"]);
       print(nameOfCustomer![widget.index!.toInt()]["ID"]);
          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
            return PendingCustomerInfo(
              dWM: widget.dWM,
              fromDt:
                  ((_searchResult == null ? false : _searchResult!.length != 0)
                      ? _searchResult![widget.index!.toInt()]["startdt"]
                      : nameOfCustomer![widget.index!.toInt()]["startdt"]),
              toDt: DateFormat("yyyy-MM-dd")
                  .format(DateTime.parse(rearrangedStartDate)),
              custId: ((_searchResult == null ? false : _searchResult!.length != 0)
                      ? _searchResult![widget.index!.toInt()]["ID"]
                      : nameOfCustomer![widget.index!.toInt()]["ID"]),
            );
          }));
        }
      },
      child: Container(
        // height: 60,
        child: Card(
          elevation: 3.0,
          child: Column(
            children: <Widget>[
              Row(
                children: [
                  Container(
                    child: CircleAvatar(
                      radius: 25,
                      backgroundColor: Colors.grey[200],
                      child: ((_searchResult == null
                                  ? false
                                  : _searchResult!.length != 0)
                              ? (_searchResult![widget.index!.toInt()]["pf_img_path"] !=
                                      "" &&
                                  _searchResult![widget.index!.toInt()]["pf_img_path"] !=
                                      "0")
                              : (nameOfCustomer![widget.index!.toInt()]["pf_img_path"] !=
                                      "" &&
                                  nameOfCustomer![widget.index!.toInt()]
                                          ["pf_img_path"] !=
                                      "0"))
                          ? ClipRRect(
                              borderRadius: BorderRadius.circular(100),
                              child: Image.network(
                                Constants.url +
                                    ((_searchResult == null
                                                ? false
                                                : _searchResult!.length != 0)
                                            ? _searchResult![widget.index!.toInt()]
                                                ["pf_img_path"]
                                            : nameOfCustomer![widget.index!.toInt()]
                                                ["pf_img_path"])
                                        .toString(),
                                width: 300,
                                height: 300,
                                fit: BoxFit.fitHeight,
                              ),
                            )
                          : Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(100)),
                              width: 300,
                              height: 300,
                              child: Icon(
                                Icons.person,
                                color: Colors.grey[800],
                              ),
                            ),
                    ),
                  ),
                  Expanded(
                      child: Container(
                    child: Column(
                      children: [
                        Row(
                          // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: Text(
                                ((_searchResult == null
                                                ? false
                                                : _searchResult!.length != 0)
                                            ? _searchResult![widget.index!.toInt()]
                                                ["accNo"]
                                            : nameOfCustomer![widget.index!.toInt()]
                                                ["accNo"])
                                        .toString() +
                                    " ",
                                style: TextStyle(
                                    color: Colors.blue[800],
                                    fontWeight: FontWeight.w500,
                                    fontSize: 15),
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  top: MediaQuery.of(context).size.height *
                                      0.01),
                              child: Text(
                                ((_searchResult == null
                                            ? false
                                            : _searchResult!.length != 0)
                                        ? _searchResult![widget.index!.toInt()]["cName"]
                                        : nameOfCustomer![widget.index!.toInt()]
                                            ["cName"])
                                    .toString(),
                                style: TextStyle(
                                    color: Colors.blue[800],
                                    fontWeight: FontWeight.w500,
                                    fontSize: 15),
                              ),
                            ),
                            Spacer(),
                           
                              Container(
                                // padding: EdgeInsets.only(
                                //     top: MediaQuery.of(context).size.height * 0.01),
                                child: Text(
                                 widget.colPen == 2 ?  "Tot. Pending" : "Tot. Collection",
                                  style: TextStyle(color: widget.colPen == 2 ? Colors.red[600] : Colors.green[600]),
                                ),
                              )
                          ],
                        ),
                        Row(
                          //crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            // Container(
                            //   child: Icon(
                            //     Icons.location_on,
                            //     color: Colors.grey,
                            //     size: 15,
                            //   ),
                            // ),
                            // Container(
                            //   padding: EdgeInsets.only(
                            //       left: MediaQuery.of(context).size.width * 0.01),
                            //   child: Text(
                            //     ((_searchResult == null
                            //                 ? false
                            //                 : _searchResult!.length != 0)
                            //             ? _searchResult![widget.index!.toInt()]["addrs"]
                            //             : nameOfCustomer![widget.index!.toInt()]["addrs"])
                            //         .toString(),
                            //     style: TextStyle(color: Colors.grey),
                            //   ),
                            // ),
                            Spacer(),
                           
                              Container(
                                child: Text(
                                  "₹ " +
                                    ( widget.colPen == 2 ? totalPayment.toStringAsFixed(2): nameOfCustomer![widget.index!.toInt()]["tot"])
                                          ,
                                  style: TextStyle(
                                      fontWeight: FontWeight.w500,
                                      fontSize: 18),
                                ),
                              )
                          ],
                        )
                      ],
                    ),
                  )),
                 
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
