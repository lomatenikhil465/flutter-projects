import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pigmi/utils/const.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class PendingCustomerInfo extends StatefulWidget {
  final int? dWM;
  final String? fromDt, toDt, custId;
  PendingCustomerInfo({this.dWM, this.fromDt, this.toDt, this.custId});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PendingCustomerInfo();
  }
}

class _PendingCustomerInfo extends State<PendingCustomerInfo> {
  SharedPreferences? prefs;
  List? pendingData;
  Future<String>? pendingResponse;
  static int count = 0;
  double total = 0;

  Future<String> getJsonData() async {
    String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=" +
        (widget.dWM == 1
            ? "pendingListCustomerDaily"
            : widget.dWM == 2
                ? "pendingListCustomerWeekly"
                : "pendingListCustomerMonthly") +
        "&userId=" +
        empId.toString() +
        "&pType=" +
        widget.dWM.toString() +
        "&regId=" +
        widget.custId.toString() +
        "&fdt=" +
        widget.fromDt.toString() +
        "&tdt=" +
        widget.toDt.toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print( "monthly " + response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      pendingData = convertDataToJson['user'];
      for (var item in pendingData!) {
        total = total + double.parse(item["amt"]);
      }
    });

    return "success";
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEmpId().then((value) {
      pendingResponse = this.getJsonData();
    });
  }

  @override
  Widget build(BuildContext context) {
    count = 0;
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title:
        pendingData == null || pendingData!.length == 0
        ? Container(
          child: Text(""),
        )
         :GestureDetector(
          onTap: (){
            // Navigator.of(context).push(MaterialPageRoute(builder: (context){
            //  return  ProfilePage(accNo: widget.accNo, custId: widget.custId);
            // }));
            
          },
          child: Container(
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.01,
            ),
            child: Column(
              // mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
              
                Row(
                  children: [
                    
                   Container(
                    
                     child: Column(
                       children: [
                          Text(
                       pendingData![0]["cName"] == null ? "" : pendingData![0]["cName"].toString(),
                        style: TextStyle(fontSize: 18),
                      ),
                      Text(
                        pendingData![0]["acNo"].toString(),
                        style: TextStyle(fontSize: 14),
                      ),
                       ],
                     ),
                   )
                  ],
                )
              ],
            ),
          ),
        ),
        backgroundColor: Colors.blue[800],
      ),
      body: (pendingData == null) ?
      Container(
        child: Center(
          child: Text("No Pending Data..."),
        ),
      ):
      FutureBuilder<String>(
        future: pendingResponse,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return pendingData!.length == 0?
            Container(
              child: Center(
                child: Text("No Pending Data..."),
              ),
            )
            :Container(
              child: Column(
                
                children: [
                  Expanded(
                   
                    child: SingleChildScrollView(
                      
                      child: DataTable(
                        //columnSpacing: 20,
                          columns: const <DataColumn>[
                            DataColumn(
                              label: Text(
                                'No.',
                                style: TextStyle(fontStyle: FontStyle.italic),
                              ),
                            ),
                            DataColumn(
                              label: Text(
                                'Date',
                                style: TextStyle(fontStyle: FontStyle.italic),
                              ),
                            ),
                            // DataColumn(
                            //   label: Text(
                            //     'Acc No.',
                            //     style: TextStyle(fontStyle: FontStyle.italic),
                            //   ),
                            // ),
                            //  DataColumn(
                            //   label: Text(
                            //     'Cust Name',
                            //     style: TextStyle(fontStyle: FontStyle.italic),
                            //   ),
                            // ),
                             DataColumn(
                              label: Text(
                                'Amt',
                                style: TextStyle(fontStyle: FontStyle.italic),
                              ),
                            ),
                          ],
                          dataTextStyle: TextStyle(
                            color: Colors.red[700],
                            fontSize: 16,
                            //fontWeight: FontWeight.bold
                          ),
                          rows: pendingData!.map((data) {
                            
                            count = count + 1;
                            return DataRow(cells: [
                              DataCell(Text(count.toString())),
                              DataCell(Text( DateFormat("dd-MM-yyyy").format(DateTime.parse(data["dt"].toString())) )),
                              // DataCell(Text(data["acNo"])),
                              // DataCell(Text(data["cName"])),
                                DataCell(Text(widget.dWM == 3 ? (double.parse(data["amt"]) + double.parse(data["int_Amt"])).toStringAsFixed(2)  : data["amt"] )),
                            ]);
                          }).toList(),
                        ),
                    ),
                  ),
                  Container(
                    height: 50,
                    width: MediaQuery.of(context).size.width,
                    color:Colors.blue[800],
                    padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Text("Total : " + total.toStringAsFixed(2), style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold
                        ),)
                      ],
                    ),
                  )
                ],
              )
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }
}
