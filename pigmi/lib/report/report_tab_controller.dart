import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pigmi/customer/ExistingCustomerPage.dart';
import 'package:pigmi/ledger/LedgerPage.dart';
import 'package:pigmi/report/report_customer_page.dart';

class ReportTabController extends StatefulWidget {
  final int? colPen, initialIndex;
  final String? fromDt, toDt;
  const ReportTabController({ this.colPen, this.initialIndex, this.fromDt, this.toDt}) ;
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ReportTabController();
  }
  
}

class _ReportTabController extends State<ReportTabController> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
      initialIndex: widget.initialIndex!.toInt(),
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text("Report"),
          backgroundColor: Colors.blue[800],
          bottom: TabBar(
                  labelColor: Colors.blue[800],
                  unselectedLabelColor: Colors.white,
                  indicatorSize: TabBarIndicatorSize.label,
                  indicator: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10)),
                      color: Colors.white),
                  tabs: [
                    Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text("Daily"),
                      ),
                    ),
                    
                    Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text("Weekly"),
                      ),
                    ),
                     Tab(
                      child: Align(
                        alignment: Alignment.center,
                        child: Text("Monthly"),
                      ),
                    ),
                  ]
              ),
        ),
        body: Container(
          child: TabBarView(
           
          children: [
            ReportCustomerPage(dWM: 1,cat: "Report",colPen: widget.colPen,fromDt: widget.fromDt,toDt: widget.toDt,),
            ReportCustomerPage(dWM: 2,cat: "Report",colPen: widget.colPen,fromDt: widget.fromDt,toDt: widget.toDt),
            ReportCustomerPage(dWM: 3,cat: "Report",colPen: widget.colPen,fromDt: widget.fromDt,toDt: widget.toDt)
          ]
          ),
        ),
      ),
    );
  }
  
}