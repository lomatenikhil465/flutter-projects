import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pigmi/utils/const.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class ChangePassword extends StatefulWidget {
  final int? inOut;

  ChangePassword({this.inOut});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ChangePassword();
  }
}

class _ChangePassword extends State<ChangePassword> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  bool _newObscureText = true, _confirmObscureText = true;
  SharedPreferences? prefs;
  String? contact, passwd, confirmPassword, otp;
  List? userData;
  TextEditingController? otpController;

  Future<String> getJsonData() async {
    String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=getUser&userId=" +
        empId.toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      userData = convertDataToJson['user'];
    });
    return "success";
  }

  Future<String> getOtp() async {
    String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=getOTP&userId=" +
        empId.toString() +
        "&contact=" +
        contact.toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      otp = convertDataToJson['user'];
    });
    return "success";
  }

  Future<String> updatePassword() async {
    String? empId = prefs!.getString("empId");
    String url = Constants.url +
        "app/pigmiAll.php?apicall=updatePass&userId=" +
        empId.toString() +
        "&pass=" +
        passwd.toString();
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.parse(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);

    return "success";
  }

  Future enterOTPDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
            elevation: 0,
            backgroundColor: Colors.transparent,
            child: Container(
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                color: Colors.white,
                borderRadius: BorderRadius.circular(5),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02),
                    color: Colors.blue[800],
                    height: 50,
                    child: Row(
                      children: [
                        Container(
                            child: Text("Enter OTP",
                                style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white)))
                      ],
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02),
                    child: Text(
                      "OTP sent to your registered mobile No.",
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                        top: MediaQuery.of(context).size.height * 0.02),
                    child: Row(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.4,
                          child: TextFormField(
                            controller: otpController,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Please Enter OTP";
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                                labelText: "Enter OTP",
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15)),
                                contentPadding: EdgeInsets.only(
                                    top: 0, left: 10, bottom: 0, right: 0)),
                            keyboardType: TextInputType.numberWithOptions(
                                signed: false, decimal: true),
                            style: new TextStyle(
                                fontFamily: "Poppins", fontSize: 18),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          child: TextButton(
                              style: TextButton.styleFrom(
                                  //side: BorderSide(color: Colors.green),
                                  primary: Colors.white,
                                  backgroundColor: Colors.green,
                                  textStyle: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                  )),
                              onPressed: () {
                                //Navigator.of(context).pop();
                                Constants.waiting(context);
                                if (otpController!.text == otp.toString()) {
                                  updatePassword().then((value) {
                                    Navigator.of(context).pop();
                                    prefs!.setString('pass', passwd.toString());
                                  });
                                } else {
                                  Navigator.of(context).pop();
                                  Constants.showDialogBox(
                                      context, "Error!", "Enter valid OTP.");
                                }
                                Navigator.of(context).pop();
                              },
                              child: Text("Submit")),
                        ),
                        Container(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.04,
                              right: MediaQuery.of(context).size.width * 0.02),
                          child: TextButton(
                              style: TextButton.styleFrom(
                                  primary: Colors.white,
                                  backgroundColor: Colors.blue[800],
                                  textStyle: TextStyle(
                                    fontSize: 16,
                                    fontWeight: FontWeight.w500,
                                  )),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text("Close")),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

  Future _validateInputs() async {
    //checkConnection();
    if (_formKey.currentState!.validate()) {
//    If all data are correct then save data to out variables
      _formKey.currentState!.save();

      if (passwd == confirmPassword) {
        Constants.waiting(context);
        if (widget.inOut == 1) {
          updatePassword().then((value) {
            Navigator.of(context).pop();
            prefs!.setString('pass', passwd.toString());
            Constants.showDialogBox(context, "Successfully Submitted", "");
            Navigator.of(context).pop();
          });
          _formKey.currentState!.reset();
        } else {
          if (userData![0]["ph"] == contact) {
            getOtp().then((value) {
              Navigator.of(context).pop();
              enterOTPDialog(
                context,
              ).then((value) {
                Constants.showDialogBox(context, "Successfully Submitted", "")
                    .then((value) {
                  _formKey.currentState!.reset();
                  Navigator.of(context).pop();
                });
              });
            });
          } else {
            Navigator.of(context).pop();
            Constants.showDialogBox(context, "Error!",
                "Contact Number is not correct. Please check.");
          }
        }
      } else {
        // Navigator.of(context).pop();
        Constants.showDialogBox(
            context, "Error!", "Password not matched. Check once again.");
      }
    } else {
//    If all data are not valid then start auto validation.

      Constants.showDialogBox(context, "Error!", "Please Check the fields");
      setState(() {
        _autoValidate = true;
      });
    }
  }

  String? validateMobile(String? value) {
// Indian Mobile number are of 10 digit only
    if (value!.length != 10)
      return 'Mobile Number must be of 10 digit';
    else
      return null;
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    otpController = TextEditingController(text: "");
    getEmpId().then((value) {
      if (widget.inOut != 1) {
        getJsonData();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[800],
        title: Text("Change Password"),
      ),
      body: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Container(
            padding: EdgeInsets.all(30.0),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  if (widget.inOut != 1)
                    Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.02),
                      child: TextFormField(
                        onSaved: (String? user) {
                          contact = user.toString();
                        },
                        decoration: InputDecoration(
                          labelText: "Contact No.",
                          contentPadding:
                              EdgeInsets.only(left: 10.0, right: 2.0),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(10),
                          new WhitelistingTextInputFormatter(RegExp("[0-9]"))
                        ],
                        validator: validateMobile,
                        keyboardType: TextInputType.phone,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  Container(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.03),
                    child: TextFormField(
                      onSaved: (String? pass) {
                        passwd = pass.toString();
                      },
                      validator: (value) {
                        if (value == "") {
                          return "please enter password";
                        } else {
                          return null;
                        }
                      },
                      decoration: InputDecoration(
                        suffixIcon: GestureDetector(
                          onTap: () {
                            setState(() {
                              _newObscureText = !_newObscureText;
                            });
                          },
                          child: Icon(
                            _newObscureText
                                ? Icons.visibility
                                : Icons.visibility_off,
                          ),
                        ),
                        labelText: "New Password",
                        contentPadding: EdgeInsets.only(left: 10.0, right: 2.0),
                        fillColor: Colors.white,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                      ),
                      obscureText: _newObscureText,
                      style: new TextStyle(
                        fontFamily: "Poppins",
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.03),
                    child: TextFormField(
                      onSaved: (String? pass) {
                        confirmPassword = pass.toString();
                      },
                      validator: (value) {
                        if (value == "") {
                          return "please enter password";
                        } else {
                          return null;
                        }
                      },
                      decoration: InputDecoration(
                        suffixIcon: GestureDetector(
                          onTap: () {
                            setState(() {
                              _confirmObscureText = !_confirmObscureText;
                            });
                          },
                          child: Icon(
                            _confirmObscureText
                                ? Icons.visibility
                                : Icons.visibility_off,
                          ),
                        ),
                        labelText: "Confirm Password",
                        contentPadding: EdgeInsets.only(left: 10.0, right: 2.0),
                        fillColor: Colors.white,
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                      ),
                      obscureText: _confirmObscureText,
                      style: new TextStyle(
                        fontFamily: "Poppins",
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: 50.0),
                  ),
                  RaisedButton(
                    elevation: 3.0,
                    onPressed: () {
                      //_showDialogBox(context, "", "");
                      _validateInputs();
                      // Navigator.of(context).pushReplacement(
                      //     MaterialPageRoute(builder: (context) {
                      //   return HomePage(
                      //     contact: contact.toString(),
                      //   );
                      // }));
                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                      side: BorderSide(width: 1.5, color: Colors.blue.shade800),
                    ),
                    textColor: Colors.blue[800],
                    padding: EdgeInsets.only(
                        right: 100.0, left: 100.0, top: 10.0, bottom: 10.0),
                    color: Colors.white,
                    child: Text(
                      "Submit",
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.bold,
                          textBaseline: TextBaseline.alphabetic),
                    ),
                  ),
                ],
              ),
            ),
          )),
    );
  }
}
