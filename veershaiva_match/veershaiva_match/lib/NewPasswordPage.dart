import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class NewPasswordPage extends StatefulWidget {
  final String email, matchId;
  NewPasswordPage({this.email, this.matchId});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _NewPasswordPage();
  }
  
}

class _NewPasswordPage extends State<NewPasswordPage> {
   String newPass;
  bool _obscureText = true;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  bool _autoValidate = false;

  Future<String> updatePass() async {
    String url =
        "https://www.veershaivamatch.com/app/forgot.php?apicall=updatePass";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'email': widget.email,
          'matchId': widget.matchId,
          
        });
    //  setState(() {
    //   var convertDataToJson = json.decode(response.body);
    //   userData = convertDataToJson['user'];
    // });
    print(response.reasonPhrase);

    return "success";
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

   Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }


   void _validateInputs1() async {
    if (_formKey.currentState.validate()) {
//    If all data are correct then save data to out variables
        waiting(context);
      _formKey.currentState.save();
      setState(() {
        _autoValidate = false;
      });
      updatePass().then((value) async {
        Navigator.of(_keyLoader.currentContext).pop();
        _showDialogBox(context,"Message","Password Updated Successfully!!!").then((value) {
          Navigator.of(context).pop();
        });
        
      });
      _formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
       appBar: AppBar(
        title: Text("New Password"),
        backgroundColor: Colors.redAccent[700],

      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: [
               Container(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02,
                 left: MediaQuery.of(context).size.width * 0.02,),
                child: Row(
                  children: [
                    Container(
                      
                      child: Text(
                        "New Password : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ],
                ),
              ),
              
              Container(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01,
                 left: MediaQuery.of(context).size.width * 0.02,),
                child: Row(
                 
                  children: [
                    
                    Container(
                      width: MediaQuery.of(context).size.width * 0.94,
                     
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your New Password';
                          }
                          return null;
                        },
                        onSaved: (String newPass) {
                          this.newPass = newPass;
                        },
                        obscureText: _obscureText,
                        decoration: InputDecoration(
                          hintText: "Password",
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                          suffixIcon: GestureDetector(
                                  onTap: () {
                                    setState(() {
                                      _obscureText = !_obscureText;
                                    });
                                  },
                                  child: Icon(
                                    _obscureText
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    color: Colors.redAccent[700],
                                  ),
                                ),
                          contentPadding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.015,
                              left: MediaQuery.of(context).size.width * 0.015),
                        ),
                        keyboardType: TextInputType.text,
                        
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height *0.01),
                width: MediaQuery.of(context).size.width * 0.8,
                child: RaisedButton(
                  color: Colors.redAccent[700],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                  onPressed: (){
                    // Navigator.of(context).push(MaterialPageRoute(builder: (context){
                    //   return LoginDetails();
                    // }));
                    //_validateInputs();

                  },
                    child: Text("Submit"), 
                  
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  
}