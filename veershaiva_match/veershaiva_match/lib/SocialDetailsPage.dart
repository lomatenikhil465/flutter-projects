import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:veershaiva_match/LifeStylePage.dart';

import 'SignUpPage.dart';

class SocialDetailsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SocialDetailsPage();
  }
}

class _SocialDetailsPage extends State<SocialDetailsPage> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String matchId = "",
      religion,
      caste,
      subcaste,
      gouthram,
      _currentTongueSelected,
      knownLang,
      _currentStarSelected,
      _currentMoonSignSelected;
  int radio;
  var _tongue = ['English', 'Marathi', 'Hindi', 'Kannada', 'Telugu'];
  var _star = [
    'Uttarashada',
    'Shravana',
    'Dhanisha',
    'Shatadisha',
    'Poorvabhadrapada',
    'Uttarabhadrapada',
    'Revati',
    'Ashwini',
    'Bharani',
    'Krutika',
    'Rohini',
    'Mrugashira',
    'Aridra',
    'Punervasu',
    'Pushya',
    'Ashlesha',
    'Magha',
    'Hubba',
    'Uttara',
    'Hasta',
    'Chitta',
    'Swati',
    'Vishakha',
    'Anuradha',
    'Jeshta',
    'Moola',
    'Poorvashada',
    'Dont know'
  ];

  var _moonsign = [
    'Mesh',
    'Vrishabha',
    'Mithun',
    'karka',
    'Simbha',
    'Kanya',
    'Tula',
    'Vrishaka',
    'Dhanu',
    'Kumbh',
    'Makara',
    'Meen',
    'Dont Know'
  ];
  DateTime _dateTime = DateTime.now();

  void radioButtonCreDeb(int val) {
    setState(() {
      print(val);
      radio = val;
    });
  }

  void _onDropDownTongueSelected(String newValueSelected) {
    setState(() {
      this._currentTongueSelected = newValueSelected;
    });
  }

  void _onDropDownStarSelected(String newValueSelected) {
    setState(() {
      this._currentStarSelected = newValueSelected;
    });
  }

  void _onDropDownMoonSignSelected(String newValueSelected) {
    setState(() {
      this._currentMoonSignSelected = newValueSelected;
    });
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  void _validateInputs(BuildContext context) {
    //checkConnection();

    if (_formKey.currentState.validate()) {
      setState(() {
        _autoValidate = false;
      });

//    If all data are correct then save data to out variables
      _formKey.currentState.save();

      // radio = null;

      registerDetails.insert(0, {
        "religion": religion,
        "caste": caste,
        "subcaste": subcaste,
        "gouthram": gouthram,
        "horoscope": radio == 0 ? 'Yes' : radio == 1 ? 'No' : 'Does Not Matter',
        "motherTongue": _currentTongueSelected,
        "knownLang": knownLang,
        "nakshatra": _currentStarSelected,
        "moonsign": _currentMoonSignSelected,
      });
      print(registerDetails[0]["matchId"]);
      new Future.delayed(new Duration(milliseconds: 500), () {
        print(_keyLoader.currentContext);
        Navigator.of(_keyLoader.currentContext).pop();
      });
      Navigator.of(context).push(MaterialPageRoute(builder: (context) {
        return LifeStylePage();
      }));

      _formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      // if (_currentReligion == null) {
      //   _showDialogBox(context, "Message", "Please Choose Religion");
      // }

      // if (_currentCaste == null) {
      //   _showDialogBox(context, "Message", "Please Choose Caste");
      // }

      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Social Details "),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.02,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Religion : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.6,
                      padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Religion';
                          }
                          return null;
                        },
                        onSaved: (String name) {
                          this.religion = name;
                        },
                        decoration: InputDecoration(
                            hintText: "Religion",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left:
                                    MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Caste/Division : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.6,
                      padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Caste/Division';
                          }
                          return null;
                        },
                        onSaved: (String caste) {
                          this.caste = caste;
                        },
                        decoration: InputDecoration(
                            hintText: "Caste/Division",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left:
                                    MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "SubCaste/Sec : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.6,
                      padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your SubCaste/Sec';
                          }
                          return null;
                        },
                        onSaved: (String subcaste) {
                          this.subcaste = subcaste;
                        },
                        decoration: InputDecoration(
                            hintText: "SubCaste/Sec",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left:
                                    MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Gouthram : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.6,
                      padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Gouthram';
                          }
                          return null;
                        },
                        onSaved: (String gouthram) {
                          this.gouthram = gouthram;
                        },
                        decoration: InputDecoration(
                            hintText: "Gouthram",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left:
                                    MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      child: Text(
                        "Horoscope \nMatch : ",
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ),
                    Radio(
                      value: 0,
                      groupValue: radio,
                      onChanged: radioButtonCreDeb,
                    ),
                    GestureDetector(
                      onTap: () {
                        radioButtonCreDeb(0);
                      },
                      child: Container(
                        child: Text(
                          "Yes",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                    ),
                    Radio(
                      value: 1,
                      groupValue: radio,
                      onChanged: radioButtonCreDeb,
                    ),
                    GestureDetector(
                      onTap: () {
                        radioButtonCreDeb(1);
                      },
                      child: Container(
                        child: Text(
                          "No",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                    ),
                    Radio(
                      value: 2,
                      groupValue: radio,
                      onChanged: radioButtonCreDeb,
                    ),
                    GestureDetector(
                      onTap: () {
                        radioButtonCreDeb(2);
                      },
                      child: Container(
                        child: Text(
                          "Does Not \nMatter",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Mother Tongue : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width: MediaQuery.of(context).size.width * 0.6,
                      margin: EdgeInsets.only(
                          right: MediaQuery.of(context).size.width * 0.02),
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(width: 1.0)),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            itemHeight: 80,
                            elevation: 1,

                            autofocus: true,
                            focusColor: Colors.lightBlue[900],
                            //style: Theme.of(context).textTheme.title,
                            isExpanded: true,
                            hint: Text("Choose a Mother Tongue"),
                            items: _tongue.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                  value: dropDownStringItem,
                                  child: Text(dropDownStringItem,
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.lightBlue[700])));
                            }).toList(),
                            onChanged: (String newValueSelected) =>
                                _onDropDownTongueSelected(newValueSelected),
                            value: _currentTongueSelected,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Known Language : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.6,
                      padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Known Language';
                          }
                          return null;
                        },
                        onSaved: (String knownLang) {
                          this.knownLang = knownLang;
                        },
                        decoration: InputDecoration(
                            hintText: "Known Language",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left:
                                    MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Star(Nakshatra) : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width: MediaQuery.of(context).size.width * 0.6,
                      margin: EdgeInsets.only(
                          right: MediaQuery.of(context).size.width * 0.02),
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(width: 1.0)),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            itemHeight: 80,
                            elevation: 1,

                            autofocus: true,
                            focusColor: Colors.lightBlue[900],
                            //style: Theme.of(context).textTheme.title,
                            isExpanded: true,
                            hint: Text("Choose a Nakshatra"),
                            items: _star.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                  value: dropDownStringItem,
                                  child: Text(dropDownStringItem,
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.lightBlue[700])));
                            }).toList(),
                            onChanged: (String newValueSelected) =>
                                _onDropDownStarSelected(newValueSelected),
                            value: _currentStarSelected,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Moonsign : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width: MediaQuery.of(context).size.width * 0.6,
                      margin: EdgeInsets.only(
                          right: MediaQuery.of(context).size.width * 0.02),
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(width: 1.0)),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            itemHeight: 80,
                            elevation: 1,

                            autofocus: true,
                            focusColor: Colors.lightBlue[900],
                            //style: Theme.of(context).textTheme.title,
                            isExpanded: true,
                            hint: Text("Choose a Moonsign"),
                            items: _moonsign.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                  value: dropDownStringItem,
                                  child: Text(dropDownStringItem,
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.lightBlue[700])));
                            }).toList(),
                            onChanged: (String newValueSelected) =>
                                _onDropDownMoonSignSelected(newValueSelected),
                            value: _currentMoonSignSelected,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                width: MediaQuery.of(context).size.width * 0.8,
                child: RaisedButton(
                  color: Colors.redAccent[700],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  onPressed: () {
                    waiting(context);
                    _validateInputs(context);
                  },
                  child: Text("Next"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
