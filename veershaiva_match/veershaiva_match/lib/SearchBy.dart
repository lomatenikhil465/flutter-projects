import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'PersonDetails.dart';

class SearchByPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SearchByPage();
  }
  
}

class _SearchByPage extends State<SearchByPage> {
  SharedPreferences prefs;
  TextEditingController _idController;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  List searchByData;

  Future<String> searchBy() async {
     
    String url =
        "https://www.veershaivamatch.com/app/searchDetails.php?apicall=searchById";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'custId': _idController.text,
          
        });
     setState(() {
      var convertDataToJson = json.decode(response.body);
      searchByData = convertDataToJson['user'];
    });
    print(response.reasonPhrase);

    return "success";
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  void _validateInputs() {
    //checkConnection();
    if (_formKey.currentState.validate() ) {
      setState(() {
        _autoValidate = false;
      });
      waiting(context);
//    If all data are correct then save data to out variables
      _formKey.currentState.save();
      searchBy().then((value) {
       // radio = null;
        _idController.text = "";
        

        Navigator.of(_keyLoader.currentContext).pop();
        Navigator.of(context).push(MaterialPageRoute(builder: (context){
          return PersonDetails(matchFoundData: searchByData,);
        }));
      });

      _formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

   Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEmpId();
    _idController = TextEditingController(text: "");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Search by ID"),
        backgroundColor: Colors.redAccent[700],

      ),
      body: SingleChildScrollView(child: Form(
        key: _formKey,
        autovalidate: _autoValidate,
              child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02, top: MediaQuery.of(context).size.height * 0.02),
              child: Row(
                children: [
                  Container(
                      
                      child: Text("Search By Match ID : ",style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.bold
                      ),),
                    ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02, top: MediaQuery.of(context).size.height * 0.01 ),
              child: Row(
                  children: [
                    
                    Container(
                      
                                  height: MediaQuery.of(context).size.height * 0.06,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width * 0.94,
                                        child: TextFormField(
                                          autofocus: false,
                                          controller: _idController,
                                          validator: (value) {
                                            if (value == "" ||
                                                value == "0" ) {
                                              return "Please Enter Match ID";
                                            } else {
                                              return null;
                                            }
                                          },
                                          decoration: InputDecoration(
                                            labelText: "Match ID",
                                            contentPadding: EdgeInsets.only(
                                                top: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.015),
                                          ),
                                         
                                          keyboardType:
                                              TextInputType.text,
                                          style: new TextStyle(
                                            fontFamily: "Poppins",
                                          ),
                                          
                                        ),
                                      ),
                                    ],
                                  ),
                                ),

                                
                  ],
                ),
            ),
              
              Container(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height *0.01),
                width: MediaQuery.of(context).size.width * 0.8,
                child: RaisedButton(
                  color: Colors.redAccent[700],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                  onPressed: (){
                    // Navigator.of(context).push(MaterialPageRoute(builder: (context){
                    //   return CareerDetailsPage();
                    // }));
                    _validateInputs();

                  },
                    child: Text("Search"), 
                  
                ),
              )
          ],
        ),
      ),),
    );
  }
  
}