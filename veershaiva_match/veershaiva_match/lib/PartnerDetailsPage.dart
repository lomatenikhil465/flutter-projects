import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class PartnerDetailsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PartnerDetailsPage();
  }
}

class _PartnerDetailsPage extends State<PartnerDetailsPage> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  var _lookingFor = ['Unmarried', 'Separated','Widowed', 'Divorce'];
  var _country = ['Afghanistan', 'Albania', 'Algeria', 'Andorra', 'Angola', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Australia', 'Austria', 'Azerbaijan','Bahamas','Bangladesh','Belgium',
  'Bhutan', 'Brazil', 'Brunei', 'Bulgaria', 'Cambodia', 'Cameroon', 'Canada', 'Central African Republic', 'China', 'Colombia', 'Cuba', 'Denmark', 'Dominica', 'Egypt', 'Eritrea', 'Ethiopia', 'Finland', 'France',
  'Gambia','Georgia','Germany','Greece','Grenada','Haiti','Hungary','Iceland','India','Indonesia','Iran','Iraq','Ireland','Israel','Italy','Japan','Jordan','Kazakhstan','Kenya','Kyrgyzstan','Liberia','Lithuania',
  'Madagascar','Malawi','Malaysia','Maldives','Marshall Islands','Mexico','Monaco','Nepal','Netherlands','New Zealand','Nigeria','North Korea','Norway','Pakistan','Philippines','Poland','Portugal','Qatar','Romania',
  'Russia','San Marino','Saudi Arabia','Serbia','Singapore','South Africa','South Korea','Spain','Sri Lanka','Sweden','Switzerland','Syria','Tanzania','Thailand','Turkey','Uganda','United Arab Emirates','United Kingdom',
  'United States of America','Uzbekistan','Vietnam','Yemen','Zambia','Zimbabwe'];
  var _height = [
    'Below 4ft',
    '4\'1\"ft',
    '4\'2\"f',
    '4\'3\"ft',
    '4\'4\"ft',
    '4\'5\"ft',
    '4\'6\"ft',
    '4\'7\"ft',
    '4\'8\"ft',
    '4\'9\"ft',
    '4\'10\"ft',
    '4\'11\"ft',
    '5\'0\"ft',
    '5\'1\"ft',
    '5\'2\"ft',
    '5\'3\"ft',
    '5\'4\"ft',
    '5\'5\"ft',
    '5\'6\"ft',
    '5\'7\"ft',
    '5\'8\"ft',
    '5\'9\"ft',
    '5\'10\"ft',
    '5\'11\"ft',
    '6\'0\"ft',
    '6\'1\"ft',
    '6\'2\"ft',
    '6\'3\"ft',
    '6\'4\"ft',
    '6\'5\"ft',
    '6\'6\"ft',
    '6\'7\"ft',
    '6\'8\"ft',
    '6\'9\"ft',
    '6\'10\"ft',
    '6\'11\"ft',
    'above 7ft'
  ];
  var _complexion = [
    'Very Fair',
    'Fair',
    'Wheatish',
    'Wheatish Medium',
    'Wheatish Brown',
    'Dark'
  ];
  var _degree = ['Bachelors', 'Doctorate','Masters', 'Diploma', 'Under Graduate', 'Associate Degree', 'Honnours Degree', 'Trade School', 'High School', 'Less than High School'];
  var religion = ["Hindu"], caste = ["Jangam", "Lingayat", "Any"];
  var _residentStatus = ['Does Not Matter', 'Citizen','Christian', 'Permanent Resident', 'Student Visa', 'Temporary visa', 'Work Permit'];
  var _hobbies = ['Acting', 'Astronomy','Astrlogy', 'Collectibles', 'Cooking', 'Crosswords', 'Dancing', 'Film-making', 'Fishing', 'Gardening/Landscaping',
  'Graphology','Nature','Numerology','Palmistry','Painting','Pets','Photography','Playing Musical Instruments','Puzzles','Travelling','Not Interest'];
  var _interests = ['Adventure Sports', 'Book Clubs','Computer games', 'Health fitness', 'Internet', 'Learning New Languages', 'Movies', 'Music', 'Politics', 'Reading',
  'Social Service','Sports','Television','Theatre','Travel','Writing','Yoga','Medicine','Not Interest'];
  String expectations, otherHobbies, otherInterests;
  String _currentLookingForSelected,_currentCountrySelected,
  _currentHeightSelected,
  _currentComplexionSelected,
  _currentReligionSelected,
  _currentEducationSelected,
  _currentCasteSelected,_currentResidentStatusSelected,
  _currentHobbiesSelected,
  _currentInterestsSelected;
  TextEditingController _ageFromController, _ageToController;

   void _onDropDowtLookingForSelected(String newValueSelected) {
    setState(() {
      this._currentLookingForSelected = newValueSelected;
    });
  }

  void _onDropDownCountrySelected(String newValueSelected) {
    setState(() {
      this._currentCountrySelected = newValueSelected;
    });
  }

  
  void _onDropDownHeightSelected(String newValueSelected) {
    setState(() {
      this._currentHeightSelected = newValueSelected;
    });
  }

   void _onDropDownComplexionSelected(String newValueSelected) {
    setState(() {
      this._currentComplexionSelected = newValueSelected;
    });
  }
   void _onDropDownReligionSelected(String newValueSelected) {
    setState(() {
      this._currentReligionSelected = newValueSelected;
    });
  }

   void _onDropDownEducationSelected(String newValueSelected) {
    setState(() {
      this._currentEducationSelected = newValueSelected;
    });
  }
   void _onDropDownCasteSelected(String newValueSelected) {
    setState(() {
      this._currentCasteSelected = newValueSelected;
    });
  }

  void _onDropDownResidentStatusSelected(String newValueSelected) {
    setState(() {
      this._currentResidentStatusSelected = newValueSelected;
    });
  }

   void _onDropDownHobbiesSelected(String newValueSelected) {
    setState(() {
      this._currentHobbiesSelected = newValueSelected;
    });
  }
   void _onDropDownInterestsSelected(String newValueSelected) {
    setState(() {
      this._currentInterestsSelected = newValueSelected;
    });
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  Future<String> homeSearch() async {
    String url =
        "https://www.veershaivamatch.com/app/searchDetails.php?apicall=homeSearch";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          // 'ageTo': _ageToController.text,
          // 'ageFrom': _ageFromController.text,
          // 'gender': radio == 0 ? "Male" : "Female",
          // 'religion': _currentReligion,
          // 'caste': _currentCaste
        });
    //  setState(() {
    //   var convertDataToJson = json.decode(response.body);
    //   homeSearchData = convertDataToJson['user'];
    // });
    //print(response.reasonPhrase);

    return "success";
  }

  void _validateInputs() {
    //checkConnection();
    if (_formKey.currentState.validate() ) {
      setState(() {
        _autoValidate = false;
      });
      waiting(context);
//    If all data are correct then save data to out variables
      _formKey.currentState.save();
      homeSearch().then((value) {
        Navigator.of(_keyLoader.currentContext).pop();
        Navigator.of(context).push(MaterialPageRoute(builder: (context){
          return null;
        }));
      });

      _formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      // if (_currentReligion == null) {
      //   _showDialogBox(context, "Message", "Please Choose Religion");
      // }

      // if (_currentCaste == null) {
      //   _showDialogBox(context, "Message", "Please Choose Caste");
      // }

      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _ageFromController =
        TextEditingController(text: "");
    _ageToController =
        TextEditingController(text: "");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Family Details "),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
                  child: Column(
            children: [
             Container(
               padding: EdgeInsets.only(
                        
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Looking For : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                        
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Family Value"),
                              items: _lookingFor
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDowtLookingForSelected(newValueSelected),
                              value: _currentLookingForSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
              Container(
                 padding: EdgeInsets.only(
                        
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                child: Row(
                  children: [
                    Container(
                     
                      child: Text(
                        "Age : ",
                        style:
                            TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.1),
                      height: MediaQuery.of(context).size.height * 0.06,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.3,
                            child: TextFormField(
                              autofocus: false,
                              controller: _ageFromController,
                              validator: (value) {
                                if (value == "" || value == "0") {
                                  return "Please Enter From Age";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                labelText: "From",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                              ),
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly
                              ],
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true, signed: false),
                              style: new TextStyle(
                                fontFamily: "Poppins",
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.06,
                      padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.1),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width * 0.3,
                            child: TextFormField(
                              autofocus: false,
                              controller: _ageToController,
                              validator: (value) {
                                if (value == "" || value == "0") {
                                  return "Please Enter To Age";
                                } else {
                                  return null;
                                }
                              },
                              decoration: InputDecoration(
                                labelText: "To",
                                contentPadding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height *
                                        0.015),
                              ),
                              inputFormatters: [
                                WhitelistingTextInputFormatter.digitsOnly
                              ],
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true, signed: false),
                              style: new TextStyle(
                                fontFamily: "Poppins",
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                 padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.01,
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                     
                      child: Text(
                        "Expectations : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.55,
                      
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Expectations';
                          }
                          return null;
                        },
                        onSaved: (String expectations) {
                          this.expectations = expectations;
                        },
                        decoration: InputDecoration(
                            hintText: "Expectations",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left: MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
             
              
            Container(
               padding: EdgeInsets.only(
                        
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Country living in: ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                        
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Country"),
                              items: _country
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownCountrySelected(newValueSelected),
                              value: _currentCountrySelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
            Container(
               padding: EdgeInsets.only(
                        
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Height : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                       
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Height"),
                              items: _height
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownHeightSelected(newValueSelected),
                              value: _currentHeightSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
            Container(
               padding: EdgeInsets.only(
                        
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Complexion : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                        
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Complexion"),
                              items: _complexion
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownComplexionSelected(newValueSelected),
                              value: _currentComplexionSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                        
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Education : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                        
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Education"),
                              items: _degree
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownEducationSelected(newValueSelected),
                              value: _currentEducationSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                        
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Religion : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                        
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Religion"),
                              items: religion.map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownReligionSelected(newValueSelected),
                              value: _currentReligionSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
            Container(
               padding: EdgeInsets.only(
                        
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Caste/Division : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                        
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Caste"),
                              items: caste
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownCasteSelected(newValueSelected),
                              value: _currentCasteSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
             Container(
               padding: EdgeInsets.only(
                        
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Resident Status : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                        
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Resident Status"),
                              items: _residentStatus
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownResidentStatusSelected(newValueSelected),
                              value: _currentResidentStatusSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),

            Container(
              padding: EdgeInsets.only(
                        
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Hobbies : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                        
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Hobbies"),
                              items: _hobbies
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownHobbiesSelected(newValueSelected),
                              value: _currentHobbiesSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                        
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Interests : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                        
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Interests"),
                              items: _interests
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownInterestsSelected(newValueSelected),
                              value: _currentInterestsSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
            
              Container(
                 padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.01,
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                     
                      child: Text(
                        "Other Hobbies : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.55,
                      
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Other Hobbies';
                          }
                          return null;
                        },
                        onSaved: (String otherHobbies) {
                          this.otherHobbies = otherHobbies;
                        },
                        decoration: InputDecoration(
                            hintText: "Other Hobbies",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left: MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              
              Container(
                 padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.01,
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                     
                      child: Text(
                        "Other Interests : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.55,
                    
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Other Interests';
                          }
                          return null;
                        },
                        onSaved: (String otherInterests) {
                          this.otherInterests = otherInterests;
                        },
                        decoration: InputDecoration(
                            hintText: "Other Interests",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left: MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                width: MediaQuery.of(context).size.width * 0.8,
                child: RaisedButton(
                  color: Colors.redAccent[700],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  onPressed: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return null;
                    }));
                  },
                  child: Text("Submit"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
