import 'EditProfile.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';


class EditPreference extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _EditPreference();
  }
}

class _EditPreference extends State<EditPreference> {
  SharedPreferences prefs;
  List partnersData, familyData;
  Future<String> partnersResponse;

   Future<String> getPartnersData() async {
    var id = prefs.getString('id');
    String url =
        "https://www.veershaivamatch.com/app/getDetails.php?apicall=family";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'custId': id, 
        });
     setState(() {
      var convertDataToJson = json.decode(response.body);
      partnersData = convertDataToJson['user'];
    });
    //print(response.reasonPhrase);

    return "success";
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEmpId().then((value) {
      partnersResponse = this.getPartnersData();
    });
    
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Partner Preference"),
          backgroundColor: Colors.redAccent[700],
        ),
        body: FutureBuilder<String>(
          future: partnersResponse,
          builder: (context,snapshot){
            if(snapshot.hasData){
              return SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 200,
                child: Center(
                    child: Image.network(
                        "https://www.veershaivamatch.com/CP/" + (basicData  == null ? "" : basicData[0]["photo"].toString()),
                        loadingBuilder: (context, child, progress) {
                              return progress == null
                                  ? child
                                  : LinearProgressIndicator();
                            },
                        )),
              ),
              Container(
                //width: MediaQuery.of(context).size.width,
                child: Card(
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01),
                        child: Row(
                          children: [
                            Text(
                              "Partner Preference",
                              style: TextStyle(
                                  fontSize: 17,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.green[600]),
                            ),
                          ],
                        ),
                      ),
                      Divider(
                        thickness: 1,
                      ),
                      Container(
                        padding: EdgeInsets.only(
                           
                            left: MediaQuery.of(context).size.width * 0.01),
                        child: Row(
                          children: [
                            Icon(
                              Icons.description,
                              color: Colors.redAccent,
                            ),
                            Text(
                              "Basic Preference",
                              style: TextStyle(
                                  fontSize: 16, fontWeight: FontWeight.w500),
                            ),
                            Spacer(),
                            IconButton(
                              icon: Icon(Icons.edit),
                              onPressed: () {},
                              iconSize: 20,
                            )
                          ],
                        ),
                      ),
                      
                     
                      
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01,
                             right: MediaQuery.of(context).size.width *
                                        0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Age",
                            ),
                            Container(
                              
                                child: Flexible(child: Text(partnersData[0]["pf_agefrm"]  == null ? "" : partnersData[0]["pf_agefrm"] + " - " + partnersData[0]["pf_ageto"]  == null ? "" : partnersData[0]["pf_ageto"] + " Yrs"))),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01,
                             right: MediaQuery.of(context).size.width *
                                        0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Height",
                            ),
                            Container(
                                
                                child: Flexible(child: Text(partnersData[0]["pf_height"]  == null ? "" : partnersData[0]["pf_height"]))),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01,
                             right: MediaQuery.of(context).size.width *
                                        0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Martial Status",
                            ),
                            Container(
                               
                                child: Flexible(child: Text(partnersData[0]["pf_looks"]  == null ? "" : partnersData[0]["pf_looks"]))),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01,
                             right: MediaQuery.of(context).size.width *
                                        0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Expectations",
                            ),
                            Container(
                              
                                child: Flexible(child: Text(partnersData[0]["pf_expectations"]  == null ? "" : partnersData[0]["pf_expectations"]))),
                          ],
                        ),
                      ),
                      
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01,
                             right: MediaQuery.of(context).size.width *
                                        0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Complexion",
                            ),
                            Container(
                               
                                child: Flexible(child: Text(partnersData[0]["pf_complexion"]  == null ? "" : partnersData[0]["pf_complexion"]))),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01,
                             right: MediaQuery.of(context).size.width *
                                        0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Country",
                            ),
                            Container(
                                
                                child: Flexible(child: Text(partnersData[0]["pf_countryliving"]  == null ? "" : partnersData[0]["pf_countryliving"]))),
                          ],
                        ),
                      ),
                      
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01,
                             right: MediaQuery.of(context).size.width *
                                        0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Residential Status",
                            ),
                            Container(
                                
                                child: Flexible(child: Text(partnersData[0]["pf_residentstatus"]  == null ? "" : partnersData[0]["pf_residentstatus"]))),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01,
                             right: MediaQuery.of(context).size.width *
                                        0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Religion",
                            ),
                            Container(
                                
                                child: Flexible(child: Text(partnersData[0]["pf_religion"]  == null ? "" : partnersData[0]["pf_religion"]))),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01,
                             right: MediaQuery.of(context).size.width *
                                        0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Caste",
                            ),
                            Container(
                                
                                child: Flexible(child: Text(partnersData[0]["pf_caste"]  == null ? "" : partnersData[0]["pf_caste"]))),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              
            ],
          ),
        );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ));
  }
}
