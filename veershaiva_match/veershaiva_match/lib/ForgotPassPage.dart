import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:veershaiva_match/NewPasswordPage.dart';

class ForgotPassPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ForgotPassPage();
  }
  
}

class _ForgotPassPage extends State<ForgotPassPage> {
  String email,matchId,matchId1,mobile ;
   final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
   final GlobalKey<FormState> _formKey1 = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  bool _autoValidate = false;
   bool _autoValidate1 = false;
   List userData;

   Future<String> getJsonData() async {
    String url =
        "https://www.veershaivamatch.com/app/forgot.php?apicall=forgotPass";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'email': email,
          'matchId': matchId,
          
        });
     setState(() {
      var convertDataToJson = json.decode(response.body);
      userData = convertDataToJson['user'];
    });
    //print(response.reasonPhrase);

    return "success";
  }

  Future<String> getJsonData1() async {
    String url =
        "https://www.veershaivamatch.com/app/forgot.php?apicall=forgotPass";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'email': mobile,
          'matchId': matchId1,
          
        });
     setState(() {
      var convertDataToJson = json.decode(response.body);
      userData = convertDataToJson['user'];
    });
    //print(response.reasonPhrase);

    return "success";
  }

   waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }


  void _validateInputs1() async {
    if (_formKey.currentState.validate()) {
//    If all data are correct then save data to out variables
        waiting(context);
      _formKey.currentState.save();
      setState(() {
        _autoValidate = false;
      });
      getJsonData().then((value) async {
        Navigator.of(_keyLoader.currentContext).pop();
        if (userData != null) {
           Navigator.of(context)
              .pushReplacement(MaterialPageRoute(builder: (context) {
            return NewPasswordPage(email: email,matchId: matchId,);
          }));
        } else {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: new Text(
                  "Error!",
                  style: TextStyle(color: Colors.red[600]),
                ),
                content: new Text("Please check your Email Id and Match Id"),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  new FlatButton(
                    child: new Text("Close"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        }
      });
      _formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  void _validateInputs2() async {
    if (_formKey1.currentState.validate()) {
//    If all data are correct then save data to out variables
        waiting(context);
      _formKey1.currentState.save();
      setState(() {
        _autoValidate1 = false;
      });
      getJsonData1().then((value) async {
        Navigator.of(_keyLoader.currentContext).pop();
        if (userData != null) {
           Navigator.of(context)
              .pushReplacement(MaterialPageRoute(builder: (context) {
            return NewPasswordPage(email: mobile,matchId: matchId1,);
          }));
        } else {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: new Text(
                  "Error!",
                  style: TextStyle(color: Colors.red[600]),
                ),
                content: new Text("Please check your Email Id and Match Id"),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  new FlatButton(
                    child: new Text("Close"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        }
      });
      _formKey1.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate1 = true;
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Forgot Password"),
        backgroundColor: Colors.redAccent[700],

      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: [
                             
              Container(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01,
                 left: MediaQuery.of(context).size.width * 0.02,),
                child: Row(
                  
                  children: [
                    
                    Container(
                      width: MediaQuery.of(context).size.width * 0.94,
                      
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Email ID';
                          }
                          return null;
                        },
                        onSaved: (String email) {
                          this.email = email;
                        },
                        decoration: InputDecoration(
                            labelText: "Email ID",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left: MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
             
              
              Container(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01,
                 left: MediaQuery.of(context).size.width * 0.02,),
                child: Row(
                  
                  children: [
                    
                    Container(
                      width: MediaQuery.of(context).size.width * 0.94,
                      
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Match ID';
                          }
                          return null;
                        },
                        onSaved: (String matchId) {
                          this.matchId = matchId;
                        },
                        decoration: InputDecoration(
                            labelText: "Match ID",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left: MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height *0.01),
                width: MediaQuery.of(context).size.width * 0.8,
                child: RaisedButton(
                  color: Colors.redAccent[700],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                  onPressed: (){
                    // Navigator.of(context).push(MaterialPageRoute(builder: (context){
                    //   return LoginDetails();
                    // }));
                    _validateInputs1();

                  },
                    child: Text("Submit"), 
                  
                ),
              ),
              Container(
                padding: EdgeInsets.only(top: MediaQuery.of(context).size.height *0.01),
                width: MediaQuery.of(context).size.width * 0.8,
                child: Center(
                  child: Text(
                    "OR",
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500
                    ),
                  ),
                )
              ),
              Form(
                key: _formKey1,
                autovalidate: _autoValidate1,
                              child: Column(
                  children: [
                           
                Container(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01,
                   left: MediaQuery.of(context).size.width * 0.02,),
                  child: Row(
                    
                    children: [
                      
                      Container(
                        width: MediaQuery.of(context).size.width * 0.94,
                        
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please Enter your Mobile No.';
                            }
                            return null;
                          },
                          onSaved: (String mobile) {
                            this.mobile = mobile;
                          },
                          decoration: InputDecoration(
                              labelText: "Mobile No.",
                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(),
                              ),
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height * 0.015,
                                  left: MediaQuery.of(context).size.width * 0.015)),
                          keyboardType: TextInputType.numberWithOptions(signed: false,decimal: true),
                          inputFormatters: [
                                    LengthLimitingTextInputFormatter(10),
                                    new WhitelistingTextInputFormatter(
                                        RegExp("[0-9]"))
                                  ],
                          style: new TextStyle(
                            fontFamily: "Poppins",
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                
                
                Container(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01,
                   left: MediaQuery.of(context).size.width * 0.02,),
                  child: Row(
                    
                    children: [
                      
                      Container(
                        width: MediaQuery.of(context).size.width * 0.94,
                        
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please Enter your Match ID';
                            }
                            return null;
                          },
                          onSaved: (String matchId1) {
                            this.matchId1 = matchId1;
                          },
                          decoration: InputDecoration(
                              hintText: "Match ID",
                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(),
                              ),
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height * 0.015,
                                  left: MediaQuery.of(context).size.width * 0.015)),
                          keyboardType: TextInputType.text,
                          style: new TextStyle(
                            fontFamily: "Poppins",
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: MediaQuery.of(context).size.height *0.01),
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: RaisedButton(
                    color: Colors.redAccent[700],
                    textColor: Colors.white,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                    onPressed: (){
                      // Navigator.of(context).push(MaterialPageRoute(builder: (context){
                      //   return LoginDetails();
                      // }));
                      _validateInputs2();

                    },
                      child: Text("Submit"), 
                    
                  ),
                ),
                  ],
                ),
              )
            ],
          )
        ),
      ),
    );
  } 
  
}