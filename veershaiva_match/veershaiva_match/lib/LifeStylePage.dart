import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:veershaiva_match/LoginDetails.dart';

import 'SignUpPage.dart';

class LifeStylePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LifeStylePage();
  }
}

class _LifeStylePage extends State<LifeStylePage> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _currentHeightSelected,
      _currentWeightSelected,
      _currentBloodGroupsSelected,
      _currentComplexionSelected,
      _currentDietSelected;
  int radio;

  var _height = [
    'Below 4ft',
    '4\'1\"ft',
    '4\'2\"f',
    '4\'3\"ft',
    '4\'4\"ft',
    '4\'5\"ft',
    '4\'6\"ft',
    '4\'7\"ft',
    '4\'8\"ft',
    '4\'9\"ft',
    '4\'10\"ft',
    '4\'11\"ft',
    '5\'0\"ft',
    '5\'1\"ft',
    '5\'2\"ft',
    '5\'3\"ft',
    '5\'4\"ft',
    '5\'5\"ft',
    '5\'6\"ft',
    '5\'7\"ft',
    '5\'8\"ft',
    '5\'9\"ft',
    '5\'10\"ft',
    '5\'11\"ft',
    '6\'0\"ft',
    '6\'1\"ft',
    '6\'2\"ft',
    '6\'3\"ft',
    '6\'4\"ft',
    '6\'5\"ft',
    '6\'6\"ft',
    '6\'7\"ft',
    '6\'8\"ft',
    '6\'9\"ft',
    '6\'10\"ft',
    '6\'11\"ft',
    'above 7ft'
  ];
  var _weight = [
    '40-45 kg',
    '45-50 kg',
    '50-55 kg',
    '55-60 kg',
    '60-65 kg',
    '65-70 kg',
    '70-75 kg',
    '75-80 kg',
    '80-85 kg',
    '85-90 kg',
    '90-95 kg',
    '95-100 kg',
    '100-105 ft',
    '105-110 kg',
    'above 110 kg'
  ];
  var _bloodGroups = ['O-', 'O+', 'A-', 'A+', 'B-', 'B+', 'AB-', 'AB+'];

  var _complexion = [
    'Very Fair',
    'Fair',
    'Wheatish',
    'Wheatish Medium',
    'Wheatish Brown',
    'Dark'
  ];
  var _diet = [
    'Veg',
    'Eggetarian',
    'Occasionally Non-veg',
    'Non-veg',
    'Jain',
    'Vegan'
  ];
  DateTime _dateTime = DateTime.now();

  void radioButtonCreDeb(int val) {
    setState(() {
      print(val);
      radio = val;
    });
  }

  void _onDropDownHeightSelected(String newValueSelected) {
    setState(() {
      this._currentHeightSelected = newValueSelected;
    });
  }

  void _onDropDownWeightSelected(String newValueSelected) {
    setState(() {
      this._currentWeightSelected = newValueSelected;
    });
  }

  void _onDropDownBloodGroupsSelected(String newValueSelected) {
    setState(() {
      this._currentBloodGroupsSelected = newValueSelected;
    });
  }

  void _onDropDownComplexionSelected(String newValueSelected) {
    setState(() {
      this._currentComplexionSelected = newValueSelected;
    });
  }

  void _onDropDownDietSelected(String newValueSelected) {
    setState(() {
      this._currentDietSelected = newValueSelected;
    });
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  void _validateInputs(BuildContext context) {
    //checkConnection();

    if (_formKey.currentState.validate()) {
      setState(() {
        _autoValidate = false;
      });

//    If all data are correct then save data to out variables
      _formKey.currentState.save();

      // radio = null;

      registerDetails.insert(0, {
        "height": _currentHeightSelected,
        "weight": _currentWeightSelected,
        "bloodGroup": _currentBloodGroupsSelected,
        "complexion": _currentComplexionSelected,
        "bodyType": radio == 0
            ? 'Slim'
            : (radio == 1 ? 'Average' : (radio == 2 ? 'Athelatic' : "Heavy")),
        "diet": _currentDietSelected,
      });
      print(registerDetails[0]["matchId"]);
      new Future.delayed(new Duration(milliseconds: 500), () {
        print(_keyLoader.currentContext);
        Navigator.of(_keyLoader.currentContext).pop();
      });
      Navigator.of(context).push(MaterialPageRoute(builder: (context) {
        return LoginDetails();
      }));

      _formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      // if (_currentReligion == null) {
      //   _showDialogBox(context, "Message", "Please Choose Religion");
      // }

      // if (_currentCaste == null) {
      //   _showDialogBox(context, "Message", "Please Choose Caste");
      // }

      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Life Style Details "),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.02,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Height : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width: MediaQuery.of(context).size.width * 0.6,
                      margin: EdgeInsets.only(
                          right: MediaQuery.of(context).size.width * 0.02),
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(width: 1.0)),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            itemHeight: 80,
                            elevation: 1,

                            autofocus: true,
                            focusColor: Colors.lightBlue[900],
                            //style: Theme.of(context).textTheme.title,
                            isExpanded: true,
                            hint: Text("Choose a height"),
                            items: _height.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                  value: dropDownStringItem,
                                  child: Text(dropDownStringItem,
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.lightBlue[700])));
                            }).toList(),
                            onChanged: (String newValueSelected) =>
                                _onDropDownHeightSelected(newValueSelected),
                            value: _currentHeightSelected,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Weight : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width: MediaQuery.of(context).size.width * 0.6,
                      margin: EdgeInsets.only(
                          right: MediaQuery.of(context).size.width * 0.02),
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(width: 1.0)),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            itemHeight: 80,
                            elevation: 1,

                            autofocus: true,
                            focusColor: Colors.lightBlue[900],
                            //style: Theme.of(context).textTheme.title,
                            isExpanded: true,
                            hint: Text("Choose a weight"),
                            items: _weight.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                  value: dropDownStringItem,
                                  child: Text(dropDownStringItem,
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.lightBlue[700])));
                            }).toList(),
                            onChanged: (String newValueSelected) =>
                                _onDropDownWeightSelected(newValueSelected),
                            value: _currentWeightSelected,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Blood Group : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width: MediaQuery.of(context).size.width * 0.6,
                      margin: EdgeInsets.only(
                          right: MediaQuery.of(context).size.height * 0.02),
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(width: 1.0)),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            itemHeight: 80,
                            elevation: 1,

                            autofocus: true,
                            focusColor: Colors.lightBlue[900],
                            //style: Theme.of(context).textTheme.title,
                            isExpanded: true,
                            hint: Text("Choose a Blood Group"),
                            items:
                                _bloodGroups.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                  value: dropDownStringItem,
                                  child: Text(dropDownStringItem,
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.lightBlue[700])));
                            }).toList(),
                            onChanged: (String newValueSelected) =>
                                _onDropDownBloodGroupsSelected(
                                    newValueSelected),
                            value: _currentBloodGroupsSelected,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Complexion : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width: MediaQuery.of(context).size.width * 0.6,
                      margin: EdgeInsets.only(
                          right: MediaQuery.of(context).size.width * 0.02),
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(width: 1.0)),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            itemHeight: 80,
                            elevation: 1,

                            autofocus: true,
                            focusColor: Colors.lightBlue[900],
                            //style: Theme.of(context).textTheme.title,
                            isExpanded: true,
                            hint: Text("Choose a Complexion"),
                            items: _complexion.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                  value: dropDownStringItem,
                                  child: Text(dropDownStringItem,
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.lightBlue[700])));
                            }).toList(),
                            onChanged: (String newValueSelected) =>
                                _onDropDownComplexionSelected(newValueSelected),
                            value: _currentComplexionSelected,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01,
                      left: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: Text(
                      "Body Type : ",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Radio(
                    value: 0,
                    groupValue: radio,
                    onChanged: radioButtonCreDeb,
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonCreDeb(0);
                    },
                    child: Container(
                      child: Text(
                        "Slim",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                  Radio(
                    value: 1,
                    groupValue: radio,
                    onChanged: radioButtonCreDeb,
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonCreDeb(1);
                    },
                    child: Container(
                      child: Text(
                        "Average",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                  Radio(
                    value: 2,
                    groupValue: radio,
                    onChanged: radioButtonCreDeb,
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonCreDeb(2);
                    },
                    child: Container(
                      child: Text(
                        "Athelatic",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                  Radio(
                    value: 3,
                    groupValue: radio,
                    onChanged: radioButtonCreDeb,
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonCreDeb(2);
                    },
                    child: Container(
                      child: Text(
                        "Heavy",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Diet : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width: MediaQuery.of(context).size.width * 0.6,
                      margin: EdgeInsets.only(
                          right: MediaQuery.of(context).size.height * 0.02),
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(width: 1.0)),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            itemHeight: 80,
                            elevation: 1,

                            autofocus: true,
                            focusColor: Colors.lightBlue[900],
                            //style: Theme.of(context).textTheme.title,
                            isExpanded: true,
                            hint: Text("Choose a Diet"),
                            items: _diet.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                  value: dropDownStringItem,
                                  child: Text(dropDownStringItem,
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.lightBlue[700])));
                            }).toList(),
                            onChanged: (String newValueSelected) =>
                                _onDropDownDietSelected(newValueSelected),
                            value: _currentDietSelected,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                width: MediaQuery.of(context).size.width * 0.8,
                child: RaisedButton(
                  color: Colors.redAccent[700],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  onPressed: () {
                    waiting(context);
                    _validateInputs(context);
                  },
                  child: Text("Next"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
