import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'PersonDetails.dart';

class AdvanceSearch extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _AdvanceSearch();
  }
}

class _AdvanceSearch extends State<AdvanceSearch> {
  SharedPreferences prefs;
  TextEditingController _ageToController, _ageFromController;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  List religion = ["Hindu"], caste = ["Jangam", "Lingayat", "Any"];
  List advanceSearchData;
  bool _autoValidate = false;
  String _currentReligion,
      _currentCaste,
      _currentDegreeSelected,
      _currentOccupationSelected,
      _currentToHeightSelected,
      _currentFromHeightSelected;
  var _degree = [
    'Bachelors',
    'Doctorate',
    'Masters',
    'Diploma',
    'Under Graduate',
    'Associate Degree',
    'Honnours Degree',
    'Trade School',
    'High School',
    'Less than High School'
  ];
  var _occupation = [
    'Student',
    'non Working',
    'Non-mainstream Professional',
    'Accountant',
    'Acting Professional',
    'Industralist',
    'Actor',
    'Administration Professional',
    'Management Professional',
    'Advertising Professional',
    'Air Hostess',
    'Architect',
    'Artisan',
    'Ayurvedic',
    'Bank Employee',
    'Officer',
    'Beautician',
    'Biologist / Botanist',
    'Business Person',
    'Chartered Accountant',
    'Civil Engineer',
    'Clerical Official',
    'Commercial Pilot',
    'Company Secretary',
    'Computer Professional',
    'Consultant',
    'Contractor',
    'Cost Accountant',
    'Creative Person',
    'Customer Support Professional',
    'Dentist',
    'Designer',
    'Doctor',
    'Economist',
    'Engineer',
    'Engineer(Mechanical)',
    'Engineer(Project)',
    'Entertainment Professionals',
    'Event manager',
    'Executive',
    'Factory Worker',
    'Farmer',
    'Fashion Designer',
    'Finance Professional',
    'Flight Attendet',
    'Government Employee',
    'Government Officer',
    'Health Care professional',
    'Home Maker',
    'Homeopathy',
    'Hotel & Restaurant Professional',
    'Human Resources',
    'IAS/IPS/IRS/IFS',
    'Interior Designer',
    'Investment Professional',
    'Telecom Professional',
    'Journalist',
    'Lawyer',
    'Lecturer',
    'Legal Professional',
    'Manager',
    'Marketing Professional',
    'Media Professional',
    'Medical Transcriptionist',
    'Merchant naval officer',
    'Nurse',
    'Occupational Therapist',
    'Optician',
    'Pharmacist',
    'Physician Assistant',
    'Physicist',
    'Physiotherapist',
    'Pilot',
    'Politician',
    'Production Professional',
    'Professor',
    'Psychologist',
    'Public Relations Professional',
    'Real Estate Professional',
    'Research Scholar',
    'Retired Person',
    'Retail Professional',
    'Sales Professional',
    'Scientist',
    'Self-employed Person',
    'Social Worker',
    'Software Consultant',
    'Sportsman',
    'Teacher',
    'Technician',
    'Training Professional',
    'Transportation',
    'Veterinary Doctor',
    'Volunteer',
    'Writer',
    'Zoologist'
  ];
  var _height = [
    'Below 4ft',
    '4\'1\"ft',
    '4\'2\"f',
    '4\'3\"ft',
    '4\'4\"ft',
    '4\'5\"ft',
    '4\'6\"ft',
    '4\'7\"ft',
    '4\'8\"ft',
    '4\'9\"ft',
    '4\'10\"ft',
    '4\'11\"ft',
    '5\'0\"ft',
    '5\'1\"ft',
    '5\'2\"ft',
    '5\'3\"ft',
    '5\'4\"ft',
    '5\'5\"ft',
    '5\'6\"ft',
    '5\'7\"ft',
    '5\'8\"ft',
    '5\'9\"ft',
    '5\'10\"ft',
    '5\'11\"ft',
    '6\'0\"ft',
    '6\'1\"ft',
    '6\'2\"ft',
    '6\'3\"ft',
    '6\'4\"ft',
    '6\'5\"ft',
    '6\'6\"ft',
    '6\'7\"ft',
    '6\'8\"ft',
    '6\'9\"ft',
    '6\'10\"ft',
    '6\'11\"ft',
    'above 7ft'
  ];

  void _onDropDownFromHeightSelected(String newValueSelected) {
    setState(() {
      this._currentFromHeightSelected = newValueSelected;
    });
  }

  void _onDropDownToHeightSelected(String newValueSelected) {
    setState(() {
      this._currentToHeightSelected = newValueSelected;
    });
  }



  void _onDropDownReligionSelected(String newValueSelected) {
    setState(() {
      this._currentReligion = newValueSelected;
    });
  }

  void _onDropDownCasteSelected(String newValueSelected) {
    setState(() {
      this._currentCaste = newValueSelected;
    });
  }

  void _onDropDownDegreeSelected(String newValueSelected) {
    setState(() {
      this._currentDegreeSelected = newValueSelected;
    });
  }

  void _onDropDownOccupationSelected(String newValueSelected) {
    setState(() {
      this._currentOccupationSelected = newValueSelected;
    });
  }

   Future<String> simpleSearch() async {
     var gender = prefs.getString('gender');
    String url =
        "https://www.veershaivamatch.com/app/searchDetails.php?apicall=advanceSearch";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'ageTo': _ageToController.text,
          'ageFrom': _ageFromController.text,
          'gender': gender == 'Female' ? "Male" : "Female",
          'religion': _currentReligion,
          'caste': _currentCaste,
          'education':_currentDegreeSelected,
          'eduDetail': _currentOccupationSelected,
        });
     setState(() {
      var convertDataToJson = json.decode(response.body);
      advanceSearchData = convertDataToJson['user'];
    });
    print(response.body);

    return "success";
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  void _validateInputs() {
    //checkConnection();
    if (_formKey.currentState.validate() &&
        _currentReligion != null &&
        _currentCaste != null) {
      setState(() {
        _autoValidate = false;
      });
      waiting(context);
//    If all data are correct then save data to out variables
      _formKey.currentState.save();
      simpleSearch().then((value) {
       // radio = null;
        _currentReligion = null;
        _currentCaste = null;
        _currentDegreeSelected = null;
        _currentOccupationSelected = null;
        _currentToHeightSelected = null;
        _currentFromHeightSelected = null;
        _ageToController.text = "";
        _ageFromController.text = "";

        Navigator.of(_keyLoader.currentContext).pop();
        Navigator.of(context).push(MaterialPageRoute(builder: (context){
          return PersonDetails(matchFoundData: advanceSearchData,);
        }));
      });

      _formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      if (_currentReligion == null) {
        _showDialogBox(context, "Message", "Please Choose Religion");
      }

      if (_currentCaste == null) {
        _showDialogBox(context, "Message", "Please Choose Caste");
      }

      setState(() {
        _autoValidate = true;
      });
    }
  }

   Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEmpId();
    _ageToController = TextEditingController(text: "");
    _ageFromController = TextEditingController(text: "");

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Advance Search"),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
                  child: Column(
            children: [
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02),
                    child: Text(
                      "Age : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  
                        Container(
                          padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.1),
                    height: MediaQuery.of(context).size.height * 0.06,
                          width: MediaQuery.of(context).size.width * 0.4,
                          child: TextFormField(
                            autofocus: false,
                            controller: _ageFromController,
                            validator: (value) {
                              if (value == "" || value == "0") {
                                return "Please Enter From Age";
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              labelText: "From",
                              contentPadding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.015),
                            ),
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            keyboardType: TextInputType.numberWithOptions(
                                decimal: true, signed: false),
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                     
                     
                  ),
                  
                        Container(
                          height: MediaQuery.of(context).size.height * 0.06,
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.1),
                          width: MediaQuery.of(context).size.width * 0.4,
                          child: TextFormField(
                            autofocus: false,
                            controller: _ageToController,
                            validator: (value) {
                              if (value == "" || value == "0") {
                                return "Please Enter To Age";
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              labelText: "To",
                              contentPadding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.015),
                            ),
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            keyboardType: TextInputType.numberWithOptions(
                                decimal: true, signed: false),
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        ),
                      
                  
                ],
              ),
              Container(
                 padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.02,
                          right: MediaQuery.of(context).size.width * 0.02),
                child: Row(
                  children: [
                    Container(
                     
                      child: Text(
                        "Height : ",
                        style: TextStyle(fontSize: 16, ),
                      ),
                    ),
                    Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.3,
                        margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.1),
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.lightBlue[900],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("From height"),
                              items: _height.map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.lightBlue[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownFromHeightSelected(newValueSelected),
                              value: _currentFromHeightSelected,
                            ),
                          ),
                        ),
                      ),
                    Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.3,
                        margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.1),
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.lightBlue[900],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("To height"),
                              items: _height.map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.lightBlue[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownToHeightSelected(newValueSelected),
                              value: _currentToHeightSelected,
                            ),
                          ),
                        ),
                      )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.02,
                          right: MediaQuery.of(context).size.width * 0.02),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                     
                      child: Text(
                        "Religion : ",
                        style: TextStyle(fontSize: 16,),
                      ),
                    ),
                   
                     
                          Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            width: MediaQuery.of(context).size.width * 0.7,
                            decoration: BoxDecoration(
                              border: Border(bottom: BorderSide(width: 1.0)),
                            ),
                            child: DropdownButtonHideUnderline(
                              child: ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton<String>(
                                  itemHeight: 80,
                                  elevation: 1,

                                  autofocus: true,
                                  focusColor: Colors.lightBlue[900],
                                  //style: Theme.of(context).textTheme.title,
                                  isExpanded: true,
                                  hint: Text("Religion"),
                                  items: religion == null
                                      ? null
                                      : religion.map((dropDownStringItem) {
                                          return DropdownMenuItem<String>(
                                              value:
                                                  dropDownStringItem
                                                      .toString(),
                                              child: Text(
                                                  dropDownStringItem,
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      fontWeight: FontWeight.normal,
                                                      color:
                                                          Colors.lightBlue[700])));
                                        }).toList(),
                                  onChanged: (newValueSelected) =>
                                      _onDropDownReligionSelected(newValueSelected),
                                  value: _currentReligion,
                                ),
                              ),
                            ),
                          )
                       
                   
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.02,
                          right: MediaQuery.of(context).size.width * 0.02),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      
                      child: Text(
                        "Caste : ",
                        style: TextStyle(fontSize: 16,),
                      ),
                    ),
                    
                          Container(
                            height: MediaQuery.of(context).size.height * 0.05,
                            width: MediaQuery.of(context).size.width * 0.7,
                            decoration: BoxDecoration(
                              border: Border(bottom: BorderSide(width: 1.0)),
                            ),
                            child: DropdownButtonHideUnderline(
                              child: ButtonTheme(
                                alignedDropdown: true,
                                child: DropdownButton<String>(
                                  itemHeight: 80,
                                  elevation: 1,

                                  autofocus: true,
                                  focusColor: Colors.lightBlue[900],
                                  //style: Theme.of(context).textTheme.title,
                                  isExpanded: true,
                                  hint: Text("Caste"),
                                  items: caste == null
                                      ? null
                                      : caste.map((dropDownStringItem) {
                                          return DropdownMenuItem<String>(
                                              value:
                                                  dropDownStringItem
                                                      .toString(),
                                              child: Text(
                                                  dropDownStringItem,
                                                  style: TextStyle(
                                                      fontSize: 17,
                                                      fontWeight: FontWeight.normal,
                                                      color:
                                                          Colors.lightBlue[700])));
                                        }).toList(),
                                  onChanged: (newValueSelected) =>
                                      _onDropDownCasteSelected(newValueSelected),
                                  value: _currentCaste,
                                ),
                              ),
                            ),
                          )
                        
                    
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.02,
                          right: MediaQuery.of(context).size.width * 0.02),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Education : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    // SizedBox(
                    //   width: MediaQuery.of(context).size.height * 0.1,
                    // ),

                    Container(
                      height: MediaQuery.of(context).size.height * 0.05,
                      width: MediaQuery.of(context).size.width * 0.7,
                      
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(width: 1.0)),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            itemHeight: 80,
                            elevation: 1,

                            autofocus: true,
                            focusColor: Colors.lightBlue[900],
                            //style: Theme.of(context).textTheme.title,
                            isExpanded: true,
                            hint: Text("Choose a Degree"),
                            items: _degree.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                  value: dropDownStringItem,
                                  child: Text(dropDownStringItem,
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.lightBlue[700])));
                            }).toList(),
                            onChanged: (String newValueSelected) =>
                                _onDropDownDegreeSelected(newValueSelected),
                            value: _currentDegreeSelected,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
               padding: EdgeInsets.only(
                          left: MediaQuery.of(context).size.width * 0.02,
                          right: MediaQuery.of(context).size.width * 0.02),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      
                      child: Text(
                        "Occupation : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    // SizedBox(
                    //     width: MediaQuery.of(context).size.height * 0.1,
                    //   ),

                    Container(
                      height: MediaQuery.of(context).size.height * 0.05,
                      width: MediaQuery.of(context).size.width * 0.7,
                     
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(width: 1.0)),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            itemHeight: 80,
                            elevation: 1,

                            autofocus: true,
                            focusColor: Colors.lightBlue[900],
                            //style: Theme.of(context).textTheme.title,
                            isExpanded: true,
                            hint: Text("Choose a Occupation"),
                            items: _occupation.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                  value: dropDownStringItem,
                                  child: Text(dropDownStringItem,
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.lightBlue[700])));
                            }).toList(),
                            onChanged: (String newValueSelected) =>
                                _onDropDownOccupationSelected(newValueSelected),
                            value: _currentOccupationSelected,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                width: MediaQuery.of(context).size.width * 0.8,
                child: RaisedButton(
                  color: Colors.redAccent[700],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  onPressed: () {
                    // Navigator.of(context).push(MaterialPageRoute(builder: (context){
                    //   return CareerDetailsPage();
                    // }));
                    _validateInputs();
                  },
                  child: Text("Search"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
