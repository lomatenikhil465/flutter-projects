import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PersonDetails extends StatefulWidget {
  final List matchFoundData;

  PersonDetails({
    this.matchFoundData,
  });
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _PersonDetails();
  }
}

class _PersonDetails extends State<PersonDetails> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Matches Found"),
        backgroundColor: Colors.redAccent[700],
      ),
      body: widget.matchFoundData == null
          ? Center(
              child: Text("No Match Found"),
            )
          : ListView.separated(
              separatorBuilder: (BuildContext context, int index) => Divider(
                thickness: 0.0,
                color: Colors.lightBlue[900],
              ),
              itemCount: widget.matchFoundData == null
                  ? 0
                  : widget.matchFoundData.length,
              itemBuilder: (BuildContext context, int index) {
                return Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.15,
                          width: MediaQuery.of(context).size.height * 0.12,
                          margin: EdgeInsets.only(
                            left: MediaQuery.of(context).size.height * 0.01,
                          ),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10.0),
                              border: Border.all(color: Colors.grey, width: 1)),
                          child: Image.network(
                            "https://www.veershaivamatch.com/CP/" +
                                (widget.matchFoundData[index]["photo"] == null ? "" : widget.matchFoundData[index]["photo"].toString()),
                            loadingBuilder: (context, child, progress) {
                              return progress == null
                                  ? child
                                  : LinearProgressIndicator();
                            },
                            fit: BoxFit.contain,
                          ),
                        ),
                        Expanded(
                          child: Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02),
                                      child: Text(
                                        widget.matchFoundData[index]["CustID"]
                                            .toString(),
                                        softWrap: true,
                                        style: TextStyle(
                                          color: Colors.redAccent[700],
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02),
                                      child: Text(
                                        widget.matchFoundData[index]["bi_name"],
                                        overflow: TextOverflow.clip,
                                        softWrap: true,
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02),
                                      child: Text(
                                        widget.matchFoundData[index]["bi_age"] +
                                            " years " +
                                            widget.matchFoundData[index]
                                                ["phs_height"],
                                        softWrap: true,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02),
                                      child: Text(
                                        widget.matchFoundData[index]
                                                ["so_religion"] +
                                            " " +
                                            widget.matchFoundData[index]
                                                ["so_caste"] +
                                            " " +
                                            widget.matchFoundData[index]
                                                ["so_subcaste"],
                                        softWrap: true,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02),
                                      child: Text(
                                        widget.matchFoundData[index]
                                                ["bi_district"] +
                                            "," +
                                            widget.matchFoundData[index]
                                                ["bi_state"] +
                                            "," +
                                            widget.matchFoundData[index]
                                                ["country"],
                                        softWrap: true,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  Flexible(
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.02),
                                      child: Text(
                                        widget.matchFoundData[index]
                                            ["ed_details"],
                                        softWrap: true,
                                        style: TextStyle(
                                            color: Colors.redAccent[700]),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                );
              },
            ),
    );
  }
}
