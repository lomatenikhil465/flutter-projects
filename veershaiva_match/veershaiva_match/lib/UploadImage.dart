import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:veershaiva_match/FamilyDetailsPage.dart';

class UploadImage extends StatefulWidget {
  final String matchId;

  UploadImage({this.matchId});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _UploadImage();
  }
}

class _UploadImage extends State<UploadImage> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  File _image;

  final picker = ImagePicker();

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  Future getImage(BuildContext context) async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _image = File(pickedFile.path);
        final kb = _image.lengthSync() / 1024;
        print(kb / 1024);
        if (kb / 1024 > 2.0) {
          _image = null;
          _showDialogBox(
              context, "Error!", "File size must be less than 2.0 MB");
        }
      } else {
        print('No image selected.');
      }
    });
  }

  Future<String> uploadImage() async {
    String url =
        "https://www.veershaivamatch.com/app/uploadImage.php?apicall=uploadpic";
    String base64Image = base64Encode(_image.readAsBytesSync());
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'custId': widget.matchId,
          'pic': base64Image,
        });
    //  setState(() {
    //   var convertDataToJson = json.decode(response.body);
    //   homeSearchData = convertDataToJson['user'];
    // });
    print(response.body);
    return "success";
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Upload Image"),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.05),
              child: Center(
                child: GestureDetector(
                  onTap: () {
                    getImage(context);
                  },
                  child: CircleAvatar(
                    radius: 100,
                    backgroundColor: Color(0xffFDCF09),
                    child: _image != null
                        ? ClipRRect(
                            borderRadius: BorderRadius.circular(100),
                            child: Image.file(
                              _image,
                              width: 300,
                              height: 300,
                              fit: BoxFit.fitHeight,
                            ),
                          )
                        : Container(
                            decoration: BoxDecoration(
                                color: Colors.grey[200],
                                borderRadius: BorderRadius.circular(100)),
                            width: 300,
                            height: 300,
                            child: Icon(
                              Icons.camera_alt,
                              color: Colors.grey[800],
                            ),
                          ),
                  ),
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.8,
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.05),
              child: RaisedButton(
                onPressed: () {
                  waiting(context);
                  uploadImage().then((value) {
                    Navigator.of(_keyLoader.currentContext).pop();
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return FamilyDetailsPage();
                    }));
                  });
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                color: Colors.redAccent[700],
                textColor: Colors.white,
                child: Text("UPLOAD"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
