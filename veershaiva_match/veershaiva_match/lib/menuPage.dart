import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:marquee/marquee.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:veershaiva_match/AboutUsPage.dart';
import 'package:veershaiva_match/ContactUsPage.dart';
import 'package:veershaiva_match/EditProfile.dart';
import 'package:veershaiva_match/EnquiryForm.dart';
import 'package:veershaiva_match/FamilyDetailsPage.dart';
import 'package:veershaiva_match/MemberLogin.dart';
import 'package:veershaiva_match/MembershipPlans.dart';
import 'package:veershaiva_match/PersonDetails.dart';
import 'package:veershaiva_match/RulesPage.dart';
import 'package:veershaiva_match/SignUpPage.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class MenuPage extends StatefulWidget {
  final int sharedPreferenceStatus;
  MenuPage({this.sharedPreferenceStatus});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MenuPage();
  }
}

class _MenuPage extends State<MenuPage> {
  TextEditingController _ageFromController, _ageToController;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  int radio;
  List religion = ["Hindu"], caste = ["Jangam", "Lingayat", "Any"];
  List homeSearchData, homeImagesData;
  SharedPreferences prefs;
  String _currentReligion, _currentCaste;
  bool _autoValidate = false;

  Future<String> getJsonData() async {
    String url =
        "https://www.veershaivamatch.com/app/getData.php?apicall=homeImages";
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      homeImagesData = convertDataToJson['data'];
    });

    return "success";
  }

  void radioButtonCreDeb(int val) {
    setState(() {
      print(val);
      radio = val;
    });
  }

  void _onDropDownReligionSelected(String newValueSelected) {
    setState(() {
      this._currentReligion = newValueSelected;
    });
  }

  void _onDropDownCasteSelected(String newValueSelected) {
    setState(() {
      this._currentCaste = newValueSelected;
    });
  }

  Future<String> homeSearch() async {
    String url =
        "https://www.veershaivamatch.com/app/searchDetails.php?apicall=homeSearch";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'ageTo': _ageToController.text,
          'ageFrom': _ageFromController.text,
          'gender': radio == 0 ? "Male" : "Female",
          'religion': _currentReligion,
          'caste': _currentCaste
        });
    setState(() {
      var convertDataToJson = json.decode(response.body);
      homeSearchData = convertDataToJson['user'];
    });
    //print(response.reasonPhrase);

    return "success";
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  void _validateInputs() {
    //checkConnection();
    if (_formKey.currentState.validate() &&
        _currentReligion != null &&
        _currentCaste != null) {
      setState(() {
        _autoValidate = false;
      });
      waiting(context);
//    If all data are correct then save data to out variables
      _formKey.currentState.save();
      homeSearch().then((value) {
        radio = null;
        _currentReligion = null;
        _currentCaste = null;

        _ageToController.text = "";
        _ageFromController.text = "";
        print(_keyLoader.currentContext);
        Navigator.of(_keyLoader.currentContext).pop();
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return PersonDetails(
            matchFoundData: homeSearchData,
          );
        }));
      });

      _formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      if (_currentReligion == null) {
        _showDialogBox(context, "Message", "Please Choose Religion");
      }

      if (_currentCaste == null) {
        _showDialogBox(context, "Message", "Please Choose Caste");
      }

      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getJsonData();
    _ageToController = TextEditingController(text: "");
    _ageFromController = TextEditingController(text: "");
    radio = null;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        backgroundColor: Colors.redAccent[700],
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.15,
              child: DrawerHeader(
                  decoration: BoxDecoration(
                    color: Colors.redAccent[700],
                  ),
                  child: Container(
                    alignment: Alignment.bottomLeft,
                    child: Text(
                      "Hi, Guest!",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.normal),
                    ),
                  )),
            ),
            ListTile(
              leading: Icon(Icons.info),
              title: Text("About  US",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return AboutUsPage();
                }));
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.person),
              title: Text("Free SignUp", // registerApp.php  case: CustId
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () {
                //SignUpPage
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return SignUpPage();
                }));
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.open_in_browser),
              title: Text("Member Login", // registerApp.php  case: login
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return MemberLogin();
                }));
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.local_offer),
              title: Text("Membership Plans",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return MembershipPlans();
                }));
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.contact_mail),
              title: Text("Contact Us",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return ContactUsPage();
                }));
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.description),
              title: Text("Rules And Regulations",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return RulesPage();
                }));
              },
            ),
            Divider(thickness: 1, height: 0),
            ListTile(
              leading: Icon(Icons.subtitles),
              title: Text("Enquiry Form",
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.normal)),
              onTap: () {
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) {
                  return EnquiryForm();
                }));
              },
            ),
          ],
        ),
      ),
      body: Form(
        autovalidate: _autoValidate,
        key: _formKey,
        child: Container(
          child: Column(
            children: [
              homeImagesData == null
                  ? CircularProgressIndicator()
                  : SizedBox(
                      height: 200.0,
                      //width: 300.0,
                      child: Carousel(
                        dotSize: 4.0,
                        dotSpacing: 15.0,
                        dotColor: Colors.redAccent[700],
                        indicatorBgPadding: 5.0,
                        dotBgColor: Colors.purple.withOpacity(0.5),
                        images: homeImagesData.map((e) {
                          return NetworkImage(
                            "https://www.veershaivamatch.com/CP/" +
                                e["imgpath"],
                          );
                        }).toList(),
                      )),
              Row(
                children: [
                  Container(
                    height: 35,
                    width: MediaQuery.of(context).size.width * 1,
                    child: Marquee(
                      text:
                          'Welcome To The Only Matrimony Portal For Lingayat And Jangam Samaj Contact No : +91 9422370687 , +91 8149070687 , +91 7588214687',
                      style: TextStyle(fontWeight: FontWeight.bold),
                      scrollAxis: Axis.horizontal,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      blankSpace: 20.0,
                      velocity: 100.0,
                      startPadding: 10.0,
                      accelerationDuration: Duration(seconds: 1),
                      accelerationCurve: Curves.linear,
                      decelerationDuration: Duration(milliseconds: 500),
                      decelerationCurve: Curves.easeOut,
                    ),
                  ),
                ],
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02),
                    child: Text(
                      "Gender : ",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Radio(
                    value: 0,
                    groupValue: radio,
                    onChanged: radioButtonCreDeb,
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonCreDeb(0);
                    },
                    child: Container(
                      child: Text(
                        "Male",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                  Radio(
                    value: 1,
                    groupValue: radio,
                    onChanged: radioButtonCreDeb,
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonCreDeb(1);
                    },
                    child: Container(
                      child: Text(
                        "Female",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02),
                    child: Text(
                      "Age : ",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.1),
                    height: MediaQuery.of(context).size.height * 0.06,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.3,
                          child: TextFormField(
                            autofocus: false,
                            controller: _ageFromController,
                            validator: (value) {
                              if (value == "" || value == "0") {
                                return "Please Enter From Age";
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              labelText: "From",
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            keyboardType: TextInputType.numberWithOptions(
                                decimal: true, signed: false),
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.06,
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.1),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.3,
                          child: TextFormField(
                            autofocus: false,
                            controller: _ageToController,
                            validator: (value) {
                              if (value == "" || value == "0") {
                                return "Please Enter To Age";
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              labelText: "To",
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            keyboardType: TextInputType.numberWithOptions(
                                decimal: true, signed: false),
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02),
                    child: Text(
                      "Religion : ",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.05,
                          width: MediaQuery.of(context).size.width * 0.7,
                          decoration: BoxDecoration(
                            border: Border(bottom: BorderSide(width: 1.0)),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButton<String>(
                                itemHeight: 80,
                                elevation: 1,

                                autofocus: true,
                                focusColor: Colors.lightBlue[900],
                                //style: Theme.of(context).textTheme.title,
                                isExpanded: true,
                                hint: Text("Religion"),
                                items: religion.map((dropDownStringItem) {
                                  return DropdownMenuItem<String>(
                                      value: dropDownStringItem,
                                      child: Text(dropDownStringItem,
                                          style: TextStyle(
                                              fontSize: 17,
                                              fontWeight: FontWeight.normal,
                                              color: Colors.lightBlue[700])));
                                }).toList(),
                                onChanged: (newValueSelected) =>
                                    _onDropDownReligionSelected(
                                        newValueSelected),
                                value: _currentReligion,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02),
                    child: Text(
                      "Caste : ",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.05,
                          width: MediaQuery.of(context).size.width * 0.7,
                          decoration: BoxDecoration(
                            border: Border(bottom: BorderSide(width: 1.0)),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButton<String>(
                                itemHeight: 80,
                                elevation: 1,

                                autofocus: true,
                                focusColor: Colors.lightBlue[900],
                                //style: Theme.of(context).textTheme.title,
                                isExpanded: true,
                                hint: Text("Caste"),
                                items: caste == null
                                    ? null
                                    : caste.map((dropDownStringItem) {
                                        return DropdownMenuItem<String>(
                                            value: dropDownStringItem,
                                            child: Text(dropDownStringItem,
                                                style: TextStyle(
                                                    fontSize: 17,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    color: Colors
                                                        .lightBlue[700])));
                                      }).toList(),
                                onChanged: (newValueSelected) =>
                                    _onDropDownCasteSelected(newValueSelected),
                                value: _currentCaste,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    RaisedButton(
                      color: Colors.redAccent[700],
                      elevation: 3.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      textColor: Colors.white,
                      onPressed: () {
                        FocusScope.of(context).requestFocus(new FocusNode());
                        _validateInputs();
                      },
                      child: Text("Search"),
                    ),
                    RaisedButton(
                      color: Colors.redAccent[700],
                      elevation: 3.0,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0)),
                      textColor: Colors.white,
                      onPressed: () {
                        FocusScope.of(context).requestFocus(new FocusNode());

                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) {
                          return SignUpPage();
                        }));
                      },
                      child: Text("Register"),
                    )
                  ],
                ),
              ),
              RaisedButton(
                color: Colors.redAccent[700],
                elevation: 3.0,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0)),
                textColor: Colors.white,
                onPressed: () {
                  FocusScope.of(context).requestFocus(new FocusNode());
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return MemberLogin();
                  }));
                },
                child: Text("Member Login"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
