import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:veershaiva_match/UploadImage.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'SignUpPage.dart';

class LoginDetails extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LoginDetails();
  }
}

class _LoginDetails extends State<LoginDetails> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String email, pass, phone;
  int radio;
  bool _obscureText = true;

  void radioButtonCreDeb(int val) {
    setState(() {
      print(val);
      radio = val;
    });
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  Future<String> insertPersonal() async {
    String url =
        "https://www.veershaivamatch.com/app/registerApp.php?apicall=signup";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'custId': registerDetails[0]["matchId"],
          'name': registerDetails[0]["name"],
          'email': registerDetails[0]["email"],
          'pwd': registerDetails[0]["pass"],
          'gender': registerDetails[0]["gender"],
          'age': registerDetails[0]["age"],
          'place': registerDetails[0]["placeOfBirth"],
          'birthTime': registerDetails[0]["timeOfBirth"],
          'country': registerDetails[0]["country"],
          'state': registerDetails[0]["state"],
          'district': registerDetails[0]["city"],
          'education': registerDetails[0]["degree"],
          'eduDetail': registerDetails[0]["eduDetails"],
          'occupation': registerDetails[0]["occupation"],
          'employed': registerDetails[0]["employeedIn"],
          'religion': registerDetails[0]["religion"],
          'caste': registerDetails[0]["caste"],
          'subcaste': registerDetails[0]["subcaste"],
          'gouthram': registerDetails[0]["gouthram"],
          'motherLang': registerDetails[0]["motherTongue"],
          'knownLang': registerDetails[0]["knownLang"],
          'star': registerDetails[0]["nakshatra"],
          'moonsign': registerDetails[0]["moonsign"],
          'horoscope': registerDetails[0]["horoscope"],
          'height': registerDetails[0]["height"],
          'weight': registerDetails[0]["weight"],
          'bloodgroup': registerDetails[0]["bloodGroup"],
          'complexion': registerDetails[0]["complexion"],
          'bodyType': registerDetails[0]["bodyType"],
          'diet': registerDetails[0]["diet"],
          'mobile': registerDetails[0]["phone"],
          'dob': registerDetails[0]["dob"],
          'income': registerDetails[0]["income"],
        });
    //  setState(() {
    //   var convertDataToJson = json.decode(response.body);
    //   homeSearchData = convertDataToJson['user'];
    // });
    print(response.body);
    return "success";
  }

  void _validateInputs(BuildContext context) {
    //checkConnection();
    waiting(context);
    if (_formKey.currentState.validate()) {
      setState(() {
        _autoValidate = false;
      });

//    If all data are correct then save data to out variables
      _formKey.currentState.save();

      // radio = null;

      registerDetails.insert(0, {
        "email": email,
        "pass": pass,
        "phone": phone,
      });
      insertPersonal().then((value) {
        print(registerDetails[0]["matchId"]);
        Navigator.of(_keyLoader.currentContext).pop();
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return UploadImage(
            matchId: registerDetails[0]["matchId"],
          );
        }));
      });

      _formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      // if (_currentReligion == null) {
      //   _showDialogBox(context, "Message", "Please Choose Religion");
      // }

      // if (_currentCaste == null) {
      //   _showDialogBox(context, "Message", "Please Choose Caste");
      // }

      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Personal Details "),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.02,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  children: [
                    Container(
                      child: Text(
                        "Email Id : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.94,
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Email Id';
                          }
                          return null;
                        },
                        onSaved: (String email) {
                          this.email = email;
                        },
                        decoration: InputDecoration(
                            hintText: "Email",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left:
                                    MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.emailAddress,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  children: [
                    Container(
                      child: Text(
                        "Password : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.94,
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Password';
                          }
                          return null;
                        },
                        onSaved: (String pass) {
                          this.pass = pass;
                        },
                        obscureText: _obscureText,
                        decoration: InputDecoration(
                          hintText: "Password",
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                _obscureText = !_obscureText;
                              });
                            },
                            child: Icon(
                              _obscureText
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.redAccent[700],
                            ),
                          ),
                          contentPadding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.015,
                              left: MediaQuery.of(context).size.width * 0.015),
                        ),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  children: [
                    Container(
                      child: Text(
                        "Phone no. : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.94,
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Phone no.';
                          }
                          return null;
                        },
                        onSaved: (String phone) {
                          this.phone = phone;
                        },
                        decoration: InputDecoration(
                            hintText: "Phone no.",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.015,
                              left: MediaQuery.of(context).size.width * 0.015,
                            )),
                        keyboardType: TextInputType.numberWithOptions(
                            signed: false, decimal: true),
                        inputFormatters: [
                          LengthLimitingTextInputFormatter(10),
                          new WhitelistingTextInputFormatter(RegExp("[0-9]"))
                        ],
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                width: MediaQuery.of(context).size.width * 0.8,
                child: RaisedButton(
                  color: Colors.redAccent[700],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  onPressed: () {
                    _validateInputs(context);
                  },
                  child: Text("Save"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
