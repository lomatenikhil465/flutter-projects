import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
//import 'package:mvkk/pages/UserPageController.dart';
//import '../pageController.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:veershaiva_match/EditProfile.dart';
import 'package:veershaiva_match/menuPage.dart';
//import '../LoginUi.dart';
//import 'AfterLoginPageController.dart';


class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SplashScreenState();
  }
}

class _SplashScreenState extends State<SplashScreen> {
  SharedPreferences prefs;

  @override
  void initState() {
    super.initState();

    _mockCheckForSession();
  }

  Future<bool> _mockCheckForSession() async {
    Timer(Duration(seconds: 5), _navigateToHome);
    prefs = await SharedPreferences.getInstance();
    return true;
  }

  void _navigateToHome() {
    var mobile = prefs.getString('mobile');
    var pass = prefs.getString('pass');
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (BuildContext context) {
      if (mobile == null && pass == null) {
        return MenuPage(sharedPreferenceStatus: 0,);
      } else {
        return EditProfile();
      }
    }));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
        body: Container(
          color: Colors.yellow[50],
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Opacity(
            opacity: 1,
            child:Image.asset(
              'assets\\images\\logo1.png',
              fit: BoxFit.cover,
              )
          ),
          
        ],
      ),
    ));
  }
}
