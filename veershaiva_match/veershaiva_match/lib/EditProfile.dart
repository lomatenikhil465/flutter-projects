import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:veershaiva_match/AdvanceSearch.dart';
import 'package:veershaiva_match/MembershipPlans.dart';
import 'package:veershaiva_match/MySubscriptions.dart';
import 'package:veershaiva_match/SearchBy.dart';
import 'package:veershaiva_match/SimpleSearch.dart';
import 'package:veershaiva_match/menuPage.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'ChangePassword.dart';
import 'EditPreference.dart';

class EditProfile extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _EditProfile();
  }
}

List basicData;

class _EditProfile extends State<EditProfile> {
  SharedPreferences prefs;
  List familyData;
  Future<String> basicResponse;

  Future<String> getBasicData() async {
    var id = prefs.getString('id');
    String url =
        "https://www.veershaivamatch.com/app/getDetails.php?apicall=basic";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'custId': id,
        });
    setState(() {
      var convertDataToJson = json.decode(response.body);
      basicData = convertDataToJson['user'];
    });
    //print(response.reasonPhrase);

    return "success";
  }

  Future<String> getFamilyData() async {
    var id = prefs.getString('id');
    String url =
        "https://www.veershaivamatch.com/app/getDetails.php?apicall=family";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'custId': id,
        });
    setState(() {
      var convertDataToJson = json.decode(response.body);
      familyData = convertDataToJson['user'];
    });
    print(response.body);

    return "success";
  }

  Future _showDialogBox(
      BuildContext context, String title, String content) async {
    return (await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () async {
                    SharedPreferences prefs =
                        await SharedPreferences.getInstance();
                    Navigator.of(context).pop();
                    prefs.remove('mobile');
                    prefs.remove('pass');
                    prefs.remove('id');
                    prefs.remove('gender');
                    Navigator.of(context)
                        .pushReplacement(MaterialPageRoute(builder: (context) {
                      return MenuPage();
                    }));
                  },
                  child: Text("Yes")),
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("No"))
            ],
          );
        }));
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    basicData = new List();

    getEmpId().then((value) {
      basicResponse = this.getBasicData();
      getFamilyData();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Edit Profile"),
          backgroundColor: Colors.redAccent[700],
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: [
              DrawerHeader(
                  decoration: BoxDecoration(color: Colors.redAccent[700]),
                  child: Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            "Hi, " +
                                (basicData == null || basicData.length == 0
                                    ? ""
                                    : basicData[0]["bi_name"].toString()),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.normal),
                          ),
                        ],
                      ),
                      GestureDetector(
                        onTap: () {
                          // Navigator.of(context)
                          //     .push(MaterialPageRoute(builder: (context) {
                          //   return ProfilePage();
                          // }));
                        },
                        child: Center(
                          child: Image.asset(
                            "assets\\images\\profile.png",
                            height: 100,
                          ),
                        ),
                      ),
                    ],
                  )),
              ListTile(
                leading: Icon(Icons.note),
                title: Text("Edit Preference",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.grey,
                        fontWeight: FontWeight.normal)),
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return EditPreference();
                  }));
                },
              ),
              Divider(thickness: 1, height: 0),
              ListTile(
                leading: Icon(Icons.local_offer),
                title: Text("Membership Plans", // registerApp.php  case: CustId
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.grey,
                        fontWeight: FontWeight.normal)),
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return MembershipPlans();
                  }));
                },
              ),
              Divider(thickness: 1, height: 0),
              ListTile(
                leading: Icon(Icons.contacts), //contacts
                title: Text("My Subscriptions", // registerApp.php  case: login
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.grey,
                        fontWeight: FontWeight.normal)),
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return MySubscriptions();
                  }));
                },
              ),
              Divider(thickness: 1, height: 0),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01,
                    left: MediaQuery.of(context).size.width * 0.05),
                child: Text(
                  "Search",
                  style: TextStyle(fontSize: 18),
                ),
              ),
              ListTile(
                leading: Icon(Icons.youtube_searched_for),
                title: Text("Simple Search",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.grey,
                        fontWeight: FontWeight.normal)),
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return SimpleSearch();
                  }));
                },
              ),
              Divider(thickness: 1, height: 0),
              ListTile(
                leading: Icon(Icons.search),
                title: Text("Search By Id",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.grey,
                        fontWeight: FontWeight.normal)),
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return SearchByPage();
                  }));
                },
              ),
              Divider(thickness: 1, height: 0),
              ListTile(
                leading: Icon(Icons.filter_list),
                title: Text("Advance Search",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.grey,
                        fontWeight: FontWeight.normal)),
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return AdvanceSearch();
                  }));
                },
              ),
              Divider(thickness: 1, height: 0),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01,
                    left: MediaQuery.of(context).size.width * 0.05),
                child: Text(
                  "Other",
                  style: TextStyle(fontSize: 18),
                ),
              ),
              ListTile(
                leading: Icon(Icons.lock_outline),
                title: Text("Change Password",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.grey,
                        fontWeight: FontWeight.normal)),
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return ChangePassword();
                  }));
                },
              ),
              Divider(thickness: 1, height: 0),
              ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text("Logout",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.grey,
                        fontWeight: FontWeight.normal)),
                onTap: () {
                  _showDialogBox(context, "Message", "Do you want to logout?");
                },
              ),
            ],
          ),
        ),
        body: FutureBuilder<String>(
          future: basicResponse,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      height: 200,
                      child: Center(
                          child: Image.network(
                        "https://www.veershaivamatch.com/CP/" +
                            (basicData == null
                                ? ""
                                : basicData[0]["photo"].toString()),
                        loadingBuilder: (context, child, progress) {
                          return progress == null
                              ? child
                              : LinearProgressIndicator();
                        },
                      )),
                    ),
                    Container(
                      //width: MediaQuery.of(context).size.width,
                      child: Card(
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01),
                              child: Row(
                                children: [
                                  Text(
                                    "Personal Information",
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.green[600]),
                                  ),
                                ],
                              ),
                            ),
                            Divider(
                              thickness: 1,
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.01),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.description,
                                    color: Colors.redAccent,
                                  ),
                                  Text(
                                    "Basic Details",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Spacer(),
                                  IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: () {},
                                    iconSize: 20,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Match ID",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                    basicData[0]["CustID"] == null
                                        ? ""
                                        : basicData[0]["CustID"],
                                    style: TextStyle(color: Colors.orange),
                                  ))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Name",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                              basicData[0]["bi_name"] == null
                                                  ? ""
                                                  : basicData[0]["bi_name"]))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Gender",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                              basicData[0]["bi_gender"] == null
                                                  ? ""
                                                  : basicData[0]
                                                      ["bi_gender"]))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Age",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                              basicData[0]["bi_age"] == null
                                                  ? ""
                                                  : basicData[0]["bi_age"] +
                                                      " Yrs"))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Height",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                              basicData[0]["phs_height"] == null
                                                  ? ""
                                                  : basicData[0]
                                                      ["phs_height"]))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Weight",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                              basicData[0]["phs_weight"] == null
                                                  ? ""
                                                  : basicData[0]
                                                      ["phs_weight"]))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Body Type",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                              basicData[0]["bodytype"] == null
                                                  ? ""
                                                  : basicData[0]["bodytype"]))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Complexion",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(basicData[0]
                                                      ["phs_complexion"] ==
                                                  null
                                              ? ""
                                              : basicData[0]
                                                  ["phs_complexion"]))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Martial Status",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                              basicData[0]["so_marry"] == null
                                                  ? ""
                                                  : basicData[0]["so_marry"]))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Mother Tongue",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(basicData[0]
                                                      ["so_mothertongue"] ==
                                                  null
                                              ? ""
                                              : basicData[0]
                                                  ["so_mothertongue"]))),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      //width: MediaQuery.of(context).size.width,
                      child: Card(
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.01),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.contact_phone,
                                    color: Colors.orange[400],
                                  ),
                                  Text(
                                    "Contact Details",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Spacer(),
                                  IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: () {},
                                    iconSize: 20,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Mobile No.",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                    basicData[0]["mobile"] == null
                                        ? ""
                                        : basicData[0]["mobile"],
                                  ))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Email Address",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                              basicData[0]["email"] == null
                                                  ? ""
                                                  : basicData[0]["email"]))),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      //width: MediaQuery.of(context).size.width,
                      child: Card(
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.01),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.spa,
                                    color: Colors.blue[400],
                                  ),
                                  Text(
                                    "Religious Information",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Spacer(),
                                  IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: () {},
                                    iconSize: 20,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Religion",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                    basicData[0]["so_religion"] == null
                                        ? ""
                                        : basicData[0]["so_religion"],
                                  ))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Caste",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                              basicData[0]["so_caste"] == null
                                                  ? ""
                                                  : basicData[0]["so_caste"]))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Gotharam",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                              basicData[0]["so_gothram"] == null
                                                  ? ""
                                                  : basicData[0]
                                                      ["so_gothram"]))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Nakshatra",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(basicData[0]
                                                      ["so_nakshatra"] ==
                                                  null
                                              ? ""
                                              : basicData[0]["so_nakshatra"]))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Raasi",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(basicData[0]
                                                      ["so_moonsign"] ==
                                                  null
                                              ? ""
                                              : basicData[0]["so_moonsign"]))),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      //width: MediaQuery.of(context).size.width,
                      child: Card(
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.01),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.school,
                                    color: Colors.blue[400],
                                  ),
                                  Text(
                                    "Professional Information",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Spacer(),
                                  IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: () {},
                                    iconSize: 20,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Education category",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                    basicData[0]["ed_details"] == null
                                        ? ""
                                        : basicData[0]["ed_details"],
                                  ))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Occupation",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(basicData[0]
                                                      ["ed_occupation"] ==
                                                  null
                                              ? ""
                                              : basicData[0]
                                                  ["ed_occupation"]))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Employeed In",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(basicData[0]
                                                      ["ed_employed"] ==
                                                  null
                                              ? ""
                                              : basicData[0]["ed_employed"]))),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      //width: MediaQuery.of(context).size.width,
                      child: Card(
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.01),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.location_on,
                                    color: Colors.pink,
                                  ),
                                  Text(
                                    "Location",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Spacer(),
                                  IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: () {},
                                    iconSize: 20,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Country",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                    basicData[0]["country"] == null
                                        ? ""
                                        : basicData[0]["country"],
                                  ))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "State",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                              basicData[0]["bi_state"] == null
                                                  ? ""
                                                  : basicData[0]["bi_state"]))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "City",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(basicData[0]
                                                      ["bi_district"] ==
                                                  null
                                              ? ""
                                              : basicData[0]["bi_district"]))),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      //width: MediaQuery.of(context).size.width,
                      child: Card(
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.01),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.group,
                                    color: Colors.green,
                                  ),
                                  Text(
                                    "Family Details",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Spacer(),
                                  IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: () {},
                                    iconSize: 20,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Family Values",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                    familyData == null
                                        ? ""
                                        : familyData[0]["fd_familyvalues"]
                                            .toString(),
                                  ))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Family Type",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(familyData == null
                                              ? ""
                                              : familyData[0]["fd_type"]
                                                  .toString()))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Family Status",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(familyData == null
                                              ? ""
                                              : familyData[0]["fd_status"]
                                                  .toString()))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Father's Occupation",
                                  ),
                                  Container(
                                    //width: 150,
                                    child: Flexible(
                                        child: Text(familyData == null
                                            ? ""
                                            : familyData[0]["fd_occupation"]
                                                .toString())),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Mother's Occupation",
                                  ),
                                  Container(
                                    //width: 150,
                                    child: Flexible(
                                        child: Text(familyData == null
                                            ? ""
                                            : familyData[0]["fd_moccupation"]
                                                .toString())),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Brother(s)",
                                  ),
                                  Container(
                                    child: Flexible(
                                        child: Text(familyData == null
                                            ? ""
                                            : familyData[0]["fd_brothers"]
                                                .toString())),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Brother(s) Married",
                                  ),
                                  Container(
                                    //width: 150,
                                    child: Flexible(
                                        child: Text(familyData == null
                                            ? ""
                                            : familyData[0]["fd_bmarried"]
                                                .toString())),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Sister(s)",
                                  ),
                                  Container(
                                    child: Flexible(
                                        child: Text(familyData == null
                                            ? ""
                                            : familyData[0]["fd_sisters"]
                                                .toString())),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Sister(s) Married",
                                  ),
                                  Container(
                                    //width: 150,
                                    child: Flexible(
                                        child: Text(familyData == null
                                            ? ""
                                            : familyData[0]["fd_smarried"]
                                                .toString())),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    Container(
                      //width: MediaQuery.of(context).size.width,
                      child: Card(
                        child: Column(
                          children: [
                            Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.01),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.headset_mic,
                                    color: Colors.teal,
                                  ),
                                  Text(
                                    "Hobbies and Interests",
                                    style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500),
                                  ),
                                  Spacer(),
                                  IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: () {},
                                    iconSize: 20,
                                  )
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Hobbies",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(
                                    basicData[0]["bi_hobby"] == null
                                        ? ""
                                        : basicData[0]["bi_hobby"],
                                  ))),
                                ],
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.01,
                                  left:
                                      MediaQuery.of(context).size.width * 0.01,
                                  right:
                                      MediaQuery.of(context).size.width * 0.02),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Interests",
                                  ),
                                  Container(
                                      child: Flexible(
                                          child: Text(basicData[0]
                                                      ["bi_interest"] ==
                                                  null
                                              ? ""
                                              : basicData[0]["bi_interest"]))),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ));
  }
}
