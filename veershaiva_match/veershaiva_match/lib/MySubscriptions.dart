import 'EditProfile.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class MySubscriptions extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MySubscriptions();
  }
}

class _MySubscriptions extends State<MySubscriptions> {
  SharedPreferences prefs;
  List mySubscriptionData;
  Future<String> mySubscriptionResponse;

  Future<String> getMySubscriptionData() async {
    var id = prefs.getString('id');
    String url =
        "https://www.veershaivamatch.com/app/getPlanDetails.php?apicall=getData";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'custId': id, 
        });
     setState(() {
      var convertDataToJson = json.decode(response.body);
      mySubscriptionData = convertDataToJson['user'];
    });
    //print(response.reasonPhrase);

    return "success";
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEmpId().then((value) {
      mySubscriptionResponse = this.getMySubscriptionData();
    });
    
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Plans"),
          backgroundColor: Colors.redAccent[700],
        ),
        body: FutureBuilder<String>(
          future: mySubscriptionResponse,
          builder: (context, snapshot){
            if(snapshot.hasData){
              return SingleChildScrollView(
          child: Column(
            children: [
              Container(
                height: 200,
                child: Center(
                    child: Image.network(
                        "https://www.veershaivamatch.com/CP/" + (basicData  == null ? "" : basicData[0]["photo"].toString()),
                        loadingBuilder: (context, child, progress) {
                              return progress == null
                                  ? child
                                  : LinearProgressIndicator();
                            },
                        )),
              ),
              Container(
                //width: MediaQuery.of(context).size.width,
                child: Card(
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01),
                        child: Row(
                          children: [
                            Text(
                              "Membership Plans",
                              style: TextStyle(
                                  fontSize: 17,
                                  fontWeight: FontWeight.w500,
                                  color: Colors.green[600]),
                            ),
                          ],
                        ),
                      ),
                      Divider(
                        thickness: 1,
                      ),
                   
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01,
                             right: MediaQuery.of(context).size.width *
                                        0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Plan Name",
                            ),
                            Container(
                              
                                child: Flexible(child: Text(mySubscriptionData[0]["plan"]  == null ? "" : mySubscriptionData[0]["plan"]))),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01,
                             right: MediaQuery.of(context).size.width *
                                        0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Activate Date",
                            ),
                            Container(
                                
                                child: Flexible(child: Text(mySubscriptionData[0]["activeDate"]  == null ? "" : mySubscriptionData[0]["activeDate"]))),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01,
                             right: MediaQuery.of(context).size.width *
                                        0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Expiry Date",
                            ),
                            Container(
                               
                                child: Flexible(child: Text(mySubscriptionData[0]["expiryDate"]  == null ? "" : mySubscriptionData[0]["expiryDate"]))),
                          ],
                        ),
                      ),
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01,
                             right: MediaQuery.of(context).size.width *
                                        0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Total Profile",
                            ),
                            Container(
                              
                                child: Flexible(child: Text(mySubscriptionData[0]["totalProfile"]  == null ? "" : mySubscriptionData[0]["totalProfile"]))),
                          ],
                        ),
                      ),
                      
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.width * 0.01,
                             right: MediaQuery.of(context).size.width *
                                        0.02),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Viewed Profile",
                            ),
                            Container(
                               
                                child: Flexible(child: Text(mySubscriptionData[0]["viewProfile"]  == null ? "" : mySubscriptionData[0]["viewProfile"]))),
                          ],
                        ),
                      ),
                     
                    ],
                  ),
                ),
              ),
              
            ],
          ),
        );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ));
  }
}
