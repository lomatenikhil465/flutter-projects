import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:async';
import 'dart:convert';

import 'PersonDetails.dart';

class SimpleSearch extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SimpleSearch();
  }
}

class _SimpleSearch extends State<SimpleSearch> {
  SharedPreferences prefs;
  TextEditingController _ageToController, _ageFromController;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  List religion = ["Hindu"], caste = ["Jangam", "Lingayat", "Any"];
  List simpleSearchData;
  bool _autoValidate = false;
  String _currentReligion, _currentCaste;

  void _onDropDownReligionSelected(String newValueSelected) {
    setState(() {
      this._currentReligion = newValueSelected;
    });
  }

  void _onDropDownCasteSelected(String newValueSelected) {
    setState(() {
      this._currentCaste = newValueSelected;
    });
  }

  Future<String> simpleSearch() async {
    var gender = prefs.getString('gender');
    String url =
        "https://www.veershaivamatch.com/app/searchDetails.php?apicall=simpleSearch";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'ageTo': _ageToController.text,
          'ageFrom': _ageFromController.text,
          'gender': gender == 'Female' ? "Male" : "Female",
          'religion': _currentReligion,
          'caste': _currentCaste
        });
    setState(() {
      var convertDataToJson = json.decode(response.body);
      simpleSearchData = convertDataToJson['user'];
    });
    //print(response.reasonPhrase);

    return "success";
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  void _validateInputs() {
    //checkConnection();
    if (_formKey.currentState.validate() &&
        _currentReligion != null &&
        _currentCaste != null) {
      setState(() {
        _autoValidate = false;
      });

//    If all data are correct then save data to out variables
      _formKey.currentState.save();
      simpleSearch().then((value) {
        // radio = null;
        _currentReligion = null;
        _currentCaste = null;

        _ageToController.text = "";
        _ageFromController.text = "";

        Navigator.of(_keyLoader.currentContext).pop();
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return PersonDetails(
            matchFoundData: simpleSearchData,
          );
        }));
      });

      _formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      if (_currentReligion == null) {
        _showDialogBox(context, "Message", "Please Choose Religion");
      }

      if (_currentCaste == null) {
        _showDialogBox(context, "Message", "Please Choose Caste");
      }

      setState(() {
        _autoValidate = true;
      });
    }
  }

  Future getEmpId() async {
    prefs = await SharedPreferences.getInstance();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getEmpId();
    _ageToController = TextEditingController(text: "");
    _ageFromController = TextEditingController(text: "");
    //radio = null;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Simple Search"),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02),
                    child: Text(
                      "Age : ",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.1),
                    height: MediaQuery.of(context).size.height * 0.06,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.3,
                          child: TextFormField(
                            autofocus: false,
                            controller: _ageFromController,
                            validator: (value) {
                              if (value == "" || value == "0") {
                                return "Please Enter From Age";
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              labelText: "From",
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            keyboardType: TextInputType.numberWithOptions(
                                decimal: true, signed: false),
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.06,
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.1),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.3,
                          child: TextFormField(
                            autofocus: false,
                            controller: _ageToController,
                            validator: (value) {
                              if (value == "" || value == "0") {
                                return "Please Enter To Age";
                              } else {
                                return null;
                              }
                            },
                            decoration: InputDecoration(
                              labelText: "To",
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015),
                            ),
                            inputFormatters: [
                              WhitelistingTextInputFormatter.digitsOnly
                            ],
                            keyboardType: TextInputType.numberWithOptions(
                                decimal: true, signed: false),
                            style: new TextStyle(
                              fontFamily: "Poppins",
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02),
                    child: Text(
                      "Religion : ",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.05,
                          width: MediaQuery.of(context).size.width * 0.7,
                          decoration: BoxDecoration(
                            border: Border(bottom: BorderSide(width: 1.0)),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButton<String>(
                                itemHeight: 80,
                                elevation: 1,

                                autofocus: true,
                                focusColor: Colors.lightBlue[900],
                                //style: Theme.of(context).textTheme.title,
                                isExpanded: true,
                                hint: Text("Religion"),
                                items: religion == null
                                    ? null
                                    : religion.map((dropDownStringItem) {
                                        return DropdownMenuItem<String>(
                                            value:
                                                dropDownStringItem.toString(),
                                            child: Text(dropDownStringItem,
                                                style: TextStyle(
                                                    fontSize: 17,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    color: Colors
                                                        .lightBlue[700])));
                                      }).toList(),
                                onChanged: (newValueSelected) =>
                                    _onDropDownReligionSelected(
                                        newValueSelected),
                                value: _currentReligion,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02),
                    child: Text(
                      "Caste : ",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02),
                    child: Row(
                      children: <Widget>[
                        Container(
                          height: MediaQuery.of(context).size.height * 0.05,
                          width: MediaQuery.of(context).size.width * 0.7,
                          decoration: BoxDecoration(
                            border: Border(bottom: BorderSide(width: 1.0)),
                          ),
                          child: DropdownButtonHideUnderline(
                            child: ButtonTheme(
                              alignedDropdown: true,
                              child: DropdownButton<String>(
                                itemHeight: 80,
                                elevation: 1,

                                autofocus: true,
                                focusColor: Colors.lightBlue[900],
                                //style: Theme.of(context).textTheme.title,
                                isExpanded: true,
                                hint: Text("Caste"),
                                items: caste == null
                                    ? null
                                    : caste.map((dropDownStringItem) {
                                        return DropdownMenuItem<String>(
                                            value:
                                                dropDownStringItem.toString(),
                                            child: Text(dropDownStringItem,
                                                style: TextStyle(
                                                    fontSize: 17,
                                                    fontWeight:
                                                        FontWeight.normal,
                                                    color: Colors
                                                        .lightBlue[700])));
                                      }).toList(),
                                onChanged: (newValueSelected) =>
                                    _onDropDownCasteSelected(newValueSelected),
                                value: _currentCaste,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                width: MediaQuery.of(context).size.width * 0.8,
                child: RaisedButton(
                  color: Colors.redAccent[700],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  onPressed: () {
                    // Navigator.of(context).push(MaterialPageRoute(builder: (context){
                    //   return CareerDetailsPage();
                    // }));
                    waiting(context);
                    _validateInputs();
                  },
                  child: Text("Search"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
