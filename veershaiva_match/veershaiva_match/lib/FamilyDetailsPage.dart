import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:veershaiva_match/CareerDetailsPage.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:veershaiva_match/PartnerDetailsPage.dart';

class FamilyDetailsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FamilyDetailsPage();
  }
}

class _FamilyDetailsPage extends State<FamilyDetailsPage> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  var _familyValue = ['Traditional', 'Orthodox','Liberal', 'Moderate'];
  var _familyType = ['Seperate Family', 'Joint Family'];
  var _familyStatus = ['Rich', 'Upper Middle Class', 'High Class', 'Middle Class', 'Do not want to tell at this time'];
  var _noOfSiblings = ['0', '1', '2', '3', '4', '4+'];
  var _noOfBroMarried = ['No Brothers', 'All are Married', 'No Married Brother', 'One Married Brother', 'Two Married Brother', 'Three Married Brother','Four Married Brother','Above Four Married Brother'];
  var _noOfSisMarried = ['No Sisters', 'All are Married', 'No Married Sister', 'One Married Sister', 'Two Married Sister', 'Three Married Sister','Four Married Sister','Above Four Married Sister'];
  String matchId = "", address, fatherName, motherName, fatherOccupation, motherOccupation;
  String _currentFamilyValueSelected,_currentFamilyTypeSelected,
  _currentFamilyStatusSelected,
  _currentNoOfBroSelected,
  _currentNoOfSisSelected,
  _currentNoOfBroMarriedSelected,
  _currentNoOfSisMarriedSelected;
  int radio, radioFather, radioMother;

  void radioButtonCreDeb(int val) {
    setState(() {
      print(val);
      radio = val;
    });
  }
  void radioButtonFather(int val) {
    setState(() {
      print(val);
      radioFather = val;
    });
  }

  void radioButtonMother(int val) {
    setState(() {
      print(val);
      radioMother = val;
    });
  }


  Future<String> getJsonData() async {
    String url =
        "https://www.veershaivamatch.com/app/registerApp.php?apicall=custId" ;
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      matchId = convertDataToJson['user'];
    });
    
    return "success";
  }

   void _onDropDownFamilyValueSelected(String newValueSelected) {
    setState(() {
      this._currentFamilyValueSelected = newValueSelected;
    });
  }

  void _onDropDownFamilyTypeSelected(String newValueSelected) {
    setState(() {
      this._currentFamilyTypeSelected = newValueSelected;
    });
  }

  
  void _onDropDownFamilyStatusSelected(String newValueSelected) {
    setState(() {
      this._currentFamilyStatusSelected = newValueSelected;
    });
  }

   void _onDropDownNoOfBroSelected(String newValueSelected) {
    setState(() {
      this._currentNoOfBroSelected = newValueSelected;
    });
  }
   void _onDropDownNoOfSisSelected(String newValueSelected) {
    setState(() {
      this._currentNoOfSisSelected = newValueSelected;
    });
  }

   void _onDropDownNoOfBroMarriedSelected(String newValueSelected) {
    setState(() {
      this._currentNoOfBroMarriedSelected = newValueSelected;
    });
  }
   void _onDropDownNoOfSisMarriedSelected(String newValueSelected) {
    setState(() {
      this._currentNoOfSisMarriedSelected = newValueSelected;
    });
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

   Future<String> homeSearch() async {
    String url =
        "https://www.veershaivamatch.com/app/searchDetails.php?apicall=homeSearch";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          // 'ageTo': _ageToController.text,
          // 'ageFrom': _ageFromController.text,
          // 'gender': radio == 0 ? "Male" : "Female",
          // 'religion': _currentReligion,
          // 'caste': _currentCaste
        });
    //  setState(() {
    //   var convertDataToJson = json.decode(response.body);
    //   homeSearchData = convertDataToJson['user'];
    // });
    //print(response.reasonPhrase);

    return "success";
  }

  void _validateInputs() {
    //checkConnection();
    if (_formKey.currentState.validate() ) {
      setState(() {
        _autoValidate = false;
      });
      waiting(context);
//    If all data are correct then save data to out variables
      _formKey.currentState.save();
      homeSearch().then((value) {
        radio = null;
        Navigator.of(_keyLoader.currentContext).pop();
        Navigator.of(context).push(MaterialPageRoute(builder: (context){
          return null;
        }));
      });

      _formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      // if (_currentReligion == null) {
      //   _showDialogBox(context, "Message", "Please Choose Religion");
      // }

      // if (_currentCaste == null) {
      //   _showDialogBox(context, "Message", "Please Choose Caste");
      // }

      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Family Details "),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
                  child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.02,
                  left: MediaQuery.of(context).size.width * 0.02,
                  right: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      child: Text(
                        "Match ID: ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    
                    Container(
                      child: Text(matchId, style: TextStyle(color: Colors.redAccent[700], fontWeight: FontWeight.w500),),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: Text(
                      "Address : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.55,
                    padding: EdgeInsets.only(
                      right: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please Enter your Address';
                        }
                        return null;
                      },
                      onSaved: (String address) {
                        this.address = address;
                      },
                      decoration: InputDecoration(
                          hintText: "Address",
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                          contentPadding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.015,
                              left: MediaQuery.of(context).size.width * 0.015)),
                      keyboardType: TextInputType.text,
                      style: new TextStyle(
                        fontFamily: "Poppins",
                      ),
                    ),
                  ),
                ],
              ),
             
              Container(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.01,
                left: MediaQuery.of(context).size.width * 0.02,
                right: MediaQuery.of(context).size.width * 0.02
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Family Value : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                        
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Family Value"),
                              items: _familyValue
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownFamilyValueSelected(newValueSelected),
                              value: _currentFamilyValueSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.01,
                left: MediaQuery.of(context).size.width * 0.02,
                right: MediaQuery.of(context).size.width * 0.02
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Family Type : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                       
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Family Type"),
                              items: _familyType
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownFamilyTypeSelected(newValueSelected),
                              value: _currentFamilyTypeSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.01,
                left: MediaQuery.of(context).size.width * 0.02,
                right: MediaQuery.of(context).size.width * 0.02
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "Family Status : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                       
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a Family Status"),
                              items: _familyStatus
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownFamilyStatusSelected(newValueSelected),
                              value: _currentFamilyStatusSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.01,
                left: MediaQuery.of(context).size.width * 0.02,
                right: MediaQuery.of(context).size.width * 0.02
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "No of Brothers : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                       
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a No of Brothers"),
                              items: _noOfSiblings
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownNoOfBroSelected(newValueSelected),
                              value: _currentNoOfBroSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.01,
                left: MediaQuery.of(context).size.width * 0.02,
                right: MediaQuery.of(context).size.width * 0.02
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "No of Brothers \nMarried : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                       
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a No of Brothers Married"),
                              items: _noOfBroMarried
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownNoOfBroMarriedSelected(newValueSelected),
                              value: _currentNoOfBroMarriedSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.01,
                left: MediaQuery.of(context).size.width * 0.02,
                right: MediaQuery.of(context).size.width * 0.02
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "No of Sisters : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                        
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a No of Sisters"),
                              items: _noOfSiblings
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownNoOfSisSelected(newValueSelected),
                              value: _currentNoOfSisSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.01,
                left: MediaQuery.of(context).size.width * 0.02,
                right: MediaQuery.of(context).size.width * 0.02
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text(
                      "No of Sisters \nMarried : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //   width: MediaQuery.of(context).size.height * 0.1,
                  // ),
                  
                      Container(
                        height: MediaQuery.of(context).size.height * 0.07,
                        width: MediaQuery.of(context).size.width * 0.55,
                        
                        decoration: BoxDecoration(
                          border: Border(bottom: BorderSide(width: 1.0)),
                        ),
                        child: DropdownButtonHideUnderline(
                          child: ButtonTheme(
                            alignedDropdown: true,
                            child: DropdownButton<String>(
                              itemHeight: 80,
                              elevation: 1,

                              autofocus: true,
                              focusColor: Colors.redAccent[700],
                              //style: Theme.of(context).textTheme.title,
                              isExpanded: true,
                              hint: Text("Choose a No of Sisters Married"),
                              items: _noOfSisMarried
                                  .map((String dropDownStringItem) {
                                return DropdownMenuItem<String>(
                                    value: dropDownStringItem,
                                    child: Text(dropDownStringItem,
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.redAccent[700])));
                              }).toList(),
                              onChanged: (String newValueSelected) =>
                                  _onDropDownNoOfSisMarriedSelected(newValueSelected),
                              value: _currentNoOfSisMarriedSelected,
                            ),
                          ),
                        ),
                      )
                    
                ],
              ),
            ),
            Row(
              children: [Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02),
                    child: Text(
                      "Body Type :",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),],
            ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  
                  Radio(
                    value: 0,
                    groupValue: radio,
                    onChanged: radioButtonCreDeb,
                    activeColor: Colors.redAccent[700],
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonCreDeb(0);
                    },
                    child: Container(
                      child: Text(
                        "Slim",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                  Radio(
                    value: 1,
                    groupValue: radio,
                    onChanged: radioButtonCreDeb,
                    activeColor: Colors.redAccent[700],
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonCreDeb(1);
                    },
                    child: Container(
                      child: Text(
                        "Average",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                  Radio(
                    value: 2,
                    groupValue: radio,
                    onChanged: radioButtonCreDeb,
                    activeColor: Colors.redAccent[700],
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonCreDeb(2);
                    },
                    child: Container(
                      child: Text(
                        "Atheletic",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                  Radio(
                    value: 3,
                    groupValue: radio,
                    onChanged: radioButtonCreDeb,
                    activeColor: Colors.redAccent[700],
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonCreDeb(3);
                    },
                    child: Container(
                      child: Text(
                        "Heavy",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      
                      child: Text(
                        "Father Name : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.55,
                     
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Father Name';
                          }
                          return null;
                        },
                        onSaved: (String fatherName) {
                          this.fatherName = fatherName;
                        },
                        decoration: InputDecoration(
                            hintText: "Father Name",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left: MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  
                  Radio(
                    value: 0,
                    groupValue: radioFather,
                    onChanged: radioButtonFather,
                    activeColor: Colors.redAccent[700],
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonFather(0);
                    },
                    child: Container(
                      child: Text(
                        "alive",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                  Radio(
                    value: 1,
                    groupValue: radioFather,
                    onChanged: radioButtonFather,
                    activeColor: Colors.redAccent[700],
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonFather(1);
                    },
                    child: Container(
                      child: Text(
                        "not alive",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                  
                ],
              ),
              Container(
                padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      
                      child: Text(
                        "Father Occupation : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.55,
                      
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Father Occupation';
                          }
                          return null;
                        },
                        onSaved: (String fatherOccupation) {
                          this.fatherOccupation = fatherOccupation;
                        },
                        decoration: InputDecoration(
                            hintText: "Father Occupation",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left: MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                 padding: EdgeInsets.only(
                   top: MediaQuery.of(context).size.height * 0.01,
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                     
                      child: Text(
                        "Mother Name : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.55,
                    
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Mother Name';
                          }
                          return null;
                        },
                        onSaved: (String motherName) {
                          this.motherName = motherName;
                        },
                        decoration: InputDecoration(
                            hintText: "Mother Name",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left: MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  
                  Radio(
                    value: 0,
                    groupValue: radioMother,
                    onChanged: radioButtonMother,
                    activeColor: Colors.redAccent[700],
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonMother(0);
                    },
                    child: Container(
                      child: Text(
                        "alive",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                  Radio(
                    value: 1,
                    groupValue: radioMother,
                    onChanged: radioButtonMother,
                    activeColor: Colors.redAccent[700],
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonMother(1);
                    },
                    child: Container(
                      child: Text(
                        "not alive",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                  
                ],
              ),
              Container(
                 padding: EdgeInsets.only(
                   
                        left: MediaQuery.of(context).size.width * 0.02,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                     
                      child: Text(
                        "Mother Occupation : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.55,
                      
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Mother Occupation';
                          }
                          return null;
                        },
                        onSaved: (String motherOccupation) {
                          this.motherOccupation = motherOccupation;
                        },
                        decoration: InputDecoration(
                            hintText: "Mother Occupation",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left: MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                width: MediaQuery.of(context).size.width * 0.8,
                child: RaisedButton(
                  color: Colors.redAccent[700],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  onPressed: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return PartnerDetailsPage();
                    }));
                  },
                  child: Text("Next"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
