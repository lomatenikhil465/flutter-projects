import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ChangePassword extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ChangePassword();
  }
  
}

class _ChangePassword extends State<ChangePassword> {
  String  oldPass,newPass;
  bool _obscureOldPass = true, _obscureNewPass = true;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Change Password"),
        backgroundColor: Colors.redAccent[700],

      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02,
               left: MediaQuery.of(context).size.width * 0.02,),
              child: Row(
                children: [
                  Container(
                    
                    child: Text(
                      "Old Password",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                ],
              ),
            ),
            
            Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01,
               left: MediaQuery.of(context).size.width * 0.02,),
              child: Row(
               
                children: [
                  
                  Container(
                    width: MediaQuery.of(context).size.width * 0.94,
                   
                    child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please Enter your Old Password';
                        }
                        return null;
                      },
                      onSaved: (String oldPass) {
                        this.oldPass = oldPass;
                      },
                      obscureText: _obscureOldPass,
                      decoration: InputDecoration(
                        hintText: "Old Password",
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        suffixIcon: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _obscureOldPass = !_obscureOldPass;
                                  });
                                },
                                child: Icon(
                                  _obscureOldPass
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: Colors.redAccent[700],
                                ),
                              ),
                        contentPadding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.015,
                            left: MediaQuery.of(context).size.width * 0.015),
                      ),
                      keyboardType: TextInputType.text,
                      
                      style: new TextStyle(
                        fontFamily: "Poppins",
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01,
               left: MediaQuery.of(context).size.width * 0.02,),
              child: Row(
                children: [
                  Container(
                    
                    child: Text(
                      "New Password : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01,
               left: MediaQuery.of(context).size.width * 0.02,),
              child: Row(
               
                children: [
                  
                  Container(
                    width: MediaQuery.of(context).size.width * 0.94,
                   
                    child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please Enter your New Password';
                        }
                        return null;
                      },
                      onSaved: (String newPass) {
                        this.newPass = newPass;
                      },
                      obscureText: _obscureNewPass,
                      decoration: InputDecoration(
                        hintText: "New Password",
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        suffixIcon: GestureDetector(
                                onTap: () {
                                  setState(() {
                                    _obscureNewPass = !_obscureNewPass;
                                  });
                                },
                                child: Icon(
                                  _obscureNewPass
                                      ? Icons.visibility
                                      : Icons.visibility_off,
                                  color: Colors.redAccent[700],
                                ),
                              ),
                        contentPadding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.015,
                            left: MediaQuery.of(context).size.width * 0.015),
                      ),
                      keyboardType: TextInputType.text,
                      
                      style: new TextStyle(
                        fontFamily: "Poppins",
                      ),
                    ),
                  ),
                ],
              ),
            ),
           
            
           Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height *0.01),
              width: MediaQuery.of(context).size.width * 0.8,
              child: RaisedButton(
                color: Colors.redAccent[700],
                textColor: Colors.white,
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                onPressed: (){
                  // Navigator.of(context).push(MaterialPageRoute(builder: (context){
                  //   return LoginDetails();
                  // }));

                },
                  child: Text("Submit"), 
                
              ),
            )
          ],
        ),
      ),
    );
  }
  
}