import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MembershipPlans extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MembershipPlans();
  }
  
}

class _MembershipPlans extends State<MembershipPlans> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Membership Plans"),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: Image.asset("assets\\images\\mrishteplans3.jpg"),
            ),
            Container(
              child: Text("For Activation, Contact Us on following details: "),
            ),
            Container(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05, top: MediaQuery.of(context).size.height *0.01),
              child: Text("Contact No.: +91 9422370687, +91 8149070687, +91 7588214687"),
            )
          ],
        ),
      ),
    );
  }
  
}