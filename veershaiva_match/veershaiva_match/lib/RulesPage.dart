import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RulesPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _RulesPage();
  }
}

class _RulesPage extends State<RulesPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Rules and Regulations"),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.01),
              child: Row(
                children: [
                  Container(
                    child: Text(
                      " Rules and Regulations",
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                    ),
                  )
                ],
              ),
            ),
            Divider(
              thickness: 1.0,
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '1.',
                  style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   Bride or groom which is going to be register here would be above 18 years of age.',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal))
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '2.',
                  style: TextStyle(color: Colors.black,fontWeight: FontWeight.bold),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '		After successful registration of profile on website its necessary to sends bride or grooms bio-data , photos and kundali to the correspondence address. (Hard Copy)',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '3.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   Please enter your actual information only for hassle free search criteria',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '4.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   In our website if any body like any profile so please let us know as communication with both profile is being done from our side only.',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '5.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   www.veershaivamatch.com would not take any responsibility for how much time would it takes for marriage after registration.',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '6.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   After finalizing profile for your bride or groom make sure that take proper investigation before finalizing marriage dates as veershaivamatch.com would not responsible for such things.',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.01),
              child: Row(
                children: [
                  Container(
                    child: Text(
                      " नियम व अटी",
                      style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                    ),
                  )
                ],
              ),
            ),
            Divider(
              thickness: 1.0,
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '1.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   ज्यांची नोंदणी करता आहात ते वधु किंवा वर सज्ञान असणे आवश्यक आहे.',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '2.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   जरी तुम्ही वेबसाईटरुन तुमची नोंदणी केलेली असली तरी तुम्हाला तुमच्या मुलाचा किंवा मुलीचा बायोडाटा फॉर्म , फोटो Email पाठवणे आवश्यक आहे.',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '3.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   www.veershaivamatch.com वर-वधु दोघांसाठी तत्काळ संपर्क करता यावा यासाठी सदस्यांनी आपली खरी माहीती द्यावी.',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '4.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   www.veeershaivamatch.com मधील कोणतेही स्थळ हे आपल्या स्थळाला योग्य आहे असे वाटल्यास www.veershaivamatch.com आपल्याला त्या स्थळाशी संपर्क साधून देईल.',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '5.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   नांव नोंदवल्यावर विवाह किती दिवसात जमेल याची कोणतीही जबाबदारी www.veershaivamatch.com घेणार नाही.',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '6.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   आपण निवडलेल्या स्थळाची वैयक्तिक माहीती स्वतः पडताळून मगच निर्णय घ्यावा. www.veershaivamatch.com जबाबदार राहणार नाही.',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '7.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   कोणत्याही स्थळाने खोटी माहीती दिल्यास अगर माहीतीचा गैरवापर केल्यास www.veershaivamatch.com मधून त्या स्थळाची माहीती रद्द करणेत येईल.',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '8.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   आपल्या मुलाचे किंवा मुलीचे लग्न जमल्यास त्याची आम्हाला तत्काळ माहीती द्यावी, आम्ही आपल्या मुलाचा किंवा मुलीचा प्रोफाईल वेबसाईटवरुन काढुन टाकू.',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '9.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   अधिक माहीतीसाठी संपर्क साधावा. 07588214687, 09422370687, 08149070687',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '10.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   आपणास आपली नांव नोंदणी आमचेकडे करावयाची असल्यास कृपया खालील कागदपत्राद्वारे करावी. आपला १ फोटो, संपूर्ण बायोडेटा व संपूर्ण पत्रिका. वरील सर्व कागदपत्रे प्रत्यक्ष द्यावीत किंवा पोस्ट, Email पाठवावीत.',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
            Container(
              child: RichText(
                text: TextSpan(
                  text: '11.',
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black),
                  children: <TextSpan>[
                    TextSpan(
                        text:
                            '   कॄपया नोंदंणी केल्यानंतर Select केलेल्या फ़ोटो प्रोफ़ाईलवर त्यांच्या पालकांना संपर्क केल्याशिवाय आम्ही तुम्हांला ती प्रोफ़ाईल देऊ शकत नाही,याची नोंद घ्यावी.',style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
