import 'dart:io';

import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:veershaiva_match/CareerDetailsPage.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class SignUpPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SignUpPage();
  }
}

List registerDetails;

class _SignUpPage extends State<SignUpPage> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String matchId = "", name, city, state, placeOfBirth, age, country;
  int radio;
  TextEditingController _dateController, _timeController;
  DateTime _dateTime = DateTime.now();

  void radioButtonCreDeb(int val) {
    setState(() {
      print(val);
      radio = val;
    });
  }

  Future<String> getJsonData() async {
    String url =
        "https://www.veershaivamatch.com/app/registerApp.php?apicall=custId";
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      matchId = convertDataToJson['user'];
    });

    return "success";
  }

  waiting(BuildContext context, GlobalKey key) {
    print("1");
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: key,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  void _validateInputs(BuildContext context) {
    //checkConnection();

    print(_keyLoader.currentContext);
    if (_formKey.currentState.validate()) {
      setState(() {
        _autoValidate = false;
      });

//    If all data are correct then save data to out variables
      _formKey.currentState.save();

      // radio = null;

      registerDetails.insert(0, {
        "matchId": matchId,
        "gender": radio == 0 ? "Male" : "Female",
        "name": name,
        "dob": _dateController.text,
        "age": age,
        "placeOfBirth": placeOfBirth,
        "timeOfBirth": _timeController.text,
        "country": country,
        "state": state,
        "city": city
      });
      print(registerDetails[0]["matchId"]);

      new Future.delayed(new Duration(milliseconds: 500), () {
        print(_keyLoader.currentContext);
        Navigator.of(_keyLoader.currentContext).pop();
      });

      //_showDialogBox(context,"MEssage",'hekllo');
      Navigator.of(context).push(MaterialPageRoute(builder: (context) {
        return CareerDetailsPage();
      }));

      //_formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      // if (_currentReligion == null) {
      //   _showDialogBox(context, "Message", "Please Choose Religion");
      // }

      // if (_currentCaste == null) {
      //   _showDialogBox(context, "Message", "Please Choose Caste");
      // }

      setState(() {
        _autoValidate = true;
      });
    }
  }

  Future<String> gettemp() async {
    String url =
        "https://www.veershaivamatch.com/app/getData.php?apicall=homeImages";
    print(url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      var homeImagesData = convertDataToJson['data'];
    });

    return "success";
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.getJsonData();
    registerDetails = new List();
    _dateController =
        TextEditingController(text: DateFormat("dd/MM/yyyy").format(_dateTime));
    _timeController =
        TextEditingController(text: DateFormat("HH:mm").format(_dateTime));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Personal Details "),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.02,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  children: [
                    Container(
                      child: Text(
                        "Match ID: ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.height * 0.1,
                    ),
                    Container(
                      child: Text(matchId),
                    ),
                  ],
                ),
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02),
                    child: Text(
                      "Gender : ",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                  Radio(
                    value: 0,
                    groupValue: radio,
                    onChanged: radioButtonCreDeb,
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonCreDeb(0);
                    },
                    child: Container(
                      child: Text(
                        "Male",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                  Radio(
                    value: 1,
                    groupValue: radio,
                    onChanged: radioButtonCreDeb,
                  ),
                  GestureDetector(
                    onTap: () {
                      radioButtonCreDeb(1);
                    },
                    child: Container(
                      child: Text(
                        "Female",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: Text(
                      "Name : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    padding: EdgeInsets.only(
                      right: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please Enter your Name';
                        }
                        return null;
                      },
                      onSaved: (String name) {
                        this.name = name;
                      },
                      decoration: InputDecoration(
                          hintText: "Name",
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                          contentPadding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.015,
                              left: MediaQuery.of(context).size.width * 0.015)),
                      keyboardType: TextInputType.text,
                      style: new TextStyle(
                        fontFamily: "Poppins",
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: Text(
                      "DOB : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.01,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: DateTimeField(
                          format: DateFormat('dd/MM/yyy'),
                          controller: _dateController,
                          initialValue: DateTime.now(),
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.calendar_today),
                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(),
                              ),
                              contentPadding: EdgeInsets.only(
                                  top: MediaQuery.of(context).size.height *
                                      0.015,
                                  left: MediaQuery.of(context).size.width *
                                      0.015)),
                          onShowPicker: (context, currentValue) {
                            return showDatePicker(
                                context: context,
                                initialDate: _dateTime == null
                                    ? DateTime.now()
                                    : _dateTime,
                                firstDate: DateTime(1950),
                                lastDate: DateTime.now());
                          },
                          validator: (val) {
                            if (val != null) {
                              return null;
                            } else {
                              return 'Date Field is Empty';
                            }
                          })),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: Text(
                      "Age : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01,
                      right: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please Enter your Age';
                        }
                        return null;
                      },
                      onSaved: (String age) {
                        this.age = age;
                      },
                      decoration: InputDecoration(
                        hintText: "Age",
                        border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(10.0),
                          borderSide: new BorderSide(),
                        ),
                        contentPadding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.015,
                            left: MediaQuery.of(context).size.width * 0.015),
                      ),
                      keyboardType: TextInputType.numberWithOptions(
                          signed: false, decimal: true),
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(10),
                        new WhitelistingTextInputFormatter(RegExp("[0-9]"))
                      ],
                      style: new TextStyle(
                        fontFamily: "Poppins",
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: Text(
                      "Place of \nBirth : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.8,
                    padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01,
                      right: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please Enter your Place of Birth';
                        }
                        return null;
                      },
                      onSaved: (String placeOfBirth) {
                        this.placeOfBirth = placeOfBirth;
                      },
                      decoration: InputDecoration(
                          hintText: "Place of Birth",
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                          contentPadding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.015,
                            left: MediaQuery.of(context).size.width * 0.015,
                          )),
                      keyboardType: TextInputType.text,
                      style: new TextStyle(
                        fontFamily: "Poppins",
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: Text(
                      "Time of \nBirth : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.01,
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                      width: MediaQuery.of(context).size.width * 0.8,
                      child: DateTimeField(
                          format: DateFormat("HH:mm"),
                          controller: _timeController,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.timer),
                              border: new OutlineInputBorder(
                                borderRadius: new BorderRadius.circular(10.0),
                                borderSide: new BorderSide(),
                              ),
                              contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left: MediaQuery.of(context).size.width * 0.015,
                              )),
                          onShowPicker: (context, currentValue) async {
                            final time = await showTimePicker(
                              context: context,
                              initialTime: TimeOfDay.fromDateTime(
                                  currentValue ?? DateTime.now()),
                            );
                            return DateTimeField.convert(time);
                          },
                          validator: (val) {
                            if (val != null) {
                              return null;
                            } else {
                              return 'Time Field is Empty';
                            }
                          })),
                ],
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                      ),
                      child: Text(
                        "Country : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Country';
                          }
                          return null;
                        },
                        onSaved: (String country) {
                          this.country = country;
                        },
                        decoration: InputDecoration(
                            hintText: "Country",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left:
                                    MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                      ),
                      child: Text(
                        "State : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your State';
                          }
                          return null;
                        },
                        onSaved: (String state) {
                          this.state = state;
                        },
                        decoration: InputDecoration(
                            hintText: "State",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left:
                                    MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                      ),
                      child: Text(
                        "City : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.8,
                      padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your City';
                          }
                          return null;
                        },
                        onSaved: (String city) {
                          this.city = city;
                        },
                        decoration: InputDecoration(
                            hintText: "City",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left:
                                    MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                width: MediaQuery.of(context).size.width * 0.8,
                child: RaisedButton(
                  color: Colors.redAccent[700],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  onPressed: () {
                    waiting(context, _keyLoader);

                    _validateInputs(context);
                  },
                  child: Text("Next"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
