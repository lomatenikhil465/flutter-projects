import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:veershaiva_match/SocialDetailsPage.dart';

import 'SignUpPage.dart';

class CareerDetailsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CareerDetailsPage();
  }
}

class _CareerDetailsPage extends State<CareerDetailsPage> {
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _currentDegreeSelected,
      education,
      _currentEmployeeSelected,
      _currentOccupationSelected,
      _currentIncomeSelected;

  DateTime _dateTime = DateTime.now();
  var _degree = [
    'Bachelors',
    'Doctorate',
    'Masters',
    'Diploma',
    'Under Graduate',
    'Associate Degree',
    'Honnours Degree',
    'Trade School',
    'High School',
    'Less than High School'
  ];
  var _employee = [
    'MNC',
    'Defence',
    'Government',
    'Business',
    'Employee',
    'Not Employeed In',
    'Private',
    'Others'
  ];
  var _occupation = [
    'Student',
    'non Working',
    'Non-mainstream Professional',
    'Accountant',
    'Acting Professional',
    'Industralist',
    'Actor',
    'Administration Professional',
    'Management Professional',
    'Advertising Professional',
    'Air Hostess',
    'Architect',
    'Artisan',
    'Ayurvedic',
    'Bank Employee',
    'Officer',
    'Beautician',
    'Biologist / Botanist',
    'Business Person',
    'Chartered Accountant',
    'Civil Engineer',
    'Clerical Official',
    'Commercial Pilot',
    'Company Secretary',
    'Computer Professional',
    'Consultant',
    'Contractor',
    'Cost Accountant',
    'Creative Person',
    'Customer Support Professional',
    'Dentist',
    'Designer',
    'Doctor',
    'Economist',
    'Engineer',
    'Engineer(Mechanical)',
    'Engineer(Project)',
    'Entertainment Professionals',
    'Event manager',
    'Executive',
    'Factory Worker',
    'Farmer',
    'Fashion Designer',
    'Finance Professional',
    'Flight Attendet',
    'Government Employee',
    'Government Officer',
    'Health Care professional',
    'Home Maker',
    'Homeopathy',
    'Hotel & Restaurant Professional',
    'Human Resources',
    'IAS/IPS/IRS/IFS',
    'Interior Designer',
    'Investment Professional',
    'Telecom Professional',
    'Journalist',
    'Lawyer',
    'Lecturer',
    'Legal Professional',
    'Manager',
    'Marketing Professional',
    'Media Professional',
    'Medical Transcriptionist',
    'Merchant naval officer',
    'Nurse',
    'Occupational Therapist',
    'Optician',
    'Pharmacist',
    'Physician Assistant',
    'Physicist',
    'Physiotherapist',
    'Pilot',
    'Politician',
    'Production Professional',
    'Professor',
    'Psychologist',
    'Public Relations Professional',
    'Real Estate Professional',
    'Research Scholar',
    'Retired Person',
    'Retail Professional',
    'Sales Professional',
    'Scientist',
    'Self-employed Person',
    'Social Worker',
    'Software Consultant',
    'Sportsman',
    'Teacher',
    'Technician',
    'Training Professional',
    'Transportation',
    'Veterinary Doctor',
    'Volunteer',
    'Writer',
    'Zoologist'
  ];
  var _income = [
    'Under Rs. 50,000',
    'Rs.50,001-1,00,000',
    'Rs.1,00,001-1,50,000',
    'Rs.1,00,001-2,00,000',
    'Rs.2,00,001-3,00,000',
    'Rs.3,00,001-4,00,000',
    'Rs.4,00,001-5,00,000',
    'Rs.5,00,001-6,00,000',
    'Rs.6,00,001-7,00,000',
    'Rs.7,00,001-8,00,000',
    'Rs.8,00,001-9,00,000',
    'Rs.9,00,001-10,00,000',
    'Rs.10,00,001-above'
  ];

  void _onDropDownDegreeSelected(String newValueSelected) {
    setState(() {
      this._currentDegreeSelected = newValueSelected;
    });
  }

  void _onDropDownEmployeeSelected(String newValueSelected) {
    setState(() {
      this._currentEmployeeSelected = newValueSelected;
    });
  }

  void _onDropDownOccupationSelected(String newValueSelected) {
    setState(() {
      this._currentOccupationSelected = newValueSelected;
    });
  }

  void _onDropDownIncomeSelected(String newValueSelected) {
    setState(() {
      this._currentIncomeSelected = newValueSelected;
    });
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  void _validateInputs(BuildContext context) {
    //checkConnection();

    if (_formKey.currentState.validate()) {
      setState(() {
        _autoValidate = false;
      });

//    If all data are correct then save data to out variables
      _formKey.currentState.save();

      // radio = null;

      registerDetails.insert(0, {
        "degree": _currentDegreeSelected,
        "eduDetails": education,
        "employeedIn": _currentEmployeeSelected,
        "occupation": _currentOccupationSelected,
        "income": _currentIncomeSelected,
      });
      print(registerDetails[0]["matchId"]);
      new Future.delayed(new Duration(milliseconds: 500), () {
        print(_keyLoader.currentContext);
        Navigator.of(_keyLoader.currentContext).pop();
      });
      Navigator.of(context).push(MaterialPageRoute(builder: (context) {
        return SocialDetailsPage();
      }));

      _formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      // if (_currentReligion == null) {
      //   _showDialogBox(context, "Message", "Please Choose Religion");
      // }

      // if (_currentCaste == null) {
      //   _showDialogBox(context, "Message", "Please Choose Caste");
      // }

      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Career Details"),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: [
              Container(
                child: Row(
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.02,
                        left: MediaQuery.of(context).size.width * 0.02,
                      ),
                      child: Text(
                        "Education",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.02,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      child: Text(
                        "Highest Education* : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    // SizedBox(
                    //   width: MediaQuery.of(context).size.height * 0.1,
                    // ),

                    Container(
                      height: MediaQuery.of(context).size.height * 0.07,
                      width: MediaQuery.of(context).size.width * 0.55,
                      margin: EdgeInsets.only(
                          left: MediaQuery.of(context).size.height * 0.01),
                      decoration: BoxDecoration(
                        border: Border(bottom: BorderSide(width: 1.0)),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            itemHeight: 80,
                            elevation: 1,

                            autofocus: true,
                            focusColor: Colors.redAccent[700],
                            //style: Theme.of(context).textTheme.title,
                            isExpanded: true,
                            hint: Text("Choose a Degree"),
                            items: _degree.map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                  value: dropDownStringItem,
                                  child: Text(dropDownStringItem,
                                      style: TextStyle(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w500,
                                          color: Colors.redAccent[700])));
                            }).toList(),
                            onChanged: (String newValueSelected) =>
                                _onDropDownDegreeSelected(newValueSelected),
                            value: _currentDegreeSelected,
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width * 0.02,
                      ),
                      child: Text(
                        "Education Details* : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.55,
                      padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02,
                      ),
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Education Details';
                          }
                          return null;
                        },
                        onSaved: (String education) {
                          this.education = education;
                        },
                        decoration: InputDecoration(
                            hintText: "Education Details",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left:
                                    MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Divider(
                thickness: 1.0,
              ),
              Row(
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: Text(
                      "Work Experience ",
                      style:
                          TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: Text(
                      "Employeed In : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //     width: MediaQuery.of(context).size.height * 0.1,
                  //   ),

                  Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                    width: MediaQuery.of(context).size.width * 0.6,
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.height * 0.01),
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(width: 1.0)),
                    ),
                    child: DropdownButtonHideUnderline(
                      child: ButtonTheme(
                        alignedDropdown: true,
                        child: DropdownButton<String>(
                          itemHeight: 80,
                          elevation: 1,

                          autofocus: true,
                          focusColor: Colors.redAccent[700],
                          //style: Theme.of(context).textTheme.title,
                          isExpanded: true,
                          hint: Text("Choose a Employeed In"),
                          items: _employee.map((String dropDownStringItem) {
                            return DropdownMenuItem<String>(
                                value: dropDownStringItem,
                                child: Text(dropDownStringItem,
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.redAccent[700])));
                          }).toList(),
                          onChanged: (String newValueSelected) =>
                              _onDropDownEmployeeSelected(newValueSelected),
                          value: _currentEmployeeSelected,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: Text(
                      "Occupation : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //     width: MediaQuery.of(context).size.height * 0.1,
                  //   ),

                  Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                    width: MediaQuery.of(context).size.width * 0.6,
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.height * 0.01),
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(width: 1.0)),
                    ),
                    child: DropdownButtonHideUnderline(
                      child: ButtonTheme(
                        alignedDropdown: true,
                        child: DropdownButton<String>(
                          itemHeight: 80,
                          elevation: 1,

                          autofocus: true,
                          focusColor: Colors.redAccent[700],
                          //style: Theme.of(context).textTheme.title,
                          isExpanded: true,
                          hint: Text("Choose a Occupation"),
                          items: _occupation.map((String dropDownStringItem) {
                            return DropdownMenuItem<String>(
                                value: dropDownStringItem,
                                child: Text(dropDownStringItem,
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.redAccent[700])));
                          }).toList(),
                          onChanged: (String newValueSelected) =>
                              _onDropDownOccupationSelected(newValueSelected),
                          value: _currentOccupationSelected,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                      left: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: Text(
                      "Annual Income : ",
                      style: TextStyle(fontSize: 16),
                    ),
                  ),
                  // SizedBox(
                  //     width: MediaQuery.of(context).size.height * 0.1,
                  //   ),

                  Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                    width: MediaQuery.of(context).size.width * 0.6,
                    margin: EdgeInsets.only(
                        left: MediaQuery.of(context).size.height * 0.01),
                    decoration: BoxDecoration(
                      border: Border(bottom: BorderSide(width: 1.0)),
                    ),
                    child: DropdownButtonHideUnderline(
                      child: ButtonTheme(
                        alignedDropdown: true,
                        child: DropdownButton<String>(
                          itemHeight: 80,
                          elevation: 1,

                          autofocus: true,
                          focusColor: Colors.redAccent[700],
                          //style: Theme.of(context).textTheme.title,
                          isExpanded: true,
                          hint: Text("Choose a Annual Income"),
                          items: _income.map((String dropDownStringItem) {
                            return DropdownMenuItem<String>(
                                value: dropDownStringItem,
                                child: Text(dropDownStringItem,
                                    style: TextStyle(
                                        fontSize: 17,
                                        fontWeight: FontWeight.w500,
                                        color: Colors.redAccent[700])));
                          }).toList(),
                          onChanged: (String newValueSelected) =>
                              _onDropDownIncomeSelected(newValueSelected),
                          value: _currentIncomeSelected,
                        ),
                      ),
                    ),
                  )
                ],
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                width: MediaQuery.of(context).size.width * 0.8,
                child: RaisedButton(
                  color: Colors.redAccent[700],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  onPressed: () {
                    waiting(context);
                    _validateInputs(context);
                  },
                  child: Text("Next"),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
