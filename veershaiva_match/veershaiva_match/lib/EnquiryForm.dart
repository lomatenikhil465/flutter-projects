import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class EnquiryForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _EnquiryForm();
  }
  
}

class _EnquiryForm extends State<EnquiryForm> {

  String name, email, message, mobile;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Enquiry Form"),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
                        //width: MediaQuery.of(context).size.width * 0.8,
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width *0.02,
                            right: MediaQuery.of(context).size.width *0.02,
                            top: MediaQuery.of(context).size.height *0.02,
                        ),
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please Enter your Name';
                            }
                            return null;
                          },
                          onSaved: (String name) {
                            this.name = name;
                          },
                          decoration: InputDecoration(
                            
                            labelText: "Name",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                          ),
                          keyboardType: TextInputType.text,
                          
                          style: new TextStyle(
                            fontFamily: "Poppins",
                          ),
                        ),
                      ),
                      Container(
                        //width: MediaQuery.of(context).size.width * 0.8,
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width *0.02,
                            right: MediaQuery.of(context).size.width *0.02,
                            top: MediaQuery.of(context).size.height *0.02,
                        ),
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please Enter your Email';
                            }
                            return null;
                          },
                          onSaved: (String email) {
                            this.email= email;
                          },
                          decoration: InputDecoration(
                            
                            labelText: "Email",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                          ),
                          keyboardType: TextInputType.emailAddress,
                          
                          style: new TextStyle(
                            fontFamily: "Poppins",
                          ),
                        ),
                      ),
                      Container(
                        //width: MediaQuery.of(context).size.width * 0.8,
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width *0.02,
                            right: MediaQuery.of(context).size.width *0.02,
                            top: MediaQuery.of(context).size.height *0.02,
                        ),
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please Enter your Mobile No.';
                            }
                            return null;
                          },
                          onSaved: (String mobile) {
                            this.mobile = mobile;
                          },
                          decoration: InputDecoration(
                            
                            labelText: "Mobile No.",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                          ),
                          keyboardType:
                                          TextInputType.numberWithOptions(
                                              decimal: true, signed: false),
                                               inputFormatters: [ LengthLimitingTextInputFormatter(10),
                              new WhitelistingTextInputFormatter(
                                  RegExp("[0-9]"))],
                          
                          style: new TextStyle(
                            fontFamily: "Poppins",
                          ),
                        ),
                      ),
                      Container(
                        //width: MediaQuery.of(context).size.width * 0.8,
                        padding: EdgeInsets.only(
                            left: MediaQuery.of(context).size.width *0.02,
                            right: MediaQuery.of(context).size.width *0.02,
                            top: MediaQuery.of(context).size.height *0.02,
                        ),
                        child: TextFormField(
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Please Enter your Message';
                            }
                            return null;
                          },
                          onSaved: (String msg) {
                            this.message = msg;
                          },
                          maxLines: 3,
                          decoration: InputDecoration(
                            
                            labelText: "Message",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                          ),
                          keyboardType: TextInputType.text,
                          
                          style: new TextStyle(
                            fontFamily: "Poppins",
                          ),
                        ),
                      ),

                      Container(
                        padding: EdgeInsets.only(top: MediaQuery.of(context).size.height *0.02, ),
                        width: MediaQuery.of(context).size.width *0.4,
                        child: RaisedButton(
                          onPressed: (){},
                          child: Text("Submit"),
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
                          color: Colors.redAccent[700],
                          textColor: Colors.white,
                        ),
                      )
          ],
        ),
      ),
    );
  }
  
}