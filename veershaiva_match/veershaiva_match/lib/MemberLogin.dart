import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:veershaiva_match/EditProfile.dart';
import 'package:veershaiva_match/ForgotPassPage.dart';
import 'package:veershaiva_match/SignUpPage.dart';

class MemberLogin extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MemberLogin();
  }
}

class _MemberLogin extends State<MemberLogin> {
  String mobile, pass;
  bool _obscureText = true;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  SharedPreferences prefs;
  bool _autoValidate = false;
  List userData;

  Future<String> getJsonData() async {
    prefs = await SharedPreferences.getInstance();
    String url =
        "https://www.veershaivamatch.com/app/registerApp.php?apicall=login";

    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'mobile': mobile,
          'pwd': pass,
        });
    setState(() {
      var convertDataToJson = json.decode(response.body);
      userData = convertDataToJson['user'];
    });
    print(response.body);

    return "success";
  }

  waiting(BuildContext context) {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return WillPopScope(
          onWillPop: () async => false,
          child: AlertDialog(
              key: _keyLoader,
              content: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                    child: CircularProgressIndicator(),
                  ),
                  Container(
                    child: Text("Loading..."),
                  )
                ],
              )),
        );
      },
    );
  }

  void _validateInputs() async {
    if (_formKey.currentState.validate()) {
//    If all data are correct then save data to out variables
      waiting(context);
      _formKey.currentState.save();
      setState(() {
        _autoValidate = false;
      });
      getJsonData().then((value) {
        Navigator.of(_keyLoader.currentContext).pop();
        if (userData != null) {
          prefs.setString('mobile', mobile);
          prefs.setString('pass', pass);
          prefs.setString('id', userData[0]["CustID"]);
          prefs.setString('gender', userData[0]["bi_gender"]);

          Navigator.of(context)
              .pushReplacement(MaterialPageRoute(builder: (context) {
            return EditProfile();
          }));
        } else {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              // return object of type Dialog
              return AlertDialog(
                title: new Text(
                  "Error!",
                  style: TextStyle(color: Colors.red[600]),
                ),
                content: new Text(
                    "Please check your Mobile No. / Match ID and Password"),
                actions: <Widget>[
                  // usually buttons at the bottom of the dialog
                  new FlatButton(
                    child: new Text("Close"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        }
      });
      _formKey.currentState.reset();
    } else {
//    If all data are not valid then start auto validation.
      setState(() {
        _autoValidate = true;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Personal Details "),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          autovalidate: _autoValidate,
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.02,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  children: [
                    Container(
                      child: Text(
                        "Mobile No. / Match ID : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.94,
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Mobile No. / Match ID';
                          }
                          return null;
                        },
                        onSaved: (String mobile) {
                          this.mobile = mobile;
                        },
                        decoration: InputDecoration(
                            hintText: "Mobile No. / Match ID",
                            border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(10.0),
                              borderSide: new BorderSide(),
                            ),
                            contentPadding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.015,
                                left:
                                    MediaQuery.of(context).size.width * 0.015)),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  children: [
                    Container(
                      child: Text(
                        "Password : ",
                        style: TextStyle(fontSize: 16),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.02,
                ),
                child: Row(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.94,
                      child: TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please Enter your Password';
                          }
                          return null;
                        },
                        onSaved: (String pass) {
                          this.pass = pass;
                        },
                        obscureText: _obscureText,
                        decoration: InputDecoration(
                          hintText: "Password",
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                          suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                _obscureText = !_obscureText;
                              });
                            },
                            child: Icon(
                              _obscureText
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Colors.redAccent[700],
                            ),
                          ),
                          contentPadding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.015,
                              left: MediaQuery.of(context).size.width * 0.015),
                        ),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.01),
                width: MediaQuery.of(context).size.width * 0.8,
                child: RaisedButton(
                  color: Colors.redAccent[700],
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5.0)),
                  onPressed: () {
                    // Navigator.of(context).push(MaterialPageRoute(builder: (context){
                    //   return LoginDetails();
                    // }));
                    _validateInputs();
                  },
                  child: Text("Login"),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02),
                    child: RawMaterialButton(
                      onPressed: () {
                        FocusScope.of(context).unfocus();
                        Navigator.of(context)
                            .push(MaterialPageRoute(builder: (context) {
                          return ForgotPassPage();
                        }));
                      },
                      child: Text(
                        "Forgot Password?",
                        style: TextStyle(color: Colors.redAccent[700]),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(
                        right: MediaQuery.of(context).size.width * 0.02),
                    child: RawMaterialButton(
                      onPressed: () {
                        FocusScope.of(context).unfocus();
                        Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (context) {
                          return SignUpPage();
                        }));
                      },
                      child: Text(
                        "Sign Up",
                        style: TextStyle(color: Colors.redAccent[700]),
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
