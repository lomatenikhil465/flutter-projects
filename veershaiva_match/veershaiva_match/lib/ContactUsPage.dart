import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ContactUsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ContactUsPage();
  }
  
}

class _ContactUsPage extends State<ContactUsPage> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Contact Us"),
        backgroundColor: Colors.redAccent[700],
      ),
      body: SingleChildScrollView(
              child: Column(
          children: [
             Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01, left: MediaQuery.of(context).size.width * 0.01),
              child: Row(
                children: [
                  Container(
                    child: Icon(Icons.call, color: Colors.redAccent[700],)
                  ),
                  Container(
                    child: Text(" Contact Us", style: TextStyle(fontSize: 20),),
                  )
                ],
              ),
            ),
            Divider(thickness: 1.0,),
            Container(
              child: Row(
               // mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.07),
                    child: Text(
                      "Contact Person",
                       style: TextStyle(
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.2,),
                  Flexible(
                    child: Text(
                      "Mr. Ishwar Swami Holimath"
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: Row(
                
                children: [
                  Container(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.07),
                    child: Text(
                      "Call Timing",
                      style: TextStyle(
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.2,),
                  Flexible(
                    child: Text(
                      "9 AM To 2 PM and 4 PM to 8 PM"
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                
                children: [
                  Container(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.07),
                    child: Text(
                      "Address",
                      style: TextStyle(
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.2,),
                  Flexible(
                    
                    child: Text(
                      "Block No:10 Sunmitra Nagar, Near Siddhivinayak Tempal,Mitra Nagar Road, Shelagi Solapur-413006."
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: Row(
                //mainAxisAlignment: MainAxisAlignment.spaceAround,
                
                children: [
                  Container(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.07),
                    child: Text(
                      "Mobile No.",
                      style: TextStyle(
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.2,),
                  Flexible(
                    child: Text(
                      "+91 9422370687, +91 8149070687, +91 7588214687"
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: Row(
                //mainAxisAlignment: MainAxisAlignment.spaceAround,
                
                children: [
                  Container(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.07),
                    child: Text(
                      "Email Id",
                      style: TextStyle(
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.2,),
                  Flexible(
                    child: Text(
                      "veershaivamatch@gmail.com"
                    ),
                  )
                ],
              ),
            ),
            
            Container(
              padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01, left: MediaQuery.of(context).size.width * 0.01),
              child: Row(
                children: [
                  Container(
                    child: Icon(Icons.monetization_on, color: Colors.redAccent[700],)
                  ),
                  Container(
                    child: Text(" Bank Account Details", style: TextStyle(fontSize: 20),),
                  )
                ],
              ),
            ),
            Divider(thickness: 1.0,),
            Container(
              child: Row(
               // mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.07),
                    child: Text(
                      "A/C Holder Name",
                       style: TextStyle(
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.2,),
                  Flexible(
                    child: Text(
                      "Mr. Ishwaryya S. Holimath"
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: Row(
               // mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.07),
                    child: Text(
                      "Bank Name",
                       style: TextStyle(
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.2,),
                  Flexible(
                    child: Text(
                      "State Bank of India Market Yard Branch , Solapur Maharashtra, India."
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: Row(
               // mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.07),
                    child: Text(
                      "Account No.",
                       style: TextStyle(
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.2,),
                  Flexible(
                    child: Text(
                      "32427916170"
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: Row(
               // mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.07),
                    child: Text(
                      "IFSC Code",
                       style: TextStyle(
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.2,),
                  Flexible(
                    child: Text(
                      "SBIN0004762"
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: Row(
               // mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.07),
                    child: Text(
                      "Bank Name",
                       style: TextStyle(
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.2,),
                  Flexible(
                    child: Text(
                      "Corporation Bank Solapur Branch , Solapur Maharashtra, India"
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: Row(
               // mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.07),
                    child: Text(
                      "Account No.",
                       style: TextStyle(
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.2,),
                  Flexible(
                    child: Text(
                      "520291040312904"
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: Row(
               // mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.07),
                    child: Text(
                      "IFSC Code",
                       style: TextStyle(
                        fontWeight: FontWeight.w500
                      ),
                    ),
                  ),
                  SizedBox(width: MediaQuery.of(context).size.width * 0.2,),
                  Flexible(
                    child: Text(
                      "CORP0000188"
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
  
}