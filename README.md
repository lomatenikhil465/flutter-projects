# Flutter Projects

Only flutter mobile applications are here.

## Technology
- Flutter - Front End
- PHP - backend Handling
- MySQL - Database
- REST

## Tools
- Android Studio
- Visual Studio code Editor
- SQLyog

## 1. Anmol Mart Project
This is The E-commerce project that have done in Flutter mobile application.

## Description
This project have the offers and categry wise pages. We can add to the cart and generate the "proceed to check out" option in the cart. In payment process, there are two options to be given "COD" and online Payment option. We can delete the items from the cart. In the home Page, you can see your banners and these banners are get from the Table. I have given various options like change password, profile change or view profile, categories and you can see your orders.

## 2. MVKK
This is the simple project for a Group.

## Description
In this project, I have done OTP authentication for a customer like if you are new to the company then you have to first register with your mobile number. I have done notification panel also where you can see your all notifications. This project also generates a bill in PDF for a customer. I have given payment option directly to the browser. And customer can pay the bills for registertion. After some matter of time they can renew there package also. We can see the PDF that uploaded in database also. 

## 3. Pigmi
This is the financial application for a customer.

## Description
Its just like the **banking system**. I have also given OTP authentication for login in this app and In home page I have given all the report for today, yesterday and so on in just a short format. There are various kind of options like the daily, weekly and monthly customers So I have given a button for all those in a home page. So the admin or owner of this app can generate the bill for all those customers. There is registration option, in this option we can create a customer of any daily, weekly and monthly customers. **Its just like the EMI for those 3 options.** So in registration, we can see the calculations of EMI in this page and I have inserted all those calculations for further need. There is a "Legder" option in home page, this option is for checking the a customer's statement its just like the statement of bank and I have given print option also for printing all the statements. There is "Expenses" option for registering daily basis or any expense you have done on that day, you can register on Expenses option. We can see all the customers completed invoice or non-completed invoice. We can also see the pending of particulars customers and all customers in one place also.

## 4. Salesman
This is the billing application for a customer.

## Description
In this project, I have give the product registeration option, generate order for a customer, display the total products, display the total orders for a customers, create a PDF bill for a customer, check the orders statement for a customer, small report for a owner, we can add employees for a particular company, purchase from any other company, take a payment from a customer. We can see particular employee's sale and purchase. We can give permissions to a particular employee for saling or purchasing or receiving payments or paying bills. We can see the stocks for a particular product. We can add or delete the customer. We can generate onlu quotation for some products. There is a option called "Day close". In this option, we can see all the reports for todays or any date of sale or purchase of single employee. We can resgiter expenses of single employee or customer also. We can see the particular customer's invoices that have generated and we can delete them also. There is a auto generated bill no. for all sale and purchase. We can see single invoice of customer that what product a customer purchase or sale and we can share a pdf to a customer or anybody.

## 5. SMiT SP
This is the billing application for a customer.

## Description
This is the same as above application of billing for a different client.

## 6. Scholars Academy
This is the classes application.

## Description
In this project, I give to see the students our score in profile, Change in there profile, to see which date our exam will be, we can give the exams from here also, see the assignments or notes that teacher have uploaded, we can watch the lectures that have uploaded in the server. There is a exam page in this application which we can start only the when the exam time is given from a teacher. We can also see the timing when we enter a exam. I have given the all question numbers in grid format so that student can jump whichever question number they want. I have given the options for see later, marked, saved completed, non-completed question grid colour. So that student can see which of those questions are missed or anything.

## 7. Veershaiva Match
This is the matrimony application.

## Description
In this project, I gave the options to a customer to choose their partner from online. We can search for a particular age or cast or anything . We can see the information about the customer. We can register for a customer. I have given the options that how many times your profile have seen. There are some packages of different amount that we can enroll. This is the application i have done for a customer.

## 8. Wagaj Classes
This is the classes application.

## Description
This is same application I have done for Scholars Academy client.

## Project status
All projects are live.
