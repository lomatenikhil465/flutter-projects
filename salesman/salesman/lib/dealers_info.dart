import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class DealersInfo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DealersInfo();
  }
}

List data;

class _DealersInfo extends State<DealersInfo> {
  String url =
      "http://eannolautomotive.com/app/getDealerList.php?apicall=getDealers";

  @override
  void initState() {
    super.initState();
    this.getJsonData();
  }

  Future<String> getJsonData() async {
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      data = convertDataToJson['data'];
    });

    return "success";
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Dealer's Information",
        ),
        backgroundColor: Colors.lightBlue[900],
      ),
      body: ListView.separated(
          separatorBuilder: (BuildContext context, int index) => Divider(
                thickness: 1.0,
              ),
          itemCount: data == null ? 0 : data.length,
          padding: EdgeInsets.only(
              top: MediaQuery.of(context).size.height / 50.0,
              right: MediaQuery.of(context).size.height / 30.0,
              left: MediaQuery.of(context).size.height / 30.0,
              bottom: MediaQuery.of(context).size.height / 50.0),
          itemBuilder: (BuildContext context, int index) {
            return Container(
              child: GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .push(new MaterialPageRoute(builder: (context) {
                      return DisplayDealer(
                        index: index,
                      );
                    }));
                  },
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Container(
                            child: Text(
                              (index + 1).toString(),
                              style: TextStyle(
                                  fontSize: 15.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Expanded(
                              child: Column(
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.05),
                                child: Row(
                                  
                                  children: <Widget>[
                                    Container(
                                      child: Text(
                                        "Name : " + data[index]["dealer_name"],
                                        style: TextStyle(
                                            fontSize: 15.0,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.05),
                                child: Row(
                                  
                                  children: <Widget>[
                                    Container(
                                      child: Text(
                                        "Contact No. : " +
                                            data[index]["contact_no"],
                                        style: TextStyle(
                                           
                                            fontSize: 15.0),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width *
                                        0.05),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      child: Text(
                                        "Area : " + data[index]["address"],
                                        style: TextStyle(
                                            fontSize: 15.0,
                                            ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ))
                        ],
                      ),
                    ],
                  )),
            );
          }),
    );
  }
}

class DisplayDealer extends StatefulWidget {
  DisplayDealer({this.index});

  final int index;
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DisplayDealer();
  }
}

class _DisplayDealer extends State<DisplayDealer> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Dealer's Information"),
        backgroundColor: Colors.lightBlue[900],
      ),
      body: Column(
        children: <Widget>[
          Center(
            child: Icon(
              Icons.person,
              color: Colors.lightBlue[900],
              size: 115.0,
            ),
          ),
          Padding(
              padding: EdgeInsets.only(
            top: MediaQuery.of(context).size.height / 50.0,
          )),
          Container(
            child: Text(
              "Name : " + data[widget.index]["dealer_name"],
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(
            top: MediaQuery.of(context).size.height / 50.0,
          )),
          Container(
            child: Text(
              "Email : " + data[widget.index]["email"],
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(
            top: MediaQuery.of(context).size.height / 50.0,
          )),
          Container(
            child: Text(
              "Contact No. : " + data[widget.index]["contact_no"],
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }
}
