import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'order_confirmation.dart';

class DealerOrders extends StatefulWidget {
  @override
  _DealerOrders createState() => _DealerOrders();
}

List orderData;

class _DealerOrders extends State<DealerOrders> {
  List data;

  var currentSelectedDealerName = "smit";
  var currentSelectedDealerID = "3";
  var currentMonth = "January";
  var _monthOfBusiness = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  //var _noOfBusiness = [1, 23, 654, 8, 6, 9, 1, 4];
  String url =
      "http://eannolautomotive.com/app/getDealerList.php?apicall=getDealers";
  var _orderNumbers = [10, 20];
  // var _orderAmount = [100, 200];
  var now = DateTime.now();
  var _statusOfOrder = ["pending", "pending"];
  @override
  void initState() {
    super.initState();
    this.getJsonData();
  }

  Future<String> getOrderData(int monthID) async {
    String url =
        "http://eannolautomotive.com/app/getDealerList.php?apicall=getDealerOrder&id=" +
            currentSelectedDealerID +
            "&month=" +
            monthID.toString();
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      orderData = convertDataToJson['data'];
    });

    return "success";
  }

  Future<String> getJsonData() async {
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      data = convertDataToJson['data'];
    });

    return "success";
  }

  void confirmationScreen(int index) async {
    var result;

    result =
        await Navigator.of(context).push(MaterialPageRoute(builder: (context) {
      return OrderConfirmation(
        index: index,
      );
    }));

    print(result);
    if (result != null) {
      showDialog(
          context: context,
          builder: (context) => AlertDialog(
                title: Text("$result"),
                content: Text("$result successfully"),
              ));
    }

    setState(() {
      if (result != null) {
        if(result.toString() == "reject"){
          orderData[index]["order_sts"] = "-1";
        }else{
          orderData[index]["order_sts"] = "1";
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Dealer Orders"),
        backgroundColor: Colors.lightBlue[900],
      ),
      body: Container(
        padding: EdgeInsets.all(MediaQuery.of(context).size.width / 10),
        child: Column(
          children: <Widget>[
            Text(
              "Select Dealer's Name : ",
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
            Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.height / 25)),
            Container(
                height: 50.0,
                width: 300.0,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(15.0),
                  border: Border.all(
                      color: Colors.lightBlue[900],
                      style: BorderStyle.solid,
                      width: 2.0),
                ),
                child: DropdownButtonHideUnderline(                                                                                                                      
                    child: ButtonTheme(
                        alignedDropdown: true,
                        child: DropdownButton(
                          elevation: 10,
                          isDense: true,
                          autofocus: true,
                          focusColor: Colors.lightBlue[900],
                          style: Theme.of(context).textTheme.title,
                          isExpanded: true,
                          items: data?.map((dropDownDealerName) {
                                return DropdownMenuItem(
                                    value: dropDownDealerName["dealer_name"],
                                    child: Container(
                                        child: Text(
                                      dropDownDealerName["dealer_name"],
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.lightBlue[900]),
                                    )));
                              })?.toList() ??
                              [],
                          onChanged: (selectedDealerName) {
                            setState(() {
                              this.currentSelectedDealerName =
                                  selectedDealerName;

                              for (int i = 0; i < data.length; i++) {
                                if (data[i]["dealer_name"] ==
                                    currentSelectedDealerName) {
                                  this.currentSelectedDealerID =
                                      data[i]["id"].toString();
                                }
                              }
                            });

                            Fluttertoast.showToast(
                                msg: this.currentSelectedDealerID.toString(),
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.CENTER,
                                timeInSecForIos: 1,
                                backgroundColor: Colors.red,
                                textColor: Colors.white,
                                fontSize: 16.0);
                          },
                          value: currentSelectedDealerName.toString(),
                          hint: Text("Select"),
                        )))),
            Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.width / 18)),
            Row(
              children: <Widget>[
                Container(
                  child: Text(
                    "Month : ",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                  ),
                ),
                Container(
                    height: 50.0,
                    width: 200.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      border: Border.all(
                          color: Colors.lightBlue[900],
                          style: BorderStyle.solid,
                          width: 2.0),
                    ),
                    child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                      alignedDropdown: true,
                      child: DropdownButton<String>(
                          hint: Text("Select"),
                          value: currentMonth,
                          elevation: 10,
                          isDense: true,
                          autofocus: true,
                          focusColor: Colors.lightBlue[900],
                          style: Theme.of(context).textTheme.title,
                          items: _monthOfBusiness?.map((String monthName) {
                                return DropdownMenuItem<String>(
                                    value: monthName,
                                    child: Container(
                                      child: Text(
                                        monthName,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.lightBlue[900]),
                                      ),
                                    ));
                              })?.toList() ??
                              [],
                          onChanged: (String changedMonthName) {
                            setState(() {
                              this.currentMonth = changedMonthName;
                              var currentMonthID = 1;
                              for (int i = 0;
                                  i < _monthOfBusiness.length;
                                  i++) {
                                if (_monthOfBusiness[i] == changedMonthName) {
                                  currentMonthID = i + 1;
                                }
                              }

                              getOrderData(currentMonthID);
                            });
                          }),
                    )))
              ],
            ),
            Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.width / 18)),
            Container(
              child: Text(
                "Total count of " +
                    currentMonth +
                    " : " +
                    (orderData == null ? "0" : orderData.length.toString()),
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.width / 18)),
            Expanded(
              child: ListView.builder(
                  itemCount: orderData == null ? 0 : orderData.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Container(
                      height: 100.0,
                      width: 500.0,
                      child: GestureDetector(
                          onTap: () => confirmationScreen(index),
                          child: Card(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              20,
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              30),
                                      child: Text(
                                        "Order Number : " +
                                            orderData[index]["deliverId"],
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15.0),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                          left: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              20,
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              30),
                                      child: Text(
                                        "status : ",
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15.0),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              30),
                                      child: Text(
                                        (orderData[index]["order_sts"] != "1"
                                            ? (orderData[index]["order_sts"] == "-1" ? "reject" : "pending")
                                            : "confirm"),
                                        style: TextStyle(
                                            color: (orderData[index]
                                                        ["order_sts"] != "1"
                                                ? Colors.red
                                                : Colors.green),
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15.0),
                                      ),
                                    ),
                                  ],
                                ),
                                Divider(),
                                Container(
                                  padding: EdgeInsets.only(
                                    left:
                                        MediaQuery.of(context).size.width / 20,
                                  ),
                                  child: Text(
                                    "Order Date : " + orderData[index]["dt"],
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15.0),
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width /
                                          20,
                                      top: MediaQuery.of(context).size.width /
                                          70),
                                  child: Text(
                                    "Order Amount : " +
                                        orderData[index]["tot_amount"],
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15.0),
                                  ),
                                )
                              ],
                            ),
                          )),
                    );
                  }),
            )
          ],
        ),
      ),
    );
  }
}
