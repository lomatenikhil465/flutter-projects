import 'package:flutter/material.dart';
import 'package:salesman/myOrders.dart';
import 'main.dart';
import 'dealers_orders.dart';
import 'dealers_info.dart';
import 'dealer_entry.dart';
import 'order_details.dart';

class Menu extends StatefulWidget {
  @override
  _MenuPage createState() => _MenuPage();
}

class _MenuPage extends State<Menu> {
  Future<bool> _onBackPressed() {
    return null;
  }

  Future buildShowDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.lightBlue[900], width: 2.0),
                borderRadius: BorderRadius.circular(10.0)), //this right here
            child: Container(
              height: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.height * 0.01,
                            left: MediaQuery.of(context).size.height * 0.01),
                        child: Text(
                          "Choose Option : ",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w500),
                        ),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          companyFlag = 1;
                          categoryUrl = "https://cp.nexenlubes.com/Application/getDealerList.php?apicall=getCategory";
                          productUrl =
                              "https://cp.nexenlubes.com/Application/getDealerList.php?apicall=getProductDetails&index=";
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return OrderDetails();
                          }));
                        },
                        child: Container(
                            height: 35,
                            width: 200,
                            margin: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.02,
                                left:
                                    MediaQuery.of(context).size.height * 0.01),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                color: Colors.lightBlue[900]),
                            child: Center(
                              child: Text(
                                "Nexen Products",
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            )),
                      )
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      GestureDetector(
                        onTap: () {
                          companyFlag = 0;
                          categoryUrl = "http://eannolautomotive.com/app/getDealerList.php?apicall=getEannolCategory";
                          productUrl =
                              "http://eannolautomotive.com/app/getDealerList.php?apicall=getProductDetails&index=";
                          Navigator.of(context)
                              .push(MaterialPageRoute(builder: (context) {
                            return OrderDetails();
                          }));
                        },
                        child: Container(
                            height: 35,
                            width: 200,
                            margin: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.02,
                                left:
                                    MediaQuery.of(context).size.height * 0.01),
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                color: Colors.lightBlue[900]),
                            child: Center(
                              child: Text(
                                "Eannol Products",
                                style: TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              ),
                            )),
                      )
                    ],
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Distributor"),
          backgroundColor: Colors.lightBlue[900],
        ),
        drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                  decoration: BoxDecoration(color: Colors.lightBlue[900]),
                  child: Text(
                    "Profile",
                    style: TextStyle(color: Colors.white, fontSize: 24),
                  )),
              ListTile(
                leading: Icon(Icons.lock),
                title: Text("Change Password"),
                onTap: () {},
              ),
            
              ListTile(
                leading: Icon(Icons.shopping_cart),
                title: Text("My cart"),
                onTap: () {},
              ),
              ListTile(
                leading: Icon(Icons.shopping_basket),
                title: Text("My Orders"),
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context){
                    return MyOrders();
                  }));
                },
              ),
              ListTile(
                leading: Icon(Icons.power_settings_new),
                title: Text("Logout"),
                onTap: () {
                  Navigator.of(context)
                      .pushReplacement(MaterialPageRoute(builder: (context) {
                    return MyHomePage();
                  }));
                },
              )
            ],
          ),
        ),
        body: WillPopScope(
          onWillPop: _onBackPressed,
          child: new GridView.count(
            crossAxisCount: 2,
            primary: false,
            padding: EdgeInsets.all(20.0),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            children: <Widget>[
              GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return DealerEntry();
                    }));
                  },
                  child: Card(
                    elevation: 5.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)
                    ),
                    child: new Container(
                      padding: EdgeInsets.all(20.0),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.lightBlue[900],
                          width: 3.0
                        ),
                        borderRadius: BorderRadius.circular(15.0),
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [Colors.deepOrange, Colors.white]),
                      ),
                      child: new Center(
                          child: Text(
                        "New Dealer Entry",
                        style:
                            new TextStyle(color: Colors.black, fontSize: 20.0),
                      )),
                    ),
                  )),
              GestureDetector(
                  child: Card(
                elevation: 5.0,
                shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)
                    ),
                child: new Container(
                  padding: EdgeInsets.all(20.0),
                  decoration: BoxDecoration(
                    border: Border.all(
                          color: Colors.lightBlue[900],
                          width: 3.0
                        ),
                    borderRadius: BorderRadius.circular(15.0),
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.deepOrange, Colors.white]),
                  ),
                  child: new Center(
                      child: Text(
                    "Stock",
                    style: new TextStyle(color: Colors.black, fontSize: 20.0),
                  )),
                ),
              )),
              GestureDetector(
                  onTap: () {
                    // Navigator.of(context).push(MaterialPageRoute(builder: (context){
                    //   return OrderDetails();
                    // }));
                    buildShowDialog(context);
                  },
                  child: Card(
                    elevation: 5.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)
                    ),
                    child: new Container(
                      padding: EdgeInsets.all(20.0),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.lightBlue[900],
                          width: 3.0
                        ),
                        borderRadius: BorderRadius.circular(15.0),
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [Colors.deepOrange, Colors.white]),
                      ),
                      child: new Center(
                          child: Text(
                        "Order",
                        style:
                            new TextStyle(color: Colors.black, fontSize: 20.0,),
                      )),
                    ),
                  )),
              GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return DealerOrders();
                    }));
                  },
                  child: Card(
                    elevation: 5.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)
                    ),
                    child: new Container(
                      padding: EdgeInsets.all(20.0),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.lightBlue[900],
                          width: 3.0
                        ),
                        borderRadius: BorderRadius.circular(15.0),
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [Colors.deepOrange, Colors.white]),
                      ),
                      child: new Center(
                          child: Text(
                        "Dealer's Orders",
                        style:
                            new TextStyle(color: Colors.black, fontSize: 20.0),
                      )),
                    ),
                  )),
              GestureDetector(
                  onTap: () {
                    Navigator.of(context)
                        .push(MaterialPageRoute(builder: (context) {
                      return DealersInfo();
                    }));
                  },
                  child: Card(
                    elevation: 5.0,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0)
                    ),
                    child: new Container(
                      padding: EdgeInsets.all(20.0),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.lightBlue[900],
                          width: 3.0
                        ),
                        borderRadius: BorderRadius.circular(15.0),
                        gradient: LinearGradient(
                            begin: Alignment.topCenter,
                            end: Alignment.bottomCenter,
                            colors: [Colors.deepOrange, Colors.white]),
                      ),
                      child: new Center(
                          child: Text(
                        "Dealer's Information",
                        style:
                            new TextStyle(color: Colors.black, fontSize: 20.0),
                      )),
                    ),
                  )),
            ],
          ),
        ));
  }
}
