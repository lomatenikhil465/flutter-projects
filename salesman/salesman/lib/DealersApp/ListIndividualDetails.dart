import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'ParticularCategoryProductDetails.dart';

typedef ChangeValueCallback = void Function();

class ListIndividualDetails extends StatefulWidget {
  final int index, companyFlag, categoryIndex;
  final ChangeValueCallback fun;
  final String companyUrl;
  ListIndividualDetails(
      {this.index, this.fun, this.companyUrl, this.companyFlag, this.categoryIndex});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ListIndividualDetails();
  }
}

int noOfCartItems = 0;

class _ListIndividualDetails extends State<ListIndividualDetails> {
  int selectedIndex = 0;
  List productInfo;
  Future<String> productInfoResponse;
  final _quantityInputController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  Future<String> insertInCart() async {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('dd/MM/yyyy').format(now);
    String formattedTime = DateFormat('HH:mm').format(now);
    print(productInfo[selectedIndex]["dealer_price"]);
    String url =
        "http://eannolautomotive.com/app/getDealerList.php?apicall=insertDealerCart&";
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'cndfId': '1',
          'asmId': '1',
          'distributorId': '1',
          'dealerId': '1',
          'prodId': particularProductData[widget.index]['ID'],
          'categoryId': widget.categoryIndex.toString(),
          'brandId': particularProductData[widget.index]["brand_nm"],
          'prodImg': particularProductData[widget.index]["image"],
          'prodName':particularProductData[widget.index]["product_nm"],
           'prodMrp':productInfo[selectedIndex]["rate"] == null ? "0" : productInfo[selectedIndex]["rate"] ,
          'prodQty': _quantityInputController.text,
          'prodLtr': (productInfo != null ? productInfo[selectedIndex]["ltr"] : ""),
          'dealerOrderDate': formattedDate,
          'dealerOrderTime': formattedTime,
          'checkoutSts': '0'
        }).then((value) {
      print(value.statusCode);
      _quantityInputController.clear();
    });

    //print(response.reasonPhrase);

    return "success";
  }

  Future<String> getJsonData() async {
    String url =
        "http://eannolautomotive.com/app/getDealerList.php?apicall=" + (widget.companyFlag == 1 ? "getNexenProductInfo": "getProductInfo") + "&prodId=" +
            particularProductData[widget.index]['ID'].toString() + "&categoryId=" + widget.categoryIndex.toString();

    print("url : " + url);
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      productInfo = convertDataToJson['data'];
    });

    return "success";
  }

  void _validateQuantity() {
    if (_quantityInputController.text != "") {
      print(noOfCartItems);
      widget.fun();
      insertInCart();
      setState(() {
        selectedIndex = 0;
      });

      Fluttertoast.showToast(
          msg: 'Successfully Added',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          textColor: Colors.black,
          fontSize: 16.0);
    } else {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Warning!"),
              content: Text("Please Fill Quantity"),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    }
  }

  @override
  void initState() {
    super.initState();
    
      productInfoResponse = this.getJsonData();
    
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                height: 125,
                width: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(color: Colors.grey, width: 1)),
                child: Image.network(widget.companyFlag == 1?
                "http://cp.nexenlubes.com/" + particularProductData[widget.index]["image"]
                  :particularProductData[widget.index]["image"],
                  loadingBuilder: (context, child, progress) {
                    return progress == null ? child : LinearProgressIndicator();
                  },
                  fit: BoxFit.fill,
                ),
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                                                  child: Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.05),
                            child: Text(
                               particularProductData[widget.index]
                                      ["product_nm"],
                                      softWrap: true,
                              style: TextStyle(
                                color: Colors.lightBlue[900],
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.05),
                            child: Text(
                              particularProductData[widget.index]
                                      ["ltr_qty"],
                              overflow: TextOverflow.clip,
                              softWrap: true,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Flexible(
                          child: Container(
                            padding: EdgeInsets.only(
                                right: MediaQuery.of(context).size.width * 0.05),
                            child: Text(
                              "MRP : " + 
                              (productInfo != null
                                      ? productInfo[selectedIndex]["retailer_rate"] == null ? "0" : productInfo[selectedIndex]["retailer_rate"]
                                      : ""),
                              overflow: TextOverflow.clip,
                              softWrap: true,
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.width * 0.03,
                          left: MediaQuery.of(context).size.width * 0.03),
                    ),
                    Form(
                      key: _formKey,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.only(
                                  left: MediaQuery.of(context).size.width *
                                      0.05)),
                          Container(
                            height: 35,
                            width: 90,
                            child: TextField(
                              controller: _quantityInputController,
                              decoration: new InputDecoration(
                                  labelText: "Enter quantity",
                                  border: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: Colors.green[400]),
                                  ),
                                  labelStyle: TextStyle(
                                    fontSize: 10,
                                  )),
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                WhitelistingTextInputFormatter.digitsOnly
                              ], // Only nu
                            ),
                          ),
                          Spacer(),
                          Container(
                            margin: EdgeInsets.only(
                                right:
                                    MediaQuery.of(context).size.width * 0.05),
                            height: 35,
                            width: 103,
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.lightBlue[900], width: 2.0),
                                borderRadius: BorderRadius.circular(5.0)),
                            child: FlatButton(
                              onPressed: productInfo == null || productInfo[selectedIndex]["retailer_rate"] == null
                                  ? null
                                  : () {
                                      _validateQuantity();
                                    },
                              child: Text(
                                "Add to Cart",
                                style: TextStyle(
                                    fontSize: 13,
                                    color: Colors.lightBlue[900],
                                    fontWeight: FontWeight.bold),
                              ),
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          Container(
            height: 40,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                physics: const ClampingScrollPhysics(),
                itemCount:  (productInfo != null ? productInfo.length :1),
                itemBuilder: (BuildContext context, int index) {
                  return productInfo == null 
                      ? Container(
                          child: Center(
                            child: Text("No data available..."),
                          ),
                        )
                      : Container(
                          padding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.width * 0.01,
                              left: MediaQuery.of(context).size.width * 0.02,
                              bottom: MediaQuery.of(context).size.width * 0.01),
                          child: FlatButton(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(color: Colors.lightBlue[900])),
                            onPressed: () {
                              setState(() {
                                selectedIndex = index;
                              });
                            },
                            child: Text(
                              (productInfo != null
                                      ? productInfo[index]["ltr"] == null ? "No Data Available...":  productInfo[index]["ltr"]
                                      : ""),
                              style: TextStyle(
                                  fontSize: 13,
                                  color: selectedIndex == index
                                      ? Colors.white
                                      : Colors.lightBlue[900]),
                            ),
                            color: selectedIndex == index
                                ? Colors.lightBlue[900]
                                : null,
                          ),
                        );
                }),
          )
        ],
      ),
    );
  }
}
