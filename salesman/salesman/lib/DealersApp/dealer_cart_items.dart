import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import './ListIndividualDetails.dart';

class DealerCartItems extends StatefulWidget {
  final ChangeCartItemCallback changeCartItemCount;
  DealerCartItems({this.changeCartItemCount});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DealerCartItems();
  }
}

List cartItems;
int selectedIndex = 0;
int totalSum = 0;
int total_qantity = 0;

class _DealerCartItems extends State<DealerCartItems> {
  Future<String> cartResponse;

  Future<String> getTotalCartItems() async {
    String url =
        'http://eannolautomotive.com/app/getDealerList.php?apicall=getDealerCartItems&dealerId=' +
            '1';
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      cartItems = convertDataToJson['data'];
      noOfCartItems = cartItems == null ? 0 : cartItems.length;
    });
    finalPrice = List<int>(noOfCartItems);
    return "success";
  }

  Future<String> updateOrderNo() async {
    String url =
        'http://eannolautomotive.com/app/getDealerList.php?apicall=updateOrderNo&orderNo=' +
            '10' +
            '&dealerId=' +
            '1' ;
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      cartItems = convertDataToJson['data'];
    });

    return "success";
  }

  

  void changeCartItem() {
  
    setState(() {
      //print("change cart items");
      
      selectedIndex = 0;
      
      cartResponse = this.getTotalCartItems();
    });
  }

  void changePrice() {
    setState(() {
      total_qantity = total_qantity;
    });
  }

  @override
  void initState() {
    super.initState();
    total_qantity = 0;
    totalSum = 0;
    finalPrice1 = [];
    cartResponse = this.getTotalCartItems();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlue[900],
        title: Text("Cart"),
      ),
      body: Center(
        child: cartItems == null
            ? Container(
                child: Center(child: Text("No Cart Items...")),
              )
            : FutureBuilder<String>(
                future: cartResponse,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return IndexedStack(
                      index: selectedIndex,
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            Expanded(
                              child: ListView.separated(
                                  separatorBuilder:
                                      (BuildContext context, int index) =>
                                          Divider(
                                            thickness: 0.0,
                                            color: Colors.lightBlue[900],
                                          ),
                                  itemCount:
                                      cartItems == null ? 0 : cartItems.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return IndividualCartItem(
                                        index: index,
                                        changeCartItem: changeCartItem,
                                        changePrice: changePrice);
                                  }),
                            ),
                            Container(
                                height: 40,
                                child: FlatButton(
                                    color: Colors.lightBlue[900],
                                    onPressed: () {},
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Container(
                                          child: Text(
                                            "Total Qty : " +
                                                total_qantity.toString(),
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white),
                                          ),
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(
                                              left: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  0.01),
                                          child: Text(
                                            "Total Price : " +
                                                totalSum.toString(),
                                            style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.white),
                                          ),
                                        )
                                      ],
                                    ))),
                            Container(
                                height: 50,
                                child: FlatButton(
                                    color: Colors.green[700],
                                    onPressed: () {
                                      if (cartItems != null) {
                                        setState(() {
                                          selectedIndex = 1;
                                        });

                                        updateOrderNo();
                                        changeCartItem();
                                        Fluttertoast.showToast(
                                            msg: 'Order Placed Successfully',
                                            toastLength: Toast.LENGTH_SHORT,
                                            gravity: ToastGravity.BOTTOM,
                                            timeInSecForIos: 1,
                                            textColor: Colors.black,
                                            fontSize: 16.0);
                                      }
                                    },
                                    child: Center(
                                      child: Text(
                                        "Place Order",
                                        style: TextStyle(
                                            fontSize: 16, color: Colors.white),
                                      ),
                                    ))),
                          ],
                        ),
                        Center(
                          child: CircularProgressIndicator(),
                        )
                      ],
                    );
                  } else {
                    print("else part");
                    return CircularProgressIndicator();
                  }
                }),
      ),
    );
  }
}

typedef ChangeCartItemCallback = void Function();

class IndividualCartItem extends StatefulWidget {
  final int index;
  final ChangeCartItemCallback changeCartItem, changePrice;
  IndividualCartItem({this.index, this.changeCartItem, this.changePrice});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _IndividualCartItem();
  }
}

List finalPrice = List<int>(noOfCartItems);
List finalPrice1 = [];
class _IndividualCartItem extends State<IndividualCartItem> {
  List productInfoData, productInfoDataTemp, productPrice;
  int result;
  String word;
  Future<String> removeFromCart(String prodId) async {
    String url =
        'http://eannolautomotive.com/app/getDealerList.php?apicall=removeFromDealerCart&prodId=' +
            prodId +
            '&dealerId=' +
            '1' +
            '&id=' +
            cartItems[widget.index]['ID'];
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    print(finalPrice);
    setState(() {
      selectedIndex = 1;
    });
    totalSum = totalSum - finalPrice[widget.index];
    total_qantity = total_qantity - int.parse(cartItems[widget.index]["prod_quantity"]);
    finalPrice[widget.index] = null;
    
    widget.changeCartItem();
    
    return "success";
  }

  Future<String> updatePrice(int sum) async {
    String url =
        'http://eannolautomotive.com/app/getDealerList.php?apicall=updatePrice&' +
            '&dealerId=' +
            '1' +
            '&prodPrice=' +
            sum.toString() +
            '&nID=' +
            cartItems[widget.index]["ID"];
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    
    return "success";
  }

  Future<String> getProductCalc() async {
    String 
      url =
        'http://eannolautomotive.com/app/getDealerList.php?apicall=getProductCalc&productId=' +
            cartItems[widget.index]["prod_id"] +
            '&categoryId=' +
            cartItems[widget.index]["category_id"] +
            '&ltr=' +
            cartItems[widget.index]["prod_litre"]+
            '&brand=' +
            cartItems[widget.index]["brand_id"];
    
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      productInfoData = convertDataToJson['data'];
    });
    productInfoDataTemp =
        new List<int>(productInfoData == null ? 0 : productInfoData.length);
    productPrice =
        new List<int>(productInfoData == null ? 0 : productInfoData.length);
    int sum = int.parse(cartItems[widget.index]["prod_quantity"]), res1, res2;
    for (int i = productInfoData.length - 1; i >= 0; i--) {
      int temp = int.parse(productInfoData[i]["s_offer_onbox"]);
      if ((sum ~/ temp).toInt() != 0) {
        productInfoDataTemp[i] = (sum ~/ temp).toInt();
        sum = sum - (productInfoDataTemp[i] * temp);
      } else {
        productInfoDataTemp[i] = 0;
      }
    }

    for (int i = 0; i < productInfoData.length; i++) {
      if (productInfoDataTemp[i] != 0) {
        res1 = int.parse(productInfoData[i]["offer_rate"]) -
            int.parse(productInfoData[i]["s_offer_per_ltr"]);
        res2 = res1 * int.parse(productInfoData[i]["total_qty"]);
        result = result + (res2 * productInfoDataTemp[i]);
        productPrice[i] = res2 * productInfoDataTemp[i];
      }
    }
    if(cartItems[widget.index]["prod_price"] == null){
      updatePrice(result);
    }
    finalPrice[widget.index] = result;
    totalSum = totalSum + result;
    total_qantity =
        total_qantity + int.parse(cartItems[widget.index]['prod_quantity']);
    print("Total quantity : " + cartItems[0]['prod_quantity']);
    //print("Result = " + totalSum.toString());
    widget.changePrice();
    return "success";
  }

  Future buildShowDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            actions: <Widget>[
              FlatButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text("Close"))
            ],
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)), //this right here
            content: Container(
              height: 300,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                      height: 35,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(18.0),
                              topRight: Radius.circular(18.0)),
                          color: Color(0xFFBC3358)),
                      child: Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.01),
                            child: Text(
                              "Product Price Calculations",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500),
                            ),
                          )
                        ],
                      )),
                  DataTable(
                    columnSpacing: 10.0,
                    dataRowHeight: 30,
                    columns: [
                      DataColumn(
                          label: Flexible(
                              child: Text(
                        "Offer Box",
                        softWrap: true,
                      ))),
                      DataColumn(
                          label: Flexible(
                              child: Text(
                        "No. of Boxes",
                        softWrap: true,
                      ))),
                      DataColumn(
                          label: Flexible(
                              child: Text(
                        "Amount",
                        softWrap: true,
                      ))),
                    ],
                    rows: List.generate(productInfoDataTemp.length, (index) {
                      if (productInfoDataTemp[index] != null) {
                        return DataRow(
                          cells: <DataCell>[
                            DataCell(
                                Text(productInfoData[index]["s_offer_onbox"])),
                            DataCell(Text(productInfoDataTemp[index] == null
                                ? "0"
                                : productInfoDataTemp[index].toString())),
                            DataCell(Text(productPrice[index] == null
                                ? "0"
                                : productPrice[index].toString())),
                          ],
                        );
                      }
                    }),
                  )
                ],
              ),
            ),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    result = 0;
    print("calculated");
    getProductCalc();
    //widget.changePrice();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return GestureDetector(
      onTap: () => buildShowDialog(context),
      child: Container(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  height: 75,
                  width: 60,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(color: Colors.grey, width: 1)),
                  child: Image.network(
                    cartItems[widget.index]['prod_img'],
                    loadingBuilder: (context, child, progress) {
                      return progress == null
                          ? child
                          : LinearProgressIndicator();
                    },
                    fit: BoxFit.fill,
                  ),
                ),
                Expanded(
                  child: Column(
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Flexible(
                            child: Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.05,
                                  top:
                                      MediaQuery.of(context).size.width * 0.01),
                              child: Text(
                                cartItems[widget.index]['prod_name'],
                                style: TextStyle(
                                    color: Colors.lightBlue[900],
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Flexible(
                            child: Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.05),
                              child: Text(
                                'Product Liter : ' +
                                    cartItems[widget.index]['prod_litre'],
                                overflow: TextOverflow.clip,
                                softWrap: true,
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.normal),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.05),
                            child: Text(
                              'DP : ' +
                                  (cartItems[widget.index]['prod_mrp'] == null
                                      ? '0'
                                      : cartItems[widget.index]['prod_mrp']),
                              overflow: TextOverflow.clip,
                              softWrap: true,
                              style: TextStyle(
                                  color: Colors.deepOrange,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                          Flexible(
                            child: Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.04),
                              child: Text(
                                ' Quantity in Box : ' +
                                    cartItems[widget.index]['prod_quantity'],
                                overflow: TextOverflow.clip,
                                softWrap: true,
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Flexible(
                            child: Container(
                              padding: EdgeInsets.only(
                                  left:
                                      MediaQuery.of(context).size.width * 0.05),
                              child: Text(
                                'Price : ' +
                                    (result == null ? "" : result.toString()),
                                overflow: TextOverflow.clip,
                                softWrap: true,
                                style: TextStyle(
                                    color: Colors.deepOrange,
                                    fontWeight: FontWeight.w500),
                              ),
                            ),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.delete_forever,
                        size: 35,
                      ),
                      onPressed: () {
                        if (cartItems != null) {
                          removeFromCart(cartItems[widget.index]['prod_id'])
                              .then((value) {
                            setState(() {
                              selectedIndex = 0;
                            });
                           
                            
                            
                            Fluttertoast.showToast(
                                msg: 'Product Removed Successfully',
                                toastLength: Toast.LENGTH_SHORT,
                                gravity: ToastGravity.BOTTOM,
                                timeInSecForIos: 1,
                                textColor: Colors.black,
                                fontSize: 16.0);
                          });
                        }
                      },
                      color: Colors.red[700],
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
