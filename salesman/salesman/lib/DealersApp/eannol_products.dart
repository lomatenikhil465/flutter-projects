import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dealers_menu.dart';
import 'ParticularCategoryProductDetails.dart';

class EannolProductsPage extends StatefulWidget {
  final int companyFlag;
  EannolProductsPage({this.companyFlag});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _EannolProductsPage();
  }
}

class _EannolProductsPage extends State<EannolProductsPage> {
  String _companyUrl =
      "http://eannolautomotive.com/app/DealerApp.php?apicall=";
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Center(
        child: FutureBuilder(
            future: eannolResponse,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.builder(
                    itemCount: eannolData == null ? 0 : eannolData.length,
                    padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height / 50.0,
                        right: MediaQuery.of(context).size.height / 30.0,
                        left: MediaQuery.of(context).size.height / 30.0,
                        bottom: MediaQuery.of(context).size.height / 50.0),
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        height: 45.0,
                        child: GestureDetector(
                          onTap: () {
                            Navigator.of(context)
                                .push(MaterialPageRoute(builder: (context) {
                              return ParticularCategoryProductDetails(
                                index: int.parse( eannolData[index]["ID"]),
                                companyUrl: _companyUrl,
                                companyFlag: widget.companyFlag,
                              );
                            }));
                          },
                          child: Card(
                              child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                padding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height /
                                        100,
                                    left: MediaQuery.of(context).size.height /
                                        50),
                                child: Image.asset(
                                  "assets/images/oil_Icon.png",
                                  color: Colors.lightBlue[900],
                                ),
                              ),
                              Container(
                                padding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height /
                                        100,
                                    left: MediaQuery.of(context).size.height /
                                        50),
                                child: Text(
                                  eannolData[index]["category"],
                                  style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.lightBlue[900]),
                                ),
                              ),
                            ],
                          )),
                        ),
                      );
                    });
              } else {
                return CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.blueAccent),
                );
              }
            }));
  }
}
