import 'package:flutter/material.dart';
import 'package:salesman/DealersApp/dealer_cart_items.dart';
import 'package:salesman/DealersApp/dealers_my_orders.dart';
import 'package:salesman/DealersApp/eannol_products.dart';
import 'package:salesman/DealersApp/nexen_products.dart';
import 'package:salesman/myOrders.dart';
import '../main.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
// import 'main.dart';
// import 'dealers_orders.dart';
// import 'dealers_info.dart';
// import 'dealer_entry.dart';
// import 'order_details.dart';

class DealerMenu extends StatefulWidget {
  @override
  _DealerMenuPage createState() => _DealerMenuPage();
}


List eannolData, nexenData;
Future<String> eannolResponse, nexenResponse;
class _DealerMenuPage extends State<DealerMenu>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  List cartItems;
    Future<String> getJsonData(String url,int companyFlag) async {
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      if(companyFlag == 0){
        eannolData = convertDataToJson['data'];
      } else if(companyFlag == 1){
        nexenData = convertDataToJson['data'];
      }
    });

    return "success";
  }
  

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    super.initState();
    eannolResponse = this.getJsonData("http://eannolautomotive.com/app/getDealerList.php?apicall=getEannolCategory", 0);
    nexenResponse = this.getJsonData("http://eannolautomotive.com/app/getDealerList.php?apicall=getCategory", 1);
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      drawer: Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              DrawerHeader(
                  decoration: BoxDecoration(color: Colors.lightBlue[900]),
                  child: Text(
                    "Profile",
                    style: TextStyle(color: Colors.white, fontSize: 24),
                  )),
              ListTile(
                leading: Icon(Icons.lock),
                title: Text("Change Password"),
                onTap: () {},
              ),
            
              ListTile(
                leading: Icon(Icons.shopping_cart),
                title: Text("My cart"),
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context){
                    return DealerCartItems(
                      changeCartItemCount: null,
                    );
                  }));
                },
              ),
              ListTile(
                leading: Icon(Icons.shopping_basket),
                title: Text("My Orders"),
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context){
                    return DealerMyOrders();
                  }));
                },
              ),
              ListTile(
                leading: Icon(Icons.power_settings_new),
                title: Text("Logout"),
                onTap: () {
                  Navigator.of(context)
                      .pushReplacement(MaterialPageRoute(builder: (context) {
                    return MyHomePage();
                  }));
                },
              )
            ],
          ),
        ),
        appBar: AppBar(
          backgroundColor: Colors.lightBlue[900],
          bottomOpacity: 1,
          bottom: TabBar(
            controller: _tabController,
            indicatorColor: Colors.white,
            indicatorSize: TabBarIndicatorSize.tab,
            tabs: [
              new Tab(
                
                child: Text(
                  "Eannol",
                  style: TextStyle(
                    fontSize: 20,
                  ),
                  ),
              ),
              new Tab(
                child: Text(
                  "Nexen",
                  style: TextStyle(
                    fontSize: 20
                  )
                  ),
              ),
            ],
          ),
          title: Text('Dealer'),
        ),
        body: TabBarView(
          children: [
            new EannolProductsPage(
              companyFlag: 0,
            ),
            new NexenProductsPage(
              companyFlag: 1,
            ),
            
          ],
          controller: _tabController,
        ));
  }
}
