import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import './dealers_my_orders.dart';

class DealerMyOrdersList extends StatefulWidget {
  final int index;
  DealerMyOrdersList({this.index});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DealerMyOrdersList();
  }
}

List myOrdersList;

class _DealerMyOrdersList extends State<DealerMyOrdersList> {
  Future<String> myOrdersListResponse;

  Future<String> getJsonData() async {
    String url =
        'http://eannolautomotive.com/app/getDealerList.php?apicall=getDealerIndividualItems&dealerId=' +
            '1' +
            "&orderNo=" +
            myOrdersData[widget.index]["order_no"];
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      myOrdersList = convertDataToJson['data'];
    });

  

    return "success";
  }

  @override
  void initState() {
    super.initState();
    myOrdersListResponse = this.getJsonData();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("My Orders Products"),
        backgroundColor: Colors.lightBlue[900],
      ),
      body: myOrdersList == null
          ? Container(
              child: Center(
                child: Text("No orders here..."),
              ),
            )
          : FutureBuilder<String>(
              future: myOrdersListResponse,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Column(
                    children: <Widget>[
                      Expanded(
                        child: ListView.separated(
                            itemBuilder: (BuildContext context, int index) {
                              return IndividualMyOrder(
                                index: index,
                              );
                            },
                            separatorBuilder:
                                (BuildContext context, int index) => Divider(
                                      thickness: 1.0,
                                    ),
                            itemCount:
                                myOrdersList != null ? myOrdersList.length : 1),
                      ),
                      Container(
                          height: 50,
                          color: Colors.lightBlue[900],
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              Center(
                                  child: Text(
                                "Total Price : " +
                                    (myOrdersData[widget.index]["prod_price_sum"] == null ? "" : myOrdersData[widget.index]["prod_price_sum"]),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                ),
                              )),
                              Center(
                                  child: Text(
                                "Total Quantity : " +
                                    (myOrdersData[widget.index]["prod_quantity_sum"] == null ? "" : myOrdersData[widget.index]["prod_quantity_sum"]),
                                style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                ),
                              )),
                            ],
                          ))
                    ],
                  );
                } else {
                  return Container(
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
                }
              },
            ),
    );
  }
}

class IndividualMyOrder extends StatefulWidget {
  final int index;
  IndividualMyOrder({this.index});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _IndividualMyOrder();
  }
}

class _IndividualMyOrder extends State<IndividualMyOrder> {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                height: 75,
                width: 60,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(color: Colors.grey, width: 1)),
                child: Image.network(
                  myOrdersList[widget.index]['prod_img'],
                  loadingBuilder: (context, child, progress) {
                    return progress == null ? child : LinearProgressIndicator();
                  },
                  fit: BoxFit.fill,
                ),
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.05),
                            child: Text(
                              myOrdersList[widget.index]['prod_name'],
                              style: TextStyle(
                                  color: Colors.lightBlue[900],
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.05),
                            child: Text(
                              'Product Liter : ' +
                                  myOrdersList[widget.index]['prod_litre'],
                              overflow: TextOverflow.clip,
                              softWrap: true,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.04),
                            child: Text(
                              ' Price : ' +
                                  myOrdersList[widget.index]['prod_price'],
                              overflow: TextOverflow.clip,
                              softWrap: true,
                              style: TextStyle(
                                  color: Colors.deepOrange,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Flexible(
                          child: Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.05),
                            child: Text(
                              ' Quantity in Box : ' +
                                  myOrdersList[widget.index]['prod_quantity'],
                              overflow: TextOverflow.clip,
                              softWrap: true,
                              style: TextStyle(
                                  color: Colors.deepOrange,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
