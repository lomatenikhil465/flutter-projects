import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

import './dealers_my_orders_list.dart';

class DealerMyOrders extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _DealerMyOrders();
  }
}

List myOrdersData;

class _DealerMyOrders extends State<DealerMyOrders> {
  Future<String> myOrdersResponse;

  Future<String> getJsonData() async {
    String url =
        'http://eannolautomotive.com/app/getDealerList.php?apicall=getDealerMyOrders&dealerId=' +
            '1';
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      myOrdersData = convertDataToJson['data'];
    });

    return "success";
  }

  @override
  void initState() {
    super.initState();
    myOrdersResponse = this.getJsonData();
  }

  void navigateToMyOrdersList(int index){
    Navigator.of(context).push(MaterialPageRoute(builder: (BuildContext context){
      return DealerMyOrdersList(index: index,);
    }));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("My Orders"),
          backgroundColor: Colors.lightBlue[900],
        ),
        body: myOrdersData == null
            ? Container(
                child: Center(
                  child: Text("No orders yet..."),
                ),
              )
            : FutureBuilder<String>(
                future: myOrdersResponse,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return ListView.builder(
                        // separatorBuilder: (BuildContext context, int index) =>
                        //     Divider(
                        //       thickness: 1.0,
                        //     ),
                        itemCount:
                            myOrdersData != null ? myOrdersData.length : 1,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            onTap: () => navigateToMyOrdersList(index),
                            child: Container(
                              
                              padding: EdgeInsets.all(
                                  MediaQuery.of(context).size.height * 0.01),
                              child: Container(
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5.0),
                                    border: Border.all(
                                        color: Colors.blue[200], width: 2.0)),
                                child: Card(
                                  elevation: 0,
                                  child: Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(
                                                left: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.01),
                                            child: Text(
                                              "Order No.: ",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.lightBlue[900],
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          Container(
                                            child: Text(
                                              myOrdersData[index]["order_no"]
                                                  .toString(),
                                              style: TextStyle(
                                                fontSize: 16,
                                                color: Colors.lightBlue[900],
                                                fontWeight: FontWeight.w500,
                                              ),
                                            ),
                                          ),
                                          Spacer(),
                                          Container(
                                            child: Text(
                                              "Order date : ",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.lightBlue[900],
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                right: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.01),
                                            child: Text(
                                              myOrdersData[index]
                                                      ["dealer_order_date"]
                                                  .toString(),
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.lightBlue[900],
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Divider(
                                        thickness: 3.0,
                                      ),
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(
                                                left: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.01),
                                            child: Text(
                                              "Total Quantity : ",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.lightBlue[900],
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          Container(
                                            child: Text(
                                              myOrdersData[index]
                                                      ["prod_quantity_sum"]
                                                  .toString(),
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.deepOrange,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          Spacer(),
                                          Container(
                                            child: Text(
                                              "Status : ",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          Container(
                                            padding: EdgeInsets.only(
                                                right: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.01),
                                            child: Text(
                                              (myOrdersData[index]["order_status"] == null || myOrdersData[index]["order_status"] == "0")? "pending" : "confirm",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w500,
                                                  color:myOrdersData[index]["order_status"] == null? Colors.deepOrange: Colors.green[600],),
                                            ),
                                          )
                                        ],
                                      ),
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.only(
                                                left: MediaQuery.of(context)
                                                        .size
                                                        .height *
                                                    0.01),
                                            child: Text(
                                              "Total Price : ",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.lightBlue[900],
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          Container(
                                            child: Text(
                                              myOrdersData[index]
                                                      ["prod_price_sum"]
                                                  .toString(),
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.green[600],
                                                  fontWeight: FontWeight.w500),
                                            ),
                                          ),
                                          
                                          
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        });
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ));
  }
}

