import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'ListIndividualDetails.dart';
import 'dealer_cart_items.dart';

class ParticularCategoryProductDetails extends StatefulWidget {
  final int index, companyFlag;
  final String companyUrl;
  ParticularCategoryProductDetails(
      {this.index, this.companyUrl, this.companyFlag});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ParticularCategoryProductDetails();
  }
}

int noOfCartItems = 0;
List particularProductData;

class _ParticularCategoryProductDetails
    extends State<ParticularCategoryProductDetails> {
  Future<String> particularProductResponse;
  List cartItems;
  Future<String> getJsonData() async {
    String url;
    if (widget.companyFlag == 1) {
      url = widget.companyUrl +
        "getProductNexenDetails&index=" +
        widget.index.toString();
        print(url);
    } else{
      url = widget.companyUrl +
        "getProductDetails&index=" +
        widget.index.toString();
    }
    
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      particularProductData = convertDataToJson['data'];
    });

    return "success";
  }

  Future<String> getTotalCartItems() async {
    String url =
        'http://eannolautomotive.com/app/getDealerList.php?apicall=getDealerCartItems&dealerId=' +
            '1';
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      cartItems = convertDataToJson['data'];
      noOfCartItems = cartItems == null ? 0 : cartItems.length;
    });

    return "success";
  }

  void changeValue() {
    setState(() {
      noOfCartItems++;
    });
  }

  @override
  void initState() {
    super.initState();
    particularProductResponse = this.getJsonData();
    getTotalCartItems();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Product Details"),
        backgroundColor: Colors.lightBlue[900],
        actions: <Widget>[
          new Padding(padding: const EdgeInsets.all(10.0)),
          Container(
            child: GestureDetector(
              onTap: () {},
              child: Stack(
                children: <Widget>[
                  new IconButton(
                    icon: new Icon(
                      Icons.shopping_cart,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return DealerCartItems(
                          changeCartItemCount: getTotalCartItems,
                        );
                      })).then((value) {
                        getTotalCartItems();
                        print(noOfCartItems);
                      });
                    },
                  ),
                  noOfCartItems == 0
                      ? Container()
                      : Positioned(
                          child: Stack(
                          children: <Widget>[
                            Icon(
                              Icons.brightness_1,
                              size: 20.0,
                              color: Colors.deepOrange,
                            ),
                            Positioned(
                                top: 3.0,
                                right: 4.0,
                                child: Center(
                                  child: Text(
                                    noOfCartItems.toString(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 11.0,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ))
                          ],
                        ))
                ],
              ),
            ),
          )
        ],
      ),
      body: Center(
        child: FutureBuilder<String>(
            future: particularProductResponse,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return particularProductData == null
                    ? Center(
                        child: Text("No data Available"),
                      )
                    : ListView.separated(
                        separatorBuilder: (BuildContext context, int index) =>
                            Divider(
                              thickness: 2.0,
                            ),
                        itemCount: particularProductData == null
                            ? 0
                            : particularProductData.length,
                        itemBuilder: (BuildContext context, int index) {
                          return ListIndividualDetails(
                            index: index,
                            categoryIndex: widget.index,
                            fun: changeValue,
                            companyUrl: widget.companyUrl,
                            companyFlag: widget.companyFlag,
                          );
                        });
              } else {
                return CircularProgressIndicator();
              }
            }),
      ),
    );
  }
}
