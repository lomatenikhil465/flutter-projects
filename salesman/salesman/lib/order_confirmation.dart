import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dealers_orders.dart';

class OrderConfirmation extends StatefulWidget {
  OrderConfirmation({this.index});

  final int index;
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _OrderConfirmation();
  }
}

class _OrderConfirmation extends State<OrderConfirmation> {
  DealerOrders object = new DealerOrders();
  
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("Order Confirmation"),
          backgroundColor: Colors.orangeAccent,
        ),
        body: Column(
          children: <Widget>[
            Padding(
                padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.width / 18)),
            Container(
              child: Text(
                "Order Number : " +
                    orderData[widget.index]["deliverId"].toString(),
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Divider(),
            Container(
              child: Text(
                "Order Date : " + orderData[widget.index]["dt"],
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Divider(),
            Container(
              child: Text(
                "Order Amount : " + orderData[widget.index]["tot_amount"],
                style: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            Padding(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.width / 18)),
            Row(
              children: <Widget>[
                Padding(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width / 18)),
                Container(
                    width: 150.0,
                    child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop("confirm");
                        },
                        shape: new RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.green)),
                        textColor: Colors.green,
                        //padding: EdgeInsets.only(right: 100.0,left: 100.0,top: 10.0,bottom: 10.0),
                        color: Colors.white,
                        child: Text(
                          "Confirm",
                          style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ))),
                Spacer(),
                Container(
                    width: 150.0,
                    child: RaisedButton(
                      onPressed: () {
                        Navigator.of(context).pop("reject");
                      },
                      shape: new RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(18.0),
                          side: BorderSide(color: Colors.red)),
                      textColor: Colors.red,
                      //padding: EdgeInsets.only(right: 100.0,left: 100.0,top: 10.0,bottom: 10.0),
                      color: Colors.white,
                      child: Text(
                        "Reject",
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            textBaseline: TextBaseline.alphabetic),
                      ),
                    )),
                Padding(
                    padding: EdgeInsets.only(
                        left: MediaQuery.of(context).size.width / 18)),
              ],
            )
          ],
        ));
  }
}
