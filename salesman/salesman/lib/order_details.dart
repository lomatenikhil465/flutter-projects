import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';
import 'cart_items.dart';
import 'package:intl/intl.dart';

class OrderDetails extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _OrderDetails();
  }
}

String productUrl = "";
String categoryUrl = "";
int companyFlag;

class _OrderDetails extends State<OrderDetails> {
  List data;
  Future<String> productResponse;
  String url = categoryUrl;

  @override
  void initState() {
    super.initState();
    productResponse = this.getJsonData();
  }

  Future<String> getJsonData() async {
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      data = convertDataToJson['data'];
    });

    return "success";
  }

  @override
  Widget build(BuildContext context) {
    print(productUrl);
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Order"),
        backgroundColor: Colors.lightBlue[900],
      ),
      body: Center(
          child: FutureBuilder(
              future: productResponse,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView.builder(
                      itemCount: data == null ? 0 : data.length,
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height / 50.0,
                          right: MediaQuery.of(context).size.height / 30.0,
                          left: MediaQuery.of(context).size.height / 30.0,
                          bottom: MediaQuery.of(context).size.height / 50.0),
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                          height: 45.0,
                          child: GestureDetector(
                            onTap: () {
                              Navigator.of(context)
                                  .push(MaterialPageRoute(builder: (context) {
                                return ParticularCategoryProductDetails(
                                  index: index + 1,
                                );
                              }));
                            },
                            child: Card(
                                child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height /
                                          100,
                                      left: MediaQuery.of(context).size.height /
                                          50),
                                  child: Image.asset(
                                    "assets/images/oil_Icon.png",
                                    color: Colors.lightBlue[900],
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(
                                      top: MediaQuery.of(context).size.height /
                                          100,
                                      left: MediaQuery.of(context).size.height /
                                          50),
                                  child: Text(
                                    data[index]["category"],
                                    style: TextStyle(
                                        fontSize: 15.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.lightBlue[900]),
                                  ),
                                ),
                              ],
                            )),
                          ),
                        );
                      });
                } else {
                  return CircularProgressIndicator(
                    valueColor:
                        AlwaysStoppedAnimation<Color>(Colors.blueAccent),
                  );
                }
              })),
    );
  }
}

class ParticularCategoryProductDetails extends StatefulWidget {
  final int index;
  ParticularCategoryProductDetails({this.index});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ParticularCategoryProductDetails();
  }
}

List particularProductData;

class _ParticularCategoryProductDetails
    extends State<ParticularCategoryProductDetails> {
  Future<String> particularProductResponse;
  List cartItems;
  Future<String> getJsonData() async {
    String url = productUrl + widget.index.toString();
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      particularProductData = convertDataToJson['data'];
    });

    return "success";
  }

  Future<String> getTotalCartItems() async {
    String url =
        'http://eannolautomotive.com/app/getDealerList.php?apicall=getCartItems&distributorId=' +
            '1';
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      cartItems = convertDataToJson['data'];
      noOfCartItems = cartItems == null ? 0 : cartItems.length;
    });

    return "success";
  }

  void changeValue() {
    setState(() {
      noOfCartItems++;
    });
  }

  @override
  void initState() {
    super.initState();
    particularProductResponse = this.getJsonData();
    getTotalCartItems();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Product Details"),
        backgroundColor: Colors.lightBlue[900],
        actions: <Widget>[
          new Padding(padding: const EdgeInsets.all(10.0)),
          Container(
            child: GestureDetector(
              onTap: () {},
              child: Stack(
                children: <Widget>[
                  new IconButton(
                    icon: new Icon(
                      Icons.shopping_cart,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (context) {
                        return CartItems(
                          changeCartItemCount: getTotalCartItems,
                        );
                      })).then((value) {
                        getTotalCartItems();
                        print(noOfCartItems);
                      });
                    },
                  ),
                  noOfCartItems == 0
                      ? Container()
                      : Positioned(
                          child: Stack(
                          children: <Widget>[
                            Icon(
                              Icons.brightness_1,
                              size: 20.0,
                              color: Colors.deepOrange,
                            ),
                            Positioned(
                                top: 3.0,
                                right: 4.0,
                                child: Center(
                                  child: Text(
                                    noOfCartItems.toString(),
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 11.0,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ))
                          ],
                        ))
                ],
              ),
            ),
          )
        ],
      ),
      body: Center(
        child: FutureBuilder<String>(
            future: particularProductResponse,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return ListView.separated(
                    separatorBuilder: (BuildContext context, int index) =>
                        Divider(
                          thickness: 2.0,
                        ),
                    itemCount: particularProductData == null
                        ? 0
                        : particularProductData.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListIndividualDetails(
                          index: index, fun: changeValue);
                    });
              } else {
                return CircularProgressIndicator();
              }
            }),
      ),
    );
  }
}

typedef ChangeValueCallback = void Function();

class ListIndividualDetails extends StatefulWidget {
  int index;
  final ChangeValueCallback fun;
  ListIndividualDetails({this.index, this.fun});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ListIndividualDetails();
  }
}

int noOfCartItems = 0;

class _ListIndividualDetails extends State<ListIndividualDetails> {
  int selectedIndex = 0;
  List productInfo;
  Future<String> productInfoResponse;
  final _quantityInputController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  Future<String> insertInCart() async {
    DateTime now = DateTime.now();
    String formattedDate = DateFormat('dd/MM/yyyy').format(now);
    String formattedTime = DateFormat('HH:mm').format(now);
    print(productInfo[selectedIndex]["lit"]);
    String url =
        "http://eannolautomotive.com/app/getDealerList.php?apicall=insertCart&";
    var response = await http.post(Uri.encodeFull(url),
        // headers: {"Content-Type": "application/json"},
        body: {
          'cndfId': '1',
          'asmId': '1',
          'distributorId': '1',
          'prodId': particularProductData[widget.index]['ID'],
          'prodImg': companyFlag == 1
              ? particularProductData[widget.index]["img"]
              : particularProductData[widget.index]["image"],
          'prodName': companyFlag == 1
              ? particularProductData[widget.index]["prodname"]
              : particularProductData[widget.index]["product_nm"],
          'prodQty': _quantityInputController.text,
          'prodLtr': companyFlag == 1
              ? '1 Ltr'
              : (productInfo != null ? productInfo[selectedIndex]["lit"] : ""),
          'distributorOrderDate': formattedDate,
          'distributorOrderTime': formattedTime,
          'checkoutSts': '0'
        }).then((value) {
      print(value.statusCode);
      _quantityInputController.clear();
    });

    //print(response.reasonPhrase);

    return "success";
  }

  Future<String> getJsonData() async {
    String url =
        "http://eannolautomotive.com/app/getDealerList.php?apicall=getProductInfo&prodId=" +
            particularProductData[widget.index]['ID'].toString();
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      productInfo = convertDataToJson['data'];
    });

    return "success";
  }

  void _validateQuantity() {
    if (_quantityInputController.text != "") {
      print(noOfCartItems);
      widget.fun();
      insertInCart();
      setState(() {
        selectedIndex = 0;
      });

      Fluttertoast.showToast(
          msg: 'Successfully Added',
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
        
          textColor: Colors.black,
          fontSize: 16.0);
    } else {
      showDialog(
          context: context,
          barrierDismissible: false,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Warning!"),
              content: Text("Please Fill Quantity"),
              actions: <Widget>[
                FlatButton(
                  child: Text('OK'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ],
            );
          });
    }
  }

  @override
  void initState() {
    super.initState();
    if(companyFlag != 1){
      productInfoResponse = this.getJsonData();
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                height: 125,
                width: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(color: Colors.grey, width: 1)),
                child: Image.network(
                  companyFlag == 1
                      ? particularProductData[widget.index]["img"]
                      : particularProductData[widget.index]["image"],
                  loadingBuilder: (context, child, progress) {
                    return progress == null ? child : LinearProgressIndicator();
                  },
                  fit: BoxFit.fill,
                ),
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.05),
                          child: Text(
                            companyFlag == 1
                                ? particularProductData[widget.index]
                                    ["prodname"]
                                : particularProductData[widget.index]
                                    ["product_nm"],
                            style: TextStyle(
                              color: Colors.lightBlue[900],
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.05),
                            child: Text(
                              companyFlag == 1
                                  ? particularProductData[widget.index]["lit"]
                                  : particularProductData[widget.index]
                                      ["ltr_qty"],
                              overflow: TextOverflow.clip,
                              softWrap: true,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.width * 0.03,
                          left: MediaQuery.of(context).size.width * 0.03),
                    ),
                    Form(
                      key: _formKey,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.only(
                                  left: MediaQuery.of(context).size.width *
                                      0.05)),
                          Container(
                            height: 35,
                            width: 90,
                            child: TextField(
                              controller: _quantityInputController,
                              decoration: new InputDecoration(
                                  labelText: "Enter quantity",
                                  border: new OutlineInputBorder(
                                    borderSide: new BorderSide(
                                        color: Colors.green[400]),
                                  ),
                                  labelStyle: TextStyle(
                                    fontSize: 10,
                                  )),
                              keyboardType: TextInputType.number,
                              inputFormatters: <TextInputFormatter>[
                                WhitelistingTextInputFormatter.digitsOnly
                              ], // Only nu
                            ),
                          ),
                          Spacer(),
                          Container(
                            margin: EdgeInsets.only(
                                right:
                                    MediaQuery.of(context).size.width * 0.05),
                            height: 35,
                            width: 103,
                            decoration: BoxDecoration(
                                border: Border.all(
                                    color: Colors.lightBlue[900], width: 2.0),
                                borderRadius: BorderRadius.circular(5.0)),
                            child: FlatButton(
                              onPressed:productInfo == null? null : () {
                                _validateQuantity();
                              },
                              child: Text(
                                "Add to Cart",
                                style: TextStyle(
                                    fontSize: 13,
                                    color: Colors.lightBlue[900],
                                    fontWeight: FontWeight.bold),
                              ),
                              color: Colors.white,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
          Container(
            height: 40,
            child:  ListView.builder(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    physics: const ClampingScrollPhysics(),
                    itemCount: companyFlag == 1
                        ? 1
                        : (productInfo != null ? productInfo.length : 1),
                    itemBuilder: (BuildContext context, int index) {
                      return productInfo == null || companyFlag == 1
                ? Container(
                    child: Center(
                      child: Text("No data available..."),
                    ),
                  )
                :Container(
                        padding: EdgeInsets.only(
                            top: MediaQuery.of(context).size.width * 0.01,
                            left: MediaQuery.of(context).size.width * 0.02,
                            bottom: MediaQuery.of(context).size.width * 0.01),
                        child: FlatButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(color: Colors.lightBlue[900])),
                          onPressed: () {
                            setState(() {
                              selectedIndex = index;
                            });
                          },
                          child: Text(
                            companyFlag == 1
                                ? ""
                                : (productInfo != null
                                    ? productInfo[index]["lit"]
                                    : ""),
                            style: TextStyle(
                                fontSize: 13,
                                color: selectedIndex == index
                                    ? Colors.white
                                    : Colors.lightBlue[900]),
                          ),
                          color: selectedIndex == index
                              ? Colors.lightBlue[900]
                              : null,
                        ),
                      );
                    }),
          )
        ],
      ),
    );
  }
}
