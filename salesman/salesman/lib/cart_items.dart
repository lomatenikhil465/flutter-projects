import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class CartItems extends StatefulWidget {
  final ChangeCartItemCallback changeCartItemCount;
  CartItems({this.changeCartItemCount});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _CartItems();
  }
}

List cartItems;
int selectedIndex = 0;

class _CartItems extends State<CartItems> {
  Future<String> cartResponse;
  int total_qantity = 0;

  Future<String> getTotalCartItems() async {
    String url =
        'http://eannolautomotive.com/app/getDealerList.php?apicall=getCartItems&distributorId=' +
            '1';
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      cartItems = convertDataToJson['data'];
    });

    for (int i = 0; i < cartItems.length; i++) {
      total_qantity = total_qantity + int.parse(cartItems[i]['prod_quantity']);
    }

    return "success";
  }

  Future<String> updateInCart(String orderNo) async {
    String url =
        'http://eannolautomotive.com/app/getDealerList.php?apicall=updateInCart&orderNo=' +
            orderNo +
            '&distributorId=' +
            '1';
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    setState(() {
      var convertDataToJson = json.decode(response.body);
      cartItems = convertDataToJson['data'];
    });

    return "success";
  }

  void changeCartItem() {
    setState(() {
      //print("change cart items");
      total_qantity = 0;
      selectedIndex = 0;
      cartResponse = this.getTotalCartItems();
    });
  }

  @override
  void initState() {
    super.initState();
    print(selectedIndex);
    cartResponse = this.getTotalCartItems();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlue[900],
        title: Text("Cart"),
      ),
      body: Center(
        child: cartItems == null
                        ? Container(
                            child: Center(child: Text("No Cart Items...")),
                          )
                        : FutureBuilder<String>(
            future: cartResponse,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return IndexedStack(
                  index: selectedIndex,
                  children: <Widget>[
                    Column(
                            children: <Widget>[
                              Expanded(
                                child: ListView.separated(
                                    separatorBuilder:
                                        (BuildContext context, int index) =>
                                            Divider(
                                              thickness: 0.0,
                                              color: Colors.lightBlue[900],
                                            ),
                                    itemCount: cartItems == null
                                        ? 0
                                        : cartItems.length,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return IndividualCartItem(
                                          index: index,
                                          changeCartItem: changeCartItem);
                                    }),
                              ),
                               Container(
                                      height: 40,
                                      child: FlatButton(
                                          color: Colors.lightBlue[900],
                                          onPressed: () {},
                                          child: Center(
                                            child: Text(
                                              "Total Product Quantity : " +
                                                  total_qantity.toString(),
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.white),
                                            ),
                                          ))),
                               Container(
                                      height: 50,
                                      child: FlatButton(
                                          color: Colors.green[700],
                                          onPressed: () {
                                            if (cartItems != null) {
                                              setState(() {
                                                selectedIndex = 1;
                                              });
                                              for (var i = 0;
                                                  i < cartItems.length;
                                                  i++) {
                                                updateInCart('1' +
                                                    cartItems.length
                                                        .toString());
                                              }

                                              changeCartItem();
                                              Fluttertoast.showToast(
                                                  msg:
                                                      'Order Placed Successfully',
                                                  toastLength:
                                                      Toast.LENGTH_SHORT,
                                                  gravity: ToastGravity.BOTTOM,
                                                  timeInSecForIos: 1,
                                                  textColor: Colors.black,
                                                  fontSize: 16.0);
                                            }
                                          },
                                          child: Center(
                                            child: Text(
                                              "Place Order",
                                              style: TextStyle(
                                                  fontSize: 16,
                                                  color: Colors.white),
                                            ),
                                          ))),
                            ],
                          ),
                    Center(
                      child: CircularProgressIndicator(),
                    )
                  ],
                );
              } else {
                print("else part");
                return CircularProgressIndicator();
              }
            }),
      ),
    );
  }
}

typedef ChangeCartItemCallback = void Function();

class IndividualCartItem extends StatefulWidget {
  final int index;
  final ChangeCartItemCallback changeCartItem;
  IndividualCartItem({this.index, this.changeCartItem});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _IndividualCartItem();
  }
}

class _IndividualCartItem extends State<IndividualCartItem> {
  Future<String> removeFromCart(String prodId) async {
    String url =
        'http://eannolautomotive.com/app/getDealerList.php?apicall=removeFromCart&prodId=' +
            prodId +
            '&distributorId=' +
            '1' +
            '&id=' +
            cartItems[widget.index]['ID'];
    var response = await http.get(
        //Encode the url
        Uri.encodeFull(url),
        //only accept Json response
        headers: {"Accept": "application/json"});

    print(response.body);
    return "success";
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Column(
        children: <Widget>[
          Row(
            children: <Widget>[
              Container(
                height: 75,
                width: 60,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(color: Colors.grey, width: 1)),
                child: Image.network(
                  cartItems[widget.index]['prod_img'],
                  loadingBuilder: (context, child, progress) {
                    return progress == null ? child : LinearProgressIndicator();
                  },
                  fit: BoxFit.fill,
                ),
              ),
              Expanded(
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.05,
                                top: MediaQuery.of(context).size.width * 0.01),
                            child: Text(
                              cartItems[widget.index]['prod_name'],
                              style: TextStyle(
                                  color: Colors.lightBlue[900],
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Flexible(
                          child: Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.05),
                            child: Text(
                              'Product Liter : ' +
                                  cartItems[widget.index]['prod_litre'],
                              overflow: TextOverflow.clip,
                              softWrap: true,
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.normal),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Flexible(
                          child: Container(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.05),
                            child: Text(
                              ' Quantity in Box : ' +
                                  cartItems[widget.index]['prod_quantity'],
                              overflow: TextOverflow.clip,
                              softWrap: true,
                              style: TextStyle(
                                  color: Colors.deepOrange,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.width * 0.03,
                          left: MediaQuery.of(context).size.width * 0.03),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[],
                    ),
                  ],
                ),
              ),
              Row(
                children: <Widget>[
                  IconButton(
                    icon: Icon(
                      Icons.delete_forever,
                      size: 35,
                    ),
                    onPressed: () {
                      if (cartItems != null) {
                        removeFromCart(cartItems[widget.index]['prod_id'])
                            .then((value) {
                          setState(() {
                            selectedIndex = 1;
                          });
                          widget.changeCartItem();
                          Fluttertoast.showToast(
                              msg: 'Product Removed Successfully',
                              toastLength: Toast.LENGTH_SHORT,
                              gravity: ToastGravity.BOTTOM,
                              timeInSecForIos: 1,
                              textColor: Colors.black,
                              fontSize: 16.0);
                        });
                      }
                    },
                    color: Colors.red[700],
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
