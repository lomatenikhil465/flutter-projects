import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class DealerEntry extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _DealerEntry();
  }
}

class _DealerEntry extends State<DealerEntry> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _date = "Not set";

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text("New Dealer's Entry"),
          backgroundColor: Colors.lightBlue[900],
        ),
        body: SingleChildScrollView(
            child: Form(
                key: _formKey,
                autovalidate: _autoValidate,
                child: Container(
                  child: Column(
                    children: <Widget>[
                      Center(
                        child: Icon(
                          Icons.person,
                          color: Colors.lightBlue[900],
                          size: 115.0,
                        ),
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height / 50.0,
                      )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.height / 50.0,
                            ),
                            child: Text(
                              "Name : ",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(
                                right:
                                    MediaQuery.of(context).size.height / 50.0,
                              ),
                              width: 280.0,
                              child: TextFormField(
                                onSaved: null,
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.person,
                                  ),
                                  labelText: "Name",
                                  contentPadding:
                                      EdgeInsets.only(left: 10.0, right: 2.0),
                                  fillColor: Colors.white,
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(15.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                style: new TextStyle(
                                  fontFamily: "Poppins",
                                ),
                              )),
                        ],
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height / 50.0,
                      )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.height / 50.0,
                            ),
                            child: Text(
                              "Email Id : ",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(
                                right:
                                    MediaQuery.of(context).size.height / 50.0,
                              ),
                              width: 280.0,
                              child: TextFormField(
                                onSaved: null,
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.person,
                                  ),
                                  labelText: "Email Id",
                                  contentPadding:
                                      EdgeInsets.only(left: 10.0, right: 2.0),
                                  fillColor: Colors.white,
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(15.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                style: new TextStyle(
                                  fontFamily: "Poppins",
                                ),
                              )),
                        ],
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height / 50.0,
                      )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.height / 50.0,
                            ),
                            child: Text(
                              "Contact No : ",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(
                                right:
                                    MediaQuery.of(context).size.height / 50.0,
                              ),
                              width: 280.0,
                              child: TextFormField(
                                onSaved: null,
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.person,
                                  ),
                                  labelText: "Contact",
                                  contentPadding:
                                      EdgeInsets.only(left: 10.0, right: 2.0),
                                  fillColor: Colors.white,
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(15.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                style: new TextStyle(
                                  fontFamily: "Poppins",
                                ),
                              )),
                        ],
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height / 50.0,
                      )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.height / 50.0,
                            ),
                            child: Text(
                              "GST No : ",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(
                                right:
                                    MediaQuery.of(context).size.height / 50.0,
                              ),
                              width: 280.0,
                              child: TextFormField(
                                onSaved: null,
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.person,
                                  ),
                                  labelText: "GST",
                                  contentPadding:
                                      EdgeInsets.only(left: 10.0, right: 2.0),
                                  fillColor: Colors.white,
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(15.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                style: new TextStyle(
                                  fontFamily: "Poppins",
                                ),
                              )),
                        ],
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height / 50.0,
                      )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.height / 50.0,
                            ),
                            child: Text(
                              "Date : ",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(
                                right:
                                    MediaQuery.of(context).size.height / 50.0,
                              ),
                              width: 280.0,
                              child: RaisedButton(
                                color: Colors.white,
                                onPressed: () {
                                  DatePicker.showDatePicker(context,
                                      theme: DatePickerTheme(
                                          containerHeight: 210.0),
                                      showTitleActions: true,
                                      minTime: DateTime(1947, 1, 1),
                                      maxTime: DateTime.now(),
                                      onConfirm: (date) {
                                    print('confirm $date');
                                    _date =
                                        '${date.year} - ${date.month} - ${date.day}';
                                    setState(() {});
                                  },
                                      currentTime: DateTime.now(),
                                      locale: LocaleType.en);
                                },
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5.0)),
                                elevation: 3.0,
                                child: Container(
                                  child: Row(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            child: Row(
                                              children: <Widget>[
                                                Icon(
                                                  Icons.date_range,
                                                  size: 18.0,
                                                  color: Colors.lightBlue[900],
                                                ),
                                                Text(
                                                  "$_date",
                                                  style: TextStyle(
                                                      color:
                                                          Colors.lightBlue[900],
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 18.0),
                                                )
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                      Text(
                                        "  Change",
                                        style: TextStyle(
                                            color: Colors.teal,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18.0),
                                      ),
                                    ],
                                  ),
                                ),
                              )),
                        ],
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height / 50.0,
                      )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.height / 50.0,
                            ),
                            child: Text(
                              "Bank Name : ",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(
                                right:
                                    MediaQuery.of(context).size.height / 50.0,
                              ),
                              width: 280.0,
                              child: TextFormField(
                                onSaved: null,
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.person,
                                  ),
                                  labelText: "Bank Name",
                                  contentPadding:
                                      EdgeInsets.only(left: 10.0, right: 2.0),
                                  fillColor: Colors.white,
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(15.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                style: new TextStyle(
                                  fontFamily: "Poppins",
                                ),
                              )),
                        ],
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height / 50.0,
                      )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.height / 50.0,
                            ),
                            child: Text(
                              "Acc Type : ",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(
                                right:
                                    MediaQuery.of(context).size.height / 50.0,
                              ),
                              width: 280.0,
                              child: TextFormField(
                                onSaved: null,
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.person,
                                  ),
                                  labelText: "Account Type",
                                  contentPadding:
                                      EdgeInsets.only(left: 10.0, right: 2.0),
                                  fillColor: Colors.white,
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(15.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                style: new TextStyle(
                                  fontFamily: "Poppins",
                                ),
                              )),
                        ],
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height / 50.0,
                      )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            width: 100.0,
                            padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.height / 50.0,
                            ),
                            child: Text(
                              "Shop Closed On : ",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(
                                right:
                                    MediaQuery.of(context).size.height / 50.0,
                              ),
                              width: 280.0,
                              child: TextFormField(
                                onSaved: null,
                                decoration: InputDecoration(
                                  labelText: "",
                                  contentPadding:
                                      EdgeInsets.only(left: 10.0, right: 2.0),
                                  fillColor: Colors.white,
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(15.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                style: new TextStyle(
                                  fontFamily: "Poppins",
                                ),
                              )),
                        ],
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height / 50.0,
                      )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.height / 50.0,
                            ),
                            child: Text(
                              "Address : ",
                              style: TextStyle(
                                  fontSize: 16.0, fontWeight: FontWeight.bold),
                            ),
                          ),
                          Container(
                              padding: EdgeInsets.only(
                                right:
                                    MediaQuery.of(context).size.height / 50.0,
                              ),
                              width: 280.0,
                              child: TextFormField(
                                textAlign: TextAlign.start,
                                maxLines: 4,
                                onSaved: null,
                                decoration: InputDecoration(
                                  prefixIcon: Icon(
                                    Icons.person,
                                  ),
                                  labelText: "Address",
                                  contentPadding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.height /
                                          50.0,
                                      right:
                                          MediaQuery.of(context).size.height /
                                              100.0,
                                      top: MediaQuery.of(context).size.height /
                                          50.0),
                                  fillColor: Colors.white,
                                  border: new OutlineInputBorder(
                                    borderRadius:
                                        new BorderRadius.circular(15.0),
                                    borderSide: new BorderSide(),
                                  ),
                                ),
                                style: new TextStyle(
                                  fontFamily: "Poppins",
                                ),
                              )),
                        ],
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height / 50.0,
                      )),
                      Container(
                        child: Center(
                            child: RaisedButton(
                          elevation: 3.0,
                          onPressed: () {},
                          shape: new RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.orangeAccent)),
                          textColor: Colors.orangeAccent,
                          padding: EdgeInsets.only(
                              right: 100.0,
                              left: 100.0,
                              top: 10.0,
                              bottom: 10.0),
                          color: Colors.white,
                          child: Text(
                            "Submit",
                            style: TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold,
                                textBaseline: TextBaseline.alphabetic),
                          ),
                        )),
                      ),
                      Padding(
                          padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).size.height / 50.0,
                      )),
                    ],
                  ),
                ))));
  }
}
