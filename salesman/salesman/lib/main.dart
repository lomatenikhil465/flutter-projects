import 'package:flutter/material.dart';
import 'menu.dart';
import './DealersApp/dealers_menu.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Distributor',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Distributor'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  bool _obscureText = true;
  String username, passwd, _currentItemSelected;
  var _customerCategory = [
    'Area Sales Manager',
    'CNDF',
    'Distributor',
    'Dealer'
  ];

  @override
  void initState() {
    super.initState();
  }

  Future _showDialogBox(BuildContext context, String title, String content) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(
              title,
              style: TextStyle(color: Colors.red),
            ),
            content: Text(content),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text("Close"))
            ],
          );
        });
  }

  void _validateInputs() {
    if (_formKey.currentState.validate() && _currentItemSelected != null) {
//    If all data are correct then save data to out variables
      _formKey.currentState.save();
      if (_currentItemSelected == 'Dealer') {
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) {
          return DealerMenu();
        }));
      } else if (_currentItemSelected == 'Distributor') {
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) {
          return Menu();
        }));
      } else if (_currentItemSelected == 'CNDF') {
        _showDialogBox(context, "", "Currently In Process");
      } else if (_currentItemSelected == 'Area Sales Manager') {
        _showDialogBox(context, "", "Currently In Process");
      }
    } else {
//    If all data are not valid then start auto validation.
      if (_currentItemSelected == null) {
        _showDialogBox(context, "Warning", "Please Choose Option");
      }
      setState(() {
        _autoValidate = true;
      });
    }
  }

  String validateUsername(String value) {
    if (value.length < 3)
      return 'Username must be more than 2 charater';
    else
      return null;
  }

  void _onDropDownItemSelected(String newValueSelected) {
    setState(() {
      this._currentItemSelected = newValueSelected;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: new Form(
            key: _formKey,
            autovalidate: _autoValidate,
            child: Container(
              padding: EdgeInsets.all(30.0),
              child: Center(
                  child: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      alignment: Alignment.topCenter,
                      padding: EdgeInsets.only(bottom: 30.0),
                      height: 200,
                      child: Image.asset(
                        "assets/images/footer_logo.png",
                      ),
                    ),
                    Container(
                      height: 50.0,
                      width: 450.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15.0),
                        border: Border.all(
                            color: Colors.lightBlue[900],
                            style: BorderStyle.solid,
                            width: 1.0),
                      ),
                      child: DropdownButtonHideUnderline(
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            elevation: 10,
                            isDense: true,
                            autofocus: true,
                            focusColor: Colors.lightBlue[900],
                            //style: Theme.of(context).textTheme.title,
                            isExpanded: true,
                            hint: Text("Choose Option"),
                            items: _customerCategory
                                .map((String dropDownStringItem) {
                              return DropdownMenuItem<String>(
                                  value: dropDownStringItem,
                                  child: Text(dropDownStringItem,
                                      style: TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.lightBlue[900])));
                            }).toList(),
                            onChanged: (String newValueSelected) =>
                                _onDropDownItemSelected(newValueSelected),
                            value: _currentItemSelected,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.02),
                      child: TextFormField(
                        validator: validateUsername,
                        onSaved: (String user) {
                          username = user;
                        },
                        decoration: InputDecoration(
                          labelText: "Username",
                          contentPadding:
                              EdgeInsets.only(left: 10.0, right: 2.0),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        keyboardType: TextInputType.text,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.03),
                      child: TextFormField(
                        onSaved: (String pass) {
                          passwd = pass;
                        },
                        decoration: InputDecoration(
                          suffixIcon: GestureDetector(
                            onTap: () {
                              setState(() {
                                _obscureText = !_obscureText;
                              });
                            },
                            child: Icon(
                              _obscureText
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                            ),
                          ),
                          labelText: "Password",
                          contentPadding:
                              EdgeInsets.only(left: 10.0, right: 2.0),
                          fillColor: Colors.white,
                          border: new OutlineInputBorder(
                            borderRadius: new BorderRadius.circular(10.0),
                            borderSide: new BorderSide(),
                          ),
                        ),
                        obscureText: _obscureText,
                        style: new TextStyle(
                          fontFamily: "Poppins",
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 50.0),
                    ),
                    RaisedButton(
                      elevation: 3.0,
                      onPressed: _validateInputs,
                      shape: new RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10.0),
                        side: BorderSide(
                            width: 1.5, color: Colors.lightBlue[900]),
                      ),
                      textColor: Colors.lightBlue[900],
                      padding: EdgeInsets.only(
                          right: 100.0, left: 100.0, top: 10.0, bottom: 10.0),
                      color: Colors.white,
                      child: Text(
                        "Login",
                        style: TextStyle(
                            fontSize: 20.0,
                            fontWeight: FontWeight.bold,
                            textBaseline: TextBaseline.alphabetic),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.25),
                      child: Text(
                        "© Designed by SMiT Solutions",
                        style: TextStyle(fontSize: 10),
                      ),
                    )
                  ],
                ),
              )),
            )));
  }
}
